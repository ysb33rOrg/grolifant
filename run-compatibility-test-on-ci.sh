#!/usr/bin/env bash

COMPATGROUPS="70 80"

if [[ -z $CI_JOB_NAME ]]; then
  echo "CI_JOB_NAME not set. Exiting"
  exit 1
fi

if [[ -z $1 ]]; then
  echo "Supply compatibility group i.e. 70"
  exit 1
fi

GRADLE_VERS=${CI_JOB_NAME/test:gradle_/}
USEGROUP=""$1""
SKIPLIST=""

if [[ -z $GRADLE_VERS ]]; then
  echo No Gradle versions could be extracted from CI_JOB_NAME. Exiting
  exit 1
fi

for i in $COMPATGROUPS; do
  if [[ $i != $USEGROUP ]]; then
    SKIPLIST="$SKIPLIST -x :grolifant5-$i:gradleTest "
  fi
done

exec ./gradlew -i -s --console=plain --no-build-cache gradleTest \
  -DgradleTest.versions=${GRADLE_VERS} ${SKIPLIST}
