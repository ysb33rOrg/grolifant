package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JvmTestSuitePlugin
import org.gradle.testing.base.TestingExtension

@CompileStatic
class ServiceProviderLibraryPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(BasePlugin)
            apply('org.ysb33r.ysf-project.sonatype.ossrh')
            apply('jvm-test-suite')
        }

        configureTestSuites(project)
    }

    private void configureTestSuites(Project project) {
        final testing = project.extensions.getByType(TestingExtension)
//        final ysfProject = project.extensions.getByType
//        testing.suites.getByName('test').tap {
//            it.useSpock(ysfProject.versionOf('spockCore') + '-groovy-3.0')
//        }
//        testing {
//            suites {
//                test {
//// end::test[]
//                    useSpock(ysfProject.versionOf('spockCore') + '-groovy-3.0')
//                    dependencies {
//                        implementation "dev.gradleplugins:gradle-test-kit:${ysfProject.versionOf('gradleApiCore')}", {
//                            exclude group: 'org.codehaus.groovy'
//                        }
//                    }
//// tag::test[]
//                    targets {
//                        all {
//                            testTask.configure {
//// end::test[]
//                                systemProperties COMPAT_TEST_RESOURCES_DIR: file('src/gradleTest/pluginCompatibility/src/test/resources')
//// tag::test[]
//                                systemProperties OVERRIDE_GRADLE_VERSION: grolifant.resolveProperty('overrideTestGradleVersion',ysfProject.versionOf('fallbackGradleForTesting')).get()
//                                systemProperties OVERRIDE_GROOVY_VERSION: ysfProject.versionOf('gradle7to8Groovy')
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }
}
