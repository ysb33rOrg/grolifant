package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.javadoc.Groovydoc
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.process.CommandLineArgumentProvider
import org.ysb33r.gradle.cloudci.CiConditionalPlugin
import org.ysb33r.gradle.cloudci.CiExtension
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.gradle.api.plugins.JavaPlugin.TEST_RUNTIME_CLASSPATH_CONFIGURATION_NAME

@CompileStatic
class BasePlugin implements Plugin<Project> {
    public static final String GROOVY_COMPILER_CONFIGURATION = 'groovyCompiler'
    public static final String GROOVY_DOC_CONFIGURATION = 'groovyDoc'
    public static final String GROOVY_COMPILER_CLASSPATH_CONFIGURATION = 'groovyCompilerClasspath'
    public static final String GROOVY_DOC_CLASSPATH_CONFIGURATION = 'groovyDocClasspath'
    public static final String LIBRARY_PREFIX = 'grolifant5'

    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply('groovy')
            apply(CiConditionalPlugin)
            apply('org.ysb33r.ysf-project.core.jvm')
        }

        final po = ProjectOperations.maybeCreateExtension(project)
        project.extensions.create(ProjectDefinitionsExtension.NAME, ProjectDefinitionsExtension, project)

        addRepositories(project)
        addConfigurations(project)
        configureJars(project)
        configureForCi(project)
        configureGroovydoc(project)
    }

    private void addRepositories(Project project) {
        project.repositories.mavenCentral()
        project.repositories.gradlePluginPortal()
    }

    private void addConfigurations(Project project) {
        final groovyCompiler = project.configurations.create(GROOVY_COMPILER_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = false
        }

        project.configurations.create(GROOVY_COMPILER_CLASSPATH_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = true
            cfg.extendsFrom(groovyCompiler)
        }

        final groovyDoc = project.configurations.create(GROOVY_DOC_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = false
        }

        project.configurations.create(GROOVY_DOC_CLASSPATH_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = true
            cfg.extendsFrom(groovyDoc)
        }
    }

    private void configureJars(Project project) {
        final java = project.extensions.getByType(JavaPluginExtension)
        java.withJavadocJar()
        java.withSourcesJar()

        final groovydoc = project.tasks.named('groovydoc', Groovydoc)
        project.tasks.named('javadocJar', Jar) {
            it.from(groovydoc)
        }
    }

    private void configureForCi(Project project) {
        final ci = project.extensions.getByType(CiExtension)
        ci.gitlabci {
            project.tasks.withType(GradleTest).configureEach { GradleTest t ->
                t.tap {
                    maxParallelForks = 1
                    testKitStrategy = directoryPerGroup
                    debugTests = false
                }
            }
        }
    }

    private void configureGroovydoc(Project project) {
        project.tasks.named('groovydoc', Groovydoc) {
            it.groovyClasspath = project.configurations.getByName(GROOVY_DOC_CLASSPATH_CONFIGURATION)
        }
        project.tasks.named('javadoc', Javadoc) {
            it.enabled = false
        }
    }
}
