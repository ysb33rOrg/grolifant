package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.javadoc.GroovydocAccess
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.grolifant.api.core.ProjectOperations

@CompileStatic
class CoreApiPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(BaseLibraryPlugin)
        }

        configureGroovydoc(project)
        configureGradleTest(project)
    }

    void configureGroovydoc(Project project) {
        final po = ProjectOperations.find(project)
        final dest = po.buildDirDescendant('docs/api')
        final grolifantProject = project.extensions.getByType(ProjectDefinitionsExtension)
        project.tasks.named('groovydoc', org.gradle.api.tasks.javadoc.Groovydoc) { t ->
            t.tap {
                include '**/*.java'
                include '**/*.groovy'
                exclude '**/internal/**'
                exclude '**/loadable/**'
                destinationDir = dest.get()
                access.set(GroovydocAccess.PROTECTED)
                docTitle = 'Grolifant ' + project.name.replaceFirst(~/(grolifant\d+-)/, '').capitalize() + ' API'
                // docTitle
                // footer
                // header
            }
        }
    }

    void configureGradleTest(Project project) {
        project.tasks.withType(GradleTest).configureEach { GradleTest t ->
            t.tap {
                minHeapSize = '512m'
                maxHeapSize = '2048m'
                forkEvery = 4
            }
        }
    }
}
