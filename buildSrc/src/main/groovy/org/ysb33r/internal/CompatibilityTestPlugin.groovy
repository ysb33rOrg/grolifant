package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME
import static org.ysb33r.internal.BaseDevelopmentPlugin.PREPARE_GROLIFANT_JARS
import static org.ysb33r.internal.BaseDevelopmentPlugin.SUBPATH_GROLIFANT_JARS_LIST

@CompileStatic
class CompatibilityTestPlugin implements Plugin<Project> {
    public static final String LEGACY_PROJECTS_CONFIGURATION = 'legacyGrolifantProjects'
    public static final String LEGACY_PROJECTS_CLASSPATH_CONFIGURATION = 'legacyGrolifantProjectsClasspath'
    public static final String SUBPATH_TEST_RESOURCES = 'src/gradleTest/pluginCompatibility/src/test/resources'
    public static final String METADATA_TASK = 'generatePluginUnderTestMetadata'

    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(BaseDevelopmentPlugin)
            apply('jvm-test-suite')
        }

        addConfigurations(project)
        createPluginMetadataTask(project)
        configureTestTask(project)
        configureGrolifantJarsTask(project)
        configureGradleTest(project)
    }

    private void configureTestTask(Project project) {
        final po = ProjectOperations.find(project)
        final metadata = po.buildDirDescendant('pluginUnderTestMetadata')
        final runtime = project.extensions.getByType(SourceSetContainer).getByName(TEST_SOURCE_SET_NAME).runtimeClasspath

        project.tasks.named('test', Test) {
            it.dependsOn 'generatePluginUnderTestMetadata'
            it.classpath = runtime + project.files(metadata)
        }
    }

    private void createPluginMetadataTask(Project project) {
        final po = ProjectOperations.find(project)
        final outputFile = po.buildDirDescendant('pluginUnderTestMetadata/plugin-under-test-metadata.properties')
        final runtimeClasspath = project.extensions.getByType(SourceSetContainer).getByName(TEST_SOURCE_SET_NAME).runtimeClasspath

        project.tasks.register(METADATA_TASK) {
            it.group = LifecycleBasePlugin.BUILD_GROUP
            it.description = 'Generates a classpath for the test task'
            it.outputs.file(outputFile)
            it.inputs.files(runtimeClasspath)

            it.doLast {
                final props = new Properties()
                props.put('implementation-classpath', runtimeClasspath.files.collect { it.absolutePath }.join(':'))
                outputFile.get().withWriter { w ->
                    props.store(w, METADATA_TASK)
                }
            }
        }
    }

    private void addConfigurations(Project project) {
        final legacy = project.configurations.create(LEGACY_PROJECTS_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = false
        }

        project.configurations.create(LEGACY_PROJECTS_CLASSPATH_CONFIGURATION) { Configuration cfg ->
            cfg.canBeConsumed = false
            cfg.canBeResolved = true
            cfg.extendsFrom(legacy)
        }
    }

    void configureGrolifantJarsTask(Project project) {
        final po = ProjectOperations.find(project)
        final outputFile = po.buildDirDescendant(SUBPATH_GROLIFANT_JARS_LIST)
        final legacy = project.configurations.getByName(LEGACY_PROJECTS_CLASSPATH_CONFIGURATION)
    final runtime = project.configurations.getByName('runtimeClasspath')
        project.tasks.named(PREPARE_GROLIFANT_JARS) { Task t ->
            t.inputs.files(legacy)

            t.doLast {
                outputFile.get().withWriterAppend { w ->
                    legacy.files.findAll { it.name.startsWith('grolifant') }.each {
                        w.println it.absolutePath
                    }
                    runtime.files.findAll { it.name.startsWith('commons-io')}.each {
                        w.println it.absolutePath
                    }
                }
            }
        }
    }

    private void configureGradleTest(Project project) {
        final resourcesDir = new File(project.projectDir, SUBPATH_TEST_RESOURCES)
        project.tasks.withType(GradleTest).configureEach { GradleTest t ->
            t.tap {
                // tag::configureGradleTest[]
                minHeapSize = '512m'
                maxHeapSize = '2048m'
                forkEvery = 4

                systemProperties WITHOUT_DEBUG: 1,
                    PLUGIN_COMPATIBILITY_TEST: 1,
                    COMPAT_TEST_RESOURCES_DIR: resourcesDir.absolutePath
                // end::configureGradleTest[]
            }
        }
        ProjectOperations.find(project).tasks.whenNamed('codenarcGradleTest') {
            it.enabled = false
        }
    }
}
