package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.testing.Test

import static org.ysb33r.internal.BaseDevelopmentPlugin.LIBRARY_PREFIX
import static org.ysb33r.internal.BaseDevelopmentPlugin.OVERRIDE_GRADLE_VERSION_TEST_PROPERTY
import static org.ysb33r.internal.Developers.THE_LIST

@CompileStatic
class ApiImplementationPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(BaseLibraryPlugin)
        }

        configureTestTask(project)
        configureGroovydoc(project)
    }

    void configureTestTask(Project project) {
        project.tasks.named('test', Test) { t ->
            t.tap {
                if (project.name ==~ /^${LIBRARY_PREFIX}-\d0$/) {
                    systemProperty OVERRIDE_GRADLE_VERSION_TEST_PROPERTY, "V${project.name.replaceFirst("${LIBRARY_PREFIX}-", '')[0]}"
                }
            }
        }
    }

    void configureGroovydoc(Project project) {

    }
}
