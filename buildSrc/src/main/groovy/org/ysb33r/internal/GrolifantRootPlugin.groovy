package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.ysf.projects.core.extensions.AsciidocSourceExtension

import static org.ysb33r.ysf.projects.core.extensions.CoreExtension.findCoreExtension

@CompileStatic
class GrolifantRootPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply('org.ysb33r.ysf-project.docs.antora')
            apply('org.ysb33r.ysf-project.sonatype.ossrh')
        }

        findCoreExtension(project, AsciidocSourceExtension).tap {
            attributes 'library-prefix': BaseDevelopmentPlugin.LIBRARY_PREFIX
        }
    }
}
