package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.DependencyResolveDetails
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.file.FileCollection
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.javadoc.Groovydoc
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.ysf.projects.core.extensions.CoreExtension
import org.ysb33r.ysf.projects.core.extensions.JvmCoreExtension

import java.util.function.Predicate

import static org.gradle.api.plugins.JavaPlugin.COMPILE_ONLY_CONFIGURATION_NAME
import static org.gradle.api.plugins.JavaPlugin.TEST_IMPLEMENTATION_CONFIGURATION_NAME
import static org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME
import static org.gradle.api.tasks.SourceSet.TEST_SOURCE_SET_NAME
import static org.ysb33r.internal.BaseDevelopmentPlugin.GROOVY_COMPILER_CLASSPATH_CONFIGURATION
import static org.ysb33r.internal.BaseDevelopmentPlugin.GROOVY_COMPILER_CONFIGURATION
import static org.ysb33r.internal.BaseDevelopmentPlugin.GROOVY_DOC_CONFIGURATION
import static org.ysb33r.ysf.projects.core.extensions.CoreExtension.findCoreExtension

@CompileStatic
class ProjectDefinitionsExtension {

    public static final String NAME = 'grolifantProjectDefinition'
    public static final String SPOCK_CATALOG_NAME = 'spockCore'
    public static final String JANSI_CATALOG_NAME = 'jansi'
    public static final String GRADLE_COMPATIBILITY_PROPERTY = 'gradleCompatibilityTestVersions'

    final libraryPrefix = BaseDevelopmentPlugin.LIBRARY_PREFIX

    ProjectDefinitionsExtension(Project project) {
        this.project = project
        this.po = ProjectOperations.find(project)
    }

    static class NoApi {
        public static final NoApi INSTANCE = new NoApi()
        public static final String NO_VERSION = '-1'

        @Override
        String toString() {
            NO_VERSION
        }
    }

    NoApi noApi() {
        NoApi.INSTANCE
    }

    void groovyAndGradleApiVersions(String groovyVerCatalogName, NoApi instance) {
        configureGroovyApiDependencies(groovyVerCatalogName)

        final cpMain = getGroovyClasspath(MAIN_SOURCE_SET_NAME, groovyVerCatalogName)
        configureGroovyCompile(
            'compileGroovy',
            groovyVerCatalogName,
            cpMain
        )

        configureGroovyCompile(
            'compileTestGroovy',
            groovyVerCatalogName,
            getGroovyClasspath(TEST_SOURCE_SET_NAME, groovyVerCatalogName)
        )

        configureGroovydoc(cpMain)
    }

    void groovyAndGradleApiVersions(String groovyVerCatalogName, String gradleVerCatalogName) {
        configureGroovyApiDependencies(groovyVerCatalogName)
        final ysfProject = CoreExtension.findExtension(project)

        final gradleApiProvider = po.provider { ->
            "dev.gradleplugins:gradle-api:${ysfProject.versionOf(gradleVerCatalogName)}".toString()
        }

        project.dependencies.addProvider(COMPILE_ONLY_CONFIGURATION_NAME, gradleApiProvider, NO_GROOVY_ALL)
        project.dependencies.addProvider(TEST_IMPLEMENTATION_CONFIGURATION_NAME, gradleApiProvider, NO_GROOVY_ALL)

        project.tasks.named('groovydoc', Groovydoc) { t ->
            t.link(
                "https://docs.gradle.org/${ysfProject.versionOf(gradleVerCatalogName)}/javadoc/",
                'org.gradle'
            )
        }

        final cpMain = getGroovyClasspath(MAIN_SOURCE_SET_NAME, groovyVerCatalogName, gradleVerCatalogName)
        configureGroovyCompile(
            'compileGroovy',
            groovyVerCatalogName,
            cpMain
        )
        configureGroovyCompile(
            'compileTestGroovy',
            groovyVerCatalogName,
            getGroovyClasspath(TEST_SOURCE_SET_NAME, groovyVerCatalogName, gradleVerCatalogName)
        )

        configureGroovydoc(cpMain)
    }

    void jdkToolchainVersion(String ver) {
        final java = findCoreExtension(project, JvmCoreExtension)
        java.javaLanguageVersion(ver)
    }

    String getJavaVersion() {
        project.extensions.getByType(JavaPluginExtension).toolchain.languageVersion.getOrNull()?.toString() ?: '8'
    }

    void gradleTestVersionFilter(Predicate<String> spec) {
        final allowed = po.resolveProperty(GRADLE_COMPATIBILITY_PROPERTY)
            .get().split(',')
            .findAll { spec.test(it) }

        project.tasks.withType(GradleTest).configureEach { GradleTest it ->
            if (allowed.size()) {
                it.versions(allowed as Iterable<Object>)
            } else {
                it.enabled = false
            }
        }
    }

    private void configureGroovyApiDependencies(String groovyVerCatalogName) {
        final ysfProject = CoreExtension.findExtension(project)
        final groovyVer = ysfProject.versionOf(groovyVerCatalogName)
        final verProvider = po.provider { ->
            "org.codehaus.groovy:groovy:${groovyVer}".toString()
        }
        final antProvider = po.provider { ->
            "org.codehaus.groovy:groovy-ant:${groovyVer}".toString()
        }
        final docProvider = po.provider { ->
            "org.codehaus.groovy:groovy-groovydoc:${groovyVer}".toString()
        }
        final templateProvider = po.provider { ->
            "org.codehaus.groovy:groovy-templates:${groovyVer}".toString()
        }
//        final spockProvider = po.provider { ->
//            "org.spockframework:spock-core:${ysfProject.versionOf(SPOCK_CATALOG_NAME)}-groovy-${groovyVer.replaceAll(/\.\d+$/, '')}".toString()
//        }

        project.dependencies.addProvider(GROOVY_COMPILER_CONFIGURATION, verProvider)
        project.dependencies.addProvider(COMPILE_ONLY_CONFIGURATION_NAME, verProvider)
        project.dependencies.addProvider(TEST_IMPLEMENTATION_CONFIGURATION_NAME, antProvider)
//        project.dependencies.addProvider(TEST_IMPLEMENTATION_CONFIGURATION_NAME, spockProvider, NO_GROOVY_ALL)

        project.dependencies.addProvider(GROOVY_DOC_CONFIGURATION, verProvider)
        project.dependencies.addProvider(GROOVY_DOC_CONFIGURATION, antProvider)
        project.dependencies.addProvider(GROOVY_DOC_CONFIGURATION, docProvider)
        project.dependencies.addProvider(GROOVY_DOC_CONFIGURATION, templateProvider)
        project.dependencies.add(GROOVY_DOC_CONFIGURATION, "org.fusesource.jansi:jansi:${ysfProject.versionOf(JANSI_CATALOG_NAME)}")
    }

    private void configureGroovyCompile(
        String taskName,
        String groovyVerCatalogName,
        FileCollection groovyClasspath
    ) {
        final ysfProject = CoreExtension.findExtension(project)
        final groovyVer = ysfProject.versionOf(groovyVerCatalogName)
        project.tasks.named(taskName, GroovyCompile) { gc ->
            gc.classpath = groovyClasspath
            gc.groovyClasspath = project.configurations.getByName(GROOVY_COMPILER_CLASSPATH_CONFIGURATION)

            project.configurations.all { Configuration cfg ->
                cfg.resolutionStrategy.eachDependency { DependencyResolveDetails details ->
                    details.requested.with { r ->
                        /// TODO: Will need an update if Gradle starts using Groovy 4 or 5.
                        if (r.group == 'org.codehaus.groovy') {
                            details.useVersion(groovyVer)
                        }
                    }
                }
            }
        }
    }

    private void configureGroovydoc(FileCollection groovyClasspath) {
        project.tasks.named('groovydoc', Groovydoc) { gc ->
            gc.classpath = groovyClasspath
        }
    }

    private FileCollection getGroovyClasspath(String sourceSetName, String groovyVerCatalogName) {
        final ysfProject = CoreExtension.findExtension(project)
        final groovyVer = ysfProject.versionOf(groovyVerCatalogName)
        final ss = project.extensions.getByType(SourceSetContainer).getByName(sourceSetName)
        ss.compileClasspath.filter { File f ->
            final fname = f.name
            if (fname.startsWith('groovy-')) {
                if (!fname.endsWith("${groovyVer}.jar")) {
                    return false
                }
            }
            true
        }
    }

    private FileCollection getGroovyClasspath(
        String sourceSetName,
        String groovyVerCatalogName,
        String gradleVerCatalogName
    ) {
        final ysfProject = CoreExtension.findExtension(project)
        final groovyVer = ysfProject.versionOf(groovyVerCatalogName)
        final gradleVer = ysfProject.versionOf(gradleVerCatalogName)
        final ss = project.extensions.getByType(SourceSetContainer).getByName(sourceSetName)

        ss.compileClasspath.filter { File f ->
            final fname = f.name
            if (gradleVer && fname.startsWith('gradle-api')) {
                if (!fname.endsWith("${gradleVer}.jar")) {
                    return false
                }
            }
            if (fname.startsWith('groovy-')) {
                if (!fname.endsWith("${groovyVer}.jar")) {
                    return false
                }
            }
            true
        }
    }

    private final ProjectOperations po
    private final Project project
    private static final Action<ExternalModuleDependency> NO_GROOVY_ALL = { ExternalModuleDependency it ->
//        it.exclude module: 'groovy-all'
//        it.exclude module: 'groovy'
//        it.exclude module: 'groovy-swing'
        it.exclude group: 'org.codehaus.groovy'
        it.exclude group: 'org.apache.groovy'
    }
}
