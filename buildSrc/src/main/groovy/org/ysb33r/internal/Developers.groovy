package org.ysb33r.internal

import groovy.transform.Canonical
import groovy.transform.CompileStatic

@CompileStatic
class Developers {

    public static final List<Developer> THE_LIST = [
        of('ysb33r', 'Schalk Cronjé', 'ysb33r@gmail.com')
    ].asImmutable()

    static Developer of(String id, String name, String email) {
        new Developer(id, name, email)
    }

    @Canonical
    private static class Developer {
        final String id
        final String name
        final String email
    }
}
