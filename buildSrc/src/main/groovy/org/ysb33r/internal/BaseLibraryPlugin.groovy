package org.ysb33r.internal

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.testing.Test
import org.gradle.plugins.signing.SigningExtension
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.gradle.api.plugins.JavaPlugin.TEST_COMPILE_CLASSPATH_CONFIGURATION_NAME
import static org.ysb33r.internal.Developers.THE_LIST

@CompileStatic
class BaseLibraryPlugin implements Plugin<Project> {
    public static final String SUBPATH_TEST_SOURCES = 'src/gradleTest/runtimeCompatibility/src/test'

    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(BaseDevelopmentPlugin)
            apply('org.ysb33r.ysf-project.sonatype.ossrh')
        }

        configureSourceSets(project)
        configureTestTask(project)
        configureGradleTest(project)
    }

    void configureSourceSets(Project project) {
        final sourceSets = project.extensions.getByType(SourceSetContainer)
        final test = sourceSets.getByName('test')
        final groovy = (SourceDirectorySet) test.extensions.getByName('groovy')
        groovy.srcDir(project.file('src/gradleTest/runtimeCompatibility/src/test/groovy'))
        test.resources.srcDir ('src/gradleTest/runtimeCompatibility/src/test/resources')
    }

    void configureTestTask(Project project) {
        project.tasks.named('test', Test) { t ->
            t.tap {
                systemProperties COMPAT_TEST_RESOURCES_DIR: project.file('src/gradleTest/runtimeCompatibility/src/test/resources').absolutePath
                useJUnitPlatform()
            }
        }
    }

    private void configureGradleTest(Project project) {
        final testSourceRoot = new File(project.projectDir, SUBPATH_TEST_SOURCES)
        project.tasks.withType(GradleTest).configureEach { GradleTest t ->
            t.tap {
                systemProperty('TEST_SOURCES_ROOT', testSourceRoot.absolutePath)
            }
        }
    }
}
