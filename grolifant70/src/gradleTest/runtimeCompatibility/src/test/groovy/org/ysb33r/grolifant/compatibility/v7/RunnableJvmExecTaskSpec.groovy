/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v7

import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.process.JavaExecSpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmExecTask
import spock.lang.IgnoreIf
import spock.lang.Specification

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_8_0

@IgnoreIf({ PRE_7_0 || !PRE_8_0 })
class RunnableJvmExecTaskSpec extends Specification {
    public static final Boolean IS_WINDOWS = OperatingSystem.current().windows
    public static final List<String> COMMANDLINE_PARAMS = ['zzz']
    public static final Map<String, ?> JVM_SYSPROPS = [chocolate: 'bar1']
    public static final String MAIN_CLASS = 'foo.bar'
    public static final String MAIN_MODULE = 'io.foo'
    public static final Map ENV = [foo: 'bar']
    public static final Map ENV_LAZY = [foo1: 'bar1']
    public static final String EXE = '/path/to/java'
    public static final String ENCODING = 'ISO-8859-2'
    public static final String MAX_HEAP = '2g'
    public static final String MIN_HEAP = '512m'
    public static final Integer DEBUG_PORT = 9999

    Project project
    ProjectOperations projectOperations
    TestExecTask execTask
    JavaExecSpec javaExecSpec
    Provider<List<String>> cmdlineProvider
    File fakeWorkingDir
    File fakeJarPath
    File bootstrapJarPath
    OutputStream output
    InputStream input
    OutputStream error

    void setup() {
        project = ProjectBuilder.builder().build()
        projectOperations = ProjectOperations.maybeCreateExtension(project)
        cmdlineProvider = projectOperations.provider { -> COMMANDLINE_PARAMS }
        fakeWorkingDir = project.file('wd')
        fakeJarPath = project.file('my-fake.jar')
        bootstrapJarPath = project.file('my-bootstrap.jar')

        input = new ByteArrayInputStream()
        output = new ByteArrayOutputStream()
        error = new ByteArrayOutputStream()

        javaExecSpec = projectOperations.jvmTools.javaExecSpec()
        execTask = project.tasks.create('jvmexec', TestExecTask)
    }

    void cleanup() {
        input?.close()
        output?.close()
        error?.close()
    }

    void 'configure argument for task when Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(javaExecSpec)

        then:
        javaExecSpec.args == ['1', '2', 'a', 'b', 'c', 'd']
        javaExecSpec.argumentProviders*.asArguments().flatten() == COMMANDLINE_PARAMS
    }

    @SuppressWarnings('UnnecessaryObjectReferences')
    void 'configure JavaExecSpec when Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(javaExecSpec)

        then:
        verifyAll {
            javaExecSpec.executable == EXE
            javaExecSpec.workingDir == fakeWorkingDir
            javaExecSpec.environment == ENV + ENV_LAZY
            javaExecSpec.bootstrapClasspath.files.contains(bootstrapJarPath)
            javaExecSpec.debug == true
            javaExecSpec.defaultCharacterEncoding == ENCODING
            javaExecSpec.enableAssertions == true
            javaExecSpec.maxHeapSize == MAX_HEAP
            javaExecSpec.minHeapSize == MIN_HEAP
            javaExecSpec.systemProperties == JVM_SYSPROPS
            javaExecSpec.classpath.files.contains(fakeJarPath)
            javaExecSpec.mainClass.get() == MAIN_CLASS

            javaExecSpec.ignoreExitValue == true
        }
    }

    void 'getCommandLine() returns correct values on Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(javaExecSpec)
        def commandLine = javaExecSpec.commandLine

        then:
        commandLine.containsAll(['1', '2', 'a', 'b', 'c', 'd'] + COMMANDLINE_PARAMS)
    }

    void 'JavaExecSpec.setAllJvmArgs when Gradle 7.0+'() {
        setup:
        File bootfile = project.file('/var/lib/foo.jar')
        configureExecTask()
        execTask.testExecSpec.copyTo(javaExecSpec)

        when:
        javaExecSpec.allJvmArgs = [
            '-Djoe=biden',
            '-Xms10m',
            '-Xsomething',
            "-Xbootclasspath:${bootfile.absolutePath}".toString()
        ]

        then:
        verifyAll {
            javaExecSpec.systemProperties == [joe: 'biden']
            javaExecSpec.minHeapSize == '10m'
            javaExecSpec.jvmArgs == ['-Xsomething']
            javaExecSpec.bootstrapClasspath.files.contains(bootfile)
        }
    }

    void 'configure debug options on Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(javaExecSpec)

        then:
        verifyAll {
            javaExecSpec.debugOptions.port.get() == DEBUG_PORT
            javaExecSpec.debugOptions.enabled.get() == true
            javaExecSpec.debugOptions.suspend.get() == true
            javaExecSpec.debugOptions.server.get() == true
        }
    }

    void 'configure ModularitySpec & MainModule on Gradle 7.0'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(javaExecSpec)

        then:
        verifyAll {
            javaExecSpec.modularity.inferModulePath.get() == true
            javaExecSpec.mainModule.get() == MAIN_MODULE
            javaExecSpec.mainClass.get() == MAIN_CLASS
        }
    }

    void configureExecTask() {
        execTask.configure { TestExecTask t ->
            t.jvm { jfo ->
                jfo.identity {
                    executable(EXE)
                    workingDir(fakeWorkingDir)
                    environment = [:]
                    environment(ENV)
                    bootstrapClasspath(bootstrapJarPath)
                    debug = true
                    defaultCharacterEncoding = ENCODING
                    enableAssertions = true
                    maxHeapSize = MAX_HEAP
                    minHeapSize = MIN_HEAP
                    systemProperties(JVM_SYSPROPS)
                    addEnvironmentProvider(t.project.provider { -> ENV_LAZY })

                    withDebug { jdo ->
                        jdo.enabled = true
                        jdo.server = true
                        jdo.suspend = true
                        jdo.port = DEBUG_PORT
                    }
                }
            }
            t.runnerSpec { cas ->
                cas.identity {
                    args = ['1', '2']
                    args('a', 'b')
                    args(['c', 'd'])
                    addCommandLineArgumentProviders(cmdlineProvider)
                }
            }
            t.entrypoint { jep ->
                jep.identity {
                    mainClass = MAIN_CLASS
                    mainModule = MAIN_MODULE
                    inferModulePath = true
                    classpath(fakeJarPath)
                }
            }
            t.process { pes ->
                pes.identity {
                    ignoreExitValue = true
                }
            }
        }
    }

    static class TestExecSpec extends AbstractJvmExecSpec<TestExecSpec> {
        TestExecSpec(ConfigCacheSafeOperations po) {
            super(po)
        }
    }

    static class TestExecTask extends AbstractJvmExecTask<TestExecSpec> {

        @Internal
        TestExecSpec getTestExecSpec() {
            (TestExecSpec) super.jvmExecSpec
        }

        TestExecTask() {
            super()
            execSpec = new TestExecSpec(this)
        }
    }
}