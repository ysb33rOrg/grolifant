/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v7

import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.runnable.AbstractCommandExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractCommandExecTask
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecTask
import org.ysb33r.grolifant5.api.core.runnable.AbstractScriptExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractScriptExecTask
import spock.lang.Specification

class RunnableExecTaskSpec extends Specification {
    public static final Boolean IS_WINDOWS = OperatingSystem.current().windows
    public static final List<String> EXE_COMMANDLINE_PARAMS = ['zzz']
    public static final List<String> COMMAND_PARAMS = ['xxx']
    public static final List<String> SCRIPT_PARAMS = COMMAND_PARAMS

    public static final Map ENV = [foo: 'bar']
    public static final Map ENV_LAZY = [foo1: 'bar1']
    public static final String EXE = new File(
        System.getProperty('COMPAT_TEST_RESOURCES_DIR'),
        "mycmd.${OperatingSystem.current().windows ? 'cmd' : 'sh'}"
    ).canonicalPath
    public static final String COMMAND = 'build'
    public static final String SCRIPT = 'env.rb'

    Project project
    ConfigCacheSafeOperations gtc
    TestExecTask execTask
    TestCommandTask commandTask
    TestScriptTask scriptTask
    ExecSpec execSpec
    Provider<List<String>> cmdlineProvider
    File fakeWorkingDir
    OutputStream output
    InputStream input
    OutputStream error

    void setup() {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply(GrolifantServicePlugin)
        gtc = ConfigCacheSafeOperations.from(project)
        cmdlineProvider = gtc.providerTools().provider { -> EXE_COMMANDLINE_PARAMS }
        fakeWorkingDir = project.file('wd')

        input = new ByteArrayInputStream()
        output = new ByteArrayOutputStream()
        error = new ByteArrayOutputStream()

        execSpec = gtc.execTools().execSpec()
        execTask = project.tasks.create('nativeExec', TestExecTask)
        commandTask = project.tasks.create('nativeCommand', TestCommandTask)
        scriptTask = project.tasks.create('nativeScript', TestScriptTask)
    }

    void cleanup() {
        input?.close()
        output?.close()
        error?.close()
    }

    void 'configure argument for task when Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(execSpec)

        then:
        execSpec.args == ['1', '2', 'a', 'b', 'c', 'd']
        execSpec.argumentProviders*.asArguments().flatten() == EXE_COMMANDLINE_PARAMS
    }

    @SuppressWarnings('UnnecessaryObjectReferences')
    void 'configure ExecSpec when Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(execSpec)

        then:
        verifyAll {
            execSpec.executable == EXE
            execSpec.workingDir == fakeWorkingDir
            execSpec.environment == ENV + ENV_LAZY
            execSpec.ignoreExitValue == true
        }
    }

    void 'getCommandLine() returns correct values for on Gradle 7.0+'() {
        setup:
        configureExecTask()

        when:
        execTask.testExecSpec.copyTo(execSpec)
        def commandLine = execSpec.commandLine

        then:
        commandLine.containsAll(['1', '2', 'a', 'b', 'c', 'd'] + EXE_COMMANDLINE_PARAMS)
    }

    void 'getCommandLine() returns correct values for AbstractCommandExecTask on Gradle 7.0+'() {
        setup:
        configureCommandExecTask()

        when:
        commandTask.testExecSpec.copyTo(execSpec)
        def commandLine = execSpec.commandLine

        then:
        commandLine.containsAll(
            ['1', '2', 'a', 'b', 'c', 'd'] +
                EXE_COMMANDLINE_PARAMS +
                [COMMAND, 'c1', 'c2', 'ca', 'cb', 'cc', 'cd'] +
                COMMAND_PARAMS
        )
    }

    void 'getCommandLine() returns correct values for AbstractScriptExecTask on Gradle 7.0+'() {
        setup:
        configureScriptExecTask()

        when:
        scriptTask.testExecSpec.copyTo(execSpec)
        def commandLine = execSpec.commandLine

        then: 'all of the script arguments and the script added to the command line'
        commandLine.containsAll(
            ['1', '2', 'a', 'b', 'c', 'd'] +
                EXE_COMMANDLINE_PARAMS +
                [SCRIPT, 's1', 's2', 'sa', 'sb', 'sc', 'sd'] +
                SCRIPT_PARAMS
        )

        and: 'the script + parameters come after the executable and executable parameters'
        verifyAll {
            commandLine.findIndexOf { it == '1' } < commandLine.findIndexOf { it == 's1' }
            commandLine.findIndexOf { it == EXE_COMMANDLINE_PARAMS[0] } < commandLine.findIndexOf { it == SCRIPT }
            commandLine.findIndexOf { it == 'sd' } < commandLine.findIndexOf { it == SCRIPT_PARAMS[0] }
        }
    }

    void configureExecTask(AbstractExecTask<?> spec = execTask) {
        spec.configure { AbstractExecTask t ->
            t.runnerSpec { cas ->
                cas.identity {
                    args = ['1', '2']
                    args('a', 'b')
                    args(['c', 'd'])
                    addCommandLineArgumentProviders(cmdlineProvider)
                }
            }
            t.entrypoint { ep ->
                ep.identity {
                    executable(EXE)
                    workingDir(fakeWorkingDir)
                    environment = [:]
                    environment(ENV)
                    addEnvironmentProvider(t.project.provider { -> ENV_LAZY })
                }
            }
            t.process { pes ->
                pes.identity {
                    output {
                        forward()
                    }
                    afterExecute { it.result.get().exitValue }
                    ignoreExitValue = true
                }
            }
        }
    }

    void configureCommandExecTask() {
        configureExecTask(commandTask)

        commandTask.configure { TestCommandTask t ->
            t.cmd { c ->
                c.identity {
                    command = COMMAND
                    args = ['c1', 'c2']
                    args('ca', 'cb')
                    args(['cc', 'cd'])
                    addCommandLineArgumentProviders(gtc.providerTools().provider { -> COMMAND_PARAMS })
                }
            }
        }
    }

    void configureScriptExecTask() {
        configureExecTask(scriptTask)

        scriptTask.configure { TestScriptTask t ->
            t.script { scr ->
                scr.identity {
                    name = SCRIPT
                    args = ['s1', 's2']
                    args('sa', 'sb')
                    args(['sc', 'sd'])
                    addCommandLineArgumentProviders(gtc.providerTools().provider { -> SCRIPT_PARAMS })
                }
            }
        }
    }

    static class TestExecSpec extends AbstractExecSpec<TestExecSpec> {
        TestExecSpec(ConfigCacheSafeOperations po) {
            super(po)
        }
    }

    static class TestExecTask extends AbstractExecTask<TestExecSpec> {
        final TestExecSpec testExecSpec

        @Override
        ExecutableEntryPoint getEntrypoint() {
            testExecSpec.entrypoint
        }

        @Override
        CmdlineArgumentSpec getRunnerSpec() {
            testExecSpec.runnerSpec
        }

        @Override
        ProcessExecutionSpec getProcess() {
            testExecSpec.process
        }

        @Override
        protected TestExecSpec getNativeExecSpec() {
            testExecSpec
        }

        TestExecTask() {
            super()
            testExecSpec = new TestExecSpec(this)
        }
    }

    static class TestCommandSpec extends AbstractCommandExecSpec<TestCommandSpec> {
        TestCommandSpec(ConfigCacheSafeOperations po) {
            super(po)
        }
    }

    static class TestCommandTask extends AbstractCommandExecTask<TestCommandSpec> {
        final TestCommandSpec testExecSpec

        @Override
        ExecutableEntryPoint getEntrypoint() {
            testExecSpec.entrypoint
        }

        @Override
        CmdlineArgumentSpec getRunnerSpec() {
            testExecSpec.runnerSpec
        }

        @Override
        ProcessExecutionSpec getProcess() {
            testExecSpec.process
        }

        @Override
        protected TestCommandSpec getNativeExecSpec() {
            testExecSpec
        }

        TestCommandTask() {
            super()
            testExecSpec = new TestCommandSpec(this)
        }
    }

    static class TestScriptSpec extends AbstractScriptExecSpec<TestScriptSpec> {
        TestScriptSpec(ConfigCacheSafeOperations po) {
            super(po)
        }
    }

    static class TestScriptTask extends AbstractScriptExecTask<TestScriptSpec> {
        final TestScriptSpec testExecSpec

        @Override
        ExecutableEntryPoint getEntrypoint() {
            testExecSpec.entrypoint
        }

        @Override
        CmdlineArgumentSpec getRunnerSpec() {
            testExecSpec.runnerSpec
        }

        @Override
        ProcessExecutionSpec getProcess() {
            testExecSpec.process
        }

        @Override
        protected TestScriptSpec getNativeExecSpec() {
            testExecSpec
        }

        TestScriptTask() {
            super()
            testExecSpec = new TestScriptSpec(this)
        }
    }
}