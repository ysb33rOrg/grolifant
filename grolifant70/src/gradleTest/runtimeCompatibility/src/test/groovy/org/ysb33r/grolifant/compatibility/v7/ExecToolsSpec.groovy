/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v7

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.ProjectOperations
import spock.lang.Specification

class ExecToolsSpec extends Specification {

    Project project
    ProjectOperations projectOperations
    ExecTools execTools

    void setup() {
        project = ProjectBuilder.builder().build()
        projectOperations = ProjectOperations.maybeCreateExtension(project)
        execTools = projectOperations.execTools
    }

    void 'Can create an ExecSpec'() {
        when:
        def execSpec = execTools.execSpec()
        execSpec.executable('exe')

        then:
        execSpec.executable == 'exe'
    }
}