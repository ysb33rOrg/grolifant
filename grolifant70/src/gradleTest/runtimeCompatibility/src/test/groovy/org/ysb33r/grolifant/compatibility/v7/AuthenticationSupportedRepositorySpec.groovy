/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v7

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.AuthenticationContainer
import org.gradle.api.artifacts.repositories.PasswordCredentials
import org.gradle.api.credentials.Credentials
import org.gradle.authentication.Authentication
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.repositories.AbstractAuthenticationSupportedRepository
import spock.lang.Specification

class AuthenticationSupportedRepositorySpec extends Specification {

    Project project
    ProjectOperations projectOperations
    AbstractAuthenticationSupportedRepository repository

    void setup() {
        project = ProjectBuilder.builder().build()
        projectOperations = ProjectOperations.maybeCreateExtension(project)
        repository = makeRepo()
    }

    void 'Set repo name'() {
        when:
        repository.name = 'foo'

        then:
        repository.name == 'foo'
    }

    void 'If credentials have not been defined, simple credentials are used'() {
        expect:
        repository.credentials != null
    }

    void 'Configure credentials'() {
        when:
        repository.credentials({ PasswordCredentials pw ->
            pw.with {
                username = 'foo'
                password = 'bar'
            }
        } as Action<PasswordCredentials>)

        then:
        repository.credentials.username == 'foo'
        repository.credentials.password == 'bar'
    }

    void 'Get credentials for a different type of class'() {
        when:
        Credentials creds = repository.getCredentials(AltCredentials)

        then:
        creds instanceof AltCredentials

        when:
        repository.credentials(AltCredentials, { AltCredentials ac ->
            ac.token = '123'
        } as Action<AltCredentials>)

        then:
        creds.token == '123'
    }

    void 'Cannot attempt a different type of credentials when another type is already initialised'() {
        when:
        final pw = repository.credentials

        then:
        pw != null

        when:
        repository.getCredentials(AltCredentials)

        then:
        thrown(IllegalArgumentException)
    }

    void 'Authentication container is not null'() {
        expect:
        repository.authentication != null
    }

    @SuppressWarnings('GetterMethodCouldBeProperty')
    void 'Configure authentication container'() {
        when:
        repository.authentication({ AuthenticationContainer ac ->
            ac.add(new Authentication() {
                @Override
                String getName() {
                    'foobar'
                }
            })
        } as Action<AuthenticationContainer>)

        then:
        repository.authentication.getByName('foobar')
    }

    AbstractAuthenticationSupportedRepository makeRepo() {
        new FakeRepo(projectOperations)
    }

    @SuppressWarnings(['SpaceAfterOpeningBrace', 'UnusedMethodParameter'])
    static class FakeRepo extends AbstractAuthenticationSupportedRepository {
        FakeRepo(ProjectOperations po) {
            super(po)
        }

        void content(Action action) {
            true
        }
    }

    static class AltCredentials implements Credentials {
        String token
    }
}