/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v7

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.internal.core.runnable.EnvironmentVariableProviders
import org.ysb33r.grolifant5.internal.v7.jvm.JavaForkOptionsWithEnvProviderProxy
import spock.lang.Specification

class JavaForkOptionsWithEnvProviderProxySpec extends Specification {

    Project project
    ProjectOperations projectOperations

    void setup() {
        project = ProjectBuilder.builder().build()
        projectOperations = ProjectOperations.maybeCreateExtension(project)
    }

    void 'Do not fail with AnnotationFormat exceptions'() {
        setup:
        final jfo = projectOperations.jvmTools.javaForkOptions()
        final evp = new EnvironmentVariableProviders()

        when:
        new JavaForkOptionsWithEnvProviderProxy(jfo, evp, project.objects)

        then:
        noExceptionThrown()
    }
}