/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v7

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.CopySpec
import org.gradle.api.file.Directory
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.file.FileOperations
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant5.api.errors.NotSupportedException
import org.ysb33r.grolifant5.loadable.core.LoadableVersion
import org.ysb33r.grolifant5.loadable.core.ProjectOperationsProxy

import javax.inject.Inject
import java.util.concurrent.Callable
import java.util.function.Function

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_0

/**
 * An extension that can be added to a project by a plugin to aid in compatibility
 *
 * @since 1.0.0
 */
@CompileStatic
@Slf4j
class DefaultProjectOperations extends ProjectOperationsProxy {

    /**
     * Constructor that sets up a number of methods to be compatible across a wide range Gradle releases.
     *
     * @param project
     */
    @Inject
    DefaultProjectOperations(Project project) {
        super(project, LoadableVersion.V7)
        if (PRE_7_0) {
            throw new NotSupportedException('This class can only be loaded on Gradle 7.0+')
        }

        this.projectCacheDir = project.gradle.startParameter.projectCacheDir ?:
            project.file("${project.rootDir}/.gradle")
        this.projectLayout = project.layout
        this.providerFactory = project.providers
        this.fileOperations = ((ProjectInternal) project).services.get(FileOperations)
        this.fileTreeFactory = { Object base -> project.objects.fileTree().from(base) }

        this.buildDir = projectLayout.buildDirectory.map({ Directory it ->
            it.asFile
        } as Transformer<File, Directory>)
    }

    @Override
    CopySpec copySpec() {
        fileOperations.copySpec()
    }

    /**
     * Creates a new ConfigurableFileTree. The tree will have no base dir specified.
     *
     * @param base Base for file tree.
     *
     * @return File tree.
     */
    @Override
    ConfigurableFileTree fileTree(Object base) {
        this.fileTreeFactory.apply(base)
    }

    /**
     * Build directory
     *
     * @return Provider to the build directory
     */
    @Override
    Provider<File> getBuildDir() {
        this.buildDir
    }

    /** Returns the project cache dir
     *
     * @return Location of cache directory
     */
    @Override
    File getProjectCacheDir() {
        this.projectCacheDir
    }

    /** Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    @Override
    public <T> Provider<T> provider(Callable<? extends T> var1) {
        providerFactory.provider(var1)
    }

    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.environmentVariable(stringTools.stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.gradleProperty(stringTools.stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        def provider = providerFactory.systemProperty(stringTools.stringize(name))
        configurationTimeSafety ? provider.forUseAtConfigurationTime() : provider
    }

    private final Function<Object, ConfigurableFileTree> fileTreeFactory
    private final File projectCacheDir
    private final ProviderFactory providerFactory
    private final ProjectLayout projectLayout
    private final Provider<File> buildDir
    private final FileOperations fileOperations
}
