/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v7

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ProviderFactory
import org.gradle.process.ExecOperations
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import org.gradle.process.internal.DefaultExecSpec
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.downloader.Downloader
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput
import org.ysb33r.grolifant5.internal.core.executable.CommandArgumentSpec
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader
import org.ysb33r.grolifant5.internal.v7.downloader.InternalDownloader
import org.ysb33r.grolifant5.internal.v7.executable.InternalAppRunnerSpec
import org.ysb33r.grolifant5.internal.v7.runnable.DefaultExecOutput
import org.ysb33r.grolifant5.loadable.core.LoadableVersion

import javax.inject.Inject
import java.util.function.Function

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_5
import static org.ysb33r.grolifant5.internal.core.executable.ExecUtils.configureStreams

/**
 * Non-JVM process execution tools for Gradle 7.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0.
 */
@CompileStatic
class DefaultExecTools implements ExecTools {

    @Inject
    DefaultExecTools(
        Project tempProjectReference,
        ObjectFactory objF,
        ProviderFactory pf,
        ProjectLayout pl
    ) {
        this.objectFactory = objF
        this.providers = pf
        this.projectDir = pl.projectDirectory.asFile
        this.stringTools = StringToolsLoader.load(tempProjectReference, LoadableVersion.V7)
        this.execOperations = ((ProjectInternal) tempProjectReference).services.get(ExecOperations)
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecResult} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput exec(OutputType stdout, OutputType stderr, Action<? super ExecSpec> action) {
        ExecUtils.ConfiguredStreams outStreams
        ExecUtils.ConfiguredStreams errStreams
        boolean ignoreExit
        final result = execOperations.exec {
            action.execute(it)
            outStreams = configureStreams(stdout, it.standardOutput, System.out)
            errStreams = configureStreams(stderr, it.errorOutput, System.err)
            ignoreExit = it.ignoreExitValue
            it.ignoreExitValue = true
            it.standardOutput = outStreams.out
            it.errorOutput = errStreams.out
        }

        ExecUtils.failOnExitCode(result, ignoreExit, stdout, stderr)
        DefaultExecOutput.fromResult(objectFactory, providers, result, outStreams.capture, errStreams.capture)
    }

    /**
     * Returns something that looks like an {@link ExecSpec}.
     *
     * @return Instance of an executable specification.
     */
    @Override
    ExecSpec execSpec() {
        objectFactory.newInstance(DefaultExecSpec)
    }

    /**
     * Creates a {@link AppRunnerSpec}.
     *
     * This is primarily used internally by classes that implements execution specifications for non-JVM processes.
     *
     * @return Implementation of {@link AppRunnerSpec}
     */
    @Override
    AppRunnerSpec appRunnerSpec() {
        objectFactory.newInstance(InternalAppRunnerSpec)
    }

    /**
     * Returns an implementation that is optimised for the running version of Gradle.
     *
     * @return Instance of {@link CommandEntryPoint}. Never {@code null}
     */
    @Override
    CommandEntryPoint commandEntryPoint() {
        objectFactory.newInstance(CommandArgumentSpec, stringTools)
    }

    /**
     * Creates a new downloader for downloading packages / distributions.
     *
     * @param distributionName Name of distribution to be downloaded.
     *
     * @return Instance of {@link Downloader} that is optimised for the running Gradle version.
     */
    @Override
    Downloader downloader(final String distributionName) {
        new InternalDownloader(distributionName, projectDir)
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput javaexec(OutputType stdout, OutputType stderr, Action<? super JavaExecSpec> action) {
        ExecUtils.ConfiguredStreams outStreams
        ExecUtils.ConfiguredStreams errStreams
        boolean ignoreExit
        final result = execOperations.javaexec {
            action.execute(it)
            outStreams = configureStreams(stdout, it.standardOutput, System.out)
            errStreams = configureStreams(stderr, it.errorOutput, System.err)
            ignoreExit = it.ignoreExitValue
            it.ignoreExitValue = true
            it.standardOutput = outStreams.out
            it.errorOutput = errStreams.out
        }

        ExecUtils.failOnExitCode(result, ignoreExit, stdout, stderr)
        DefaultExecOutput.fromResult(objectFactory, providers, result, outStreams.capture, errStreams.capture)
    }

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser A parser for the output of running the executable which could extract the version.
     * @param configurator Additional configurator to customise the execution specification.
     * @return The version string.
     */
    @Override
    String parseVersionFromOutput(
        Iterable<String> argsForVersion,
        File executablePath,
        Function<String, String> versionParser,
        Action<ExecSpec> configurator
    ) {
        ExecUtils.parseVersionFromOutput(
            this,
            argsForVersion,
            executablePath,
            versionParser,
            configurator
        )
    }

    /**
     * Executes the specified external process on-demand.
     *
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput provideExec(Action<? super ExecSpec> action) {
        if (PRE_7_5) {
            provideExecPre75(action)
        } else {
            provideExecPre80(action)
        }
    }

    /**
     * Executes the specified external java process on-demand.
     *
     * @param action Configures a {@link JavaExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput provideJavaExec(Action<? super JavaExecSpec> action) {
        if (PRE_7_5) {
            provideJavaExecPre75(action)
        } else {
            provideJavaExecPre80(action)
        }
    }

    private ExecOutput provideExecPre75(Action<? super ExecSpec> action) {
        final out = new ByteArrayOutputStream()
        final err = new ByteArrayOutputStream()

        final result = execOperations.exec { ExecSpec spec ->
            spec.standardOutput = out
            spec.errorOutput = err
            action.execute(spec)
        }

        finalizeExecPre75(result, out, err)
    }

    private ExecOutput provideJavaExecPre75(Action<? super JavaExecSpec> action) {
        final out = new ByteArrayOutputStream()
        final err = new ByteArrayOutputStream()

        final result = execOperations.javaexec { JavaExecSpec spec ->
            spec.standardOutput = out
            spec.errorOutput = err
            action.execute(spec)
        }

        finalizeExecPre75(result, out, err)
    }

    private ExecOutput finalizeExecPre75(
        ExecResult result,
        ByteArrayOutputStream out,
        ByteArrayOutputStream err
    ) {
        DefaultExecOutput.fromResult(
            providers.provider { -> result },
            providers.provider { -> out.toByteArray() },
            providers.provider { -> out.toString() },
            providers.provider { -> err.toByteArray() },
            providers.provider { -> err.toString() }
        )
    }

    @CompileDynamic
    private ExecOutput provideExecPre80(Action<? super ExecSpec> action) {
        finalizeExecPre80(providers.exec(action))
    }

    @CompileDynamic
    private ExecOutput provideJavaExecPre80(Action<? super JavaExecSpec> action) {
        finalizeExecPre80(providers.javaexec(action))
    }

    @CompileDynamic
    private ExecOutput finalizeExecPre80(Object execResult) {
        DefaultExecOutput.fromResult(
            execResult.result,
            execResult.standardOutput.asBytes,
            execResult.standardOutput.asText,
            execResult.standardError.asBytes,
            execResult.standardError.asText
        )
    }

    private final ObjectFactory objectFactory
    private final ProviderFactory providers
    private final File projectDir
    private final StringTools stringTools
    private final ExecOperations execOperations
}
