/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v7

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.process.ExecOperations
import org.ysb33r.grolifant5.api.core.ExecOperationsProxy

@CompileStatic
class DefaultExecOperations implements ExecOperationsProxy {
    DefaultExecOperations(Project project) {
        this.execOperations = ((ProjectInternal) project).services.get(ExecOperations)
    }

    @Delegate
    private final ExecOperations execOperations
}
