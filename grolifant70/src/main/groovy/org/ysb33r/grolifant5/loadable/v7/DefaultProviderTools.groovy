/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v7

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.loadable.core.LoadableVersion
import org.ysb33r.grolifant5.loadable.core.ProviderToolsProxy

import javax.inject.Inject

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_4

/**
 * Safely deal with Providers down to Gradle 7.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
@InheritConstructors
class DefaultProviderTools extends ProviderToolsProxy {
    @Inject
    DefaultProviderTools(Project tempProjectReference) {
        super(tempProjectReference, LoadableVersion.V7)
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the an environmental variable.
     * @since 5.0
     */
    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        final p = super.environmentVariable(name)
        if (PRE_7_4) {
            configurationTimeSafety ? p.forUseAtConfigurationTime() : p
        } else {
            p
        }
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the project property.
     * @since 5.0
     */
    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        final p = super.gradleProperty(name)
        if (PRE_7_4) {
            configurationTimeSafety ? p.forUseAtConfigurationTime() : p
        } else {
            p
        }
    }

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the system property.
     * @since 5.0
     */
    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        final p = super.systemProperty(name)
        if (PRE_7_4) {
            configurationTimeSafety ? p.forUseAtConfigurationTime() : p
        } else {
            p
        }
    }
}
