/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v7

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.attributes.AttributeContainer
import org.ysb33r.grolifant5.api.core.ConfigurationTools
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.loadable.core.ConfigurationToolsProxy

/**
 * Provides an implementation of {@link ConfigurationTools} for Gradle 7.x.
 *
 * @since 2.1
 */
@CompileStatic
class DefaultConfigurationTools extends ConfigurationToolsProxy {
    DefaultConfigurationTools(
        ProjectOperations incompleteReference, Project project) {
        super(incompleteReference, project)
    }

    /**
     * Creates three configurations that are related to each other.
     *
     * This works on the same model as to how {@code implementation}, {@code runtimeClasspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName The name of a configuration which can be resolved.
     * @param consumableConfigurationName The name of a configuration that can be consumed by other subprojects.
     * @param visible Whether configurations should be marked visible or invisible.
     * @param attributes Action to configure attributes for resolvable and consumable configurations.
     *
     * @since 5.0
     */
    @Override
    void createRoleFocusedConfigurations(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        createTrinityConfigurationsPre84(
            dependencyScopedConfigurationName,
            resolvableConfigurationName,
            consumableConfigurationName,
            visible,
            attributes
        )
    }

    /**
     * Creates two configurations that are related to each other and which are only meant to be used within the
     * same (sub)project.
     * <p>
     * This works on the same model as to how {@code implementation} and {@code runtimeClasspath}.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName The name of a configuration which can be resolved.
     * @param visible Whether the configurations should be visible.
     *
     * @since 5.0
     */
    @Override
    void createLocalRoleFocusedConfiguration(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        boolean visible
    ) {
        createDualConfigurationsPre84(dependencyScopedConfigurationName, resolvableConfigurationName, visible)
    }

    /**
     * Creates a single configuration to be used for outgoing publications.
     * <p>Very useful for sharing internal outputs between subprojects.</p>
     *
     * @param configurationName Name of outgoing configuration.
     * @param visible Whether the configuration is visible.
     * @param attributes Action to configure attributes for resolvable and consumable configurations.
     *
     * @since 5.0
     */
    @Override
    void createSingleOutgoingConfiguration(
        String configurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        createOutgoingConfigurationPre84(configurationName, visible, attributes)
    }
}
