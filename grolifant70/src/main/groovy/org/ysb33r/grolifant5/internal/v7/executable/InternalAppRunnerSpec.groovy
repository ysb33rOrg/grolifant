/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.executable

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec
import org.ysb33r.grolifant5.internal.core.runnable.AbstractAppRunnerSpec
import org.ysb33r.grolifant5.internal.core.runnable.EnvironmentVariableProviders

import javax.inject.Inject

/**
 * Implementation of an {@link AppRunnerSpec} for Gradle 7.0+
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class InternalAppRunnerSpec extends AbstractAppRunnerSpec implements AppRunnerSpec {

    /**
     * Internal application runner specification.
     *
     * @param tempProjectReference Temporary project reference.
     *
     * @since 5.0
     */
    @Inject
    InternalAppRunnerSpec(
        Project tempProjectReference
    ) {
        super(
            ConfigCacheSafeOperations.from(tempProjectReference),
            tempProjectReference.providers,
            tempProjectReference.objects,
            { ConfigCacheSafeOperations gtc, ExecSpec es, EnvironmentVariableProviders ep ->
                new InternalEntryPointProxy(gtc, es, ep)
            }.curry(ConfigCacheSafeOperations.from(tempProjectReference))
        )
    }

//    // Deprecated in 5.0
//    @Deprecated
//    InternalAppRunnerSpec(ProjectOperations po) {
//        super(
//            GrolifantTaskComponents.from(po),
//            po.providers,
//            { GrolifantTaskComponents gtc, ExecSpec es, EnvironmentVariableProviders ep ->
//                new InternalEntryPointProxy(gtc, es, ep)
//            }.curry(GrolifantTaskComponents.from(po))
//        )
//    }
}
