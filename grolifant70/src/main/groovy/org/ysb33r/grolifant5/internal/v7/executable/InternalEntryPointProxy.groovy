/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.executable

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.process.ProcessForkOptions
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProviderToString
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint
import org.ysb33r.grolifant5.internal.core.runnable.EnvironmentVariableProviders

/**
 * Implementation of an {@link ExecutableEntryPoint} for Gradle 7.0+
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class InternalEntryPointProxy implements ExecutableEntryPoint {

    /**
     * A proxy used to populate an entry point to application runners.
     *
     * @param gtc {@link ConfigCacheSafeOperations} instance to use to lazy-resolve values.
     * @param instance The instance of {@link ProcessForkOptions} that this will be the proxy to.
     * @param evp Providers of environment variables.
     *
     * @since 5.0
     */
    InternalEntryPointProxy(
        ConfigCacheSafeOperations gtc,
        ProcessForkOptions instance,
        EnvironmentVariableProviders evp
    ) {
        this.delegate = instance
        this.evp = evp
        this.stringTools = gtc.stringTools()
        this.fsOperations = gtc.fsOperations()
    }

    // Deprecated in 5.0
    @Deprecated
    InternalEntryPointProxy(
        ProjectOperations po,
        ProcessForkOptions instance,
        EnvironmentVariableProviders evp
    ) {
        this.delegate = instance
        this.evp = evp
        this.stringTools = po.stringTools
        this.fsOperations = po.fsOperations
    }

    /**
     * Adds a provider to environment variables.
     *
     * The values of the provider are processed after any value set via an {@code environment} call.
     *
     * @param envProvider Provider to a resolved map.
     */
    @Override
    void addEnvironmentProvider(Provider<Map<String, String>> envProvider) {
        evp.addEnvironmentProvider(envProvider)
    }

    /**
     * Sets the name of the executable to use.
     *
     * @param exec The executable. Must not be null.
     */
    @Override
    void setExecutable(Object exec) {
        delegate.executable(ProviderToString.proxy(stringTools.provideString(exec)))
    }

    /**
     * Sets the name of the executable to use.
     *
     * @param exec The executable. Must not be null.
     * @return this
     */
    @Override
    ProcessForkOptions executable(Object exec) {
        delegate.executable(ProviderToString.proxy(stringTools.provideString(exec)))
        delegate
    }

    /**
     * Sets the working directory for the process. The supplied argument is evaluated as per {@link
     * org.gradle.api.Project#file(Object)}.
     *
     * @param dir The working directory. Must not be null.
     */
    @Override
    void setWorkingDir(Object dir) {
        delegate.workingDir(fsOperations.provideFile(dir))
    }

    /**
     * Sets the working directory for the process. The supplied argument is evaluated as per {@link
     * org.gradle.api.Project#file(Object)}.
     *
     * @param dir The working directory. Must not be null.
     * @return this
     */
    @Override
    ProcessForkOptions workingDir(Object dir) {
        delegate.workingDir(fsOperations.provideFile(dir))
        delegate
    }

    @Delegate(interfaces = true, deprecated = true, methodAnnotations = true, parameterAnnotations = true)
    private final ProcessForkOptions delegate

    private final EnvironmentVariableProviders evp
    private final StringTools stringTools
    private final FileSystemOperations fsOperations
}
