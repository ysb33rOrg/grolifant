/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.jvm

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Internal
import org.gradle.process.CommandLineArgumentProvider
import org.gradle.process.JavaExecSpec
import org.gradle.workers.WorkerExecutor
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.executable.CmdLineArgumentSpecEntry
import org.ysb33r.grolifant5.api.core.executable.ProcessExecOutput
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec
import org.ysb33r.grolifant5.api.core.jvm.JavaForkOptionsWithEnvProvider
import org.ysb33r.grolifant5.api.core.jvm.JvmAppRunnerSpec
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerExecSpec
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise
import org.ysb33r.grolifant5.api.core.runnable.AbstractCmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmExecSpec
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils
import org.ysb33r.grolifant5.internal.core.runnable.EnvironmentVariableProviders
import org.ysb33r.grolifant5.internal.core.runnable.ProcessExecutionSpecProxy
import org.ysb33r.grolifant5.internal.v7.jvm.worker.WorkerSubmission

import javax.inject.Inject
import java.security.MessageDigest

import static org.ysb33r.grolifant5.api.core.Transform.toList

/**
 * Provides a class that can be populated with various fork options for Java
 * and which can then be used to copy to other methods in the Gradle API that provides a
 * {@link org.gradle.process.JavaForkOptions} in the parameters.
 *
 * Intended for Gradle 7.x
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class InternalJvmAppExecSpec implements JvmAppRunnerSpec, ProcessExecOutput {

    /**
     * Creates the JvmExecSpec on Gradle 7.
     *
     * @param tempProjectReference A temporary project reference.
     */
    @Inject
    InternalJvmAppExecSpec(Project tempProjectReference) {
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(tempProjectReference)
        this.javaExecSpec = configCacheSafeOperations.jvmTools().javaExecSpec()
        this.arguments = new Arguments(configCacheSafeOperations.stringTools(), tempProjectReference.providers)
        this.envProviders = new EnvironmentVariableProviders()
        this.jfoProxy = new JavaForkOptionsWithEnvProviderProxy(
            this.javaExecSpec,
            this.envProviders,
            tempProjectReference.objects
        )
        this.jepProxy = new JvmEntryPointProxy(this.javaExecSpec, configCacheSafeOperations.stringTools())
        this.psProxy = tempProjectReference.objects.newInstance(ProcessExecutionSpecProxy)
    }

    @Override
    JavaExecSpec copyTo(JavaExecSpec options) {
        copyToJavaExecSpec(options)
        options
    }

    @Override
    void configureForkOptions(Action<JavaForkOptionsWithEnvProvider> configurator) {
        configurator.execute(this.jfoProxy)
    }

    @Override
    void configureForkOptions(@DelegatesTo(JavaForkOptionsWithEnvProvider) Closure<?> configurator) {
        ClosureUtils.configureItem(this.jfoProxy, configurator)
    }

    @Override
    void configureCmdline(Action<CmdlineArgumentSpec> configurator) {
        configurator.execute(this.arguments)
    }

    @Override
    void configureCmdline(@DelegatesTo(CmdlineArgumentSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(this.arguments, configurator)
    }

    @Override
    void configureEntrypoint(Action<JvmEntryPoint> configurator) {
        configurator.execute(this.jepProxy)
    }

    @Override
    void configureEntrypoint(@DelegatesTo(JvmEntryPoint) Closure<?> configurator) {
        ClosureUtils.configureItem(this.jepProxy, configurator)
    }

    @Override
    void configureProcess(Action<ProcessExecutionSpec> configurator) {
        configurator.execute(this.psProxy)
    }

    @Override
    void configureProcess(@DelegatesTo(ProcessExecutionSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(this.psProxy, configurator)
    }

    /**
     * Submits this to a worker queue using an appropriate isolation mode.
     * @param isolationMode Isolation mode which is either classpath isolated or out of process.
     * @param worker A worker execution instance.
     * @param workerAppExecutorFactory A factory instances that can create executor logic.
     * @param parameterFactory A factory which can create parameters and populate them from a {@link JavaExecSpec}.
     * @param <P>               The type of the POJO/POGO that holds the parameters.
     */
    @Override
    public <P extends SerializableWorkerAppParameters> WorkerPromise submitToWorkQueue(
        WorkerIsolation isolationMode,
        WorkerExecutor worker,
        WorkerAppExecutorFactory<P> workerAppExecutorFactory,
        WorkerAppParameterFactory<P> parameterFactory
    ) {
        new WorkerSubmission<P>().toWorkQueue(
            isolationMode,
            worker,
            new InternalWorkerExecSpec(this),
            workerAppExecutorFactory,
            parameterFactory
        )
    }

    /**
     * Runs a job using {@code javaexec}.
     *
     * @param spec Execution specification
     * @return Result of execution.
     *
     * @since 5.0
     */
    @Override
    ExecOutput submitAsJavaExec(AbstractJvmExecSpec spec) {
        ExecUtils.submitAsJavaExec(
            configCacheSafeOperations.execTools(),
            spec,
            psProxy.output.outputType,
            psProxy.output.fileCapture,
            psProxy.errorOutput.outputType,
            psProxy.errorOutput.fileCapture
        )
    }

    /**
     * Adds a command-line processor that will process command-line arguments in a specific order.
     *
     * For instance in a script, one want to proccess the exe args before the script args.
     *
     * In a system that has commands and subcommands, one wants to process this in the order of exe args, command args,
     * and then subcommand args.
     *
     * This method allows the implementor to control the order of processing  for all the groupings.
     *
     * @param name Name of command-line processor.
     * @param order Order in queue to process.
     * @param processor The specific grouping.
     */
    @Override
    void addCommandLineProcessor(String name, Integer order, CmdlineArgumentSpec processor) {
        this.cmdlineProcessors.add(new CmdLineArgumentSpecEntry(name, order, processor))
    }

    /**
     * Provides direct access to the list of command-line processors.
     *
     * In many cases there will only be one item in the list which is for providing arguments to the executable
     * itself. Some implementations will have more. Implementors can use this interface to manipulate order of
     * evaluation.
     *
     * @return Collection of command-line processors. Can be empty, but never {@code null}.
     */
    @Override
    Collection<CmdLineArgumentSpecEntry> getCommandLineProcessors() {
        this.cmdlineProcessors
    }

    /**
     * A unique string which determines whether there were any changes.
     *
     * @return Signature string
     */
    @Override
    String getExecutionSignature() {
        MessageDigest.getInstance('SHA-256')
            .digest(executionParameters.toString().bytes).sha256()
    }

    /**
     * Processes the output.
     *
     * <p>
     *     Checks first if any outputs should be forwarded.
     *     Thereafter run all registered actions.
     * </p>
     *
     * @param output Output to process
     *
     * @since 5.0
     */
    @Override
    void processOutput(ExecOutput output) {
        psProxy.processOutput(output)
    }

    /**
     * Loads executions parameters from the current execution specification.
     * <p>
     *     The primary purpose of this method is to build a map for use by {@link #getExecutionSignature}.
     *     The default implementation will use the executable path and the arguments.
     *     </p>
     * @return Execution parameters.
     */
    @Internal
    protected Map<String, ?> getExecutionParameters() {
        final st = configCacheSafeOperations.stringTools()
        [
            exe             : st.stringize(javaExecSpec.executable),
            args1           : st.stringize(javaExecSpec.jvmArgs),
            args2           : st.stringize(javaExecSpec.jvmArgumentProviders*.asArguments().flatten()),
            args3           : st.stringize(javaExecSpec.args),
            args4           : st.stringize(javaExecSpec.argumentProviders*.asArguments().flatten()),
            systemProperties: st.stringizeValues(javaExecSpec.systemProperties),
            classpath       : javaExecSpec.classpath.asPath,
            main            : st.stringize(javaExecSpec.mainClass)
        ]
    }

    /**
     * Copies all settings to a target {@link JavaExecSpec}.
     *
     * Resolves environment then copies everything, because Gradle's API for the env does not recursively resolve
     * values in the env.
     *
     * @param target Target {@link JavaExecSpec}
     */
    protected void copyToJavaExecSpec(JavaExecSpec target) {
        this.javaExecSpec.copyTo(target)
        target.environment(configCacheSafeOperations.stringTools().stringizeValues(this.javaExecSpec.environment))
        toList(envProviders.environmentProviders) {
            it.get()
        }.each {
            target.environment(it)
        }
        copyArguments(target)
        copyDebugOptions(target)
        jepProxy.copyTo(target)
        target.ignoreExitValue = psProxy.ignoreExitValue
    }

    /**
     * Copies command arguments (non-JVM) target {@link JavaExecSpec} and well as command providers.
     *
     * @param target Target {@link JavaExecSpec}.
     */
    protected void copyArguments(JavaExecSpec target) {
        target.args = arguments.args
        def cmdlineProviders = toList(arguments.commandLineArgumentProviders) { p ->
            new CommandLineArgumentProvider() {
                @Override
                Iterable<String> asArguments() {
                    p.get()
                }
            }
        }
        target.argumentProviders.addAll(cmdlineProviders)
        commandLineProcessors.each {
            final providedArgs = it.argumentSpec.commandLineArgumentProviders

            if (it.order) {
                final allArgs = it.argumentSpec.allArgs
                target.argumentProviders.add({ -> allArgs.get() } as CommandLineArgumentProvider)
            } else {
                target.args(it.argumentSpec.args)

                providedArgs.each { p ->
                    target.argumentProviders.add({ -> p.get() } as CommandLineArgumentProvider)
                }
            }
        }
    }

    /**
     * Builds a list of arguments by taking all the set arguments as well as the argument providers.
     *
     * THe main purpose of this method is to provide a list of arguments to be passed to a Worker instance.
     *
     * @return All application arguments
     */
    protected List<String> buildArguments() {
        final List<String> result = []
        final tmpSpec = configCacheSafeOperations.jvmTools().javaExecSpec()
        copyArguments(tmpSpec)
        result.addAll(tmpSpec.args)
        result.addAll(tmpSpec.argumentProviders*.asArguments().flatten().toList() as List<String>)
        result
    }

    /**
     * Copies debug options to target {@link JavaExecSpec}.
     *
     * @param targetTarget {@link JavaExecSpec}.
     */
    protected void copyDebugOptions(JavaExecSpec target) {
        def dopt = javaExecSpec.debugOptions
        target.debugOptions.identity {
            enabled.set(dopt.enabled)
            server.set(dopt.server)
            suspend.set(dopt.suspend)
            port.set(dopt.port)
        }
    }

    static protected class Arguments extends AbstractCmdlineArgumentSpec {
        Arguments(StringTools str, ProviderFactory pf) {
            super(str, pf)
        }
    }

    static private class InternalWorkerExecSpec implements WorkerExecSpec {
        InternalWorkerExecSpec(InternalJvmAppExecSpec thisRef) {
            this.ref = thisRef
        }

        @Override
        JavaExecSpec getJavaExecSpec() {
            ref.javaExecSpec
        }

        @Override
        JvmEntryPoint getJvmEntrypoint() {
            ref.jepProxy
        }

        @Override
        List<String> getApplicationArguments() {
            ref.buildArguments()
        }

        private final InternalJvmAppExecSpec ref
    }

    private final JavaExecSpec javaExecSpec
    private final Arguments arguments
    private final ConfigCacheSafeOperations configCacheSafeOperations
    private final EnvironmentVariableProviders envProviders
    private final JavaForkOptionsWithEnvProvider jfoProxy
    private final JvmEntryPointProxy jepProxy
    private final ProcessExecutionSpecProxy psProxy
    private final SortedSet<CmdLineArgumentSpecEntry> cmdlineProcessors = [] as SortedSet<CmdLineArgumentSpecEntry>
}
