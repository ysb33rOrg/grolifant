/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.jvm.worker;

import org.gradle.api.provider.Property;
import org.gradle.workers.WorkParameters;
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutor;

/**
 * Interface glue between the implementation-independent parameters and the Gradle {@link WorkParameters}.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface WorkerAppParameters extends WorkParameters {

    /** The executor code that will be run inside the worker.
     *
     * @return Reference to an executor.
     */
    Property<WorkerAppExecutor<?>> getExecutor();

    /**
     * Access to the object that holds all the parameters.
     *
     * @return Reference to the parameters.
     */
    Property<SerializableWorkerAppParameters> getParams();
}
