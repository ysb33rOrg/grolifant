/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.jvm.worker

import groovy.transform.CompileStatic
import org.gradle.workers.WorkAction
import org.ysb33r.grolifant5.api.remote.worker.WorkerExecutionException

@SuppressWarnings('AbstractClassWithoutAbstractMethod')
@CompileStatic
abstract class InternalWorkerAppExecutor
    implements WorkAction<WorkerAppParameters> {

    /**
     * The work to perform when this work item executes.
     *
     * @throw WorkerExecutionException if process does not complete successfully.
     */
    @SuppressWarnings('CatchException')
    @Override
    void execute() {
        try {
            parameters.executor.get().executeWith(parameters.params.get())
        } catch (WorkerExecutionException e) {
            throw e
        } catch (Exception e) {
            throw new WorkerExecutionException(e)
        }
    }
}
