/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.jvm

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.process.JavaExecSpec
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.jvm.ModularitySpec

/**
 * Proxy forward to modularity settings on Gradle 7.0+
 *
 * @authos Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultModularitySpec implements ModularitySpec {
    DefaultModularitySpec(JavaExecSpec spec, ProjectOperations po) {
        this.javaExecSpec = spec
        this.projectOperations = po
    }

    /**
     * Provider to the main module name.
     *
     * @return Provider.
     */
    @Override
    Provider<String> getMainModule() {
        javaExecSpec.mainModule
    }

    /**
     * Set the main module name.
     *
     * @param mod Anything convertible to a string.
     */
    void setMainModule(Object mod) {
        projectOperations.stringTools.updateStringProperty(javaExecSpec.mainModule, mod)
    }

    /**
     * Whether module path should be inferred.
     *
     * @return Provider to the detection state.
     */
    @Override
    Provider<Boolean> getInferModulePath() {
        javaExecSpec.modularity.inferModulePath
    }

    /**
     * Whether module path should be inferred.
     *
     * @param toggle Whether to infer the module path.
     */
    @Override
    void setInferModulePath(boolean toggle) {
        javaExecSpec.modularity.inferModulePath.set(toggle)
    }

    /**
     * Whether module path should be inferred.
     *
     * @param toggle Whether to infer the module path.
     */
    @Override
    void setInferModulePath(Provider<Boolean> toggle) {
        javaExecSpec.modularity.inferModulePath.set(toggle)
    }

    private final JavaExecSpec javaExecSpec
    private final ProjectOperations projectOperations
}
