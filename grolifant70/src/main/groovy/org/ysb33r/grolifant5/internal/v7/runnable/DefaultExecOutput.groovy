/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v7.runnable

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.process.ExecResult
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput
import org.ysb33r.grolifant5.api.core.runnable.ExecStreamContent

/**
 * Wraps the output from an execution on Gradle 7.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class DefaultExecOutput implements ExecOutput {
    static ExecOutput fromResult(
        Provider<ExecResult> result,
        Provider<byte[]> outBytes,
        Provider<String> outText,
        Provider<byte[]> errBytes,
        Provider<String> errText
    ) {
        new DefaultExecOutput(
            result,
            new StreamContent(outText, outBytes),
            new StreamContent(errText, errBytes)
        )
    }

    static ExecOutput fromResult(
        ObjectFactory objects,
        ProviderFactory providers,
        ExecResult result,
        Optional<ByteArrayOutputStream> out,
        Optional<ByteArrayOutputStream> err
    ) {
        final text = objects.property(String)
        final bytes = text.map { it.bytes }
        new DefaultExecOutput(
            providers.provider { -> result },
            new StreamContent(
                out.present ? providers.provider { -> out.get().toString() } : text,
                out.present ? providers.provider { -> out.get().toByteArray() } : bytes,
            ),
            new StreamContent(
                err.present ? providers.provider { -> err.get().toString() } : text,
                err.present ? providers.provider { -> err.get().toByteArray() } : bytes,
            )
        )
    }

    final Provider<ExecResult> result
    final ExecStreamContent standardOutput
    final ExecStreamContent standardError

    private DefaultExecOutput(
        Provider<ExecResult> result,
        ExecStreamContent out,
        ExecStreamContent err
    ) {
        this.result = result
        this.standardOutput = out
        this.standardError = err
    }

    private static class StreamContent implements ExecStreamContent {
        final Provider<String> asText
        final Provider<byte[]> asBytes

        StreamContent(
            final Provider<String> asText,
            final Provider<byte[]> asBytes
        ) {
            this.asText = asText
            this.asBytes = asBytes
        }
    }
}
