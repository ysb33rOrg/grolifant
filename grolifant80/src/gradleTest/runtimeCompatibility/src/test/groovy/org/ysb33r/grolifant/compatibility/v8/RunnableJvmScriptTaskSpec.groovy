/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v8

import org.gradle.api.Project
import org.gradle.api.tasks.Internal
import org.gradle.process.JavaExecSpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmScriptExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmScriptExecTask
import spock.lang.Specification

class RunnableJvmScriptTaskSpec extends Specification {

    Project project
    ConfigCacheSafeOperations gtc
    TestExecTask scriptTask
    JavaExecSpec javaExecSpec

    void setup() {
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply(GrolifantServicePlugin)
        gtc = ConfigCacheSafeOperations.from(project)
        javaExecSpec = gtc.jvmTools.javaExecSpec()
        scriptTask = project.tasks.create('jvmexec', TestExecTask)
    }

    void 'Can configure script by name'() {
        setup:
        configureExecTask()

        scriptTask.configure { TestExecTask t ->
            t.script { ScriptSpec s ->
                s.name = 'fooScript'
                s.args('a', 'b')
                s.args(['c', 'd'])
            }
        }

        when:
        scriptTask.testExecSpec.copyTo(javaExecSpec)
        def commandLine = javaExecSpec.args + javaExecSpec.argumentProviders*.asArguments().flatten()

        then:
        verifyAll {
            commandLine.containsAll(['1', '2', 'fooScript', 'a', 'b', 'c', 'd'])
        }
    }

    void 'Can configure script by path'() {
        setup:
        configureExecTask()

        scriptTask.configure { TestExecTask t ->
            t.script { ScriptSpec s ->
                s.path = 'fooScript'
                s.args('a', 'b')
                s.args(['c', 'd'])
            }
        }

        when:
        scriptTask.testExecSpec.copyTo(javaExecSpec)
        def commandLineParams = javaExecSpec.args + javaExecSpec.argumentProviders*.asArguments().flatten()

        then:
        verifyAll {
            commandLineParams.every { it }
            commandLineParams.find { it.endsWith('fooScript') }
        }
    }

    void configureExecTask() {
        scriptTask.configure { TestExecTask t ->
            t.runnerSpec { cas ->
                cas.identity {
                    args = ['1', '2']
                }
            }
        }
    }

    static class TestExecSpec extends AbstractJvmScriptExecSpec<TestExecSpec> {
        protected TestExecSpec(ConfigCacheSafeOperations po) {
            super(po)
        }
    }

    static class TestExecTask extends AbstractJvmScriptExecTask<TestExecSpec> {

        @Internal
        TestExecSpec getTestExecSpec() {
            (TestExecSpec) super.jvmExecSpec
        }

        TestExecTask() {
            super()
            execSpec = new TestExecSpec(this)
        }
    }
}
