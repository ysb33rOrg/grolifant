/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v8

import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.provider.ProviderFactory
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.errors.UnexpectedNullException
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException
import spock.lang.Specification
import spock.lang.Unroll

import java.util.concurrent.Callable
import java.util.function.Supplier

@SuppressWarnings('UnnecessaryBooleanExpression')
class FileSystemOperationsSpec extends Specification {

    public static final String FILE_PATH = 'a/b/c.txt'

    Project project
    ProjectOperations po
    FileSystemOperations fsa
    ProviderFactory pv

    void setup() {
        project = ProjectBuilder.builder().build()
        po = ProjectOperations.maybeCreateExtension(project)
        fsa = po.fsOperations
        pv = project.providers
    }

    @Unroll
    void 'Safe file name: #name'() {
        expect:
        safe == fsa.toSafeFileName(name)

        where:
        name         || safe
        'abc'        || 'abc'
        'a.bc_d-e$f' || 'a.bc_d-e$f'
        'a@b!c&e'    || 'a#40!b#21!c#26!e'
    }

    void 'Extract file collection from copy specification'() {
        given: 'some files in a source directory'
        File srcDir = new File(project.projectDir, 'src')
        srcDir.mkdirs()
        new File(srcDir, '1.txt').text = '123'
        new File(srcDir, '2.txt').text = '456'

        when: 'a copy specification is created'
        CopySpec cs = project.copySpec {
            from srcDir
            include '**/*.txt'
        }

        and: 'a file collection is requested'
        Set<String> files = fsa.resolveFilesFromCopySpec(cs).files*.name

        then: 'the files in the copy specification should be listed'
        files.contains('1.txt')
        files.contains('2.txt')
    }

    @Unroll
    void 'Find class location for #clazz'() {
        expect:
        po.jvmTools.resolveClassLocation(clazz) != null

        where:
        clazz << [File, FileSystemOperationsSpec]
    }

    void 'Update a property'() {
        setup:

        def oldProv = project.objects.property(File)
        oldProv.set(new File('foo'))
        def cachedProv = oldProv

        when:
        po.fsOperations.updateFileProperty(oldProv, 'bar')

        then:
        oldProv.get() == project.file('bar')
        cachedProv.get() == project.file('bar')
    }

    void 'fsOperations.file can convert various types'() {
        setup:
        final target = new File(project.projectDir, FILE_PATH)

        expect: 'CharSequence, File, Path & URI can be converted'
        fsa.file(FILE_PATH) == target
        fsa.file("${FILE_PATH}") == target
        fsa.file(new File(FILE_PATH)) == target
        fsa.file(new File(FILE_PATH).toPath()) == target
        fsa.file(target.absoluteFile.toURI()) == target

        and: 'Closures can be converted'
        fsa.file { -> FILE_PATH } == target
        fsa.file { -> new File(FILE_PATH) } == target

        and: 'Provider instances can be converted'
        fsa.file(pv.provider { -> FILE_PATH }) == target
        fsa.file(pv.provider { -> new File(FILE_PATH) }) == target

        and: 'Supplier instances can be converted'
        fsa.file({ -> FILE_PATH } as Supplier<String>) == target
        fsa.file({ -> new File(FILE_PATH) } as Supplier<File>) == target

        and: 'Callable instances can be converted'
        fsa.file({ -> FILE_PATH } as Callable<String>) == target
        fsa.file({ -> new File(FILE_PATH) } as Callable<File>) == target

        and: 'TextResource instances can be converted'
        fsa.file(project.resources.text.fromFile(FILE_PATH)) == target

        and: 'Optionals can be converted'
        fsa.file(Optional.of(FILE_PATH)) == target
        fsa.file(Optional.of(new File(FILE_PATH))) == target

        and: 'Directory & RegularFile can be converted'
        fsa.file(project.layout.projectDirectory.dir(FILE_PATH)) == target
        fsa.file(project.layout.projectDirectory.file(FILE_PATH)) == target
    }

    @Unroll
    void 'fsOperations.file cannot convert collections'() {
        when:
        fsa.file(input)

        then:
        thrown(UnsupportedConfigurationException)

        where:
        input << [[], [] as Iterable<String>]
    }

    void 'fsOperations.file do not accept null values'() {
        when:
        fsa.file(null)

        then:
        thrown(UnexpectedNullException)
    }

    void 'fsOperations.fileOrNull accept null values'() {
        expect: 'CharSequence, FIle, Path & URI can be converted'
        fsa.fileOrNull(null) == null

        and: 'Closures can be converted'
        fsa.fileOrNull { -> null } == null

        and: 'Provider instances can be converted'
        fsa.fileOrNull(pv.provider { -> null }) == null

        and: 'Supplier instances can be converted'
        fsa.fileOrNull({ -> null } as Supplier<String>) == null

        and: 'Callable instances can be converted'
        fsa.fileOrNull({ -> null } as Callable<String>) == null

        and: 'Optionals can be converted'
        fsa.fileOrNull(Optional.empty()) == null
        fsa.fileOrNull(Optional.ofNullable((File) null)) == null
    }

    void 'fsOperations.files can convert multiple collections'() {
        setup:
        final target = new File(project.projectDir, FILE_PATH)

        when:
        final allFiles = fsa.files([
            FILE_PATH,
            "${FILE_PATH}",
            new File(FILE_PATH),
            new File(FILE_PATH).toPath(),
            target.absoluteFile.toURI(),
            [{ -> FILE_PATH }],
            [[{ -> new File(FILE_PATH) }]],
            pv.provider { -> FILE_PATH },
            pv.provider { -> new File(FILE_PATH) },
            { -> FILE_PATH } as Callable<String>,
            { -> new File(FILE_PATH) } as Supplier<File>,
            Optional.of(FILE_PATH),
            project.layout.projectDirectory.dir(FILE_PATH),
            project.layout.projectDirectory.file(FILE_PATH)
        ])

        then:
        allFiles.files.every { it == target }

        when:
        final allFiles2 = fsa.files([allFiles])

        then:
        allFiles2.files.every { it == target }
    }

    void 'fsOperations.filesDropNull can convert multiple collections'() {
        when:
        final allFiles = fsa.filesDropNull([FILE_PATH, null])

        then:
        allFiles.files.size() == 1
    }
}
