/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.TaskInputFilePropertyBuilder
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.TaskInputFileOptions
import org.ysb33r.grolifant5.api.core.TaskTools
import org.ysb33r.grolifant5.loadable.core.TaskToolsProxy

import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.IGNORE_EMPTY_DIRECTORIES
import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.NORMALIZE_LINE_ENDINGS
import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.OPTIONAL
import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.SKIP_WHEN_EMPTY

/**
 * Implements {@link TaskTools} for Gradle 8.x
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultTaskTools extends TaskToolsProxy {

    DefaultTaskTools(ProjectOperations incompleteReference, Project project) {
        super(incompleteReference, project)
    }

    /**
     * Registers a task in a lazy-manner.
     *
     * @param taskName Name of task to register. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param < T >             Type of task.
     */
    @Override
    public <T extends DefaultTask> void register(String taskName, Class<T> taskType, Action<T> configurator) {
        tasks.register(taskName, taskType, configurator)
    }

    /**
     * Registers a task in a lazy-manner.
     *
     * @param taskName Name of task to register. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param args C-tor arguments.
     * @param configurator Configurating action.
     * @param <T>                 Type of task.
     */
    @Override
    public <T extends DefaultTask> void register(
        String taskName,
        Class<T> taskType,
        Iterable<Object> args,
        Action<T> configurator
    ) {
        final task = tasks.register(taskName, taskType, args.toList() as Object[])
        task.configure(configurator)
    }

    /**
     * Configures a task, preferably in a lazy-manner.
     *
     * @param taskName Name of task to configure.  Task must have been registered or configured previously.
     * @param configurator Configurating action.
     */
    @Override
    @CompileDynamic
    void named(String taskName, Action<Task> configurator) {
        tasks.named(taskName, configurator)
    }

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName Name of task to configure.
     * @param configurator Configurating action.
     */
    @Override
    void whenNamed(String taskName, Action<Task> configurator) {
        tasks.configureEach { Task it ->
            if (it.name == taskName) {
                configurator.execute(it)
            }
        }
    }

    /**
     * Configures a task, preferably in a lazy-manner.
     *
     * @param taskName Name of task to configure. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param <T >             Type of task.
     */
    @Override
    public <T extends DefaultTask> void named(String taskName, Class<T> taskType, Action<T> configurator) {
        tasks.named(taskName, taskType, configurator)
    }

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName Name of task to configure.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param <T >                Type of task.
     */
    @Override
    @CompileDynamic
    public <T extends DefaultTask> void whenNamed(String taskName, Class<T> taskType, Action<T> configurator) {
        tasks.withType(taskType).configureEach { T task ->
            if (task.name == taskName) {
                configurator.execute(task)
            }
        }
    }

    /**
     * Allows the onlyIf reason structure to be used backwards on Gradle <7.6.
     *
     * @param task Apply to task.
     * @param onlyIfReason
     * @param spec
     *
     * @since 5.0
     */
    @Override
    void onlyIf(Task task, String onlyIfReason, Spec<? super Task> spec) {
        task.onlyIf(onlyIfReason, spec)
    }

    /**
     * Creates an input to a task that is based upon a collection of files.
     *
     * @param inputsBuilder The property builder
     * @param options Additional options to assign to the task.
     * Some options might be ignored, depending on the version of Gradle.
     */
    @Override
    protected void createTaskInputsFileEntry(
        TaskInputFilePropertyBuilder inputsBuilder,
        List<TaskInputFileOptions> options
    ) {
        options.forEach {
            switch (it) {
                case SKIP_WHEN_EMPTY:
                    inputsBuilder.skipWhenEmpty()
                    break
                case OPTIONAL:
                    inputsBuilder.optional()
                    break
                case IGNORE_EMPTY_DIRECTORIES:
                    inputsBuilder.ignoreEmptyDirectories()
                    break
                case NORMALIZE_LINE_ENDINGS:
                    inputsBuilder.normalizeLineEndings()
                    break
            }
        }
    }

    /**
     * Whether this is a {@code TaskProvider<?>}.
     *
     * @param o Object to evaluate
     * @return {@code true} is an instance of {@link org.gradle.api.tasks.TaskProvider}.
     */
    @Override
    protected boolean isTaskProvider(Object o) {
        o instanceof TaskProvider
    }
}
