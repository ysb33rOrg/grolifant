/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.CopySpec
import org.gradle.api.file.Directory
import org.gradle.api.file.FileSystemOperations
import org.gradle.api.file.ProjectLayout
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant5.api.core.LegacyLevel
import org.ysb33r.grolifant5.loadable.core.ProjectOperationsProxy

import javax.inject.Inject
import java.util.concurrent.Callable
import java.util.function.Function

import static org.ysb33r.grolifant5.loadable.core.LoadableVersion.V8

/**
 * An extension that can be added to a project by a plugin to aid in compatibility
 *
 * @since 1.0.0
 */
@CompileStatic
@Slf4j
class DefaultProjectOperations extends ProjectOperationsProxy {

    /**
     * Constructor that sets up a number of methods to be compatible across a wide range Gradle releases.
     *
     * @param tempProjectReference temporary project
     */
    @Inject
    DefaultProjectOperations(
        Project tempProjectReference,
        ProjectLayout layout,
        ProviderFactory pf,
        FileSystemOperations fileOperations,
        ObjectFactory objects
    ) {
        super(tempProjectReference, V8)
//        this.projectCacheDir = tempProjectReference.gradle.startParameter.projectCacheDir ?:
//            tempProjectReference.file("${tempProjectReference.rootDir}/.gradle")
        this.projectLayout = layout
        this.providerFactory = pf
//        this.execs = new DefaultExecOperations(tempProjectReference)
        this.fileTreeFactory = { Object base -> objects.fileTree().from(base) }
//        this.propertyProvider = GradleSysEnvProviderFactory.loadInstance(this)
        this.createCopySpec = copySpecFactory(tempProjectReference, fileOperations)
        this.buildDir = projectLayout.buildDirectory.map({ Directory it ->
            it.asFile
        } as Transformer<File, Directory>)
    }

    /**
     * Creates an empty CopySpec.
     *
     * @return {@link CopySpec}
     */
    @Override
    CopySpec copySpec() {
        createCopySpec.call()
    }

    /**
     * Creates a new ConfigurableFileTree.
     *
     * @param base Base for file tree.
     *
     * @return File tree.
     */
    @Override
    ConfigurableFileTree fileTree(Object base) {
        this.fileTreeFactory.apply(base)
    }

    /**
     * Build directory
     *
     * @return Provider to the build directory
     */
    @Override
    Provider<File> getBuildDir() {
        this.buildDir
    }

    /** Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    @Override
    public <T> Provider<T> provider(Callable<? extends T> var1) {
        providerFactory.provider(var1)
    }

    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        providerFactory.environmentVariable(stringTools.stringize(name))
    }

    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        providerFactory.gradleProperty(stringTools.stringize(name))
    }

    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        providerFactory.systemProperty(stringTools.stringize(name))
    }

//    @Override
//    protected ExecOperationsProxy getExecOperations() {
//        this.execs
//    }

//    @Override
//    protected GradleSysEnvProvider getPropertyProvider() {
//        this.propertyProvider
//    }

    @CompileDynamic
    private Callable<CopySpec> copySpecFactory(Project tempProjectReference, FileSystemOperations fso) {
        if (LegacyLevel.PRE_8_5) {
            { -> tempProjectReference.copySpec() } as Callable<CopySpec>
        } else {
            { -> fso.copySpec() } as Callable<CopySpec>
        }
    }

    private final Function<Object, ConfigurableFileTree> fileTreeFactory
//    private final File projectCacheDir
    private final ProviderFactory providerFactory
    private final ProjectLayout projectLayout
    private final Provider<File> buildDir
//    private final ExecOperationsProxy execs
//    private final GradleSysEnvProvider propertyProvider
    private final Callable<CopySpec> createCopySpec
}
