/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.ProjectLayout
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ProviderFactory
import org.gradle.process.ExecOperations
import org.gradle.process.ExecSpec
import org.gradle.process.JavaExecSpec
import org.gradle.process.internal.DefaultExecSpec
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.downloader.Downloader
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput
import org.ysb33r.grolifant5.internal.core.executable.CommandArgumentSpec
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader
import org.ysb33r.grolifant5.internal.v8.downloader.InternalDownloader
import org.ysb33r.grolifant5.internal.v8.executable.InternalAppRunnerSpec
import org.ysb33r.grolifant5.internal.v8.runnable.DefaultExecOutput
import org.ysb33r.grolifant5.loadable.core.LoadableVersion

import javax.inject.Inject
import java.util.function.Function

import static org.ysb33r.grolifant5.internal.core.executable.ExecUtils.configureStreams

/**
 * Non-JVM process execution tools for Gradle 8.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0.
 */
@CompileStatic
class DefaultExecTools implements ExecTools {

    @Inject
    DefaultExecTools(
        Project tempProjectReference,
        ObjectFactory objF,
        ProviderFactory pf,
        ProjectLayout pl,
        ExecOperations execOps
    ) {
        this.objectFactory = objF
        this.providers = pf
        this.projectDir = pl.projectDirectory.asFile
        this.stringTools = StringToolsLoader.load(tempProjectReference, LoadableVersion.V8)
        this.execOps = execOps
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput exec(OutputType stdout, OutputType stderr, Action<? super ExecSpec> action) {
        ExecUtils.ConfiguredStreams outStreams
        ExecUtils.ConfiguredStreams errStreams
        boolean ignoreExit
        try {
            final result = execOps.exec {
                action.execute(it)
                outStreams = configureStreams(stdout, it.standardOutput, System.out)
                errStreams = configureStreams(stderr, it.errorOutput, System.err)
                ignoreExit = it.ignoreExitValue
                it.ignoreExitValue = true
                it.standardOutput = outStreams.out
                it.errorOutput = errStreams.out
            }

            ExecUtils.failOnExitCode(result, ignoreExit, stdout, stderr)
            DefaultExecOutput.fromResult(objectFactory, providers, result, outStreams.capture, errStreams.capture)
        } finally {
            outStreams?.close()
            errStreams?.close()
        }
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    @Override
    ExecOutput javaexec(OutputType stdout, OutputType stderr, Action<? super JavaExecSpec> action) {
        ExecUtils.ConfiguredStreams outStreams
        ExecUtils.ConfiguredStreams errStreams
        boolean ignoreExit
        final result = execOps.javaexec {
            action.execute(it)
            outStreams = configureStreams(stdout, it.standardOutput, System.out)
            errStreams = configureStreams(stderr, it.errorOutput, System.err)
            ignoreExit = it.ignoreExitValue
            it.ignoreExitValue = true
            it.standardOutput = outStreams.out
            it.errorOutput = errStreams.out
        }

        ExecUtils.failOnExitCode(result, ignoreExit, stdout, stderr)
        DefaultExecOutput.fromResult(objectFactory, providers, result, outStreams.capture, errStreams.capture)
    }

    /**
     * Executes the specified external process on-demand.
     *
     * @param action
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     *
     * @since 1.0
     */
    @Override
    ExecOutput provideExec(Action<? super ExecSpec> action) {
        DefaultExecOutput.fromResult(providers.exec(action))
    }

    /**
     * Executes the specified external java process on-demand.
     *
     * @param action Configures a {@link JavaExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     *
     * @since 5.0
     */
    @Override
    ExecOutput provideJavaExec(Action<? super JavaExecSpec> action) {
        DefaultExecOutput.fromResult(providers.javaexec(action))
    }

    /**
     * Returns something that looks like an {@link ExecSpec}.
     *
     * @return Instance of an executable specification.
     */
    @Override
    ExecSpec execSpec() {
        objectFactory.newInstance(DefaultExecSpec)
    }

    /**
     * Creates a {@link AppRunnerSpec}.
     *
     * This is primarily used internally by classes that implements execution specifications for non-JVM processes.
     *
     * @return Implementation of {@link AppRunnerSpec}
     */
    @Override
    AppRunnerSpec appRunnerSpec() {
        objectFactory.newInstance(InternalAppRunnerSpec)
    }

    /**
     * Returns an implementation that is optimised for the running version of Gradle.
     *
     * @return Instance of {@link CommandEntryPoint}. Never {@code null}
     */
    @Override
    CommandEntryPoint commandEntryPoint() {
        objectFactory.newInstance(CommandArgumentSpec, stringTools)
    }

    /**
     * Creates a new downloader for downloading packages / distributions.
     *
     * @param distributionName Name of distribution to be downloaded.
     *
     * @return Instance of {@link Downloader} that is optimised for the running Gradle version.
     */
    @Override
    Downloader downloader(final String distributionName) {
        new InternalDownloader(distributionName, projectDir)
    }

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser A parser for the output of running the executable which could extract the version.
     * @param configurator Additional configurator to customise the execution specification.
     * @return The version string.
     */
    @Override
    String parseVersionFromOutput(
        Iterable<String> argsForVersion,
        File executablePath,
        Function<String, String> versionParser,
        Action<ExecSpec> configurator
    ) {
        ExecUtils.parseVersionFromOutput(
            this,
            argsForVersion,
            executablePath,
            versionParser,
            configurator
        )
    }

    private final ObjectFactory objectFactory
    private final ProviderFactory providers
    private final File projectDir
    private final StringTools stringTools
    private final ExecOperations execOps
}
