/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.AuthenticationContainer
import org.gradle.api.model.ObjectFactory
import org.gradle.internal.authentication.DefaultAuthenticationContainer
import org.gradle.internal.reflect.DirectInstantiator
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.RepositoryTools

/**
 * Providers repository tools that is compatible with Gradle 8.x
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultRepositoryTools implements RepositoryTools {
    @SuppressWarnings('UnusedMethodParameter')
    DefaultRepositoryTools(ProjectOperations incompleteReference, Project project) {
        this.objectFactory = project.objects
    }

    /**
     * Creates an instance of an {@link AuthenticationContainer}
     *
     * @return Authentication container that is usable with a repository.
     */
    @Override
    AuthenticationContainer authenticationContainer() {
        new DefaultAuthenticationContainer(
            DirectInstantiator.INSTANCE,
            org.gradle.api.internal.CollectionCallbackActionDecorator.NOOP
        )
    }

    private final ObjectFactory objectFactory
}
