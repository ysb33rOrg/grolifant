/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.file.ProjectLayout
import org.gradle.api.model.ObjectFactory
import org.gradle.internal.jvm.Jvm
import org.gradle.process.JavaExecSpec
import org.gradle.process.internal.DefaultJavaExecSpec
import org.ysb33r.grolifant5.api.core.ClassLocation
import org.ysb33r.grolifant5.api.core.JvmTools
import org.ysb33r.grolifant5.api.core.jvm.JvmAppRunnerSpec
import org.ysb33r.grolifant5.internal.v8.jvm.InternalJvmAppExecSpec80

import javax.inject.Inject
import java.util.regex.Pattern

import static org.ysb33r.grolifant5.internal.core.jvm.ClassPathUtils.resolveClassLocationWithSubstitution

@Slf4j
@CompileStatic
class DefaultJvmTools implements JvmTools {

    @Inject
    DefaultJvmTools(Project tempProjectReference) {
        this.objectFactory = tempProjectReference.objects
        this.layout = tempProjectReference.layout
    }

    /**
     * Creates a {@link JavaExecSpec}.
     *
     * @return Returns something compatible with a {@link JavaExecSpec}.
     */
    @Override
    JavaExecSpec javaExecSpec() {
        objectFactory.newInstance(DefaultJavaExecSpec).tap {
            executable = Jvm.current().javaExecutable.absolutePath
            workingDir = layout.projectDirectory.asFile
        }
    }

    /**
     * Creates a {@link JvmAppRunnerSpec}.
     *
     * This is primarily used internally by classes that implements execution specifications on the JVM.
     *
     * @return Implementation of {@link JvmAppRunnerSpec}
     */
    @Override
    JvmAppRunnerSpec jvmAppRunnerSpec() {
        objectFactory.newInstance(InternalJvmAppExecSpec80)
    }

    /**
     * Returns the classpath location for a specific class.
     * <p>
     * Works around configuration cache-related instrumentation issue in Gradle 6.5+.
     * See {@link https://github.com/gradle/gradle/issues/14727} for details.
     * <p>
     * If the JAR name is the same as the {@code instrumentJarName}, then search the substitution collection for the
     * first hit that matches the provided pattern.
     *
     * @param aClass Class to find.
     * @param substitutionSearch Files to search. A typical example would be to look in
     * {@code rootProject.buildscript.configurations.getByName( 'classpath' )}.
     * @param substitutionMatch The pattern to look for. Typically the name of a jar with a version.
     * @param redoIfMatch A pattern that will cause a recheck if the path matches.
     *   By default, this is if the filename ends in {@code .jar}.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     *   on classpath
     * @throw ClassNotFoundException
     */
    @Override
    ClassLocation resolveClassLocation(
        Class aClass,
        FileCollection substitutionSearch,
        Pattern substitutionMatch,
        Pattern redoIfMatch,
        Pattern ignoreFromPaths
    ) {
        resolveClassLocationWithSubstitution(
            aClass,
            substitutionSearch,
            substitutionMatch,
            redoIfMatch,
            ignoreFromPaths
        )
    }

    private final ObjectFactory objectFactory
    private final ProjectLayout layout
}
