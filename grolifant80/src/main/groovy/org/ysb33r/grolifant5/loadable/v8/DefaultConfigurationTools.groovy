/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.NamedDomainObjectProvider
import org.gradle.api.Project
import org.gradle.api.attributes.AttributeContainer
import org.ysb33r.grolifant5.api.core.ConfigurationTools
import org.ysb33r.grolifant5.api.core.LegacyLevel
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.loadable.core.ConfigurationToolsProxy

/**
 * Provides an implementation of {@link ConfigurationTools} for Gradle 8.x.
 *
 * @since 2.1
 */
@CompileStatic
class DefaultConfigurationTools extends ConfigurationToolsProxy {

    DefaultConfigurationTools(
        ProjectOperations incompleteReference, Project project) {
        super(incompleteReference, project)
    }

    /**
     * Creates three configurations that are related to each other.
     *
     * This works on the same model as to how {@code implementation}, {@code runtimeClaspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName The name of a configuration which can be resolved.
     * @param consumableConfigurationName The name of a configuration that can be consumed by other subprojects.
     * @param visible Whether configurations should be marked visible or invisible.
     * @param attributes Action to configure attributes for resolvable and consumable configurations.
     *
     * @since 5.0
     */
    @Override
    void createRoleFocusedConfigurations(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        if (LegacyLevel.PRE_8_4) {
            createTrinityConfigurationsPre84(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                consumableConfigurationName,
                visible,
                attributes
            )

            if (!LegacyLevel.PRE_8_2) {
                markUndeclarablePre84([resolvableConfigurationName, consumableConfigurationName])
            }
        } else {
            createTrinityConfigurations(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                consumableConfigurationName,
                visible,
                attributes
            )
        }
    }

    /**
     * Creates two configurations that are related to each other and which are only meant to be used within the
     * same (sub)project.
     * <p>
     * This works on the same model as to how {@code implementation} and {@code runtimeClasspath}.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName The name of a configuration which can be resolved.
     * @param visible Whether the configurations will be visible.
     *
     * @since 5.0
     */
    @Override
    void createLocalRoleFocusedConfiguration(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        boolean visible
    ) {
        if (LegacyLevel.PRE_8_4) {
            createDualConfigurationsPre84(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                visible
            )

            if (!LegacyLevel.PRE_8_2) {
                markUndeclarablePre84([resolvableConfigurationName])
            }
        } else {
            createDualConfigurations(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                visible
            )
        }
    }

    /**
     * Creates a single configuration to be used for outgoing publications.
     * <p>Very useful for sharing internal outputs between subprojects.</p>
     *
     * @param configurationName Name of outgoing configuration.
     * @param visible Whether the configuration is visible.
     * @param attributes Action to configure attributes for resolvable and consumable configurations.
     *
     * @since 5.0
     */
    @Override
    void createSingleOutgoingConfiguration(
        String configurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        if (LegacyLevel.PRE_8_4) {
            createOutgoingConfigurationPre84(configurationName, visible, attributes)
        } else {
            createOutgoingConfiguration(configurationName, visible, attributes)
        }
    }

    @CompileDynamic
    private void markUndeclarablePre84(List<String> configurationNames) {
        configurationNames.each {
            configurations.getByName(it).canBeDeclared = false
        }
    }

    @CompileDynamic
    private createOutgoingConfiguration(
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        final consumable = configurations.consumable(consumableConfigurationName)
        consumable.configure {
            it.visible = visible
            it.attributes(attributes)
        }
    }

    @CompileDynamic
    private void createDualConfigurations(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        boolean visible
    ) {
        final depScope = configurations.dependencyScope(dependencyScopedConfigurationName)
        final resolvable = configurations.resolvable(resolvableConfigurationName)

        depScope.configure {
            it.visible = visible
        }
        resolvable.configure {
            it.extendsFrom(depScope.get())
            it.visible = visible
        }
    }

    @CompileDynamic
    private void createTrinityConfigurations(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        NamedDomainObjectProvider<?> depScope = configurations.dependencyScope(dependencyScopedConfigurationName)
        final resolvable = configurations.resolvable(resolvableConfigurationName)
        final consumable = configurations.consumable(consumableConfigurationName)

        [resolvable, consumable].each { c ->
            c.configure {
                it.extendsFrom(depScope.get())
                it.attributes(attributes)
                it.visible = visible
            }
        }
    }
}
