/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.api.Project
import org.ysb33r.grolifant5.loadable.core.LoadableVersion
import org.ysb33r.grolifant5.loadable.core.ProviderToolsProxy

import javax.inject.Inject

/**
 * Safely deal with Providers down to Gradle 8.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@InheritConstructors
class DefaultProviderTools extends ProviderToolsProxy {
    @Inject
    DefaultProviderTools(Project tempProjectReference) {
        super(tempProjectReference, LoadableVersion.V8)
    }
}
