/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.v8

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.ArchiveOperations
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.CopySpec
import org.gradle.api.file.DeleteSpec
import org.gradle.api.file.FileSystemOperations as GradleFileSytemOperations
import org.gradle.api.file.FileTree
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.file.copy.DefaultCopySpec
import org.gradle.api.model.ObjectFactory
import org.gradle.api.resources.ReadableResource
import org.gradle.api.resources.TextResource
import org.gradle.api.tasks.WorkResult
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.LegacyLevel
import org.ysb33r.grolifant5.api.core.Transform
import org.ysb33r.grolifant5.loadable.core.FileSystemOperationsProxy
import org.ysb33r.grolifant5.loadable.core.LoadableVersion

import javax.inject.Inject

/**
 * Implements {@link FileSystemOperations} for Gradle 8.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DefaultFileSystemOperations extends FileSystemOperationsProxy {

    @Inject
    DefaultFileSystemOperations(
        Project tempProjectReference,
        ProjectLayout layout,
        ObjectFactory objects,
        GradleFileSytemOperations fso,
        ArchiveOperations archives
    ) {
        super(tempProjectReference, LoadableVersion.V8)
        this.fso = fso
        this.projectLayout = layout
        this.objectFactory = objects
        this.archives = archives
    }

    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource bzip2Resource(Object file) {
        archives.bzip2(provideFile(file))
    }

    /**
     * Copies the specified files.
     * @param action Configures a {@link CopySpec}
     * @return Result of copy to check whether it was successful.
     */
    @Override
    WorkResult copy(Action<? super CopySpec> action) {
        fso.copy(action)
    }

    /**
     * Creates an empty {@link CopySpec}.
     *
     * @return Empty copy specification.
     *
     * @since 5.1
     */
    @Override
    CopySpec copySpec() {
        if (LegacyLevel.PRE_8_5) {
            copySpecPre85()
        } else {
            createCopySpec()
        }
    }

    /**
     * Deletes the specified files.
     * @param action Configures a {@link DeleteSpec}
     * @return Result of deletion to check whether it was successful.
     */
    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        fso.delete(action)
    }

    /**
     * Creates an empty file collection.
     *
     * @return Empty file collection.
     */
    @Override
    ConfigurableFileCollection emptyFileCollection() {
        objectFactory.fileCollection()
    }

    /**
     * Converts a file-like object to a {@link java.io.File} instance with project context.
     * <p>
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory} (Gradle 4.1+)
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * @param file Potential {@link File} object
     * @return File instance.
     */
    @Override
    File file(Object file) {
        Transform.convertItem(file) { f ->
            switch (f) {
                case TextResource:
                    projectLayout.files(((TextResource) f).asFile()).singleFile
                    break
                default:
                    projectLayout.files(f).singleFile
            }
        }
    }

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     */
    @Override
    File fileOrNull(Object file) {
        Transform.convertItemOrNull(file) { f ->
            if (f == null) {
                return null
            }
            switch (f) {
                case TextResource:
                    return fileOrNull(((TextResource) f).asFile())
                default:
                    projectLayout.files(f).singleFile
            }
        }
    }

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    @Override
    ReadableResource gzipResource(Object file) {
        archives.gzip(provideFile(file))
    }

    /**
     * Synchronizes the contents of a destination directory with some source directories and files.
     *
     * @param action Action to configure the CopySpec.
     * @return {@link WorkResult} that can be used to check if the sync did any work.
     */
    @Override
    WorkResult sync(Action<? super CopySpec> action) {
        fso.sync(action)
    }

    /**
     * Expands a tar file into a {@link FileTree}.
     *
     * @param tarPath Path to tar file.
     *    Anything that can be converted with {@link #file}
     * @return Tree of tar contents.
     *
     * @since 5.0
     */
    @Override
    FileTree tarTree(Object tarPath) {
        if (tarPath instanceof ReadableResource) {
            archives.tarTree(tarPath)
        } else {
            archives.tarTree(provideFile(tarPath))
        }
    }

    /**
     * Expands a ZIP file into a {@link FileTree}.
     *
     * @param zipPath Path to tar file.
     *    Anything that can be converted with {@link #file}
     * @return Tree of ZIP contents.
     *
     * @since 5.0
     */
    @Override
    FileTree zipTree(Object zipPath) {
        if (zipPath instanceof ReadableResource) {
            archives.zipTree(zipPath)
        } else {
            archives.zipTree(provideFile(zipPath))
        }
    }

    private CopySpec copySpecPre85() {
        objectFactory.newInstance(DefaultCopySpec)
    }

    @CompileDynamic
    private CopySpec createCopySpec() {
        fso.copySpec()
    }

    private final GradleFileSytemOperations fso
    private final ProjectLayout projectLayout
    private final ObjectFactory objectFactory
    private final ArchiveOperations archives
}
