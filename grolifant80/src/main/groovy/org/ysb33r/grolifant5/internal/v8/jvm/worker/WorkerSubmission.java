/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v8.jvm.worker;

import org.gradle.process.JavaExecSpec;
import org.gradle.workers.WorkQueue;
import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerExecSpec;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise;
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutor;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory;

/**
 * A utility class for submitting JVM jobs on workers for Gradle 6.0+
 *
 * @param <P> The type of the POJO/POGO that holds the parameters
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public class WorkerSubmission<P extends SerializableWorkerAppParameters> {
    /**
     * Submits this to a worker queue using an appropriate isolation mode.
     *
     * @param isolationMode            Isolation mode which is either classpath isolated or out of process.
     * @param worker                   A worker execution instance.
     * @param workerExecSpec           A worker execution specification
     * @param workerAppExecutorFactory A factory instances that can create executor logic.
     * @param parameterFactory         A factory which can create parameters and populate them from a
     *                                 {@link JavaExecSpec}..
     */
    public WorkerPromise toWorkQueue(
            WorkerIsolation isolationMode,
            WorkerExecutor worker,
            final WorkerExecSpec workerExecSpec,
            WorkerAppExecutorFactory<P> workerAppExecutorFactory,
            WorkerAppParameterFactory<P> parameterFactory
    ) {
        final SerializableWorkerAppParameters exeParams = parameterFactory.createAndConfigure(workerExecSpec);
        final WorkerAppExecutor<P> executor = workerAppExecutorFactory.createExecutor();
        final WorkQueue wq = workQueue(isolationMode, worker, workerExecSpec);
        wq.submit(
                InternalWorkerAppExecutor.class,
                p -> {
                    p.getParams().set(exeParams);
                    p.getExecutor().set(executor);
                }
        );
        return () -> wq.await();
    }

    private WorkQueue workQueue(
            WorkerIsolation isolationMode,
            WorkerExecutor worker,
            final WorkerExecSpec workerExecSpec
    ) {
        if (isolationMode.equals(WorkerIsolation.CLASSPATH)) {
            return worker.classLoaderIsolation(spec -> {
                spec.getClasspath().from(workerExecSpec.getJavaExecSpec().getClasspath());
            });
        } else {
            return worker.processIsolation(spec -> {
                spec.getClasspath().from(workerExecSpec.getJavaExecSpec().getClasspath());
                workerExecSpec.getJavaExecSpec().copyTo(spec.getForkOptions());
            });
        }
    }
}
