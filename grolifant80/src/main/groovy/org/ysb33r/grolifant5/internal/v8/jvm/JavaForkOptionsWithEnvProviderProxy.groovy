/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v8.jvm

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Action
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Provider
import org.gradle.process.JavaForkOptions
import org.ysb33r.grolifant5.api.core.jvm.CopyDebugOptions
import org.ysb33r.grolifant5.api.core.jvm.JavaForkOptionsWithEnvProvider
import org.ysb33r.grolifant5.api.core.jvm.JvmDebugOptions
import org.ysb33r.grolifant5.internal.core.jvm.DefaultJvmDebugOptions
import org.ysb33r.grolifant5.internal.core.jvm.DefaultJvmDebugOptionsProxy
import org.ysb33r.grolifant5.internal.core.runnable.EnvironmentVariableProviders

/**
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@Slf4j
class JavaForkOptionsWithEnvProviderProxy implements JavaForkOptionsWithEnvProvider {

    JavaForkOptionsWithEnvProviderProxy(
        JavaForkOptions instance,
        EnvironmentVariableProviders evp,
        ObjectFactory objects
    ) {
        this.delegate = instance
        this.evp = evp
        this.debugOptions = new DefaultJvmDebugOptions(objects)
        this.jvmDebugOptionsProxy = new DefaultJvmDebugOptionsProxy()
    }

    @Override
    void addEnvironmentProvider(Provider<Map<String, String>> envProvider) {
        evp.addEnvironmentProvider(envProvider)
    }

    @Override
    void withDebug(Action<JvmDebugOptions> configurator) {
        configurator.execute(debugOptions)
        jvmDebugOptionsProxy.copyDebugOptions(debugOptions, delegate)
    }

    @Delegate(interfaces = true, deprecated = true, methodAnnotations = false, parameterAnnotations = false)
    private final JavaForkOptions delegate

    private final JvmDebugOptions debugOptions
    private final EnvironmentVariableProviders evp
    private final CopyDebugOptions jvmDebugOptionsProxy
}
