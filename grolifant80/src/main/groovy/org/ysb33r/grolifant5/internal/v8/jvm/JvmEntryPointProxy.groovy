/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v8.jvm

import groovy.transform.CompileStatic
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.gradle.process.JavaExecSpec
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint

@CompileStatic
class JvmEntryPointProxy implements JvmEntryPoint {

    JvmEntryPointProxy(
        JavaExecSpec es,
        StringTools stringTools
    ) {
        this.javaExecSpec = es
        this.stringTools = stringTools
    }

    JavaExecSpec setClasspath(FileCollection fc) {
        javaExecSpec.classpath = fc
        javaExecSpec
    }

    JavaExecSpec classpath(Object... paths) {
        javaExecSpec.classpath(paths)
        javaExecSpec
    }

    FileCollection getClasspath() { javaExecSpec.classpath }

    void setInferModulePath(Provider<Boolean> p) { javaExecSpec.modularity.inferModulePath.set(p) }

    void setInferModulePath(boolean p) { javaExecSpec.modularity.inferModulePath.set(p) }

    Provider<Boolean> getInferModulePath() { javaExecSpec.modularity.inferModulePath }

    void setMainModule(Object o) {
        stringTools.updateStringProperty(javaExecSpec.mainModule, o)
    }

    Provider<String> getMainModule() { javaExecSpec.mainModule }

    /**
     * Get the main class name.
     *
     * @return Provider.
     */
    @Override
    Provider<String> getMainClass() {
        javaExecSpec.mainClass
    }

    @Override
    void setMainClass(Object name) {
        stringTools.updateStringProperty(javaExecSpec.mainClass, name)
    }

    /**
     * Copy entry point information to a {@link JavaExecSpec}
     *
     * @param target {@link JavaExecSpec}
     */
    void copyTo(JavaExecSpec target) {
        target.mainClass.set(javaExecSpec.mainClass)
        target.classpath = classpath
        target.modularity.inferModulePath.set(javaExecSpec.modularity.inferModulePath)
        target.mainModule.set(javaExecSpec.mainModule)
    }

    private final StringTools stringTools
    private final JavaExecSpec javaExecSpec
}
