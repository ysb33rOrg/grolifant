/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v8.downloader

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.wrapper.Download
import org.gradle.wrapper.IDownload
import org.gradle.wrapper.Logger
import org.gradle.wrapper.PathAssembler
import org.gradle.wrapper.WrapperConfiguration
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.DownloadedLocalFile
import org.ysb33r.grolifant5.api.core.downloader.Downloader

/**
 * Implements a downloader for Gradle 8.x.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@Slf4j
@CompileStatic
class InternalDownloader implements Downloader {

    InternalDownloader(final String distributionName, File projectDir) {
        this.logger = new Logger(!log.infoEnabled)
        this.downloader = new Download(logger, distributionName, INSTALLER_VERSION)
        this.projectDir = projectDir
    }

    @Deprecated
    InternalDownloader(final String distributionName, ProjectOperations po) {
        this.logger = new Logger(!log.infoEnabled)
        this.downloader = new Download(logger, distributionName, INSTALLER_VERSION)
        this.projectDir = po.projectDir
    }

    /**
     * Logs a progress message.
     *
     * @param msg Message.
     */
    @Override
    void logProgress(String msg) {
        logger.log(msg)
    }

    /**
     * Downloads a package / distribution / file
     *
     * By default file and http(s) schemes are supported. If Gradle adds additional schemes they are automatically
     * supported.
     *
     * @param address URI to download package from.
     * @param destination Destination to download file to.
     */
    @Override
    void download(URI address, File destination) {
        downloader.download(address, destination)
    }

    /**
     * Returns information on where files are downloaded to.
     *
     * @param address URI to download package from.
     * @param downloadRoot THe root directory where download are sent to.
     * @param relativeBasePath A relative path to the download root where to place files and temporary artifacts.
     * @param Checksum for when verification is required.
     * @return Information on where the file will be downloaded to.
     */
    @Override
    DownloadedLocalFile downloadLocation(
        URI address,
        File downloadRoot,
        String relativeBasePath,
        String checksum = null
    ) {
        final WrapperConfiguration configuration = new WrapperConfiguration()
        configuration.distribution = address
        configuration.distributionPath = configuration.zipPath = relativeBasePath

        if (checksum) {
            configuration.distributionSha256Sum = checksum
        }

        final PathAssembler pathAssembler = new PathAssembler(downloadRoot, projectDir)
        final PathAssembler.LocalDistribution localDistribution = pathAssembler.getDistribution(configuration)

        new DownloadedLocalFile(
            localDistribution.distributionDir,
            localDistribution.zipFile,
            configuration.distribution
        )
    }

    private final IDownload downloader
    private final Logger logger
    private final File projectDir

    private static final String INSTALLER_VERSION = '1.0' // TODO: Load from Grolifant version
}
