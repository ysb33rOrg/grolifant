/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.v8.jvm

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import javax.inject.Inject

/**
 * Provides a class that can be populated with various fork options for Java
 * and which can then be used to copy to other methods in the Gradle API that provides a
 * {@link org.gradle.process.JavaForkOptions} in the parameters.
 *
 * Intended for Gradle 8.x
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class InternalJvmAppExecSpec80 extends InternalAbstractJvmAppExecSpec {
    /**
     * Creates the JvmExecSpec on Gradle 8.0+.
     *
     * @param tempProjectReference Temporary {@link Project}
     * @param objects Object factory.
     * @param jfoProxyFactory Create a proxy instance for updating {@link org.gradle.process.JavaForkOptions}.
     * @param modularitySpecFactory Create a proxy instance for updating modularity specifications.
     */
    @Inject
    InternalJvmAppExecSpec80(Project tempProjectReference) {
        super(ConfigCacheSafeOperations.from(tempProjectReference), tempProjectReference.objects)
    }
}
