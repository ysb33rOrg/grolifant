.build.gradle
[source,groovy]
[subs=+attributes]
----
dependencies {
  api '{gradle-project-group}:{library-prefix}-core:{gradle-project-version}'
  implementation '{gradle-project-group}:{library-prefix}-rawhide:{gradle-project-version}' // <1>
  runtimeOnly '{gradle-project-group}:{library-prefix}-herd:{gradle-project-version}'
}
----
<1> Only needed if you want to access the worker API directly from your plugin.

OR if you are using the version catalog

.gradle/libs.versions.toml
[source,toml]
[subs=+attributes]
----
[versions]
grolifant = "{gradle-project-version}"

[libraries]
grolifantApi = { module = "{gradle-project-group}:{library-prefix}-api", version.ref = "grolifant" }
grolifantRuntime = { module = "{gradle-project-group}:{library-prefix}-herd", version.ref = "grolifant" }
grolifantRawhide = { module = "{gradle-project-group}:{library-prefix}-rawhide", version.ref = "grolifant" } # <1>
----
<1> Only needed if you want to access the worker API directly from your plugin.