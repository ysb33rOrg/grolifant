= Project Tools

The xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html[ProjectTools] object has some methods to obtain project information.

An instance of it can be obtained via `ConfigCacheSafeOptions.projectTools()` or `ProjectOperations.getProjectTools()`.
This instance is configuration cache compatible and can be called from within a task action.

== Console output

A console mode can be requested on the command-line via `--console`.
This value can safely be access in a task action using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getConsoleMode[`getConsoleMode`].
If no value was requested this returns `ConsoleMode.Auto`.

== Offline mode

A provider that indicated whether Gradle is running in offline mode is available as xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getOfflineProvider[`getOfflineProvider`]

== Lazy-evaluated project version

Project version is something in flux in Gradle world.
The `project.version` has evolved over time and will still evolve in future Gradle releases.
In its current state (at least until Gradle 8.x), it still is not a lazy property.
It also has the quirk that if it is assigned an object, that object is evaluated using `toString`.
Although there are plans to change it in the API, this is not available in Gradle 8 (at least).
Grolifant tries to provide a way of keeping up with any API changes.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getVersionProvider[`getVersionProvider`] - Obtains the project version as a provider.
 This allows tasks to push evaluation of version os late as possible.
 This might get tricky when configuration cache is involved as the project object might not be available.
 Grolifant leaves that decision in the hands of the build script author.
 However, it is possible for a build script author to apply a lazy-evaluated provider.
* xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#setVersionProvider[`setVersionProvider`] - Sets a lazy-evaluated version.
 This also sets `project.version` to use this provider.
 In this way most version-evaluating scenarios are safely resolved.

[source,groovy]
----
projectTools.versionProvider = project.provider { -> '1.2.3' }
----

== Project group

The project group can be returned via a `Provider` using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getGroupProvider[`getGroupProvider`]

== Project description

The project group can be returned via a `Provider` using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getProjectDescriptionProvider[`getProjectDescriptionProvider`].

In value is defined it returns a empty string, not an empty provider.

== Project name

The project name can be returned via a `Provider` using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getProjectNameProvider[`getProjectNameProvider`].

== Project full path

The project path can be returned via using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#getFullProjectPath[`getFullProjectPath`].
  This will include the name of the root project, rather than just `:`.

== Project structure

Some methods related to project structure is available that indicates multi-project mode etc.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#isMultiProject[`isMultiProject`] - Indicates whether this a a single project or a multi-project layout.
* xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#isRootProject[`isRootProject`] - Indicates whether the current (sub)project is the root project.

== Refreshing dependencies

Some tasks or extensions might need to act differently when `--refresh-dependencies` is specified.
It is possible to safely access this value from a task execution using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#isRefreshDependencies[`isRefreshDependencies`].

== Rerunning all tasks

Some tasks or extensions might behave differently if `--rerun-tasks` is specified.
It is possible to safely access this value from a task execution using xref:project-artifacts:attachment${groovydoc-core}/ProjectTools.html#isRerunTasks[`isRerunTasks`].
