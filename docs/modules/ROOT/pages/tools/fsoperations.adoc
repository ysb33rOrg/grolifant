= File Utilities

The xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html[FileSystemOperations] object has many methods to work with files and directories

An instance of it can be obtained via `ConfigCacheSafeOptions.fsOperations()` or `ProjectOperations.getFsOperations()`.
This instance is configuration cache compatible and can be called from within a task action.

.Recursive evaluation
Some conversion methods take an `Object` instance of a collection of `Object` as inputs.
They can recursively unpack  `Callable`, `Closure`, `Optional`, `Provider` and `Supplier` until it gets to something it can evaluate.

== Conversions

You are probably familiar with `project.file` to convert a single object to a file.
Grolifant offers similar, but more powerful methods.
There are also methods that take a collection of objects and convert them to a list of files.

.Supported types
* Any `CharSequence`
* `java.io.File` and `java.nio.file.Path`
* URIs of scheme `file:`.
* `org.gradle.api.file.Directory`
* `org.gradle.api.file.RegularFile`
* `org.gradle.api.resources.TextResource`
* `org.gradle.api.file.FileCollection`
* Any wrapper described aboce such as `Provider`

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#file[`file(Object)`] - Converts a single object to a file.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#fileOrNull[`fileOrNull(Object)`] - Similar to `file`, but allows for `null` to be passed.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#files[`files(Collection<Object>)>`] - Take a collection of objects and convert them to files.
Any item in the collection that is a `null` will result in an exception being emitted.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#filesDropNull[`filesDropNull(Collection<Object>)`] - Take a collection of objects and convert them to files.
Any `null` entries will be removed from the final result.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#provideFile[`provideFile(Object)`] - Create a `File` provider to a lazy-evaluated object.

== File collections

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#emptyFileCollection[`emptyFileCollection`] - Return a configurable empty file collection.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#fileTree[`fileTree`] - A `CondifugrableFileTree` instance staring at the provided starting point.
Anything convertible with `file(Object)` will suffice as input.

== CopySpec(s)

.Creating an empty `CopySpec`
Use xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#copySpec[`copySpec`] to create an empty `CopySpec`.

.Resolving a `CopySpec` to files
Sometimes one wants to use a `org.gradle.api.file.CopySpec` as part of a DSL, but resolve it to a file collection without performing a copy or synchronisation.
There is one method available to do so - xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#resolveFilesFromCopySpec[`resolveFilesFromCopySpec`].
It returns a `FileTree`.

== Resolving a TAR or ZIP to files.

.Methods
xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#tarTree[`tarTree`] - For Tar archives.
xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#zipTree[`zipTree`] - For Zip archives.

Both meotds can be passed an object that can be converted using the earlier mentioned `file` method.

== Readable compression resources

If you have a file that is BZIP2 or GZ comrpessed, you can easily create a `org.gradle.api.resources.ReadableResource` from it.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#bzip2Resource[bzip2Resource(Object)] - BZIP2 resources
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#gzipResource[gzipResource(Object)] - GZIP resources

Anything convertible to a file using the earlier mentioned conversion methods, can be passed as parameter.

== Creating safe filenames

Sometimes you might want to use entities such as task names in file names.
Use the xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#toSafeFilename[toSafeFilename] method to obtain a string that can be used as part of a filename.

In similar fashion you can concatenate a number of subpaths and ensure that the resulting path only consists of safe file names.
There are two methods for this.

* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#toSafePath[toSafePath(String...)] - Creates a `Path` instance.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#toSafeFile[toSafeFile(String...)] - Creates a `File` instance.

== Listing child directories of a directory

xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#listDirs[listDirs] provides a list of child directories of a directory.

== Resolving the location of a class

For some cases it is handy to resolve the location of a class, so that it can be added to a classpath.
One use case is the for `javaexec` processes and Gradle workers.
Use
xref:project-artifacts:attachment${groovydoc-core}/JvmTools.html#resolveClassLocation[resolveClassLocation] to obtain a `File` object to the class.
If the class is located in a JAR it path to the JAR will be returned.
If the class is directly on the filesystem, the toplevel directory for the package hierarchy that the class belongs to, will be returned.

== Copy, delete and sync operations

These ensure the old `projext.cop` and `project.sync` works across Gradle API releases.
They can be called from within task actions.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#copy[`copy`] - Executes a copy specification into a specific directory.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#delete[`delete`] - Deletes all the specified files and directories.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#sync[`sync`] - Execution a synchronisation into a specific directory using a specification.

== Working with resources

There are a number of methods to simplify loading content from resource files.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#copyResourceToFile[`copyResourceToFile`] - Copy a file at a resource path to a destination file.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#loadPropertiesFromResource[`loadPropertiesFromResource`] - Load the properties file at the given resource path.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#providePropertiesFromResource[`providePropertiesFromResource`] - Load the properties file at the given resource path only on-demand.

NOTE: There a versions of the above which also allow for specifying the class loader.

== Temporary directories

xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#createTempDirectory[`createTempDirectory`] will create a temporary directory within the build directory.

== Updating a File property

You can also update any `Property<File>` using the xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#updateFileProperty[`updateFileProperty`] utility method.

== File locations

=== Obtaining a subfolder of the build directory

Use the xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#buildDirDescendant[`buildDirDescendant`] method, which returns a provider to a `File`.

Anything that can be evaluated to a string using xref:tools/stringtools.adoc[`StringTools`] can be passed as a parameter.

[source,groovy]
----
fsOperations().buildDirDescendant('docs/asciidoc')
----

=== Project cache directory

If a plugin might need to cache information in the local cache directory it is important that it determines this folder correctly.

You can call xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#getProjectCacheDir[`getProjectCacheDir`] to achieve this.

You can also use it in provider form - xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#provideProjectCacheDir[`provideProjectCacheDir`] to achieve this.

If you want to get a directory below the cache directory then use xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#provideProjectCacheDirDescendant[`provideProjectCacheDirDescendant`].

=== Gradle distribution directory

xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#getGradleHomeDir[`getGradleHomeDir`] will provide the home of the currently active Gradle distribution.

=== Gradle user home directory

xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#getGradleUserHomeDir[`getGradleUserHomeDir`] will provide the currently active home directory for the user.

=== Project directory

Use xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#getProjectDir[`getProjectDir`] to obtain the directory of the current (sub)project.

You can also get it in `Provider` form - xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#provideProjectDir[`provideProjectDir`].

=== Project root directory

Use xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#getProjectRootDir[`getProjectRootDir`] to obtain the root directory of the current (sub)project.

You can also get it in `Provider` form - xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#provideRootDir[`provideRootDir`]

== Relative paths

Relative paths can be very useful in many cases.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativePath[`relativePath`] - The relative path FROM the project directory to the given path
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativePathNotEmpty[`relativePathNotEmpty`] - Similar to `relativePath`, but if the two entities are siblings, return `.` rather than an empty string.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativeRootPath[`relativeRootPath`] - The relative path FROM the root project directory to the given path.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativePathToProjectDir[`relativePathToProjectDir`] - The relative path from the given path to the project directory.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativePathToRootDir[`relativePathToRootDir`] - The relative path from the given path to the root project directory.
* xref:project-artifacts:attachment${groovydoc-core}/FileSystemOperations.html#relativize[`relativize`] - For two instances return the relative path of a target from a base i.e. the path to traverse starting at the base file.
One version of this method takes two `File` instances and another two `Path` instances.
