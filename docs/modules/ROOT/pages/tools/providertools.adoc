= Provider and Property Tools

In Gradle 6.1, the link:{gradle-javadoc}/provider/ProviderFactory.html#environmentVariable-java.lang.String-.html[ProviderFactory] introduced methods for obtaining Gradle properties, system properties and environment variables as providers.
Grolifant offers additional features above and beyond the ones by the Gradle API.
This is offered via the xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html[ProviderTools] object has some methods to obtain project information.

An instance of it can be obtained via `ConfigCacheSafeOptions.providerTools()` or `ProjectOperations.getProviderTools()`.
This instance is configuration cache compatible and can be called from within a task action.


== Property Providers

Using these methods, can safeguard your plugin aginst cusage on a Gradle version where configuration-cache is enabled.

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#environmentVariable[`environmentVariable(Object)`] - returns an environment variable as a provider.
  The name of the variable is lazy-evaluated and can be anything that `stringize` in xref:tools/stringtools.adoc[StringTools] will resolve.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#gradleProperty[`gradleProperty(Object)`] - returns a Gradle property as a provider.
The name of the variable is lazy-evaluated and can be anything that `stringize` in xref:tools/stringtools.adoc[StringTools] will resolve.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#systemProperty[`systemProperty(Object)`] - returns a Gradle property as a provider.
The name of the variable is lazy-evaluated and can be anything that `stringize` in xref:tools/stringtools.adoc[StringTools] will resolve.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#resolveProperty[`resolveProperty(Object)`] - Attempts to resolve a Gradle property by that name first.
  If it does not exist, then attempt a system property and finally en environment variable.
  The name of the variable is lazy-evaluated and can be anything that `stringize` in xref:tools/stringtools.adoc[StringTools] will resolve.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#resolveProperty[`resolveProperty(Object,Object)`] - Similar to the previous method, but if none of the three resolves, apply a default value.
 The latter value is lazily-converted to a string.

NOTE: For `resolveProperty`, if the provided named is `ab.cd.ef`, then the corresponding environment variable will be called `AB_CD_EF`.
 Although this approach is modelled upon Spring Boot & Micronaut property naming, it is not nearly as comprehensive in converting in naming formats.

== Other Provider methods

=== Create a Provider from a Callable

A convenience method to create a Gradle `Provider` from a Java `Callable`.
Use xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#provider[`provider`] if you already have an instance of `ProviderTools`, but not Gradle's `ProviderFactory.

=== Create a list or map property

.Methods
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#listProperty[`listProperty(Class)`] - convenience method to create a `ListProperty`.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#listProperty[`listProperty(Class,Iterator<?>)`] - create a `ListProperty` and initialise it from a data collection.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#mapProperty[`mapProperty(Class,Class)`] - convenience method to create a `MapProperty`.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#property[`property(Class)`] - convenience method to create a `Property`.
* xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#property[`property(Class,Object)`] - create a `Property` and set the default value.

=== Orderly resolve three providers

Creates a provider that can resolve three providers in order.
If the first is not present, it will attempt to resolve the second and then the third.

Use xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#resolveOrderly[`resolveOrderly`] to execute this common pattern.

=== Resolve a Provider to Optional

Need to convert Gradle's `Provider` to Java's `Optional`?
Then use xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#resolveToOptional[`resolveToOptional`]

== Miscellaneous

xref:project-artifacts:attachment${groovydoc-core}/ProviderTools.html#newInstance[`newInstance`] is a shortcut to the similarly named method on Gradle's `ObjectFactory`.