= Creating execution specifications

There are three base classes for execution specifications

include::partial$execspec-classes.adoc[]

== Creating a command specification

Use this when the external executable is command-driven i.e. `terraform apply` or `npm install`.

This can start as simple as

[source,groovy]
----
include::example$test-in-plugin-context/src/main/groovy/org/ysb33r/grolifant/validation/MyCmdExecSpec.groovy[tags=example-execspec]
----

You can also do something like

[source,groovy]
----
include::example$test-in-plugin-context/src/main/groovy/org/ysb33r/grolifant/validation/MyCmdExecSpec.groovy[tags=example-execspec-inject-ctor,indent=0]
----

The latter will allow you to do `project.objects.newInstance(MyCmdExecSpec)`.

