# ##########################################################################
#
#  ~~APP_BASE_NAME~~ wrapper script for Windows
#
# ##########################################################################
Set-StrictMode -Version Latest

function run_gradle()
{
    if (Test-Path "$env:GRADLE_WRAPPER_RELATIVE_PATH\gradlew.bat") {
        & $env:GRADLE_WRAPPER_RELATIVE_PATH\gradlew.bat $args
    } else {
        & gradle $args
    }
}

# Relative path from this script to the directory where the Gradle wrapper
# might be found.
$env:GRADLE_WRAPPER_RELATIVE_PATH='~~GRADLE_WRAPPER_RELATIVE_PATH~~'

#  Relative path from this script to the project cache dir (usually .gradle).
$env:DOT_GRADLE_RELATIVE_PATH='~~DOT_GRADLE_RELATIVE_PATH~~'

$env:DIRNAME=$PSScriptRoot
$env:APP_BASE_NAME=Split-Path -Leaf $PSCommandPath
$env:APP_HOME=$env:DIRNAME

#init
# Get command-line arguments, handling Windows variants

$env:CMD_LINE_ARGS=$MyInvocation.UnboundArguments

#execute
# Setup the command line

$env:APP_LOCATION_FILE="$env:DOT_GRADLE_RELATIVE_PATH/~~APP_LOCATION_FILE~~"

# If the app location is not available, set it first via Gradle
if (-Not (Test-Path "$env:APP_LOCATION_FILE")) { run_gradle -q '~~CACHE_TASK_NAME~~' }

# Read settings in from app location properties
# - APP_LOCATION
# - USE_GLOBAL_CONFIG
# - CONFIG_LOCATION
. $env:APP_LOCATION_FILE

# If the app is not available, download it first via Gradle
if (-Not (Test-Path "$env:APP_LOCATION")) { run_gradle -q '~~CACHE_TASK_NAME~~' }


# If global configuration is disabled which is the default, then
#  point the Terraform config to the generated configuration file
#  if it exists.
if ($env:TF_CLI_CONFIG_FILE -eq "") {
    if ($env:USE_GLOBAL_CONFIG -ne'true') {
        if (Test-Path "$env:CONFIG_LOCATION") {
            $env:TF_CLI_CONFIG_FILE=$env:CONFIG_LOCATION
        } else {
            Write-Error "Config location specified as $env:CONFIG_LOCATION, but file does not exist."
            Write-Error "Please run the terraformrc Gradle task before using $env:APP_BASE_NAME again"
        }
    }
}
#:cliconfigset

#  If we are in a project containing a default Terraform source set
#  then point the data directory to the default location.
if ("$env:TF_DATA_DIR" -eq "") {
    if (Test-Path "$pwd\src\tf\main") {
        $env:TF_DATA_DIR="$pwd\build\tf\main"
        Write-Error "$env:TF_DATA_DIR will be used as data directory"
    }
}

# Execute ~~APP_BASE_NAME~~
& $env:APP_LOCATION $env:CMD_LINE_ARGS

#:end
# End local scope for the variables with windows NT shell
if ($LastExitCode -ne 0)
{
    #:fail
    exit 1
}
else
{
    #:omega
    exit 0
}
