/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.remote.worker;

/**
 * Simplifies the setup for JVM workers, but always abstracts away from API differences between some Gradle versions.
 *
 * @param <P> Parameters that will be passed.
 *            This type only needs to be serializable, but does not need to be defined as an interface of
 *            {@code Property} instances.
 *            It can be a POJO, a POGOor a Java record.
 */
public interface WorkerAppExecutorFactory<P extends SerializableWorkerAppParameters> {
    /**
     * Creates an executor that can work with a set of parameters.
     *
     * @return Executor.
     */
    WorkerAppExecutor<P> createExecutor();
}
