/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.remote.worker;

import java.io.Serializable;
import java.util.List;

/**
 * An executor and result interpreter for a standard class.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface StandardWorkerAppExecutor extends Serializable {

    /**
     * Executes a static method on the given class, or instantiates the class and executes a given method on the
     * instance.
     *
     * @param entrypoint A class to be used
     * @param arguments Arguments that will be passed to the class
     * @return An exit code
     */
    Integer executeAndInterpret(Class<?> entrypoint, List<String> arguments);
}
