/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.remote.worker

import groovy.transform.CompileStatic

/**
 * Run a JVM main class in a worker.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class StandardWorkerAppExecutorFactory implements WorkerAppExecutorFactory<StandardWorkerAppParameters>, Serializable {

    StandardWorkerAppExecutorFactory(StandardWorkerAppExecutor executor) {
        this.executor = executor
    }

    /**
     * Creates an executor that can work with a set of parameters.
     *
     * @return Executor.
     */
    @Override
    WorkerAppExecutor<StandardWorkerAppParameters> createExecutor() {
        new Executor(this.executor)
    }

    private static class Executor implements WorkerAppExecutor<StandardWorkerAppParameters>, Serializable {

        Executor(StandardWorkerAppExecutor executeAndInterpret) {
            this.executor = executeAndInterpret
        }

        @Override
        @SuppressWarnings('CatchThrowable')
        void executeWith(StandardWorkerAppParameters parameters) {
            try {
                final mainClass = this.class.classLoader.loadClass(parameters.mainClass)
                Integer result = executor.executeAndInterpret(mainClass, parameters.arguments)
                if (result && parameters.ignoreExitValue) {
                    throw new WorkerExecutionException("Process exited with code ${result}")
                }
            } catch (WorkerExecutionException e) {
                throw e
            } catch (Throwable e) {
                println "Failed to successfully executed ${parameters.mainClass} " + e
                throw new WorkerExecutionException("Failed to successfully execute ${parameters.mainClass}", e)
            }
        }

        private final StandardWorkerAppExecutor executor
    }

    private final StandardWorkerAppExecutor executor
}
