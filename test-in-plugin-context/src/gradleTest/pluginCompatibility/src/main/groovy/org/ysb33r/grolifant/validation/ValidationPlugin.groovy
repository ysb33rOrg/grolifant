/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.attributes.Attribute
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin

@CompileStatic
@SuppressWarnings(['DuplicateStringLiteral'])
class ValidationPlugin implements Plugin<Project> {

    @SuppressWarnings('EmptyClass')
    static class SimpleAttribute implements Serializable {
    }

    @Override
    void apply(Project project) {
        project.pluginManager.apply(GrolifantServicePlugin)
        final po = ProjectOperations.find(project)
        project.tasks.register('mySimpleWrapperTask', MySimpleWrapperTask)

        project.extensions.create('myExtensionWrapper', MyToolExtension, po)
        project.tasks.register('myExtensionWrapperTask', MyExtensionWrapperTask)

        project.extensions.create('myExtensionWrapperWithDownloader', MyToolExtensionWithDownloader, po)
        project.tasks.register('myExtensionWrapperWithDownloaderTask', MyExtensionWrapperWithDownloaderTask)

        project.extensions.create('propertyResolver', PropertyResolverExtension, po)

        addRepositories(project)
        addGitArchives(project, po)
        addJRubyDependency(project)
        addJvmExecTask(po)
        addDependencyScopeConfigurations(po)
        addPackerWrapper(project)
        project.tasks.register('taskWithParameters', TaskWithParameters, ['aValue'])

        addSecureStringStuff(project)
    }

    void addSecureStringStuff(Project project) {
        project.extensions.create(MySecureStringExtension.NAME, MySecureStringExtension, project)
    }

    void addPackerWrapper(Project project) {
        // tag::packer-wrapper[]
        final cache = project.tasks.register('packerWrapperCache', MyPackerWrapperCacheTask)
        project.tasks.register('packerWrapper', MyPackerWrapper) {
            it.associateCacheTask(cache)
        }
        // end::packer-wrapper[]
    }

    void addGitArchives(Project project, ProjectOperations projectOperations) {
        final gitArchive = project.extensions.create(
            'gitArchive',
            MyGitArchiveExtension,
            projectOperations
        )
        gitArchive.destDir = projectOperations.buildDirDescendant('my-archive').get()

        projectOperations.tasks.register('gitArchiveDownloader', MyGitArchiveTask)
    }

    void addRepositories(Project project) {
        project.repositories.mavenCentral()
    }

    void addJRubyDependency(Project project) {
        final jruby = project.configurations.create('jruby')
        project.dependencies.add(jruby.name, 'org.jruby:jruby-complete:9.4.0.0')
    }

    void addJvmExecTask(ProjectOperations projectOperations) {
        projectOperations.tasks.register('jrubyRunner', MyJavaExecTask)
        projectOperations.tasks.register('jrubyScriptRunner', MyJavaExecScriptTask)
        projectOperations.tasks.register('jrubyScriptRunnerJavaExecOnly', MyJavaExecScriptJavaExecOnlyTask)
    }

    void addDependencyScopeConfigurations(ProjectOperations projectOperations) {
        projectOperations.configurations.createRoleFocusedConfigurations(
            'myScope',
            'myResolvable',
            'myConsumable'
        ) { ac ->
            ac.attribute(
                Attribute.of(SimpleAttribute),
                new SimpleAttribute()
            )
        }
    }
}
