/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader

@CompileStatic
class Helpers {
    public static final File SCRIPTS_DIR = new File(
        System.getProperty('COMPAT_TEST_RESOURCES_DIR'), 'scripts'
    ).absoluteFile

    public static final String TOOL_EXT = OperatingSystem.current().windows ? 'bat' : 'sh'
    public static final File SCRIPT_TO_PASS = new File(SCRIPTS_DIR, 'mycmd.' + TOOL_EXT)

    static class FakeExecutableDownloader implements ExecutableDownloader {
        @Override
        File getByVersion(String version) {
            SCRIPT_TO_PASS
        }
    }
}
