/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperCacheBinaryTask

// tag::wrapper-cache[]
@CompileStatic
class MyPackerWrapperCacheTask extends AbstractWrapperCacheBinaryTask {
    MyPackerWrapperCacheTask() {
        super('packer-wrapper.properties') // <.>
        final packerExtension = project.extensions.getByType(MyToolExtensionWithDownloader) // <.>

        this.binaryLocation = packerExtension.executable.map { it.absolutePath }
        this.binaryVersion = packerExtension.resolvedExecutableVersion()
    }
    // end::wrapper-cache[]
    // tag::wrapper-cache-config[]
    @Override
    protected Provider<String> getBinaryLocationProvider() {
        this.binaryLocation // <.>
    }

    @Override
    protected Provider<String> getBinaryVersionProvider() {
        this.binaryVersion // <.>
    }

    @Override
    protected String getPropertiesDescription() {
        "Describes the Packer usage for the ${projectTools().projectNameProvider.get()} project" // <.>
    }

    // end::wrapper-cache-config[]
    // tag::wrapper-cache[]
    private final Provider<String> binaryLocation
    private final Provider<String> binaryVersion
}
// end::wrapper-cache[]
