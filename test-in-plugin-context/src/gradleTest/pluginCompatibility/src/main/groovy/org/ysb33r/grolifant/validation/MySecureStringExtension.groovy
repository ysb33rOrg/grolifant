/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService

@CompileStatic
class MySecureStringExtension {
    public static final String NAME = 'secureStringExtension'

    MySecureStringExtension(Project project) {
        // tag::access-service-1[]
        this.theService = (Provider<GrolifantSecurePropertyService>) project.gradle.sharedServices.registrations
            .getByName(GrolifantServicePlugin.SECURE_STRING_SERVICE).service
        // end::access-service-1[]
    }

    // tag::access-service-2[]
    final Provider<GrolifantSecurePropertyService> theService
    // end::access-service-2[]
}
