/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.git.CloudGitConfigurator
import org.ysb33r.grolifant5.api.core.git.GitHubArchive
import org.ysb33r.grolifant5.api.core.git.GitLabArchive
import org.ysb33r.grolifant5.api.core.git.GitRepoArchiveDownloader

@CompileStatic
class MyGitArchiveExtension {

    final GitLabArchive gitlab
    final GitHubArchive github

    MyGitArchiveExtension(ProjectOperations po) {
        this.gtc = ConfigCacheSafeOperations.from(po)
        gitlab = new GitLabArchive(po)
        github = new GitHubArchive(po)
    }

    void setDestDir(File target) {
        this.destDir = target
    }

    File downloadFor(CloudGitConfigurator git) {
        final downloader = new GitRepoArchiveDownloader(git, gtc)
        downloader.downloadRoot = destDir
        downloader.archiveRoot
    }

    private File destDir
    private final ConfigCacheSafeOperations gtc
}
