/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecWrapperWithExtensionTask

// tag::example-task[]
@CompileStatic
class MyExtensionWrapperTask extends AbstractExecWrapperWithExtensionTask<MyToolExtension, MyCmdExecSpec> { // <.>

    MyExtensionWrapperTask() {
        super()
        this.execSpec = new MyCmdExecSpec(this) // <.>
        this.executableLocation = toolExtension.executable // <.>
        // end::example-task[]
        this.execSpec.cmd { command = 'foo' }
        // tag::example-task[]
    }

    @Override
    protected MyCmdExecSpec getExecSpec() { // <.>
        this.execSpec
    }

    @Override
    protected MyToolExtension getToolExtension() { // <.>
        project.extensions.getByType(MyToolExtension)
    }

    @Override
    protected Provider<File> getExecutableLocation() { // <.>
       this.executableLocation
    }

    private final Provider<File> executableLocation
    private final MyCmdExecSpec execSpec
}
// end::example-task[]
