/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant5.api.errors.ConfigurationException

@SuppressWarnings(['CatchRuntimeException', 'ClosureAsLastMethodParameter'])
// tag::example-extension-with-version[]
// tag::example-extension[]
@CompileStatic
class MyToolExtension extends AbstractToolExtension<MyToolExtension> { // <1>
    static final String NAME = 'toolConfig'

    MyToolExtension(ProjectOperations po) { // <2>
        super(po)
    }

    MyToolExtension(Task task, ProjectOperations po, MyToolExtension projectExt) {
        super(task, po, projectExt) // <3>
    }

    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException { // <4>
        try {
            projectOperations.execTools.parseVersionFromOutput( // <5>
                ['--version'], // <6>
                executablePathOrNull(), // <7>
                { String output -> '1.0.0' } // <8>
            )
        } catch (RuntimeException e) {
            throw new ConfigurationException('Cannot determine version', e)
        }
    }

    @Override
    protected ExecutableDownloader getDownloader() { // <9>
        // end::example-extension[]
        new Helpers.FakeExecutableDownloader()
        // tag::example-extension[]
    }
}
// end::example-extension-with-version[]
// end::example-extension[]