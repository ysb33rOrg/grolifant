/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.AbstractDistributionInstaller

import static org.ysb33r.grolifant.validation.MyTestInstaller.TESTDIST_DIR

@CompileStatic
class MyTestInstallerForVariousFormats extends AbstractDistributionInstaller {

    static final String DISTPATH = MyTestInstaller.DISTPATH
    static final String DISTVER = MyTestInstaller.DISTVER
    final String ext

    MyTestInstallerForVariousFormats(final String extension, ProjectOperations po) {
        super('Test Distribution', DISTPATH, ConfigCacheSafeOperations.from(po))
        ext = extension
    }

    @Override
    URI uriFromVersion(String version) {
        TESTDIST_DIR.toURI().resolve("testdist-${DISTVER}.${ext}")
    }
}
