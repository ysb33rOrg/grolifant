/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.git.CloudGitConfigurator

@CompileStatic
class MyGitArchiveTask extends DefaultTask {
    MyGitArchiveTask() {
        gitArchive = project.extensions.getByType(MyGitArchiveExtension)
    }

    @Internal
    CloudGitConfigurator git

    @TaskAction
    void exec() {
        gitArchive.downloadFor(git)
    }
    private final MyGitArchiveExtension gitArchive
}
