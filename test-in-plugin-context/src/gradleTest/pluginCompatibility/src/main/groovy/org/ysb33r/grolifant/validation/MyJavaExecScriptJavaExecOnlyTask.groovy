/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.workers.WorkerExecutor
import org.ysb33r.grolifant5.api.core.jvm.ExecutionMode
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmScriptExecTask

import javax.inject.Inject

@CompileStatic
@SuppressWarnings(['DuplicateStringLiteral'])
class MyJavaExecScriptJavaExecOnlyTask extends AbstractJvmScriptExecTask<MyJavaExecScriptSpec> {
    @Inject
    MyJavaExecScriptJavaExecOnlyTask(WorkerExecutor we) {
        super(we)
        execSpec = new MyJavaExecScriptSpec(this)
        preventExecutionMode(ExecutionMode.CLASSPATH, 'JRuby uses System.exit which breaks workers')
        preventExecutionMode(ExecutionMode.OUT_OF_PROCESS, 'JRuby uses System.exit which breaks workers')
    }
}
