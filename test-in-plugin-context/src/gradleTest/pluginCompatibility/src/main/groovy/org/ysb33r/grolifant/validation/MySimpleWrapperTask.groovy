/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecWrapperTask

@SuppressWarnings('PublicMethodsBeforeNonPublicMethods')
// tag::example-simple-wrapper[]
@CompileStatic
class MySimpleWrapperTask extends AbstractExecWrapperTask<MyCmdExecSpec> { // <1>

    MySimpleWrapperTask() {
        super()
        this.execSpec = new MyCmdExecSpec(this)
        // end::example-simple-wrapper[]
        execSpec.cmd {
            command = 'status'
            args('--color=yellow')
        }
        execSpec.runnerSpec {
            args('--profile=ysb33r')
        }
        // tag::example-simple-wrapper[]
    }

    @Override
    protected MyCmdExecSpec getExecSpec() { // <2>
        this.execSpec
    }

    @Override
    protected Provider<File> getExecutableLocation() { // <3>
        // end::example-simple-wrapper[]
        providerTools().provider { -> Helpers.SCRIPT_TO_PASS }
        // tag::example-simple-wrapper[]
    }

    private final MyCmdExecSpec execSpec

    // end::example-simple-wrapper[]
    @Internal
    MyCmdExecSpec getTestExecSpec() {
        execSpec
    }
    // tag::example-simple-wrapper[]
}
// end::example-simple-wrapper[]
