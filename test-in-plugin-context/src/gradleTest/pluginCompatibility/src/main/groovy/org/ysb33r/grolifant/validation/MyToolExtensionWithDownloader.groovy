/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader

import static org.ysb33r.grolifant.validation.Helpers.TOOL_EXT

@CompileStatic
class MyToolExtensionWithDownloader extends MyToolExtension {
    MyToolExtensionWithDownloader(ProjectOperations po) {
        super(po)
        // tag::downloader0[]
        installer = new MyTestInstaller(po)
        installer.addExecPattern('**/*.sh')
        // end::downloader0[]
    }

    // tag::downloader1[]
    @Override
    protected ExecutableDownloader getDownloader() {
        new ExecutableDownloader() {
            @Override
            File getByVersion(String version) {
                installer.getDistributionRoot(version).get()
                // end::downloader1[]
                new File(installer.getDistributionRoot(version).get(), "test.${TOOL_EXT}")
                // tag::downloader1[]
            }
        }
    }
    // end::downloader1[]

    // tag::downloader2[]
    private final MyTestInstaller installer
    // end::downloader2[]
}
