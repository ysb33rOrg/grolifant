/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.AbstractDistributionInstaller

// tag::test_installer[]
@CompileStatic
class MyTestInstaller extends AbstractDistributionInstaller {

    public static final String DISTPATH = 'foo/bar'
    // end::test_installer[]
    public static final String DISTVER = '0.2'
    public static final File TESTDIST_DIR_ROOT = new File(System.getProperty('COMPAT_TEST_RESOURCES_DIR') ?:
        'src/gradleTest/pluginCompatibility/src/test/resources')
    public static final File TESTDIST_DIR = new File(TESTDIST_DIR_ROOT, 'distributions')

    // tag::test_installer[]
    MyTestInstaller(ProjectOperations projectOperations) {
        super('Test Distribution', DISTPATH, ConfigCacheSafeOperations.from(projectOperations)) // <1>
        // end::test_installer[]
        // tag::test_installer_exec_pattern[]
        addExecPattern('**/*.sh') // <1>
        // end::test_installer_exec_pattern[]
        // tag::test_installer[]
    }

    @Override
    URI uriFromVersion(String version) { // <2>
        "https://distribution.example/download/testdist-${DISTVER}.zip".toURI() // <3>
        // end::test_installer[]
        TESTDIST_DIR.toURI().resolve("testdist-${DISTVER}.zip")
        // tag::test_installer[]
    }
}
// end::test_installer[]
