/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperGeneratorTask

@SuppressWarnings(['LineLength', 'DuplicateStringLiteral', 'UnnecessaryCast'])
// tag::wrapper[]
@CompileStatic
class MyPackerWrapper extends AbstractWrapperGeneratorTask {
    // end::wrapper[]

    // tag::injected-ctor[]
    // tag::tokens-in-ctor[]
    MyPackerWrapper() {
        super()
        useWrapperTemplatesInResources( // <.>
            '/packer-wrappers', // <.>
            ['wrapper-template.sh' : 'packerw', // <.>
             'wrapper-template.bat': 'packerw.bat'
            ]
        )

        this.locationPropertiesFile = project.objects.property(File) // <.>
        this.cacheTaskName = project.objects.property(String) // <.>
        // end::injected-ctor[]

        final root = fsOperations().projectRootDir
        this.tokenValues = locationPropertiesFile.zip(cacheTaskName) { lpf, cacheTaskName -> // <.>
            [
                APP_BASE_NAME               : 'packer',
                GRADLE_WRAPPER_RELATIVE_PATH: fsOperations().relativePathNotEmpty(root), // <.>
                DOT_GRADLE_RELATIVE_PATH    : fsOperations().relativePath(lpf.parentFile), // <.>
                APP_LOCATION_CONFIG         : lpf.name, // <.>
                CACHE_TASK_NAME             : cacheTaskName // <.>
            ] as Map<String, String>
        }
    // tag::injected-ctor[]
    }
    // end::injected-ctor[]
    // end::tokens-in-ctor[]

    // tag::associate-cache-task[]
    void associateCacheTask(TaskProvider<MyPackerWrapperCacheTask> ct) {
        inputs.files(ct) // <.>
        this.locationPropertiesFile.set(ct.flatMap { it.locationPropertiesFile }) // <.>
        this.cacheTaskName.set(ct.map { it.name }) // <.>
    }
    // end::associate-cache-task[]

    @Override
    protected String getBeginToken() { // <.>
        '~~'
    }

    @Override
    protected String getEndToken() { // <.>
        '~~'
    }

    // tag::example-tokens-2[]
    @Override
    protected Map<String, String> getTokenValuesAsMap() {
        // end::example-tokens-2[]
        // tag::example-tokens-1[]
        [
            APP_BASE_NAME               : 'packer', // <.>
            APP_LOCATION_CONFIG         : '/path/to/packer', // <.>
            CACHE_TASK_NAME             : 'myCacheTask', // <.>
            GRADLE_WRAPPER_RELATIVE_PATH: '.', // <.>
            DOT_GRADLE_RELATIVE_PATH    : './.gradle' // <.>
        ]
        // end::example-tokens-1[]
        // tag::example-tokens-2[]
        tokenValues.get()
        // tag::wrapper[]
    }
    // end::example-tokens-2[]
    // tag::injected-ctor[]

    private final Property<File> locationPropertiesFile
    private final Property<String> cacheTaskName
    // end::injected-ctor[]
    // tag::tokens-in-ctor[]
    private final Provider<Map<String, String>> tokenValues
    // end::tokens-in-ctor[]
    // tag::wrapper[]
}
// end::wrapper[]
