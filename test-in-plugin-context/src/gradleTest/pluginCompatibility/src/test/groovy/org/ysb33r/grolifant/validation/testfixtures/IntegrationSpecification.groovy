/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation.testfixtures

import org.gradle.testkit.runner.GradleRunner
import org.gradle.util.GradleVersion
import org.ysb33r.grolifant.validation.Helpers
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.Transform
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.PosixFilePermission

class IntegrationSpecification extends Specification {
    public static final Boolean WITH_DEBUG = System.getProperty('WITHOUT_DEBUG') == null
    public static final String OVERRIDE_GRADLE_VERSION = System.getProperty('OVERRIDE_GRADLE_VERSION')
    public static final String OVERRIDE_PLUGIN_CLASSPATH = System.getProperty('OVERRIDE_PLUGIN_CLASSPATH')
    public static final String CURRENT_GRADLE_VERSION = OVERRIDE_GRADLE_VERSION ?
        GradleVersion.version(OVERRIDE_GRADLE_VERSION) :
        GradleVersion.current()

    public static final List<GradleVersion> CONFIG_CACHE_VERSIONS = Transform.toList([
        '7.5.1',
        '8.9'
    ]) { GradleVersion.version(it) }
    public static final boolean CONFIG_CACHE_ACTIVE_IN_TESTS = CURRENT_GRADLE_VERSION in CONFIG_CACHE_VERSIONS
    public static final List<String> CONFIG_CACHE_ARGS = CONFIG_CACHE_ACTIVE_IN_TESTS ?
        ['--configuration-cache', '--configuration-cache-problems=fail'] : []

    File workdir
    File buildFile
    File settingsFile

    GradleRunner getGradleRunner(String task, String... args) {
        final runner = GradleRunner.create()
            .withProjectDir(workdir)
            .withArguments([task, '-i', '-s'] + (args as List) + CONFIG_CACHE_ARGS)
            .withDebug(WITH_DEBUG)
            .withPluginClasspath()
            .forwardOutput()
        if (OVERRIDE_GRADLE_VERSION) {
            runner.withGradleVersion(OVERRIDE_GRADLE_VERSION)
        }

        if (OVERRIDE_PLUGIN_CLASSPATH) {
            runner.withPluginClasspath(
                new File(OVERRIDE_PLUGIN_CLASSPATH)
                    .readLines()
                    .findAll { it }
                    .collect { new File(it) }
            )
        } else {
            runner.withPluginClasspath()
        }
        runner
    }

    void setupSpec() {
        if (!OperatingSystem.current().windows) {
            Path scriptPath = new File(Helpers.SCRIPTS_DIR, 'mycmd.sh').toPath()
            Set perms = Files.getPosixFilePermissions(scriptPath)
            perms.add(PosixFilePermission.OWNER_EXECUTE)
            Files.setPosixFilePermissions(scriptPath, perms)
        }
    }

    void cleanup() {
        if (workdir) {
            workdir.deleteDir()
        }
    }

    void setup() {
        workdir = Files.createTempDirectory('grolifant-plugin-compat').toFile()
        workdir.deleteOnExit()
        buildFile = new File(workdir, 'build.gradle')
        settingsFile = new File(workdir, 'settings.gradle')

        settingsFile.text = ''
        createBuildFile()
    }

    void createBuildFile() {
        buildFile.text = '''
        plugins {
            id 'org.ysb33r.grolifant.validation'
        }
        '''.stripIndent()
    }
}