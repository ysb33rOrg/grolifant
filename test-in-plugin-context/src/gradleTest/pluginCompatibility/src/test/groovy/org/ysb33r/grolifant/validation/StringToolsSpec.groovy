/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.UnitTestSpecification
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.errors.UnexpectedNullException
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException
import spock.lang.TempDir

import java.util.concurrent.Callable
import java.util.function.Supplier
import java.util.regex.Pattern

class StringToolsSpec extends UnitTestSpecification {

    @TempDir
    File tempDir

    StringTools stringTools

    void setup() {
        stringTools = ccso.stringTools()
    }

    void 'Can convert patterns'() {
        setup:
        final expected = Pattern.compile('.+').pattern()

        expect:
        stringTools.patternize(input).pattern() == expected
        stringTools.patternize(project.provider { -> input }).pattern() == expected
        stringTools.patternize(Optional.of(project.provider { -> input })).pattern() == expected

        where:
        input << [
            '.+',
            Pattern.compile('.+'),
            { -> '.+' },
        ]
    }

    void 'Will not patternize arbitrary object'() {
        when:
        stringTools.patternize(new File('.'))

        then:
        thrown(UnsupportedConfigurationException)
    }

    void 'Can patternize a collection'() {
        setup:
        final inputs = [
            [
                '.+',
                Pattern.compile('.+'),
                { -> '.+' },
            ],
            [
                project.provider { -> '.*' },
                { -> '.*?' },
                { -> '.+?' } as Callable<Pattern>,
                { -> '[aA]?' } as Supplier<Pattern>,
                Optional.of(
                    project.provider { ->
                        { ->
                            { -> '[bB]?' } as Supplier<Pattern>
                        } as Callable<Pattern>
                    }
                )
            ]
        ]

        when:
        final result = stringTools.patternize(inputs)
        final patterns = result*.pattern()

        then:
        result.size() == 8
        patterns.findAll { it == '.+' }.size() == 3
        patterns.containsAll(['.*', '.*?', '.+?', '[aA]?', '[bB]?'])
    }

    void 'patternize() will complain about null'() {
        when:
        stringTools.patternize(null)

        then:
        thrown(UnexpectedNullException)

        when:
        stringTools.patternize(['.+', null])

        then:
        thrown(UnexpectedNullException)
    }

    void 'patternizeOrNull() will except a null'() {
        expect:
        stringTools.patternizeOrNull(null) == null
    }

    void 'patternizeDropNull() will drop nulls and empty wrappers'() {
        setup:
        final inputs = [
            '.+',
            null,
            project.provider { -> null },
            Optional.ofNullable(null),
            { -> null },
            { -> null } as Callable<String>,
            { -> null } as Supplier<Pattern>
        ]

        expect:
        stringTools.patternizeDropNull(inputs).size() == 1
    }

    void 'Can create providers to patterns'() {
        expect:
        stringTools.providePattern('.+').get().pattern() == '.+'
        stringTools.providePatternOrNull('.+').get().pattern() == '.+'
    }

    void 'providePattern() does not tolerate a null'() {
        setup:
        final provider = stringTools.providePattern(null)

        when:
        provider.present

        then:
        thrown(UnexpectedNullException)

        when:
        provider.get()

        then:
        thrown(UnexpectedNullException)
    }

    void 'providePatternOrNull can create an empty provider'() {
        setup:
        final provider = stringTools.providePatternOrNull(null)

        expect:
        !provider.present
    }

    void 'Convert strings and byte arrays to byte[]'() {
        setup:
        final string = '1234'
        final bytes = string.bytes
        final Byte[] boxedBytes = [20, 30, 40]

        expect:
        stringTools.makeByteArray(string) == bytes
        stringTools.makeByteArray(bytes) == bytes
        stringTools.makeByteArray(boxedBytes)[1] == 30
    }

    void 'Will only accept nulls for makeByteArrayOrNull'() {
        when:
        stringTools.makeByteArray(null)

        then:
        thrown(UnexpectedNullException)

        when:
        final result = stringTools.makeByteArrayOrNull(null)

        then:
        result == null
    }

    void 'Can convert files and paths to byte[]'() {
        setup:
        final file = new File(tempDir, 'test.txt')
        final path = file.toPath()
        final content = '123'
        file.bytes = content.bytes

        expect:
        stringTools.makeByteArray(file) == content.bytes
        stringTools.makeByteArray(path) == content.bytes
    }

    void 'Can create a provider to byte[]'() {
        setup:
        final content = '123'

        when:
        final p1 = stringTools.provideByteArray(content)

        then:
        p1.get() == content.bytes

        when:
        final p2 = stringTools.provideByteArrayOrNull(project.provider { -> null })

        then:
        !p2.present
    }

    void 'Will convert char[] to string'() {
        setup:
        final value = '123'.toCharArray()

        expect:
        stringTools.stringize(value) == '123'
    }
}