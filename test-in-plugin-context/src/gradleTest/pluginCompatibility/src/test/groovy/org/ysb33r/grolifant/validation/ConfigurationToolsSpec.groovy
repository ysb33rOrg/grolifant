/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class ConfigurationToolsSpec extends IntegrationSpecification {
    public static final String TEST_TASK = 'configurationEvaluator'

    void 'Can create dependency scope configurations'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def configValues = ['myScope','myResolvable','myConsumable'].collectEntries { cv ->
                def pc = project.configurations.getByName(cv)
                [ cv, [ canBeResolved: pc.canBeResolved, canBeConsumed: pc.canBeConsumed ] ]
            }
            doLast {
                assert !configValues.myScope.canBeResolved
                assert !configValues.myScope.canBeConsumed
                assert configValues.myResolvable.canBeResolved
                assert !configValues.myResolvable.canBeConsumed
                assert !configValues.myConsumable['canBeResolved']
                assert configValues.myConsumable['canBeConsumed']
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK).build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }
}