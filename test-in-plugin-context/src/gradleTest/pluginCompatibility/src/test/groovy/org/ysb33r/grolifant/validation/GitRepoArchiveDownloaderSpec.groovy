/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class GitRepoArchiveDownloaderSpec extends IntegrationSpecification {
    public static final URI BASE_URI = new File(
        System.getProperty('COMPAT_TEST_RESOURCES_DIR'),
        'git-archives/github'
    ).toURI()
    public static final String TASK_NAME = 'gitArchiveDownloader'

    @Unroll
    @SuppressWarnings('UnnecessaryBooleanExpression')
    void 'Download an archive from #name'() {
        setup:
        buildFile << """
        gitArchive {
            ${name}.baseUri = '${BASE_URI}'.toURI()
            ${name}.organisation = 'ysb33rOrg'
            ${name}.repository = 'grolifant'
            ${name}.branch = 'master' 
        }
        ${TASK_NAME} {
            git = gitArchive.${name}
        }
        """.stripIndent()

        final baseDir = new File(workdir, "build/my-archive/${subdir}")

        when:
        final result = getGradleRunner(TASK_NAME).build()

        then:
        result.task(":${TASK_NAME}").outcome == SUCCESS
        new File(baseDir.listFiles()[0], 'grolifant-master').exists()

        where:
        name     | subdir
        'github' | 'github-cache/ysb33rorg/grolifant/master'
        'gitlab' | 'gitlab-cache/ysb33rorg/grolifant/grolifant-master'
    }
}