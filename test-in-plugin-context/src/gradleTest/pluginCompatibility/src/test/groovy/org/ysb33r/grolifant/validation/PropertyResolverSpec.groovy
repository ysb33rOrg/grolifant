/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification
import spock.lang.IgnoreIf

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_5_2

class PropertyResolverSpec extends IntegrationSpecification {

    public static final String TEST_TASK = 'evaluatePropertyResolver'

    void 'Can read a system property'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = propertyResolver.get('foo')
            doLast {
                assert pr == 'bar'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK, '-Dfoo=bar').build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    @IgnoreIf({ PRE_5_2 })
    void 'Can read an environment variable'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = propertyResolver.get('foo1')
            doLast {
                assert pr == 'BAR'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK,)
            .withEnvironment(System.getenv() + [FOO1: 'BAR'])
            .withDebug(false)
            .build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    void 'Can return null if nothing is defined'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = propertyResolver.get('foo.bar.king.kong')
            doLast {
                assert pr == null
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK, '-Dfoo=bar').build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    void 'Can return default value if nothing is defined'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = propertyResolver.get('foo.bar.king.kong','123')
            doLast {
                assert pr == '123'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK, '-Dfoo=bar').build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    void 'An empty string is not a null value'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = propertyResolver.get('foo')
            doLast {
                assert pr == ''
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK, '-Dfoo=').build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }
}
