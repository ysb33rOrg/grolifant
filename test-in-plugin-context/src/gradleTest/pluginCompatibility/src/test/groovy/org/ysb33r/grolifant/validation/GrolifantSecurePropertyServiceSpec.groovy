/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.UnitTestSpecification

class GrolifantSecurePropertyServiceSpec extends UnitTestSpecification {

    void setup() {
        project.pluginManager.apply(ValidationPlugin)
    }

    void 'Can create and unpack a secure property via a service'() {
        final ext = project.extensions.getByType(MySecureStringExtension)

        when:
        final secStr = ext.theService.get().pack('123')

        then:
        ext.theService.get().unpack(secStr) == '123'.toCharArray()
    }
}