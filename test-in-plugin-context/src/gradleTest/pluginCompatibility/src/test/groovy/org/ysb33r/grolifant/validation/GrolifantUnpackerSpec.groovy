/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker
import spock.lang.Specification
import spock.lang.Unroll

class GrolifantUnpackerSpec extends Specification {

    void 'Can activate the Grolifant Service loader'() {
        when:
        ServiceLoader<GrolifantUnpacker> loader = ServiceLoader.load(GrolifantUnpacker)

        then:
        loader != null
    }

    @Unroll
    void 'Can load the #ext unpacker'() {
        when:
        GrolifantUnpacker unpacker = ServiceLoader.load(GrolifantUnpacker).find { GrolifantUnpacker it ->
            it.supportExtension(ext)
        }

        then:
        unpacker != null

        when:
        final project = ProjectBuilder.builder().build()
        project.pluginManager.apply(GrolifantServicePlugin)
        unpacker.create(ConfigCacheSafeOperations.from(project))

        then:
        noExceptionThrown()

        where:
        ext << [ 'xz']
    }
}