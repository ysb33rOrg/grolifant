/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant5.api.core.LegacyLevel
import org.ysb33r.grolifant5.api.core.jvm.ExecutionMode
import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification
import spock.lang.Timeout
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.ysb33r.grolifant5.api.core.jvm.ExecutionMode.CLASSPATH
import static org.ysb33r.grolifant5.api.core.jvm.ExecutionMode.JAVA_EXEC
import static org.ysb33r.grolifant5.api.core.jvm.ExecutionMode.OUT_OF_PROCESS

class AbstractJvmExecTaskSpec extends IntegrationSpecification {

    public static final String TEST_TASK = 'jrubyRunner'
    public static final String TEST_SCRIPT_TASK = 'jrubyScriptRunner'
    public static final List<ExecutionMode> TEST_THESE_EXECUTION_MODES = ExecutionMode.values()

    @Unroll
    @Timeout(180)
    void 'Can run a JVM task using #execMode'() {
        setup:
        buildFile << """
        ${TEST_TASK} {
            executionMode = '${execMode}'
            entrypoint {
                mainClass = 'org.jruby.Main'
                classpath = configurations.jruby
            }
            runnerSpec {
                args '-v'
            }
        }   
        """.stripIndent()

        when:
        final result = getGradleRunner(TEST_TASK).build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS

        where:
        execMode << TEST_THESE_EXECUTION_MODES
    }

    @Unroll
    @Timeout(180)
    void 'Can run a JVM script task using #execMode'() {
        setup:
        boolean doesNotProvideOutput = execMode == CLASSPATH || (!LegacyLevel.PRE_4_5 && LegacyLevel.PRE_4_7)
        buildFile << """
        ${TEST_SCRIPT_TASK} {
            executionMode = '${execMode}'
            entrypoint {
                mainClass = 'org.jruby.Main'
                classpath = configurations.jruby
            }
            runnerSpec {
                args '-S'
            }
            script {
                name = 'rdoc'
                args '-h'
            }
            process {
                output {
                    forward()
                }
                errorOutput {
                    captureAndForward()
                }
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_SCRIPT_TASK).build()

        then:
        result.task(":${TEST_SCRIPT_TASK}").outcome == SUCCESS
        doesNotProvideOutput || result.output.contains('RDoc understands the following file formats')

        where:
        execMode << TEST_THESE_EXECUTION_MODES
    }

    void 'Tasks can also prevent certain execution modes to be used'() {
        setup:
        final taskName = 'jrubyScriptRunnerJavaExecOnly'
        buildFile << """
        ${taskName} {
            executionMode = '${execMode.name()}'
            entrypoint {
                mainClass = 'org.jruby.Main'
                classpath = configurations.jruby
            }
            runnerSpec {
                args '-S'
            }
            script {
                name = 'rdoc'
                args '-h'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(taskName).buildAndFail()

        then:
        result.output.contains('JRuby uses System.exit which breaks workers')

        where:
        execMode << [OUT_OF_PROCESS, CLASSPATH]
    }

    void 'Tasks which prevent certain execution modes, will allow other to be used'() {
        setup:
        buildFile << """
        ${TEST_SCRIPT_TASK} {
            executionMode = '${execMode}'
            entrypoint {
                mainClass = 'org.jruby.Main'
                classpath = configurations.jruby
            }
            runnerSpec {
                args '-S'
            }
            script {
                name = 'rdoc'
                args '-h'
            }
        }
        """.stripIndent()

        when:
        final result = getGradleRunner(TEST_SCRIPT_TASK).build()

        then:
        result.task(":${TEST_SCRIPT_TASK}").outcome == SUCCESS
        result.output.contains('RDoc understands the following file formats')

        where:
        execMode << [JAVA_EXEC]
    }

}