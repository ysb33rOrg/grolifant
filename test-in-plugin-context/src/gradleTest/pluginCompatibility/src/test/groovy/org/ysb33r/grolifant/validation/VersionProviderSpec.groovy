/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class VersionProviderSpec extends IntegrationSpecification {

    public static final String TEST_TASK = 'evaluatePVersionProvider'

    void 'Will work if no version is defined'() {
        setup:
        buildFile << """
        task ${TEST_TASK} {
            def pr = grolifantOps.projectTools.versionProvider
            doLast {
                assert pr.get()
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK).build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    void 'Can set a version in the build script'() {
        setup:
        buildFile << """
        project.version = '1.2.3'
        task ${TEST_TASK} {
            def pr = grolifantOps.projectTools.versionProvider
            doLast {
                assert pr.get() == '1.2.3'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK).build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }

    void 'Can set a custom version in the build script'() {
        setup:
        buildFile << """
        class MyTest {
          @Override
          String toString() {
              '4.5.6'
          }
        }

        project.version = new MyTest()
        task ${TEST_TASK} {
            def pr = grolifantOps.projectTools.versionProvider
            doLast {
                assert pr.get() == '4.5.6'
            }
        }
        """.stripIndent()

        when:
        def result = getGradleRunner(TEST_TASK).build()

        then:
        result.task(":${TEST_TASK}").outcome == SUCCESS
    }
}
