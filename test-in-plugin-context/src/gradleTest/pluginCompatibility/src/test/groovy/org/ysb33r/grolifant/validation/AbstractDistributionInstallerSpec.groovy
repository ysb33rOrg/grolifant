/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

@SuppressWarnings(['LineLength'])
class AbstractDistributionInstallerSpec extends IntegrationSpecification {

    void "Download a distribution from a URL"() {
        setup:
        buildFile << '''
        myExtensionWrapperWithDownloader {
            executableByVersion('0.2')
        }
        '''.stripIndent()

        when:
        def result = getGradleRunner('myExtensionWrapperWithDownloaderTask').build()

        then:
        result.task(':myExtensionWrapperWithDownloaderTask').outcome == SUCCESS
    }

//    @Unroll
//    def "Download a #ext from a URL"() {
//        given: 'A basic distribution'
//        final installer = new MyTestInstallerForVariousFormats(ext, projectOperations)
//
//        when: 'The #ext based distribution is downloaded'
//        File downloaded = installer.getDistributionRoot(MyTestInstallerForVariousFormats.DISTVER).get()
//
//        then: 'The distribution should be unpacked'
//        downloaded.exists()
//        downloaded.absolutePath.contains(distPathString(MyTestInstallerForVariousFormats.DISTPATH))
//        downloaded.absolutePath.endsWith("testdist-${MyTestInstallerForVariousFormats.DISTVER}")
//
//        where:
//        ext << ['tar', 'tar.gz', 'tgz', 'tar.bz2', 'tbz', 'tar.xz']
//    }
//
//    def 'Checksums should be checked if supplied'() {
//        given: 'A basic distribution'
//        MyTestInstaller installer = new MyTestInstaller(projectOperations)
//
//        when: 'A checksum is set'
//        // tag::installer_checksum[]
//        installer.checksum = 'b1741e3d2a3f7047d041c79d018cf55286d1168fd6f0533e7fae897478abcdef'  // <1>
//        // end::installer_checksum[]
//
//        and: 'the distribution is downloaded'
//        File downloaded = installer.getDistributionRoot(MyTestInstaller.DISTVER)
//
//        then: 'it should fail by throwing an exception, because the checksum did not match'
//        thrown(ChecksumFailedException)
//
//        when: 'A correct checksum is set'
//        installer = new MyTestInstaller(projectOperations)
//        installer.checksum = new File(TESTDIST_DIR, 'testdist-0.1.zip.sha256').text.trim()
//
//        and: 'The distribution is downloaded'
//        downloaded = installer.getDistributionRoot(MyTestInstaller.DISTVER).get()
//
//        then: 'The distribution should be unpacked'
//        downloaded.exists()
//        downloaded.absolutePath.contains(distPathString(MyTestInstaller.DISTPATH))
//        downloaded.absolutePath.endsWith("testdist-${MyTestInstaller.DISTVER}")
//
//        when: 'An invalid checksum is provided (not correct length)'
//        installer = new MyTestInstaller(projectOperations)
//        installer.checksum = 'abcde'
//
//        then: 'An exception will be raised'
//        thrown(IllegalArgumentException)
//
//        when: 'An invalid checksum is provided (bad characters)'
//        installer.checksum = '_' * 64
//
//        then: 'An exception will be raised'
//        thrown(IllegalArgumentException)
//    }
//
//    @Issue('https://gitlab.com/ysb33rOrg/grolifant/issues/41')
//    @SuppressWarnings('CatchException')
//    void 'Safely access the same resources from multiple installers in the same VM'() {
//        given:
//        int numberOfInstallers = 50
//        CyclicBarrier barrier = new CyclicBarrier(numberOfInstallers)
//        ConcurrentLinkedDeque<File> results = new ConcurrentLinkedDeque<File>()
//        ConcurrentLinkedDeque<Exception> failures = new ConcurrentLinkedDeque<Exception>()
//
//        when:
//        def jobs = (1..numberOfInstallers).collect {
//            Thread task = new Thread(new Runnable() {
//                @Override
//                void run() {
//                    MyTestInstaller installer = new MyTestInstaller(projectOperations)
//                    barrier.await()
//                    try {
//                        results.add(installer.distributionRoot)
//                    } catch (Exception e) {
//                        failures.add(e)
//                    }
//                }
//            })
//            task.start()
//            task
//        }
//
//        jobs.each { it.join() }
//        Set<String> paths = results*.canonicalPath.toSet()
//
//        then:
//        verifyAll {
//            results.size() == numberOfInstallers
//            paths.size() == 1
//            failures.empty
//        }
//    }
//
//    @Issue('https://gitlab.com/ysb33rOrg/grolifant/issues/17')
//    @PendingFeature
//    @IgnoreIf({ !MyTestInstallerForMSI.IS_WINDOWS })
//    void 'Unpacking an MSI'() {
//        given: 'A MSI distribution'
//        MyTestInstallerForMSI installer = new MyTestInstallerForMSI(projectOperations)
//
//        when: 'The MSI based distribution is downloaded'
//        File downloaded = installer.getDistributionRoot(MyTestInstallerForVariousFormats.DISTVER).get()
//
//        then: 'The MSI distribution should be unpacked on Windows (or exception or other OS)'
//        downloaded.exists()
//        downloaded.absolutePath.contains(distPathString(MyTestInstallerForVariousFormats.DISTPATH))
//        downloaded.absolutePath.endsWith("testdist-${MyTestInstallerForVariousFormats.DISTVER}")
//    }
//
//
//    String distPathString(final String s) {
//        AbstractDistributionInstaller.IS_WINDOWS ? s.replaceAll(~/\//, '\\\\') : s
//    }
}