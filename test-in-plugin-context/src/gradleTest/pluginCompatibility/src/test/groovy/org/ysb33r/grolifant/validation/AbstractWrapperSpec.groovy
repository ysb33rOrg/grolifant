/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class AbstractWrapperSpec extends IntegrationSpecification {

    void 'Create wrapper and cache the binary'() {
        setup:
        buildFile << '''
        myExtensionWrapperWithDownloader {
            executableByVersion('1.0.0')
        }
        '''.stripIndent()

        when:
        def result = getGradleRunner('packerWrapper',).build()

        then:
        result.task(':packerWrapper').outcome == SUCCESS
        result.task(':packerWrapperCache').outcome == SUCCESS
        new File(workdir, 'packerw').exists()
        new File(workdir, 'packerw.bat').exists()
    }
}