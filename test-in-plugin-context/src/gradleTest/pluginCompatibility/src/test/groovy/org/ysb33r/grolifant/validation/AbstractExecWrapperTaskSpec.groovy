/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class AbstractExecWrapperTaskSpec extends IntegrationSpecification {

    void 'Run a command-line tool as a task'() {
        setup:
        buildFile << '''
        mySimpleWrapperTask {
            environment USE_COLORS: 'YES'
        }
        '''.stripIndent()
        when:
        def result = getGradleRunner('mySimpleWrapperTask').build()

        then:
        result.task(':mySimpleWrapperTask').outcome == SUCCESS
        result.output.contains('--profile=ysb33r')
//        result.output.contains('USE_COLORS=YES')
    }

    void 'Run a command-line tool using an extension wrapper'() {
        setup:
        buildFile << '''
        myExtensionWrapper {
            executableByVersion('1.0.0')
        }
        '''.stripIndent()
        when:
        def result = getGradleRunner('myExtensionWrapperTask').build()

        then:
        result.task(':myExtensionWrapperTask').outcome == SUCCESS
        result.output.contains('foo')
    }
}