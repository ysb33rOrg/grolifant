/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.validation

import org.ysb33r.grolifant.validation.testfixtures.UnitTestSpecification
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import spock.lang.TempDir

class FileSystemOperationsSpec extends UnitTestSpecification {

    @TempDir
    File tempDir

    FileSystemOperations fsOperations

    void setup() {
        fsOperations = ccso.fsOperations()
    }

    void 'Can create a copySpec'() {
        when:
        final cs = fsOperations.copySpec()

        then:
        cs != null

        when:
        final cs1 = fsOperations.copySpec {
            cs.into 'foo'
        }

        then:
        cs1 != null
    }

    void 'Can convert FileCollection using files'() {
        setup:
        final fc = fsOperations.emptyFileCollection().from(tempDir)

        when:
        final files = fsOperations.files([fc])

        then:
        files.files.contains(tempDir)
    }

    void 'Can convert FileTree using files'() {
        setup:
        final foo = new File(tempDir, 'foo')
        foo.text = ''
        final fc = fsOperations.fileTree(tempDir)

        when:
        final files = fsOperations.files([fc])

        then:
        files.files.contains(foo)
    }
}