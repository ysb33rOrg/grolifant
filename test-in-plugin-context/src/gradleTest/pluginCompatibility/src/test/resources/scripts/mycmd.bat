@REM
@REM ============================================================================
@REM (C) Copyright Schalk W. Cronje 2016 - 2025
@REM
@REM This software is licensed under the Apache License 2.0
@REM See http://www.apache.org/licenses/LICENSE-2.0 for license details
@REM
@REM Unless required by applicable law or agreed to in writing, software
@REM distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
@REM WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
@REM License for the specific language governing permissions and limitations
@REM under the License.
@REM ============================================================================
@REM

@echo %1
