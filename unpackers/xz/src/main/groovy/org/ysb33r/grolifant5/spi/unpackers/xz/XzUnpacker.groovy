/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.spi.unpackers.xz

import groovy.transform.CompileStatic
import org.tukaani.xz.XZInputStream
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker

@CompileStatic
class XzUnpacker implements GrolifantUnpacker {
    @Override
    boolean supportExtension(String extension) {
        extension.toLowerCase(Locale.US) == 'xz'
    }

    /**
     * Create a new unpack engine.
     *
     * @param ops Configuration cache-safe operations
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops) {
       new XzEngine()
    }

    /**
     * Creates a new unpack engine along with additional parameters.
     *
     * @param ops Configuration cache-safe operations
     * @param parameters Configuration parameters
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops, Parameters parameters) {
        new XzEngine()
    }

    static class XzEngine implements GrolifantUnpacker.Engine {
        @Override
        void unpack(File srcArchive, File destDir) {
            destDir.withOutputStream { OutputStream xz ->
                srcArchive.withInputStream { tarXZ ->
                    new XZInputStream(tarXZ).withStream { strm ->
                        xz << strm
                    }
                }
            }
        }
    }
}
