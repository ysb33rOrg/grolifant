/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.spi.unpackers.dmg

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.errors.DistributionFailedException
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker

/**
 * Provides the capability of unpacking an MSI file under Mac OSX by calling out to {@code hdiutil}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class DmgUnpacker implements GrolifantUnpacker {
    @Override
    boolean supportExtension(String extension) {
        if (extension.toLowerCase(Locale.US) == 'dmg') {
            if (OperatingSystem.current().macOsX) {
                true
            } else {
                throw new DistributionFailedException('DMG unpacking is only supported under Mac OSX')
            }
        } else {
            false
        }
    }

    /**
     * Create a new unpack engine.
     *
     * @param ops Configuration cache-safe operations
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops) {
        new DmgEngine(ops, ops.providerTools().provider { -> EMPTY })
    }

    /**
     * Creates a new unpack engine along with additional parameters.
     *
     * @param ops Configuration cache-safe operations
     * @param parameters Configuration parameters
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops, Parameters parameters) {
        new DmgEngine(ops, parameters.baseRelativePath.orElse(EMPTY) ?: ops.providerTools().provider { -> EMPTY })
    }

    static class DmgEngine implements GrolifantUnpacker.Engine {

        DmgEngine(ConfigCacheSafeOperations gtc, Provider<String> relPath) {
            this.gtc = gtc
            this.relPath = relPath
        }

        /**
         * Unpacks a source archive or compressed file to a specific directory
         * @param srcArchive Source archive or compressed file.
         * @param destDir Destination directory.
         *               If the directory does not exist, the unpacking process will create it.
         */
        @Override
        void unpack(File srcArchive, File destDir) {
            final File mountRoot = gtc.fsOperations().createTempDirectory('grolifant-dmg-unpacker')
            final File mountedPath = new File(mountRoot, srcArchive.name)
            mountedPath.mkdirs()
            mountDMG(srcArchive, mountedPath)
            try {
                copyDMGFiles(mountedPath, destDir)
            } finally {
                unmountDMG(mountedPath)
                mountedPath.deleteDir()
            }
        }

        private void mountDMG(final File srcArchive, final File mountedPath) {
            gtc.execTools().exec { ExecSpec spec ->
                spec.tap {
                    executable HDIUTIL
                    args 'attach', srcArchive.absolutePath, '-nobrowse', '-readonly'
                    args '-mountpoint', mountedPath.absolutePath
                }
            }
        }

        private void copyDMGFiles(
            final File mountedPath,
            final File destDir
        ) {
            final String subPath = relPath.get()
            gtc.fsOperations().copy { spec ->
                spec.tap {
                    from "${mountedPath}/${subPath}", {
                        include '**'
                    }
                    into "${destDir}/${subPath}"
                }
            }
        }

        private void unmountDMG(final File mountedPath) {
            gtc.execTools().exec { spec ->
                spec.tap {
                    executable HDIUTIL
                    args 'detach', mountedPath.absolutePath
                    ignoreExitValue = true
                }
            }
        }
        private final ConfigCacheSafeOperations gtc
        private final Provider<String> relPath
        private final static String HDIUTIL = 'hdiutil'
    }

    private static final String EMPTY = ''
}
