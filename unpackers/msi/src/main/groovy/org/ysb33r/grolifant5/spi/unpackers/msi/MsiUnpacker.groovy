/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.spi.unpackers.msi

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.errors.DistributionFailedException
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker

/**
 * Provides the capability of unpacking an MSI file under Mac OSX by calling out to {@code hdiutil}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class MsiUnpacker implements GrolifantUnpacker {
    @Override
    boolean supportExtension(String extension) {
        if (extension.toLowerCase(Locale.US) == 'msi') {
            if (OperatingSystem.current().windows) {
                true
            } else {
                throw new DistributionFailedException('MSI unpacking is only supported under Windows')
            }
        } else {
            false
        }
    }

    /**
     * Create a new unpack engine.
     *
     * @param ops Configuration cache-safe operations
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops) {
        new MsiEngine(ops, ops.providerTools().provider { -> Collections.EMPTY_MAP })
    }

    /**
     * Creates a new unpack engine along with additional parameters.
     *
     * @param ops Configuration cache-safe operations
     * @param parameters Configuration parameters
     * @return An unpacking engine
     */
    @Override
    Engine create(ConfigCacheSafeOperations ops, Parameters parameters) {
        new MsiEngine(ops, parameters.environment)
    }

    static class MsiEngine implements GrolifantUnpacker.Engine {

        MsiEngine(ConfigCacheSafeOperations gtc, Provider<Map<String, String>> env) {
            this.msiUnpacker = new LessMSIUnpackerTool(gtc)
            this.env = env
        }

        /**
         * Unpacks a source archive or compressed file to a specific directory
         * @param srcArchive Source archive or compressed file.
         * @param destDir Destination directory.
         *               If the directory does not exist, the unpacking process will create it.
         */
        @Override
        void unpack(File srcArchive, File destDir) {
            msiUnpacker.unpackMSI(srcArchive, destDir, env.get())
        }

        private final Provider<Map<String, String>> env
        private final LessMSIUnpackerTool msiUnpacker
    }
}
