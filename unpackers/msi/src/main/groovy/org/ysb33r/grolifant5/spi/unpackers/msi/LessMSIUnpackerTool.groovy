/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.spi.unpackers.msi

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.downloader.AbstractDistributionInstaller

/** Downloads an MSI to use at a later stage.
 *
 * @since 5.0
 */
@CompileStatic
class LessMSIUnpackerTool extends AbstractDistributionInstaller {

    public static final String LESS_MSI_EXE = 'lessmsi.exe'
    public static final String LESSMSI_DOWNLOAD_URI = 'https://github.com/activescott/lessmsi/releases/download'
    public static final String LESSMSI_BASE_PATH = 'native-binaries/lessmsi'
    public static final String LESSMSI = 'lessmsi'

    /** Creates setup for installing to a local cache.
     *
     * @param projectOperations Gradle project that this downloader is attached to.
     */
    LessMSIUnpackerTool(ConfigCacheSafeOperations gtc) {
        super(
            LESSMSI,
            LESSMSI_BASE_PATH,
            gtc
        )
        final props = gtc.fsOperations().loadPropertiesFromResource('META-INF/grolifant/msi.properties')
        this.msiVersion = gtc.providerTools().resolveProperty(props['lessmsi.property'], props[LESSMSI])
    }

    /** Creates a download URI from a given distribution version
     *
     * @param version Version of the distribution to download
     * @return
     */
    @Override
    @SuppressWarnings('LineLength')
    URI uriFromVersion(String version) {
        "${System.getProperty('org.ysb33r.grolifant.lessmsi.uri') ?: LESSMSI_DOWNLOAD_URI}/v${version}/lessmsi-v${version}.zip".toURI()
    }

    /** Returns the path to the {@code lessmsi} exe.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code lessmsi} or null if not a supported operating system.
     */
    File getLessMSIExecutablePath() {
        File root = getDistFromCache(msiVersion.get())
        root ? new File(root, LESS_MSI_EXE) : null
    }

    /** Unpacks an MSI given the {@code lessmsi} exe downloaded by this incantation.
     *
     * @param srcArchive Location of MSI
     * @param destDir Directory to unpack to
     * @param env Environment to use when unpacking. If null or empty will add {@code TEMP}, {@code TMP}
     *   from Gradle environment.
     */
    void unpackMSI(File srcArchive, File destDir, final Map<String, String> env) {
        if (env) {
            doUnpackMSI(srcArchive, destDir, env)
        } else {
            doUnpackMSI(srcArchive, destDir, [
                TMP : System.getenv('TMP'),
                TEMP: System.getenv('TEMP')
            ])
        }
    }

    private void doUnpackMSI(File srcArchive, File destDir, Map<String, String> env) {
        configCacheSafeOperations.execTools().exec { spec ->
            spec.identity {
                executable lessMSIExecutablePath
                args 'x', srcArchive.absolutePath, destDir.absolutePath
                environment env
            }
        }
    }

    private final Provider<String> msiVersion
}
