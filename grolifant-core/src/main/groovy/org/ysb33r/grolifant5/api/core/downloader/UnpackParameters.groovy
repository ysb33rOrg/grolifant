/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader

import groovy.transform.CompileStatic
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker

/**
 * An implementation of {@link GrolifantUnpacker.Parameters}
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class UnpackParameters implements GrolifantUnpacker.Parameters {

    UnpackParameters(ConfigCacheSafeOperations gtc) {
        this.environment = gtc.providerTools().mapProperty(String, Object)
        this.envResolver = this.environment.map { gtc.stringTools().stringizeValues(it) }
        this.relPath = gtc.providerTools().property(String)
        this.stringTools = gtc.stringTools()
    }

    /**
     * Add to the environment variables.
     *
     * @param envVars Additional variables.
     */
    void environment(Map<String, ?> envVars) {
        this.environment.putAll(envVars)
    }

    /**
     * Customised environment to run under.
     * <p>
     *     Typically used when running an external binary.
     * </p>
     * @return Environment.
     */
    @Override
    Provider<Map<String, String>> getEnvironment() {
        this.envResolver
    }

    /**
     * Sets the relative path.
     *
     * @param path Anything convertible to a string using {@link org.ysb33r.grolifant5.api.core.StringTools#stringize}
     */
    void setBaseRelativePath(Object path) {
        stringTools.updateStringProperty(this.relPath, path)
    }

    /**
     * Relative path below Gradle User Home to create cache for all versions of the distribution type.
     *
     * @return
     */
    @Override
    Provider<String> getBaseRelativePath() {
        this.relPath
    }

    private final Provider<Map<String, String>> envResolver
    private final MapProperty<String, Object> environment
    private final Property<String> relPath
    private final StringTools stringTools
}
