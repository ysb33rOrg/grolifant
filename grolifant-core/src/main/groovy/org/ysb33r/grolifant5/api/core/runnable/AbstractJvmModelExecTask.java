/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Nested;
import org.gradle.api.tasks.TaskAction;
import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;
import org.ysb33r.grolifant5.api.core.jvm.ExecutionMode;
import org.ysb33r.grolifant5.api.core.jvm.JavaForkOptionsWithEnvProvider;
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise;
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException;
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Configures a task that will run an execution in a forked JVM.
 *
 * @author Schalk W. Cronj̧é
 * @since 2.0
 */
abstract public class AbstractJvmModelExecTask<T extends AbstractJvmExecSpec, P extends SerializableWorkerAppParameters>
        extends GrolifantDefaultTask
        implements ForkedJvmExecutable<T>, JvmExecutionModel {

    /**
     * Sets the execution mode.
     * <p>
     * This allows for JVM tasks to be executed either on a worker queue inside the JVM using an isolated classpath,
     * outside the JVM in a separate process, OR using a classic {@code javaexec}.
     *
     * <p>
     * If nothing is set, the default is {@link ExecutionMode#JAVA_EXEC}.
     *
     * @param em Execution mode.
     * @throw UnsupportedConfigurationException is mode is illegal within context.
     */
    @Override
    public void setExecutionMode(ExecutionMode em) {
        if (this.illegalExecutionModes.containsKey(em)) {
            throw new UnsupportedConfigurationException(em.name() + " cannot be used because: " + illegalExecutionModes.get(em));
        }
        this.execMode = em;
    }

    /**
     * Set whether other tasks in this project should be blocked until this task's worker queue completed.
     * <p>
     * Ignored if {@code executionMode == JAVA_EXEC}.
     *
     * @param mode {@code true} is blocking is required.
     */
    @Override
    public void setAwaitMode(Boolean mode) {
        this.awaitMode = mode;
    }

    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator An action to configure the JVM's fork options.
     * @return The configured executable
     */
    @Override
    public T jvm(Action<JavaForkOptionsWithEnvProvider> configurator) {
        return (T) getJvmExecSpec().jvm(configurator);
    }

    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator A closure to configure the JVM's fork options.
     * @return The configured executable
     */
    @Override
    public T jvm(@DelegatesTo(JavaForkOptionsWithEnvProvider.class) Closure<?> configurator) {
        return (T) getJvmExecSpec().jvm(configurator);
    }

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     * @return The configured executable
     */
    @Override
    public T runnerSpec(Action<CmdlineArgumentSpec> configurator) {
        return ((T) (getJvmExecSpec().runnerSpec(configurator)));
    }

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     * @return The configured executable
     */
    @Override
    public T runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator) {
        return ((T) (getJvmExecSpec().runnerSpec(configurator)));
    }

    /**
     * Configures the entrypoint for the JVM executable.
     * <p>
     * This includes the classpath, modularity and main class.
     * </p>
     *
     * @param configurator An action to configure the entrypoint
     * @return The configured executable.
     */
    @Override
    public T entrypoint(Action<JvmEntryPoint> configurator) {
        return ((T) (getJvmExecSpec().entrypoint(configurator)));
    }

    /**
     * Configures the entrypoint for the JVM executable
     *
     * @param configurator A closure to configure the entrypoint
     * @return The configured executable.
     */
    @Override
    public T entrypoint(@DelegatesTo(JvmEntryPoint.class) Closure<?> configurator) {
        return ((T) (getJvmExecSpec().entrypoint(configurator)));
    }

    /**
     * Configures the stream redirection and exit code checks.
     * <p>
     * This includes the inputs and output streams as well as the exit value.
     * </p>
     *
     * @param configurator An action to configure the execution.
     * @return The configured executable.
     */
    @Override
    public T process(Action<ProcessExecutionSpec> configurator) {
        return ((T) (getJvmExecSpec().process(configurator)));
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     * @return The configured executable.
     */
    @Override
    public T process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator) {
        return ((T) (getJvmExecSpec().process(configurator)));
    }

    /**
     * Base class for executing tasks on the JVM.
     *
     * @param we Can be null, in which case workers will not be supported
     */
    protected AbstractJvmModelExecTask(@Nullable WorkerExecutor we) {
        this.execMode = ExecutionMode.JAVA_EXEC;
        this.awaitMode = false;
        this.workerExecutor = we;
        this.illegalExecutionModes = new TreeMap<>();
    }

    /**
     * Create a worker app executor factory.
     *
     * @return Instance of an execution factory.
     */
    protected abstract WorkerAppExecutorFactory<P> createExecutorFactory();

    /**
     * Create a worker app parameter factory.
     *
     * @return Instance of a parameter factory.
     */
    protected abstract WorkerAppParameterFactory<P> createParameterFactory();


    @Nested
    protected T getJvmExecSpec() {
        return this.execSpec;
    }

    protected void setExecSpec(T execSpec) {
        this.execSpec = execSpec;
    }

    /**
     * Prevents a specific execution mode from being set.
     *
     * @param mode   Illegal execution mode
     * @param reason Reason why execution mode cannot be used.
     */
    protected void preventExecutionMode(ExecutionMode mode, String reason) {
        this.illegalExecutionModes.put(mode, reason);
    }

    @TaskAction
    protected void exec() {
        // tag::execution-in-multi-mode[]
        if (execMode.equals(ExecutionMode.JAVA_EXEC) || this.workerExecutor == null) {
            ExecOutput result = getJvmExecSpec().submitAsJavaExec();
            processOutput(result);
            result.assertNormalExitValue();
        } else {
            WorkerPromise promise = getJvmExecSpec().submitToWorkQueue(
                    execMode.workerIsolation(),
                    workerExecutor,
                    createExecutorFactory(),
                    createParameterFactory()
            );
            if (awaitMode.equals(true)) {
                promise.await();
            }
        }
        // end::execution-in-multi-mode[]
    }

    /**
     * Process the output from an {@link ExecOutput}.
     *
     * @param output Output to process.
     *
     * @since 5.0
     */
    protected void processOutput(ExecOutput output) {
        execSpec.processOutput(output);
    }

    private T execSpec;
    private ExecutionMode execMode;

    private Boolean awaitMode;

    private final WorkerExecutor workerExecutor;

    private final Map<ExecutionMode, String> illegalExecutionModes;
}
