/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Nested;
import org.ysb33r.grolifant5.api.core.ClosureUtils;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.executable.ScriptDefinition;
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec;

/**
 * Defines the basics for executing a script from a JVM-based scripting language.
 *
 * @since 2.0
 */
abstract public class AbstractJvmScriptExecSpec<T extends AbstractJvmScriptExecSpec<T>>
        extends AbstractJvmExecSpec<T>
        implements ForkedJvmScript<T> {

    /**
     * Configures the script specification.
     *
     * @param configurator Action to configure the script.
     * @return This object.
     */
    @Override
    public T script(Action<ScriptSpec> configurator) {
        configurator.execute(getScriptDefinition());
        return (T) this;
    }

    /**
     * Configures script elements by Groovy closure.
     *
     * @param specConfigurator Configurating closure.
     * @return {@code this}
     */
    @Override
    public T script(@DelegatesTo(ScriptSpec.class) Closure<?> specConfigurator) {
        ClosureUtils.configureItem(getScriptDefinition(), specConfigurator);
        return (T) this;
    }

    @Deprecated
    protected AbstractJvmScriptExecSpec(ProjectOperations po) {
        super(po);
        this.jvmScript = new ScriptDefinition(po);
        getJvmAppRunnerSpec().addCommandLineProcessor(SCRIPT_BLOCK, 1, jvmScript);
    }

    /**
     * Constructs the core of a script specification for a JVM language.
     *
     * @param po Implementation of {@link ConfigCacheSafeOperations}.
     *
     * @since 5.0
     */
    protected AbstractJvmScriptExecSpec(ConfigCacheSafeOperations po) {
        super(po);
        this.jvmScript = new ScriptDefinition(po);
        getJvmAppRunnerSpec().addCommandLineProcessor(SCRIPT_BLOCK, 1, jvmScript);
    }

    /**
     * Use a custom scirpt definition.
     *
     * @param po {@link ProjectOperations} to bind to this exec spec.
     * @param customScriptDefinition Custom script definition.
     *
     * @since 2.2
     *
     * @deprecated
     */
    @Deprecated
    protected AbstractJvmScriptExecSpec(
            ProjectOperations po,
            ScriptDefinition customScriptDefinition
    ) {
        super(po);
        this.jvmScript = customScriptDefinition;
        getJvmAppRunnerSpec().addCommandLineProcessor(SCRIPT_BLOCK, 1, jvmScript);
    }

    /**
     * Use a custom scirpt definition.
     *
     * @param po {@link ProjectOperations} to bind to this exec spec.
     * @param customScriptDefinition Custom script definition.
     *
     * @since 5.0
     */
    protected AbstractJvmScriptExecSpec(
            ConfigCacheSafeOperations po,
            ScriptDefinition customScriptDefinition
    ) {
        super(po);
        this.jvmScript = customScriptDefinition;
        getJvmAppRunnerSpec().addCommandLineProcessor(SCRIPT_BLOCK, 1, jvmScript);
    }
    /**
     * Returns the instance of the script definition.
     *
     * @return Script definition object. Never {@code null}.
     */
    @Nested
    protected ScriptSpec getScriptDefinition() {
        return this.jvmScript;
    }

    private final ScriptSpec jvmScript;
    private static final String SCRIPT_BLOCK = "script";
}
