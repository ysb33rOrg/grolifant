/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Nested;
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec;


/**
 * Running a script on a JVM-based scripting language in a separate JVM.
 *
 * @author Schalk W. Cronjé
 *
 * @param <T> is a class that implements the {@link ForkedJvmScript} interface,
 * @param <S> is class that implements the {@Link JvmScript} interface.
 *
 * @since 2.0
 */
//public interface ForkedJvmScript<T extends ForkedJvmScript,S extends AbstractJvmScriptSpec> extends ForkedJvmExecutable<T> {
public interface ForkedJvmScript<T extends ForkedJvmScript> extends ForkedJvmExecutable<T> {

    /**
     * Configures the script specification.
     *
     * @param configurator Action to configure the script.
     *
     * @return This object.
     */
    @Nested
    T script(Action<ScriptSpec> configurator);

    /**
     * Configures script elements by Groovy closure.
     * @param specConfigurator Configurating closure.
     * @return {@code this}
     */
     T script(@DelegatesTo(ScriptSpec.class) Closure<?> specConfigurator);
}
