/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.internal.core.runnable.SimpleConfigCacheSafeExecMethods;

/**
 * Execution methods that can be applied to an extension, copied from an extension and be safely applied to
 * task action when configuration caching is active.
 *
 * @param <E> Something is considered an executable specificaiton within Grolifant context.
 *
 * @since 5.0
 *
 * @author Schalk W. Cronjé
 */
public interface ConfigCacheSafeExecMethods<E extends AbstractExecSpec<E>>  {

    static ConfigCacheSafeExecMethods from(ConfigCacheSafeExecMethods other) {
        return new SimpleConfigCacheSafeExecMethods(other);
    }

    /**
     * Grolifant's configuration cache-safe operations.
     *
     * @return instance.
     *
     * @since 5.0
     */
    ConfigCacheSafeOperations getConfigCacheSafeOperations();

    /**
     * Executes an existing execution specification.
     *
     * @param spec Specification to execute.
     * @return Execution result.
     */
    ExecOutput exec(E spec);
}
