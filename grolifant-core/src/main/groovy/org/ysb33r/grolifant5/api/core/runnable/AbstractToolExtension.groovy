/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader

import java.util.concurrent.Callable

/**
 * Use as a base class for project and task extensions that will wrap tools.
 *
 * <p> This base class will also enable extensions to discover whether they are inside a task or a
 * project.
 *
 * @param <T >       The extension class that extends this base class.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractToolExtension<T extends AbstractToolExtension> extends AbstractBaseToolExtension<T>
    implements ToolLocation {

    /**
     * Locate an executable by version, probably downloading it if not local.
     *
     * @param version Something resolvable to a string.
     */
    void executableByVersion(Object version) {
        this.version = version
        unsetPaths()
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /** Attach this extension to a project
     *
     * @param projectOperations ProjectOperations instance.
     */
    protected AbstractToolExtension(ProjectOperations projectOperations) {
        super(projectOperations)
        this.executableVersionProvider = initExecutableVersionProvider(projectOperations)
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to.
     * @param projectOperations ProjectOperations instance.
     * @param projectExtName Extension that is attached to the project.
     *   Pass {@Link #noProjectExtension} is project extension is available or required.
     */
    protected AbstractToolExtension(Task task, ProjectOperations projectOperations, T projectExtension) {
        super(task, projectOperations, projectExtension)
        this.executableVersionProvider = initExecutableVersionProvider(projectOperations)
    }

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     */
    abstract protected ExecutableDownloader getDownloader()

    /**
     * Resolves the version if it has been set.
     *
     * @return Version string or {@code null}.
     */
    protected String executableVersionOrNull() {
        if (task && noGlobalExecSearch || !task) {
            this.version ? projectOperations.stringTools.stringize(this.version) : null
        } else if (task && projectExtension) {
            ((AbstractToolExtension) projectExtension).executableVersionOrNull()
        } else {
            failNoExecConfig()
        }
    }

    @Override
    protected File resolveExecutable() {
        if (version) {
            downloader.getByVersion(projectOperations.stringTools.stringize(version))
        } else {
            super.resolveExecutable()
        }
    }

    /**
     * This is used when path or search path is set to unset other search mechanisms.
     *
     * This is primarily intended to be used within Grolifant itself as part of its base classes and MUST be overridden
     * in those.
     *
     * The default operation is NOOP.
     */
    @Override
    protected void unsetNonPaths() {
        this.version = null
    }

    private Provider<String> initExecutableVersionProvider(ProjectOperations po) {
        po.provider(new Callable<String>() {
            @Override
            String call() throws Exception {
                executableVersionOrNull() ?: runExecutableAndReturnVersion()
            }
        })
    }

    protected Object version
}
