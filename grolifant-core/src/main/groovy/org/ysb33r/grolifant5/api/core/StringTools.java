/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

/**
 * Tools for working with strings.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface StringTools {

    /**
     * A single forward slash.
     *
     * @since 5.2
     */
    static final String SLASH = "/";

    /**
     * A single backslash.
     *
     * @since 5.2
     */
    static final String BACKSLASH = "\\";

    /**
     * A double backslash.
     *
     * @since 5.2
     */
    static final String DOUBLE_BACKSLASH = "\\\\";

    /**
     * A single spacem
     *
     * @since 5.2
     */
    static final String SPACE = " ";

    /**
     * An empty string.
     *
     * @since 5.2
     */
    static final String EMPTY = "";

    /**
     * A comma.
     *
     * @since 5.2
     */
    static final String COMMA = ",";

    /**
     * A comma and a space.
     *
     * @since 5.2
     */
    static final String COMMA_SPACE = ", ";

    /**
     * Converts a string in one format to another.
     *
     * @param inputFormat Input case format.
     * @param outputFormat Output case format.
     * @param stringy Object to convert
     * @return Transformed string.
     *
     * @since 5.2
     */
    default String caseConversion(CaseFormats inputFormat, CaseFormats outputFormat, final Object stringy) {
        return inputFormat.convertTo(outputFormat, stringize(stringy));
    }

    /**
     * Converts a source to a byte array.
     *
     * <p>
     *     Can convert the following:
     *     <ul>
     *         <li>{@code byte[]} and {@code Byte[]}</li>
     *         <li>{@code CharSequence} including {@code String} and {@code GString}.</li>
     *         <li>Contents of {@link java.io.File}</li>
     *         <li>Contents of {@link java.nio.file.Path} if it is on the default file system</li>
     *     </ul>
     * </p>
     *
     * <p>
     * Unwraps the following recursively until it gets to something of the above:
     *
     * <ul>
     *   <li> {@link Pattern}.</li>
     *   <li> Groovy Closures.</li>
     *   <li>{@link Callable}</li>
     *   <li>{@link Optional}</li>
     *   <li>{@link Provider}</li>
     *   <li>{@link java.util.function.Supplier}</li>
     * </ul>
     * </p>
     *
     * <p>
     *     Be careful when passing large files for conversion as all of the content will be read into memory.
     * </p>
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *            Empty wrappers and {@code null} are not accepted.
     * @return A native {@code byte[]}.
     *
     * @since 5.0
     */
    byte[] makeByteArray(Object src);

    /**
     * Converts a source to a byte array.
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *
     * @return A native {@code byte[]} or {@code null} if {@code null} or an empty wrapper is supplied.
     *
     * @since 5.0
     */
    byte[] makeByteArrayOrNull(Object src);

    /**
     * Creates a SHA-256 hash of a URI.
     *
     * @param uri URI to hash
     * @return SHA-256 hash that is hex-encoded.
     */
    String hashUri(final URI uri);

    /**
     * Loads text from a resource file.
     *
     * @param resourcePath Resource path.
     *
     * @return Text
     *
     * @throws org.gradle.api.resources.ResourceException
     *
     * @since 5.0
     */
    String loadTextFromResource(String resourcePath);

    /**
     * Get final package or directory name from a URI
     *
     * @param uri URI to process
     * @return Last part of URI path.
     */
    String packageNameFromUri(final URI uri);

    /**
     * Converts most things to a pattern. Closures are evaluated as well.
     * <p>
     * Unwraps the following recursively until it gets to a pattern:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link Pattern}.</li>
     *   <li> Groovy Closures.</li>
     *   <li>{@link Callable}</li>
     *   <li>{@link Optional}</li>
     *   <li>{@link Provider}</li>
     *   <li>{@link java.util.function.Supplier}</li>
     * </ul>
     * </p>
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A pattern object
     *
     * @since 5.0
     */
    Pattern patternize(final Object stringy);

    /**
     * Like {@link #patternize}, but returns {@code null} rather than throwing an exception, when item is {@code null},
     * an empty {@Link Provider} or an empty {@link java.util.Optional}.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return Pattern or {@code null}
     *
     * @since 5.0
     */
    Pattern patternizeOrNull(final Object stringy);

    /**
     * Converts a collection of most things to a list of regex patterns. Closures are evaluated as well.
     *
     * @param stringyThings Iterable list of objects that can be converted to patterns,
     *                      including closure that can be evaluated
     *                      into objects that can be converted to patterns.
     * @return A list of patterns
     *
     * @since 5.0
     */
    List<Pattern> patternize(final Collection<?> stringyThings);

    /**
     * Like {@link #patternize}, but drops any nulls, or empty instances of {@link Provider} and {@link Optional}.
     *
     * @param stringyThings Iterable list of objects that can be converted to patterns,
     *                      including closure that can be evaluated
     *                      into objects that can be converted to patterns.
     * @return A list of patterns
     *
     * @since 5.0
     */
    List<Pattern> patternizeDropNull(final Collection<?> stringyThings);

    /**
     * Like {@link #provideByteArray(Object)}, but lazy-evaluates the value..
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *            Empty wrappers and {@code null} are not accepted.
     * @return A provider to {@code byte[]}
     *
     * @since 5.0
     */
    Provider<byte[]> provideByteArray(Object src);

    /**
     * Like {@link #provideByteArrayOrNull(Object)}, but lazy-evaluates the value..
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *
     * @return A provider to {@code byte[]}. Empty wrappers and {@code null} will result in an empty provider.
     *
     * @since 5.0
     */
    Provider<byte[]> provideByteArrayOrNull(Object src);

    /**
     * Like {@link #patternize}, but does not immediately evaluate the passed parameter.
     * The latter is only done when the provider is resolved.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A provider to a regex pattern.
     *
     * @since 5.0
     */
    Provider<Pattern> providePattern(final Object stringy);

    /**
     * Like {@link #patternizeOrNull(Object)}, but does not evaluate the passed parameter immediately.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A provider to a regex pattern. Can be empty.
     *
     * @since 5.0
     */
    Provider<Pattern> providePatternOrNull(final Object stringy);

    /**
     * Like {@link #stringize}, but does not immediately evaluate the passed parameter.
     * The latter is only done when the provider is resolved.
     *
     * @param stringy An object that can be converted to a string or a closure that
     *                can be evaluated to something that can be converted to a string.
     * @return A provider to a string.
     *
     * @since 5.0
     */
    Provider<String> provideString(final Object stringy);

    /**
     * Like {@link #stringizeOrNull(Object)}, but does not evaluate the passed parameter immediately.
     *
     * @param stringy Anything convertible to a string.
     * @return Provider to a string. Can be empty.
     *
     * @since 5.0
     */
    Provider<String> provideStringOrNull(final Object stringy);

    /**
     * Converts a string in one format to another.
     *
     * @param inputFormat Input case format.
     * @param outputFormat Output case format.
     * @param stringy Object to convert
     * @return Provider to a transformed string.
     */
    default Provider<String> provideCaseConversion(
            CaseFormats inputFormat,
            CaseFormats outputFormat,
            final Object stringy
    ) {
        return provideString(stringy).map(x -> inputFormat.convertTo(outputFormat, x));
    }

    /**
     * Lazy-loads text from a resource file.
     *
     * @param resourcePath Resource path.
     *
     * @return Provider to text
     *
     * @throws org.gradle.api.resources.ResourceException
     *
     * @since 5.0
     */
    Provider<String> provideTextFromResource(String resourcePath);

    /**
     * Similar to {@link #stringizeValues}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    Provider<Map<String, String>> provideValues(Map<String, ?> props);

    /**
     * Similar to {@link #stringizeValues}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A provider to a map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    Provider<Map<String, String>> provideValues(Provider<Map<String, Object>> props);

    /**
     * Similar to {@link #stringizeValuesDropNull(Map)}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    Provider<Map<String, String>> provideValuesDropNull(Map<String, ?> props);

    /**
     * Similar to {@link #stringizeValuesDropNull(Map)}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A provider to a map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    Provider<Map<String, String>> provideValuesDropNull(Provider<Map<String, Object>> props);

    /**
     * Similar to {@link #urize(Object)}, but does not evaluate the parameter immediately, only when the provider is
     * resolved.
     *
     * @param uriThingy Anything that could be converted to a URI
     * @return Provider to a URI
     *
     * @since 5.0
     */
    Provider<URI> provideUri(Object uriThingy);

    /**
     * Create a URI where the user/password is masked out.
     *
     * @param uri Original URI
     * @return URI with no credentials.
     */
    URI safeUri(URI uri);

    /**
     * Converts most things to a string. Closures are evaluated as well.
     * <p>
     * Converts collections of the following recursively until it gets to a string:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URIs.
     *   <li> Groovy Closures.
     *   <li>{@link Callable}</li>
     *   <li>{@link Optional}</li>
     *   <li>{@link Provider}</li>
     *   <li>{@link java.util.function.Supplier}</li>
     *   <li> {@link org.gradle.api.resources.TextResource}</li>
     * </ul>
     *
     * @param stringy An object that can be converted to a string or a closure that
     *                can be evaluated to something that can be converted to a string.
     * @return A string object
     */
    String stringize(final Object stringy);

    /**
     * Like {@link #stringize}, but returns {@code null} rather than throwing an exception, when item is {@code null},
     * an empty {@Link Provider} or an empty {@link java.util.Optional}.
     *
     * @param stringy
     * @return string or {@code null}
     */
    String stringizeOrNull(final Object stringy);

    /**
     * Converts a collection of most things to a list of strings. Closures are evaluated as well.
     *
     * @param stringyThings Iterable list of objects that can be converted to strings, including closure that can be evaluated
     *                      into objects that can be converted to strings.
     * @return A list of strings
     */
    List<String> stringize(final Collection<?> stringyThings);

    /**
     * Like {@link #stringize}, but drops any nulls, or empty instances of {@link Provider} and {@link Optional}.
     *
     * @param stringyThings
     * @return A list of strings
     */
    List<String> stringizeDropNull(final Collection<?> stringyThings);

    /**
     * Evaluates a map of objects to a map of strings.
     * <p>
     * Anything value that can be evaluated by {@link #stringize} is
     * evaluated
     *
     * @param props Map that will be evaluated
     * @return Converted {@code Map<String,String>}
     */
    default Map<String, String> stringizeValues(Map<String, ?> props) {
        return props.entrySet().stream().collect(
                HashMap::new,
                (map, entry) -> map.put(entry.getKey(), stringize(entry.getValue())),
                Map::putAll
        );
    }

    /**
     * Like {@link #stringizeValues(Map)}, but drops the key, if the corresponding value is null or an empty
     * {@link Provider} or {@link Optional}.
     *
     * @param props Map that wil be evaluated
     * @return Converted map. Can be empty, but never {@code null}.
     *
     * @subce 5.0
     */
    Map<String, String> stringizeValuesDropNull(Map<String, ?> props);

    /**
     * Updates a {@code Property<String>}.
     *
     * THis provides a more powerful way than {@link Property#set}.
     *
     * @param provider String property
     * @param stringy Objexct that is a string or can be converted to a string.
     */
    void updateStringProperty(Property<String> provider, Object stringy);

    /**
     * Attempts to convert object to a URI.
     *
     * <p>
     * Converts collections of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URIs.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li>{@link Callable}</li>
     *   <li>{@link Optional}</li>
     *   <li>{@link java.util.function.Supplier}</li>
     * </ul>
     *
     * @param uriThingy Anything that could be converted to a URI
     * @return URI object
     */
    URI urize(final Object uriThingy);
}