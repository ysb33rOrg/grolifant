/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Action;
import org.gradle.process.ExecResult;
import org.gradle.process.ExecSpec;
import org.gradle.process.JavaExecSpec;

/**
 * Proxies the {@code ExecOperations} service that was added in Gradle 6.0,
 * so that equivalent functionality can be provided in Gradle 4.0+
 *
 * @since 1.0
 *
 * @deprecated Use methods on {@link ExecTools} instead.
 *   Will be removed in 6.0
 */
@Deprecated
public interface ExecOperationsProxy {
    /**
     * Executes the specified external process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     * @since 1.0
     */
    ExecResult exec(Action<? super ExecSpec> action);

    /**
     * Executes the specified external java process.
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     *
     * @since 1.0
     */
    ExecResult	javaexec(Action<? super JavaExecSpec> action);

}
