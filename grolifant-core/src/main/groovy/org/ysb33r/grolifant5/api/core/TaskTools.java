/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.Task;
import org.gradle.api.file.FileCollection;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.specs.Spec;
import org.gradle.api.tasks.PathSensitivity;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskInputs;
import org.ysb33r.grolifant5.internal.core.EmptyAction;

import java.util.List;

import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.IGNORE_EMPTY_DIRECTORIES;
import static org.ysb33r.grolifant5.api.core.TaskInputFileOptions.SKIP_WHEN_EMPTY;

/**
 * Utilities for working with tasks in a consistent manner across Gradle versions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.3
 */
public interface TaskTools {

    /**
     * This is similar to putting both a {@link org.gradle.api.tasks.SkipWhenEmpty} and a
     * {@code org.gradle.api.tasks.IgnoreEmptyDirectories} annotation on a property, but it deals with both
     * backwards and forward compatibility around Gradle 6.8.
     * <p>
     * Accepts any of the following
     *     <ul>
     *         <li>{@link FileCollection}</li>
     *         <li>{@link org.gradle.api.file.Directory}</li>
     *         <li>File as long as it points to a directory</li>
     *         <li>ANy Provider, Supplier, Optional, Callable</li>
     *     </ul>
     * </p>
     *
     * @param inputs Inputs for a specific task
     * @param files  File collections
     *
     * @deprecated No longer needed as minimum supported version of Gradle is 7.3.
     */
    @Deprecated
    default void ignoreEmptyDirectories(TaskInputs inputs, Object files) {
        inputFiles(inputs, files, SKIP_WHEN_EMPTY, IGNORE_EMPTY_DIRECTORIES);
    }

    /**
     * Configure a collection of files as a task input.
     *
     * @param inputs  The inputs for a specific task.
     * @param files   The inputs for a specific task.
     * @param options Various additional options to set.
     *                Some might be ignored, depending on the version of Gradle.
     */
    void inputFiles(TaskInputs inputs, Object files, TaskInputFileOptions... options);

    /**
     * Configure a collection of files as a task input.
     *
     * @param inputs  The inputs for a specific task.
     * @param files   The inputs for a specific task.
     * @param ps      The {@link PathSensitivity} of the file collection
     * @param options Various additional options to set.
     *                Some might be ignored, depending on the version of Gradle.
     */
    void inputFiles(TaskInputs inputs, Object files, PathSensitivity ps, TaskInputFileOptions... options);

    /**
     * Configures a task, preferably in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it will use {@code getByName} internally.
     *
     * @param taskName     Name of task to configure.  Task must have been registered or configured previously.
     * @param configurator Configurating action.
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    void named(String taskName, Action<Task> configurator);

    /**
     * Configures a task, preferably in a lazy-manner.
     *
     * @param taskName     Name of task to configure. Task must have been registered or configured previously.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    <T extends DefaultTask> void named(String taskName, Class<T> taskType, Action<T> configurator);

    /**
     * Configures a task in a lazy-manner.
     *
     * @param taskName Name of task to configure. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param <T>      Type of task.
     *
     * @since 2.0
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    default <T extends DefaultTask> void register(String taskName, Class<T> taskType) {
        register(taskName, taskType, EmptyAction.INSTANCE);
    }

    /**
     * Registers a task in a lazy-manner.
     *
     * @param taskName     Name of task to register. Task must have been registered or configured previously.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    <T extends DefaultTask> void register(String taskName, Class<T> taskType, Action<T> configurator);

    /**
     * Registers a task in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it create the task.
     *
     * @param taskName     Name of task to register. Task must have been registered or configured previously.
     * @param taskType     Type of task.
     * @param args         C-tor arguments.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    <T extends DefaultTask> void register(String taskName, Class<T> taskType, Iterable<Object> args, Action<T> configurator);

    /**
     * Registers a task in a lazy-manner.
     *
     * @param taskName Name of task to register. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param args     C-tor arguments.
     * @param <T>      Type of task.
     * @since 2.0
     *
     * @deprecated Left for backwards-compatibility
     */
    @Deprecated
    default <T extends DefaultTask> void register(String taskName, Class<T> taskType, List<Object> args) {
        register(taskName, taskType, args, x -> {
        });
    }

    /**
     * Resolves a list of many objects to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    Provider<List<? extends Task>> taskize(Object... taskies);

    /**
     * Resolves a list of many objects to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    Provider<List<? extends Task>> taskize(Iterable<Object> taskies);

    /**
     * Creates a basic task {@link Provider} instance from an object.
     *
     * @param tasks           Override the task containerto use.
     * @param providerFactory Override the provider factory to use.
     * @param o               Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     * @since 2.0
     */
    Provider<? extends Task> taskProviderFrom(TaskContainer tasks, ProviderFactory providerFactory, Object o);

    /**
     * Creates a basic task {@link Provider} instance from an object.
     *
     * @param o Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     * @since 2.0
     */
    Provider<? extends Task> taskProviderFrom(Object o);

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName     Name of task to configure.
     * @param configurator Configurating action.
     */
    void whenNamed(String taskName, Action<Task> configurator);

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName     Name of task to configure.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     */
    <T extends DefaultTask> void whenNamed(String taskName, Class<T> taskType, Action<T> configurator);

    /**
     * Allows the onlyIf reason structure to be used backwards on Gradle <7.6.
     *
     * @param task         Apply to task.
     * @param onlyIfReason Reason why task can be executed.
     * @param spec         Predicate.
     * @since 5.0
     */
    void onlyIf(Task task, String onlyIfReason, Spec<? super Task> spec);
}
