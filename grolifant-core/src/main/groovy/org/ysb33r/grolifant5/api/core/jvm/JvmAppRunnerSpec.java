/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.process.JavaExecSpec;
import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.executable.ProcessOutputProcessor;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise;
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec;
import org.ysb33r.grolifant5.api.core.executable.CmdLineArgumentSpecEntry;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmExecSpec;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory;

import java.util.Collection;

/**
 * A specification to something that can be executed on the JVM and which be populated with various fork options
 * for Java and in addition, can then be used to copy to other methods in the Gradle API that provides a
 * {@link org.gradle.process.JavaForkOptions} in the parameters.
 *
 * @since 2.0
 */
public interface JvmAppRunnerSpec {

    /** The default command-line block name.
     *
     */
    static final String DEFAULT_BLOCK = AppRunnerSpec.DEFAULT_BLOCK;

    /**
     * Copies these options to the given options.
     *
     * @param target The target options.
     * @return The options that were passed in.
     */
    JavaExecSpec copyTo(JavaExecSpec target);

    /**
     * Submits this to a worker queue using an appropriate isolation mode.
     *
     * @param isolationMode            Isolation mode which is either classpath isolated or out of process.
     * @param worker                   A worker execution instance.
     * @param workerAppExecutorFactory A factory instances that can create executor logic.
     * @param parameterFactory         A factory which can create parameters and populate them from a {@link JavaExecSpec}.
     * @param <P>                      The type of the POJO/POGO that holds the parameters.
     */
    <P extends SerializableWorkerAppParameters> WorkerPromise submitToWorkQueue(
            WorkerIsolation isolationMode,
            WorkerExecutor worker,
            WorkerAppExecutorFactory<P> workerAppExecutorFactory,
            WorkerAppParameterFactory<P> parameterFactory
    );

    /**
     * Runs a job using {@code javaexec}.
     *
     * @param spec Execution specification
     * @return Result of execution.
     *
     * @since 5.0
     */
    ExecOutput submitAsJavaExec(AbstractJvmExecSpec spec);

    /**
     * Configures a {@link JavaForkOptionsWithEnvProvider} instance.
     *
     * @param configurator Configurator.
     */
    void configureForkOptions(Action<JavaForkOptionsWithEnvProvider> configurator);

    /**
     * Configures a {@link JavaForkOptionsWithEnvProvider} instance.
     *
     * @param configurator Configurator.
     */
    void configureForkOptions(@DelegatesTo(JavaForkOptionsWithEnvProvider.class) Closure<?> configurator);

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     *
     * @param configurator Configurator.
     */
    void configureCmdline(Action<CmdlineArgumentSpec> configurator);

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     *
     * @param configurator Configurator.
     */
    void configureCmdline(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator);

    /**
     * Configures a {@link JvmEntryPoint} instance.
     *
     * @param configurator Configurator.
     */
    void configureEntrypoint(Action<JvmEntryPoint> configurator);

    /**
     * Configures a {@link JvmEntryPoint} instance.
     *
     * @param configurator Configurator.
     */
    void configureEntrypoint(@DelegatesTo(JvmEntryPoint.class) Closure<?> configurator);

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator Configurator.
     */
    void configureProcess(Action<ProcessExecutionSpec> configurator);

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator Configurator.
     */
    void configureProcess(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator);

    /**
     * Adds a command-line processor that will process command-line arguments in a specific order.
     *
     * For instance in a script, one want to proccess the exe args before the script args.
     *
     * In a system that has commands and subcommands, one wants to process this in the order of exe args, command args,
     * and then subcommand args.
     *
     * This method allows the implementor to control the order of processing  for all the groupings.
     *
     * @param name Name of command-line processor.
     * @param order Order in queue to process.
     * @param processor The specific grouping.
     */
    void addCommandLineProcessor(String name, Integer order, CmdlineArgumentSpec processor);

    /**
     * Provides direct access to the list of command-line processors.
     *
     * In many cases there will only be one item in the list which is for providing arguments to the executable
     * itself. Some implementations will have more. Implementors can use this interface to manipulate order of
     * evaluation.
     *
     * @return Collection of command-line processors. Can be empty, but never {@code null}.
     */
    @Internal
    Collection<CmdLineArgumentSpecEntry> getCommandLineProcessors();

    /**
     * A unique string which determines whether there were any changes.
     *
     * @return Signature string
     */
    @Input
    String getExecutionSignature();
}
