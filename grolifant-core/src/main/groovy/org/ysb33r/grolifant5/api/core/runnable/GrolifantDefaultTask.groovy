/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.JvmTools
import org.ysb33r.grolifant5.api.core.ProjectTools
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService

/**
 * A base task that can provide configuration-cache safe access to various Grolifant helpers.
 * <p>
 * Use this instead of {@link DefaultTask}.
 * </p>
 *
 * @author Schalk W. Cronjé
 * @since 5.0
 */
@CompileStatic
class GrolifantDefaultTask extends DefaultTask implements ConfigCacheSafeOperations {
    @Override
    FileSystemOperations fsOperations() {
        gtc.fsOperations()
    }

    @Override
    JvmTools jvmTools() {
        gtc.jvmTools()
    }

    @Override
    ProjectTools projectTools() {
        gtc.projectTools()
    }

    @Override
    ProviderTools providerTools() {
        gtc.providerTools()
    }

    @Override
    StringTools stringTools() {
        gtc.stringTools()
    }

    @Override
    ExecTools execTools() {
        gtc.execTools()
    }

    protected GrolifantDefaultTask() {
        this.gtc = ConfigCacheSafeOperations.from(project)
        this.gsps = (Provider<GrolifantSecurePropertyService>) project.gradle.sharedServices.registrations
            .getByName(GrolifantServicePlugin.SECURE_STRING_SERVICE).service
    }

    @Internal
    protected Provider<GrolifantSecurePropertyService> getGrolifantSecurePropertyService() {
        this.gsps
    }

    private final ConfigCacheSafeOperations gtc
    private final Provider<GrolifantSecurePropertyService> gsps
}
