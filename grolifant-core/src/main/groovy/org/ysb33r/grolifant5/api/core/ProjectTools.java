/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.artifacts.Configuration;
import org.gradle.api.logging.configuration.ConsoleOutput;
import org.gradle.api.provider.Provider;

import java.util.Collection;

/**
 * Provides various project-related information.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ProjectTools {

    /**
     * Get the full project path including the root project name in case of a multi-project.
     *
     * @return The fully qualified project path including root project.
     * @since 5.2
     */
    String getFullProjectPath();

    /**
     * Console output mode
     *
     * @return How the console output has been requested.
     *
     * @since 5.2
     */
    ConsoleOutput getConsoleOutput();

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    Provider<String> getVersionProvider();

    /**
     * Lazy-evaluated project group.
     *
     * @return provider to project group
     */
    Provider<String> getGroupProvider();

    /**
     * Description of the project.
     *
     * @return Provider to description. If no description was provided, returns an empty string.
     *
     * @since 5.0
     */
    Provider<String> getProjectDescriptionProvider();

    /**
     * A value that indicates whether Gradle is run in offline mode or not.
     *
     * @return Provider that will return {@code true} iof offline.
     *   Never empty.
     *
     * @since 5.0
     */
    Provider<Boolean> getOfflineProvider();

    /**
     * Name of the project.
     *
     * @return Provider to name.
     *
     * @since 5.0
     */
    Provider<String> getProjectNameProvider();

    /**
     * Whether the setup is a multi-project or a single project.
     *
     * @return {@code true} for a multi-project
     *
     * @since 5.0
     */
    boolean isMultiProject();

    /**
     * Whether the current project is the root project.
     *
     * @return {@code true} if the root project.
     *   (Always {@code true} for a single project).
     *
     * @since 5.0
     */
    boolean isRootProject();

    /**
     * Whether dependencies should be refreshed.
     *
     * @return {@code true} to check dependencies again.
     *
     * @since 5.2
     */
    boolean isRefreshDependencies();

    /**
     * Whether tasks should be re-ruin
     *
     * @return {@code true} if tasks were set to be re-run.
     *
     * @since 5.2
     */
    boolean isRerunTasks();

    /**
     * Sets a new version for a project.
     * <p>This creates an internal objhect that can safely be evaluated by
     * Gradle's {@link org.gradle.api.Project#getVersion}.
     * </p>
     *
     * @param newVersion Anything that can be converted to a string using
     *    {@link StringTools}.
     */
    void setVersionProvider(Object newVersion);
}
