/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import groovy.transform.PackageScope;
import org.gradle.api.Action;
import org.gradle.api.tasks.Nested;
import org.gradle.process.JavaExecSpec;
import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecOutput;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;
import org.ysb33r.grolifant5.api.core.jvm.JavaForkOptionsWithEnvProvider;
import org.ysb33r.grolifant5.api.core.jvm.JvmAppRunnerSpec;
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerPromise;
import org.ysb33r.grolifant5.api.remote.worker.SerializableWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory;

/**
 * A base class for specifying the execution of an entrypoint on a forked JVM.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public class AbstractJvmExecSpec<T extends AbstractJvmExecSpec<T>>
        implements ForkedJvmExecutable<T> {

    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator An action to configure the JVM's fork options.
     * @return The configured executable
     */
    @Override
    public T jvm(Action<JavaForkOptionsWithEnvProvider> configurator) {
        jvmAppRunnerSpec.configureForkOptions(configurator);
        return (T) this;
    }

    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator A closure to configure the JVM's fork options.
     * @return The configured executable
     */
    @Override
    public T jvm(@DelegatesTo(JavaForkOptionsWithEnvProvider.class) Closure<?> configurator) {
        jvmAppRunnerSpec.configureForkOptions(configurator);
        return (T) this;
    }

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     * @return The configured executable
     */
    @Override
    public T runnerSpec(Action<CmdlineArgumentSpec> configurator) {
        jvmAppRunnerSpec.configureCmdline(configurator);
        return (T) this;
    }

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     * @return The configured executable
     */
    @Override
    public T runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator) {
        jvmAppRunnerSpec.configureCmdline(configurator);
        return (T) this;
    }

    /**
     * Configures the entrypoint for the JVM executable
     *
     * @param configurator An action to configure the entrypoint
     * @return The configured executable.
     */
    @Override
    public T entrypoint(Action<JvmEntryPoint> configurator) {
        jvmAppRunnerSpec.configureEntrypoint(configurator);
        return (T) this;
    }

    /**
     * Configures the entrypoint for the JVM executable
     *
     * @param configurator A closure to configure the entrypoint
     * @return The configured executable.
     */
    @Override
    public T entrypoint(@DelegatesTo(JvmEntryPoint.class) Closure<?> configurator) {
        jvmAppRunnerSpec.configureEntrypoint(configurator);
        return (T) this;
    }

    /**
     * Configures the stream redirection and exit code checks.
     *
     * @param configurator An action to configure the execution.
     * @return The configured executable.
     */
    @Override
    public T process(Action<ProcessExecutionSpec> configurator) {
        jvmAppRunnerSpec.configureProcess(configurator);
        return (T) this;
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     * @return The configured executable.
     */
    @Override
    public T process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator) {
        jvmAppRunnerSpec.configureProcess(configurator);
        return (T) this;
    }

    /**
     * Copies this specification to a standard {@link JavaExecSpec}.
     *
     * @param execSpec Target execution specification.
     */
    public void copyTo(JavaExecSpec execSpec) {
        jvmAppRunnerSpec.copyTo(execSpec);
    }

    /**
     * Submits this to a worker queue using an appropriate isolation mode.
     *
     * @param isolationMode            Isolation mode which is either classpath isolated or out of process.
     * @param worker                   A worker execution instance.
     * @param workerAppExecutorFactory A factory instances that can create executor logic.
     * @param parameterFactory         A factory which can create parameters and populate them from a {@link JavaExecSpec}.
     * @param <P>                      The type of the POJO/POGO that holds the parameters.
     */
    public <P extends SerializableWorkerAppParameters> WorkerPromise submitToWorkQueue(
            WorkerIsolation isolationMode,
            WorkerExecutor worker,
            WorkerAppExecutorFactory<P> workerAppExecutorFactory,
            WorkerAppParameterFactory<P> parameterFactory
    ) {
        return jvmAppRunnerSpec.submitToWorkQueue(
                isolationMode,
                worker,
                workerAppExecutorFactory,
                parameterFactory
        );
    }

    /**
     * Runs a job using {@code javaexec}.
     *
     * @return Result of execution.
     *
     * @since 5.0
     */
    public ExecOutput submitAsJavaExec() {
        return jvmAppRunnerSpec.submitAsJavaExec(this);
    }

    /**
     * Processes the output.
     *
     * <p>
     * Checks first if any outputs should be forwarded.
     * Thereafter run all registered actions.
     * </p>
     *
     * @param output Output to process
     *
     * @since 5.0
     */
    @PackageScope
    void processOutput(ExecOutput output) {
        if(this.jvmAppRunnerSpec instanceof ProcessExecOutput) {
            ((ProcessExecOutput) this.jvmAppRunnerSpec).processOutput(output);
        }
    }

    @Deprecated
    protected AbstractJvmExecSpec(ProjectOperations po) {
        this.jvmAppRunnerSpec = po.getJvmTools().jvmAppRunnerSpec();
    }

    /**
     * Constructs the basics of a Jvm execution specification.
     *
     * @param po Implementation of {@link ConfigCacheSafeOperations}.
     *
     * @since 5.0
     */
    protected AbstractJvmExecSpec(ConfigCacheSafeOperations po) {
        this.jvmAppRunnerSpec = po.jvmTools().jvmAppRunnerSpec();
    }


    @Nested
    protected JvmAppRunnerSpec getJvmAppRunnerSpec() {
        return this.jvmAppRunnerSpec;
    }

    private final JvmAppRunnerSpec jvmAppRunnerSpec;
}
