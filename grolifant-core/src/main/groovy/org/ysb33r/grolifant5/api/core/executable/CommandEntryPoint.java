/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;

/**
 * Configures a command along with its arguments.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface CommandEntryPoint extends CmdlineArgumentSpec {

    /**
     * Set the lazy-evalued command.
     *
     * @param name Name of command. Must evaluate to a string.
     */
    void setCommand(Object name);

    /**
     * Get the command.
     *
     * @return A provider to the command.
     */
    Provider<String> getCommand();
}
