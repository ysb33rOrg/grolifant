/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec

/**
 * Representation of a command-line specification and its order in processing.
 */
@CompileStatic
class CmdLineArgumentSpecEntry implements Comparable<CmdLineArgumentSpecEntry> {
    final String name
    final Integer order
    final CmdlineArgumentSpec argumentSpec

    CmdLineArgumentSpecEntry(String name, Integer order, org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec spec) {
        this.name = name
        this.order = order
        this.argumentSpec = spec
    }

    @Override
    int compareTo(CmdLineArgumentSpecEntry rhs) {
        if (order == rhs.order) {
            name <=> rhs.name
        } else {
            order <=> rhs.order
        }
    }
}
