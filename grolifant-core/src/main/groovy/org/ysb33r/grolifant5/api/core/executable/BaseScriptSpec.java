/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;

/**
 * Describes how to configure a script and its arguments.
 *
 * @since 2.0
 */
public interface BaseScriptSpec extends CmdlineArgumentSpec {

    /**
     * Set the name of the script.
     *
     * This is useful for cases where the scripting system can auto-resolve the location of the script.
     *
     * @param lazyName Lazy evaluated name. Needs to evaluate to a string.
     */
    void setName(Object lazyName);

    /**
     * Provider to the name of a script.
     *
     * @return Script name provider. Can be empty.
     */
    @Input
    @Optional
    Provider<String> getName();
}
