/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.AbstractCmdlineArgumentSpec

/**
 * Defines a base class for implementing a script definition.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ScriptDefinition extends AbstractCmdlineArgumentSpec implements CmdlineArgumentSpec, ScriptSpec {

    ScriptDefinition(ConfigCacheSafeOperations gtc) {
        super(gtc.stringTools(), gtc.providerTools())
        this.gtc = ConfigCacheSafeOperations.from(gtc)

        final po = gtc.providerTools()
        this.nameProvider = po.property(String)
        this.pathProvider = po.property(File)
        this.nameOrPathProvider = nameProvider.orElse(pathProvider.map { it?.absolutePath })
        this.defaultPreArgsProvider = po.listProperty(String)
        this.defaultPreArgsProvider.set(nameOrPathProvider.map { [it] }.orElse(Collections.EMPTY_LIST))
    }

    @Deprecated
    ScriptDefinition(ProjectOperations po) {
        this(ConfigCacheSafeOperations.from(po))
    }

    void setName(Object lazyName) {
        this.pathProvider.set((File)null)
        gtc.stringTools().updateStringProperty(this.nameProvider, lazyName)
    }

    void setPath(Object lazyPath) {
        this.nameProvider.set((String)null)
        gtc.fsOperations().updateFileProperty(this.pathProvider, lazyPath)
    }

    @Internal
    Provider<String> getName() {
        nameProvider
    }

    @Internal
    Provider<File> getPath() {
        pathProvider
    }

    @Input
    Provider<String> getNameOrPath() {
        nameOrPathProvider
    }

    /**
     * A provider to arguments that will be inserted before any supplied arguments.
     *
     * @return Arguments provider
     */
    @Override
    Provider<List<String>> getPreArgs() {
        this.defaultPreArgsProvider
    }

    /**
     * Override the default pre-argument provider.
     *
     * @param newPreArgs
     *
     * @since 2.2
     */
    protected void setDefaultPreArgsProvider(Provider<List<String>> newPreArgs) {
        defaultPreArgsProvider.set(newPreArgs)
    }

    private final ConfigCacheSafeOperations gtc
    private final Property<String> nameProvider
    private final Property<File> pathProvider
    private final Provider<String> nameOrPathProvider
    private final ListProperty<String> defaultPreArgsProvider
}
