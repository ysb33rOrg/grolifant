/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;

import java.io.File;

/**
 * Describes how to configure a script and its arguments.
 *
 * @since 2.0
 */
public interface ScriptSpec extends BaseScriptSpec {

    /**
     * Sets the path of the script including the name of the script.
     *
     * Use this when the script is at a known location.
     *
     * @param lazyPath LAzy evaluated path on file system.
     */
    void setPath(Object lazyPath);

    /**
     * Provider to the path of a script.
     *
     * @return Script file location provider. Can be empty.
     */
    @Input
    @Optional
    Provider<File> getPath();

    /**
     * Provider to either a script name or a string-based representation of the file path where the script can be
     * found.
     *
     * Normally the name should be resolved first, and if that is empty the path to the script.
     *
     * @return String-based resolver.
     */
    @Input
    Provider<String> getNameOrPath();
}
