/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader;

import java.io.File;
import java.net.URI;

/**
 * Indicates whether download is required.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
public interface ArtifactRequiresDownload {
    /** Indicates whether download is required.
     *
     * @param downloadURI URI where file is to be downloaded from.
     * @param localPath Path to local file. (File does not need to exist locally).
     * @return {@b true} is the file should be downloaded.
     */
    boolean download(URI downloadURI, File localPath);
}
