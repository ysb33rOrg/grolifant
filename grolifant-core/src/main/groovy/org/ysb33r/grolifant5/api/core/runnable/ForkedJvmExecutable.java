/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;
import org.ysb33r.grolifant5.api.core.jvm.JavaForkOptionsWithEnvProvider;
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint;

/**
 * A process that can be executed on a JVM
 *
 * @param <T> Classes that extends {@code ForkedJvmExecutable}.
 * @since 2.0
 */
public interface ForkedJvmExecutable<T extends ForkedJvmExecutable> {
    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator An action to configure the JVM's fork options.
     * @return The configured executable
     */
    T jvm(Action<JavaForkOptionsWithEnvProvider> configurator);

    /**
     * Configures a JVM that will be forked.
     *
     * @param configurator A closure to configure the JVM's fork options.
     * @return The configured executable
     */
    T jvm(@DelegatesTo(JavaForkOptionsWithEnvProvider.class) Closure<?> configurator);

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     * @return The configured executable
     */
    T runnerSpec(Action<CmdlineArgumentSpec> configurator);

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     * @return The configured executable
     */
    T runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator);

    /**
     * Configures the entrypoint for the JVM executable
     *
     * @param configurator An action to configure the entrypoint
     * @return The configured executable.
     */
    T entrypoint(Action<JvmEntryPoint> configurator);

    /**
     * Configures the entrypoint for the JVM executable
     *
     * @param configurator A closure to configure the entrypoint
     * @return The configured executable.
     */
    T entrypoint(@DelegatesTo(JvmEntryPoint.class) Closure<?> configurator);

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator An action to configure the execution.
     * @return The configured executable.
     */
    T process(Action<ProcessExecutionSpec> configurator);


    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     * @return The configured executable.
     */
    T process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator);
}
