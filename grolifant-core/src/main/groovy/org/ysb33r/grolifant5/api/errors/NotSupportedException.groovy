/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.errors

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.util.GradleVersion

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_6_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_8_0

/** Feature is not supported on this version of Gradle.
 *
 * @since 0.16.0
 */
@CompileStatic
@InheritConstructors
class NotSupportedException extends RuntimeException implements GrolifantError {

    static void needsSpecificGrolifantLibrary(final String feature) {
        String grolifant
        if (PRE_6_0) {
            grolifant = 'grolifant50'
        } else if (PRE_7_0) {
            grolifant = 'grolifant60'
        } else if (PRE_8_0) {
            grolifant = 'grolifant70'
        } else {
            grolifant = 'grolifant80'
        }

        final String message = "You are running Gradle ${GradleVersion.current().version}. " +
            "In order to support '${feature}' correctly, please ensure ${grolifant} is on the plugin classpath. " +
            'Either ask the maintainers of the plugin that you are using to add an additional dependency ' +
            'or workaround it by adding the artifact to the buildscript.dependencies.classpath.'

        throw new NotSupportedException(message)
    }
}
