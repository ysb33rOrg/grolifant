/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Task
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * Base class for an extension that can both be used on a project or a task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
@Slf4j
class CombinedProjectTaskExtensionBase<T extends CombinedProjectTaskExtensionBase> {

    /**
     * {@link ProjectOperations} instance that the extension can use.
     */
    final ProjectOperations projectOperations

    /** Attach this extension to a project
     *
     * @param project Project to attach to.
     */
    protected CombinedProjectTaskExtensionBase(ProjectOperations projectOperations) {
        this.projectOperations = projectOperations
        this.projectExtension = (T) this
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to
     */
    protected CombinedProjectTaskExtensionBase(Task task, ProjectOperations projectOperations, T projectExtension) {
        this.task = task
        this.projectOperations = projectOperations
        this.projectExtension = projectExtension
    }

    /**
     * Task this extension is attached to. Will be {@code null} if extension is attached to a project.
     */
    protected Task getTask() {
        this.task
    }

    /**
     * If this extension is attached to a task, this point to the global project extension if it exists,
     * other {@code null}.
     */
    protected T getProjectExtension() {
        this.projectExtension
    }

    private final Task task
    private final T projectExtension
}
