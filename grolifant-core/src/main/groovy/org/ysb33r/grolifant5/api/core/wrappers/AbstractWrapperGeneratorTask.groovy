/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.wrappers

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.file.CopySpec
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask
import org.ysb33r.grolifant5.api.errors.WrapperCreationException
import org.ysb33r.grolifant5.api.core.Transform

import java.util.concurrent.Callable
import java.util.function.Function

/**
 * An abstract base class for creating tasks that create script wrappers.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractWrapperGeneratorTask extends GrolifantDefaultTask implements ConfigCacheSafeOperations {

    /**
     * Sets the wrapper destination directory
     *
     * @param dest Destination directory
     */
    void setWrapperDestinationDirectory(Object dest) {
        fsOperations().updateFileProperty(destDirProvider, dest)
    }

    @TaskAction
    void exec() {
        validate()
        final copyAndSubstitute = copyAndSubstituteAction
        Set<File> templates = toolWrapper.copyWrappersFromResources(templateResourcePath, templateMapping)
        fsOperations().copy(copyAndSubstitute.apply(templates))
        if (deleteTemplatesAfterUsage) {
            templates*.delete()
        }
    }

    protected AbstractWrapperGeneratorTask() {
        super()
        this.destDirProvider = project.objects.property(File)
        this.destDirProvider.set(project.projectDir)
        this.toolWrapper = new ToolWrapper(fsOperations())
        this.templateMapping = [:]

        final calcOutput = new Callable<Set<File>>() {
            @Override
            Set<File> call() throws Exception {
                final root = wrapperDestinationDirProvider.get()
                Transform.toSet(templateMapping.values()) {
                    new File(root, it)
                }
            }
        }
        this.outputFiles = project.provider(calcOutput)
        outputs.files(calcOutput)
    }

    /** Directory for writing wrapper files to.
     *
     * @return Directory
     *
     * @since 1.3
     */
    @Internal
    protected Provider<File> getWrapperDestinationDirProvider() {
        this.destDirProvider
    }

    /** Use wrapper templates from resources classpath
     *
     * @param templateResourcePath
     * @param templateMapping
     */
    protected void useWrapperTemplatesInResources(
        final String templateResourcePath,
        final Map<String, String> templateMapping
    ) {
        this.templateMapping.clear()
        this.templateMapping.putAll(templateMapping)
        deleteTemplatesAfterUsage = true
        this.templateResourcePath = templateResourcePath
    }

    /** If the default of {@link org.apache.tools.ant.filters.ReplaceTokens} is used, this method
     * will return the start token delimiter.
     *
     * The default is {@code @}. Implementors should override this method.
     *
     * @return Start token.
     */
    @Internal
    protected String getBeginToken() {
        '@'
    }

    /** If the default of {@link org.apache.tools.ant.filters.ReplaceTokens} is used, this method
     * will return the end token delimiter.
     *
     * The default is {@code @}. Implementors should override this method.
     *
     * @return End token.
     */
    @Internal
    protected String getEndToken() {
        '@'
    }

    /** If the default of {@link org.apache.tools.ant.filters.ReplaceTokens} is used, this method
     * will return the collection of tokens.
     *
     * The default is an empty map. Implementors should override this method.
     *
     * @return End token.
     */
    @Internal
    protected Map<String, String> getTokenValuesAsMap() {
        [:]
    }

    /**
     * Whether templates should be deleted after usage.
     * This might be necessary if templates are copied to a temporary space.
     *
     * @param del {@code true} if templates should be deleted.
     */
    protected void setDeleteTemplatesAfterUsage(boolean del) {
        this.deleteTemplatesAfterUsage = del
    }

    @Internal
    protected Property<File> getDestDirProvider() {
        this.destDirProvider
    }

    private enum CopyFilter {
        REPLACETOKENS
    }

    @SuppressWarnings('DuplicateStringLiteral')
    private void validate() {
        if (!templateResourcePath) {
            throw new WrapperCreationException(
                'Method for preparing templates has not been specified. If you see this error in a Gradle plugin, ' +
                    'then please contact the maintainers of the plugin as something has possibly not been setup ' +
                    'correctly in the plugin.'
            )
        }

        if (!copyFilterType) {
            throw new WrapperCreationException(
                'Method for determining copy filter types has not been specified. ' +
                    'If you see this error in a Gradle plugin, ' +
                    'then please contact the maintainers of the plugin as something has possibly not been setup ' +
                    'correctly in the plugin.'
            )
        }
    }

    @SuppressWarnings('DuplicateStringLiteral')
    private Function<Collection<File>, Action<CopySpec>> getCopyAndSubstituteAction() {
        switch (copyFilterType) {
            case CopyFilter.REPLACETOKENS:
                return new Function<Collection<File>, Action<CopySpec>>() {
                    @Override
                    Action<CopySpec> apply(Collection<File> templates) {
                        toolWrapper.wrapperCopyAction(
                            templates,
                            wrapperDestinationDirProvider.get(),
                            beginToken,
                            endToken,
                            tokenValuesAsMap
                        )
                    }
                }
            default:
                throw new WrapperCreationException(
                    "${copyFilterType} for template value substitution has not been implemented. " +
                        'If you see this error in a Gradle plugin, ' +
                        'then please contact the maintainers of the plugin as something has possibly not been setup ' +
                        'correctly in the plugin.'
                )
        }
    }

    private final Property<File> destDirProvider
    private boolean deleteTemplatesAfterUsage = false
    private final CopyFilter copyFilterType = CopyFilter.REPLACETOKENS
    private final ToolWrapper toolWrapper
    private final Provider<Set<File>> outputFiles
    private final Map<String, String> templateMapping
    private String templateResourcePath
}
