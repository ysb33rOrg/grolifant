/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm;

import org.gradle.api.provider.Provider;

public interface JvmDebugOptions {
    /**
     * Whether to attach a debug agent to the forked process.
     *
     * @return Provider to setting.
     */
    Provider<Boolean> getEnabled();

    /**
     * Whether to attach a debug agent to the forked process.
     * @param flag Value convertible to boolean.
     */
    void setEnabled(Object flag);

    /**
     * Whether a socket-attach or a socket-listen type of debugger is expected.
     * @return Provider to setting.
     */
    Provider<Boolean> getServer();

    /**
     * Whether a socket-attach or a socket-listen type of debugger is expected.
     *
     * @param flag Value convertible to boolean.
     */
    void setServer(Object flag);

    /**
     * Whether the forked process should be suspended until the connection to the debugger is established.
     * @return Provider to setting.
     */
    Provider<Boolean> getSuspend();

    /**
     * Whether the forked process should be suspended until the connection to the debugger is established.
     * @param flag Value convertible to boolean.
     */
    void setSuspend(Object flag);

    /**
     * The debug port.
     *
     * @return Provider to debug port.
     */
    Provider<Integer> getPort();

    /**
     * THe debug port.
     *
     * @param port Value convertible to integer.
     */
    void setPort(Object port);
}
