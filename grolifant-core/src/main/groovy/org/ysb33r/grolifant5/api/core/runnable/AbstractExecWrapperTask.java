/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.TaskAction;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;

import java.io.File;
import java.util.Map;

/**
 * Base task class to wrap external tool executions without exposing command-line parameters directly.
 *
 * <p> {@code E} is the execution specification.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public abstract class AbstractExecWrapperTask<E extends AbstractExecSpec<E>> extends GrolifantDefaultTask
        implements ConfigCacheSafeOperations {

    /**
     * Replace current environment with new one.
     *
     * @param args New environment key-value map of properties.
     */
    public void setEnvironment(final Map<String, ?> args) {
        getExecSpec().entrypoint(ep -> ep.setEnvironment(args));
    }

    /**
     * Adds an environment provider.
     *
     * @param provider Provider that will add additional environments.
     */
    public void addEnvironmentProvider(final Provider<? extends Map<String, String>> provider) {
        getExecSpec().entrypoint(ep -> ep.addEnvironmentProvider(provider));
    }

    /**
     * Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     */
    public void environment(Map<String, ?> args) {
        getExecSpec().entrypoint(ep -> ep.environment(args));
    }

    /**
     * The default implementation will build an execution specification
     * and run it.
     */
    @TaskAction
    protected void exec() throws Exception {
        String exe = stringTools().stringize(getExecutableLocation().get());
        getExecSpec().entrypoint(ep -> ep.executable(exe));
        ExecOutput result = getExecSpec().submitAsExec();
        result.assertNormalExitValue();
    }

    protected AbstractExecWrapperTask() {
        super();
    }

    /**
     * Access to the execution specification.
     *
     * @return This task's execution specification
     */
    @Internal
    abstract protected E getExecSpec();

    /**
     * Location of executable
     *
     * @return Location as {@link String}.
     */
    @Internal
    abstract protected Provider<File> getExecutableLocation();
}
