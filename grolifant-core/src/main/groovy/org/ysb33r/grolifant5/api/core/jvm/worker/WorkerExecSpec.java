/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm.worker;

import org.gradle.process.JavaExecSpec;
import org.ysb33r.grolifant5.api.core.jvm.JvmEntryPoint;

import java.util.List;

/**
 *
 * Describes the necessary information that might be needed to run a JVM process inside a worker.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface WorkerExecSpec {
    /**
     * The configured {@link JavaExecSpec}.
     *
     * @return {@link JavaExecSpec}
     */
    JavaExecSpec getJavaExecSpec();

    /**
     * The JVM entry point.
     *
     * Although some of this information will be found in the {@link #getJavaExecSpec()}, it will be presented in
     * a way that is more portable between Gradle versions.
     *
     * @return Entrypoint info.
     */
    JvmEntryPoint getJvmEntrypoint();

    /**
     * The list of all resovled application arguments.
     *
     * @return Arguyments list.
     */
    List<String> getApplicationArguments();
}
