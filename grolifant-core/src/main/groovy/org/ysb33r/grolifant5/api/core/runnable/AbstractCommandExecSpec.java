/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint;

/**
 * Basic building
 *
 * @param <T>
 */
public class AbstractCommandExecSpec<T extends AbstractCommandExecSpec<T>>
        extends AbstractExecSpec<T>
        implements ExecutableCommand<T> {

    public static final String COMMAND_BLOCK = "command";

    /**
     * Configures the command along with its arguments.
     *
     * @param configurator An action to configure the command
     * @return The configured command
     */
    @Override
    public void cmd(Action<CommandEntryPoint> configurator) {
        appRunnerSpec.configureCmdline(COMMAND_BLOCK, configurator);
    }

    /**
     * Configures the command along with its arguments.
     *
     * @param configurator A closure to configure the command
     * @return The configured command
     */
    @Override
    public void cmd(@DelegatesTo(CommandEntryPoint.class) Closure<?> configurator) {
        appRunnerSpec.configureCmdline(COMMAND_BLOCK, configurator);
    }

    @Deprecated
    protected AbstractCommandExecSpec(ProjectOperations po) {
        super(ConfigCacheSafeOperations.from(po));
        addCommandLineProcessor(COMMAND_BLOCK, 1, po.getExecTools().commandEntryPoint());
    }

    /**
     * Constructs this from an instance of {@link ConfigCacheSafeOperations}.
     *
     * <p>
     *     A task having {@link GrolifantDefaultTask} in its parent lineage, can just pass a {@code this} reference.
     * </p>
     *
     * @param po Instance of {@link ConfigCacheSafeOperations}.
     *
     * @since 5.0
     */
    protected AbstractCommandExecSpec(ConfigCacheSafeOperations po) {
        super(po);
        addCommandLineProcessor(COMMAND_BLOCK, 1, getTaskOperations().execTools().commandEntryPoint());
    }

}
