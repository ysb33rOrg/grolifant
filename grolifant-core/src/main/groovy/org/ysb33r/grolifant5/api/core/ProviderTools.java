/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Transformer;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.reflect.ObjectInstantiationException;

import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Tools to safely deal with providers across all Gradle versions 4.0+.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ProviderTools {

    /**
     * Value that can be read safely at configuration time
     * <p>
     * As from Gradle 8.0, methods take this as an input, will ignore this value as all providers that can be resolved
     * at configuration-time are available.
     * </p>
     * @return Always returns true
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    default boolean atConfigurationTime() {
        return true;
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of an environmental variable.
     * The property cannot safely be read at configuration time.
     * @since 5.0
     */
    Provider<String> environmentVariable(Object name);

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of an environmental variable.
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety);

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the project property.
     * The property cannot safely be read at configuration time
     * @since 5.0
     */
    Provider<String> gradleProperty(Object name);

    /**
     * Creates a provider to a project property.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the project property.
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety);

    /**
     * Resolves a provider and turn it into an {@link Optional}.
     *
     * <p>
     *     If the provider is empty, then an empty {@link Optional} is created.
     * </p>
     *
     * @param providerValue Provider to be resolved.
     * @return {@link Optional} instance.
     * @param <T> Type of value.
     *
     * @since 5.0
     */
    default <T> Optional<T> resolveToOptional(Provider<T> providerValue) {
        if (providerValue.isPresent()) {
            return Optional.of(providerValue.get());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Convenience interface for creating a provider.
     *
     * <p>
     *     Delegates to {@link org.gradle.api.provider.ProviderFactory#provider(Callable)}.
     * </p>
     *
     * @param value A function that will return a value or {@code null}
     * @return Provider (promise) to a future value (or null).
     * @param <T> Type of value to be returned.
     *
     * @since 5.0
     */
    <T> Provider<T> provider(Callable<? extends T> value);

    /**
     * Allow flatMap functionality for providers even before Gradle 5.0.
     *
     * @param provider    Existing provider.
     * @param transformer Transform one provider to another.
     * @param <S>         Return type of new provider.
     * @param <T>         Return type of existing provider.
     * @return New provider.
     * @since 1.2
     *
     * @deprecated No longer needed as 7.3 is the minimum version supported.
     */
    @Deprecated
    default <S, T> Provider<S> flatMap(
            Provider<T> provider,
            Transformer<? extends Provider<? extends S>, ? super T> transformer
    ) {
        return provider.flatMap(transformer);
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * <p>
     * Returns a Provider whose value is the value of this provider, if present, otherwise the given default value.
     *
     * @param provider Original provider.
     * @param value    The default value to use when this provider has no value.
     * @param <T>      Provider type.
     * @return Provider value or default value.
     * @since 1.2
     *
     * @deprecated No longer needed as 7.3 is the minimum version supported.
     */
    @Deprecated
    default <T> Provider<T> orElse(Provider<T> provider, T value) {
        return provider.orElse(value);
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * Returns a Provider whose value is the value of this provider, if present, otherwise uses the value from
     * the given provider, if present.
     *
     * @param provider     Original provider
     * @param elseProvider The provider whose value should be used when this provider has no value.
     * @param <T>          Provider type
     * @return Provider chain.
     * @since 1.2
     *
     *
     */
    @Deprecated
    default <T> Provider<T> orElse(Provider<T> provider, Provider<? extends T> elseProvider) {
        return provider.orElse(elseProvider);
    }

    /**
     * Creates a {@link ListProperty} instance.
     *
     * @param valueType Class.
     * @return Property instance
     * @param <T> Type of property
     *
     * @since 5.0
     */
    <T> ListProperty<T> listProperty(Class<T> valueType);

    /**
     * Creates a {@link ListProperty} instance and initialises it from the supplied default values
     *
     * @param valueType Class.
     * @param defaultValues Defaults values for the list.
     * @return Property instance
     * @param <T> Type of property
     *
     * @since 5.0
     */
    default <T> ListProperty<T> listProperty(Class<T> valueType, Iterator<? extends T> defaultValues) {
        ListProperty<T> provider = listProperty(valueType);
        provider.value((Iterable<? extends T>) defaultValues);
        return provider;
    }

    /**
     * Creates a {@link MapProperty} instance
     * @param keyType Key type
     * @param valueType Value type
     * @return A map property with uninitialised content.
     * @param <K> Key type
     * @param <V> Value type
     *
     * @since 5.0
     */
    <K, V> MapProperty<K, V> mapProperty(java.lang.Class<K> keyType, java.lang.Class<V> valueType);

    /**
     * Creates a {@link Property} instance
     *
     * @param valueType Class.
     * @return Property instance
     * @param <T> Type of property
     *
     * @since 2.0
     */
    <T> Property<T> property(Class<T> valueType);

    /**
     * Creates a {@link Property} instance with a default value
     *
     * @param valueType Class.
     * @param defaultValue A provider that can provider this value.
     * @return Property instance
     * @param <T> Type of property
     *
     * @since 2.0
     */
    <T> Property<T> property(Class<T> valueType, Provider<T> defaultValue);

    /**
     * Creates a provider that can resolve the three providers in order.
     * If the first is not present, it will attempt to resolve the second and then the third.
     *
     * @param one First provider.
     * @param two Second provider.
     * @param three Third provider.
     * @return Combined resolver.
     *
     * @since 2.0
     */
    default Provider<String> resolveOrderly(
            Provider<String> one,
            Provider<String> two,
            Provider<String> three
    ) {
        return orElse(one, orElse(two, three));
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name Anything convertible to a string
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     *
     * @since 5.0
     */
    default Provider<String> resolveProperty(Object name) {
        return resolveProperty(name, null, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to finding a property by the specified name.
     *
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    default Provider<String> resolveProperty(Object name, boolean configurationTimeSafety) {
        return resolveProperty(name, null, configurationTimeSafety);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name         Anything convertible to a string
     * @param defaultValue Default value to return if the property search order does not return any value.
     *                     Can be {@code null}. Anything convertible to a string.
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     *
     * @since 5.0
     */
    default Provider<String> resolveProperty(Object name, Object defaultValue) {
        return resolveProperty(name, defaultValue, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name                    Anything convertible to a string
     * @param defaultValue            Default value to return if the property search order does not return any value.
     *                                Can be {@code null}. Anything convertible to a string.
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to finding a property by the specified name.
     *
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety);

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the system property. The property cannot safely be read at configuration time
     * @since 5.0
     */
    Provider<String> systemProperty(Object name);

    /**
     * Creates a provider to a system property.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the system property.
     * @since 5.0
     * @deprecated Only required for Gradle 7.3 & 7.4 compatibility.
     */
    @Deprecated
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety);

    /**
     * Shortcut for creating a new instance of an object.
     *
     * <p>
     *     Can be used with injected constructors to inject Gradle objects.
     * </p>
     *
     * @param type Type of object to create
     * @param parameters Zero or more parameters to pass to object.
     * @return New instance of object
     * @param <T> Type ob object.
     * @throws ObjectInstantiationException If object cannot be instantiated.
     *
     * @since 5.2
     */
    <T> T newInstance(Class<? extends T> type, Object... parameters) throws ObjectInstantiationException;
}
