/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.git

import groovy.transform.CompileStatic
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.downloader.ArtifactDownloader
import org.ysb33r.grolifant5.api.core.downloader.ArtifactRootVerification
import org.ysb33r.grolifant5.api.core.downloader.ArtifactUnpacker
import org.ysb33r.grolifant5.api.core.downloader.Downloader
import org.ysb33r.grolifant5.api.errors.DistributionFailedException

/**
 * Downloads an archive from a Git repository.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class GitRepoArchiveDownloader {

    /**
     * Download archive from Git Cloud provider.
     *
     * @param descriptor Cloud Git provider metadata
     * @param projectOperations Project operations.
     *
     * @since 1.0.0
     */
    @SuppressWarnings('DuplicateStringLiteral')
    GitRepoArchiveDownloader(
        final CloudGitDescriptor descriptor,
        final ConfigCacheSafeOperations gtc
    ) {
        this.gitDescriptor = descriptor
        this.configurationCacheSafeOperations = gtc
        this.isOffline = gtc.projectTools().offlineProvider
        this.gradleUserHomeDir = gtc.fsOperations().gradleUserHomeDir
        this.projectDir = gtc.fsOperations().projectDir
        this.downloader = gtc.execTools().downloader(
            "${gitDescriptor.name}:${gitDescriptor.organisation}/${gitDescriptor.repository}"
        )
        this.unpacker = new ArtifactUnpacker() {
            @Override
            void accept(File source, File destDir) {
                final FileTree archiveTree = gtc.fsOperations().zipTree(source)
                gtc.fsOperations().copy { CopySpec cs ->
                    cs.from archiveTree
                    cs.into destDir
                }
            }
        }

        this.verifyRoot = new ArtifactRootVerification() {
            @Override
            File apply(File unpackedRoot) {
                List<File> dirs = gtc.fsOperations().listDirs(unpackedRoot)
                if (dirs.empty) {
                    throw new DistributionFailedException(
                        "Download for '${safeUri(gitDescriptor.archiveUri)}' does not contain any directories. " +
                            'Expected to find exactly 1 directory.'
                    )
                }
                if (dirs.size() != 1) {
                    throw new DistributionFailedException(
                        "Download for '${safeUri(gitDescriptor.archiveUri)}' contains too many directories. " +
                            'Expected to find exactly 1 directory.'
                    )
                }
                dirs[0]
            }
        }
    }

    /**
     * Returns the location which is the top or home folder for a distribution.
     *
     *  This value is affected by {@link #setDownloadRoot(java.io.File)} and
     *  the parameters passed in during construction time.
     *
     * @return Location of the distribution.
     */
    File getArchiveRoot() {
        ArtifactDownloader repoDownloader = new ArtifactDownloader(
            gitDescriptor.archiveUri,
            this.downloadRoot ?: this.gradleUserHomeDir.get(),
            configurationCacheSafeOperations,
            configurationCacheSafeOperations.fsOperations().toSafeFile(
                "${gitDescriptor.name.toLowerCase()}-cache",
                gitDescriptor.organisation.toLowerCase(),
                gitDescriptor.repository.toLowerCase()
            ).path,
            verifyRoot,
            unpacker,
            null
        )

        repoDownloader.getFromCache(
            safeUri(gitDescriptor.archiveUri).toString(),
            isOffline.get(),
            this.downloader
        )
    }

    /** Sets a download root directory for the distribution.
     *
     * If not supplied the default is to use the Gradle User Home.
     * This method is provided for convenience and is mostly only used for testing
     * purposes.
     *
     * The folder will be created at download time if it does not exist.
     *
     * @param downloadRootDir Any writeable directory on the filesystem.
     */
    void setDownloadRoot(File downloadRootDir) {
        this.downloadRoot = downloadRootDir
    }

    private String safeUri(URI uri) {
        configurationCacheSafeOperations.stringTools().safeUri(uri)
    }

    private final ConfigCacheSafeOperations configurationCacheSafeOperations
    private final CloudGitDescriptor gitDescriptor
    private final ArtifactUnpacker unpacker
    private final ArtifactRootVerification verifyRoot
    private final Downloader downloader
    private File downloadRoot
    private final Provider<Boolean> isOffline
    private final Provider<File> gradleUserHomeDir
    private final File projectDir
}
