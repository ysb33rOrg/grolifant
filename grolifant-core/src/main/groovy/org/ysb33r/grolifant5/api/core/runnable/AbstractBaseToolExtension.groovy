/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.errors.ConfigurationException
import org.ysb33r.grolifant5.api.errors.ExecutionException
import org.ysb33r.grolifant5.api.core.Transform

import java.util.concurrent.Callable
import java.util.function.Function
import java.util.regex.Pattern

/**
 * Use as a base class for project and task extensions that will wrap tools.
 *
 * <p> This base class will also enable extensions to discover whether they are inside a task or a
 * project.
 *
 * @param <T >       The extension class that extends this base class.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractBaseToolExtension<T extends AbstractBaseToolExtension>
    extends CombinedProjectTaskExtensionBase<T>
    implements BaseToolLocation {

    /**
     * A provider for a resolved executable.
     *
     * @return File provider.
     */
    Provider<File> getExecutable() {
        this.executableProvider
    }

    /**
     * Locate an executable by a local path.
     *
     * @param path Something resolvable to a {@link File}.
     */
    void executableByPath(Object path) {
        this.path = path
        this.searchPath = null
        unsetNonPaths()
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /**
     * Locate executable by searching the current environmental search path.
     *
     * @param baseName The base name of the executable
     */
    void executableBySearchPath(Object baseName) {
        this.path = null
        this.searchPath = baseName
        unsetNonPaths()
        if (task) {
            noGlobalExecSearch = true
        }
    }

    /**
     * When searching for executables, the order in which to use check for file extensions.
     *
     * @param order List of extensions.
     */
    void setWindowsExtensionSearchOrder(Iterable<String> order) {
        this.windowSearchOrder.clear()
        this.windowSearchOrder.addAll(order)
    }

    void setWindowsExtensionSearchOrder(String... order) {
        this.windowSearchOrder.clear()
        this.windowSearchOrder.addAll(order)
    }

    /**
     * The order in which extensions will be checked for locating an executable on Windows.
     *
     * The default is to use whatever is in the {@code PATHEXT} environmental variable.
     *
     * @return
     */
    List<String> getWindowsExtensionSearchOrder() {
        this.windowSearchOrder ?: (projectExtension ? projectExtension.windowSearchOrder : [])
    }

    /**
     * If configured by version returns that, otherwise it might run the executable to obtain the
     * version.
     *
     * @return Provider to a version string.
     */
    Provider<String> resolvedExecutableVersion() {
        this.executableVersionProvider
    }

    /** Attach this extension to a project
     *
     * @param projectOperations ProjectOperations instance.
     */
    protected AbstractBaseToolExtension(org.ysb33r.grolifant5.api.core.ProjectOperations projectOperations) {
        super(projectOperations)
        this.windowSearchOrder = initWindowsSearchOrder()
        this.executableProvider = initExecutableResolver(projectOperations)
        this.executableVersionProvider = projectOperations.providerTools
            .property(String, initExecutableVersionProvider(projectOperations))
    }

    /** Attach this extension to a task
     *
     * @param task Task to attach to.
     * @param projectOperations ProjectOperations instance.
     * @param projectExtName Extension that is attached to the project.
     *   Pass {@Link #noProjectExtension} is project extension is available or required.
     */
    protected AbstractBaseToolExtension(Task task, ProjectOperations projectOperations, T projectExtension) {
        super(task, projectOperations, projectExtension)
        if (projectExtension == null) {
            this.windowSearchOrder = initWindowsSearchOrder()
        }
        this.executableProvider = initExecutableResolver(projectOperations)
        this.executableVersionProvider = projectOperations.providerTools
            .property(String, initExecutableVersionProvider(projectOperations))
    }

    /**
     * Pass to the task constructor when no project extension will be available or required.
     */
    @Internal
    protected T noProjectExtension() { null }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link org.ysb33r.grolifant5.api.core.ExecTools#parseVersionFromOutput} as a helper to implement this method.
     *
     * @return Version string.
     * @throws ConfigurationException if configuration is not correct or version could not be determined.
     */
    abstract protected String runExecutableAndReturnVersion() throws ConfigurationException

    /**
     * Unset both the fixed path and search path.
     *
     * THis can be used by derived classes to ensure that paths are not included in exectuble resolution
     */
    protected void unsetPaths() {
        this.path = null
        this.searchPath = null
    }

    /**
     * This is used when path or search path is set to unset other search mechanisms.
     *
     * This is primarily intended to be used within Grolifant itself as part of its base classes and MUST be overridden
     * in those.
     *
     * The default operation is NOOP.
     */
    protected void unsetNonPaths() {
    }

    /**
     * If a path has been set instead of a version, resolve the path.
     *
     * @return Path to executable or {@code null}
     */
    protected File executablePathOrNull() {
        if (task && noGlobalExecSearch || !task) {
            this.path ? projectOperations.fsOperations.file(path).canonicalFile : null
        } else if (task && projectExtension) {
            projectExtension.executablePathOrNull()
        } else {
            failNoExecConfig()
        }
    }

    /**
     * If a search path has been set, resolve the location by searching path
     *
     * @return Path to executable or {@code null} if search path was not set
     */
    protected File executableSearchPathOrNull() {
        if (task && noGlobalExecSearch || !task) {
            if (searchPath) {
                def baseName = projectOperations.stringTools.stringize(searchPath)
                File found
                if (IS_WINDOWS) {
                    final targets = Transform.toList(
                        windowSearchOrder,
                        { "${baseName}${it}".toString() } as Function<String, String>
                    )
                    for (String target : targets) {
                        found = OperatingSystem.current().findInPath(target)
                        if (found) {
                            break
                        }
                    }
                } else {
                    found = OperatingSystem.current().findInPath(baseName)
                }
                if (found == null) {
                    throw new ExecutionException("Could not locate ${baseName} in search path")
                } else {
                    return found
                }
            } else {
                null
            }
        } else if (task && projectExtension) {
            projectExtension.executableSearchPathOrNull()
        } else {
            failNoExecConfig()
        }
    }

    /**
     * Resolves an executable to a physical local file location.
     *
     * @return Local location.
     */
    protected File resolveExecutable() {
        if (path) {
            executablePathOrNull()
        } else if (searchPath) {
            executableSearchPathOrNull()
        } else {
            null
        }
    }

    /**
     * Overrides the default way or determining what the version is of a tool.
     *
     * @param provider Provider of the version.
     */
    protected void setExecutableVersionProvider(Provider<String> provider) {
        this.executableVersionProvider.set(provider)
    }

    /**
     * Call this when there is no preset way to local the executable.
     */
    protected void failNoExecConfig() {
        throw new ConfigurationException('No method was configured to locate an executable')
    }

    private Provider<String> initExecutableVersionProvider(ProjectOperations po) {
        po.provider(new Callable<String>() {
            @Override
            String call() throws Exception {
                runExecutableAndReturnVersion()
            }
        })
    }

    private Provider<File> initExecutableResolver(ProjectOperations po) {
        Task t = task
        AbstractBaseToolExtension pe = (AbstractBaseToolExtension) projectExtension
        po.provider(new Callable<File>() {
            @Override
            File call() throws Exception {
                if (t && noGlobalExecSearch) {
                    resolveExecutable()
                } else if (t && pe) {
                    pe.resolveExecutable()
                } else if (t && !pe) {
                    failNoExecConfig()
                } else {
                    resolveExecutable()
                }
            }
        })
    }

    private static List<String> initWindowsSearchOrder() {
        if (IS_WINDOWS) {
            String path = System.getenv('PATHEXT')
            final List<String> entries = []
            if (path != null) {
                for (String entry : path.split(Pattern.quote(OperatingSystem.current().pathSeparator))) {
                    entries.add(entry)
                }
            } else {
                entries.addAll('.COM', '.EXE', '.BAT', '.CMD')
            }
            entries
        } else {
            null
        }
    }

    @Internal
    protected boolean noGlobalExecSearch = false

    private Object path
    private Object searchPath
    private final List<String> windowSearchOrder
    private final Provider<File> executableProvider
    private final Property<String> executableVersionProvider

    private static final boolean IS_WINDOWS = org.ysb33r.grolifant5.api.core.OperatingSystem.current().windows
}
