/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec;

/**
 * Configures a task that will run a non-JVM script-based execution.
 *
 * @author Schalk W. Cronj̧é
 * @since 2.0
 */
abstract public class AbstractScriptExecTask<T extends AbstractScriptExecSpec<T>>
        extends AbstractExecTask<T> implements ExecutableScript<T> {

    /**
     * Configures the script along with its arguments.
     *
     * @param configurator An action to configure the script
     */
    @Override
    public void script(Action<ScriptSpec> configurator) {
        getNativeExecSpec().script(configurator);
    }

    /**
     * Configures script elements by Groovy closure.
     *
     * @param specConfigurator Configurating closure.
     */
    @Override
    public void script(@DelegatesTo(ScriptSpec.class) Closure<?> specConfigurator) {
        getNativeExecSpec().script(specConfigurator);
    }

    protected AbstractScriptExecTask() {
        super();
    }
}
