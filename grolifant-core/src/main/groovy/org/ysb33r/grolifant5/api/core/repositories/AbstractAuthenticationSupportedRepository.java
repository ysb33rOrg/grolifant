/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.repositories;

import org.gradle.api.Action;
import org.gradle.api.artifacts.repositories.ArtifactRepository;
import org.gradle.api.artifacts.repositories.AuthenticationContainer;
import org.gradle.api.artifacts.repositories.AuthenticationSupported;
import org.gradle.api.artifacts.repositories.PasswordCredentials;
import org.gradle.api.credentials.Credentials;
import org.ysb33r.grolifant5.api.core.ProjectOperations;

/**
 * Base class for creating repository types that optionally support authentication.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public class AbstractAuthenticationSupportedRepository implements ArtifactRepository, AuthenticationSupported, AuthenticationSupportedRepositoryContent {
    /**
     * Base class constructor
     * insance
     *
     * @param po {@link ProjectOperations}
     */
    protected AbstractAuthenticationSupportedRepository(ProjectOperations po) {
        this.authentications = po.getRepositoryTools().authenticationContainer();
    }

    @Override
    public PasswordCredentials getCredentials() {
        return getIfPasswordCredentials();
    }

    @Override
    public void credentials(Action<? super PasswordCredentials> action) {
        PasswordCredentials pw = getIfPasswordCredentials();
        action.execute(pw);
    }

    @Override
    public <T extends Credentials> T getCredentials(final Class<T> aClass) {
        if (this.repCredentials == null) {
            try {
                this.repCredentials = aClass.newInstance();
            } catch (InstantiationException e) {
                throw new IllegalArgumentException("Cannot instantiate " + String.valueOf(aClass), e);
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException("Access failure for " + String.valueOf(aClass), e);
            }
        }

        try {
            return aClass.cast(this.repCredentials);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Current credentials are not of type " + String.valueOf(aClass), e);
        }
    }

    /**
     * Implements the repository method added in Gradle 6.6.
     *
     * @param aClass Credentials class
     */
    public void credentials(Class<? extends Credentials> aClass) {
        try {
            repCredentials = aClass.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Cannot set credentials for" + String.valueOf(aClass), e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Cannot set credentials for" + String.valueOf(aClass), e);
        }
    }

    @Override
    public <T extends Credentials> void credentials(Class<T> aClass, Action<? super T> action) {
        action.execute(getCredentials(aClass));
    }

    @Override
    public void authentication(Action<? super AuthenticationContainer> action) {
        action.execute(this.authentications);
    }

    @Override
    public AuthenticationContainer getAuthentication() {
        return this.authentications;
    }

    /**
     * Check is any credentials has been accessed.
     *
     * @return {@code true} is any credentials has been accessed
     */
    public boolean hasCredentials() {
        return this.repCredentials != null;
    }

    /**
     * This is currently a NOOP.
     *
     * @param action Ignored action.
     */
    @Override
    public void content(Action action) {
    }

    private void initCredentialsIfNull() {
        if (this.repCredentials == null) {
            this.repCredentials = new SimpleRepositoryCredentials();
        }
    }

    private PasswordCredentials getIfPasswordCredentials() {
        initCredentialsIfNull();
        if (this.repCredentials instanceof PasswordCredentials) {
            return (PasswordCredentials) this.repCredentials;
        } else {
            throw new IllegalArgumentException("Current credentials do not implement PasswordCredentials");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private Credentials repCredentials;
    private final AuthenticationContainer authentications;
}
