/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader

import groovy.transform.CompileStatic

/**
 * Provides information on where a downloaded file ends up.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DownloadedLocalFile {
    final File distributionDir
    final File downloadedFile
    final File markerFile
    final URI distributionUrl

    /**
     *
     * @param dir Directory used for the package / distribution or single file.
     * @param file Location of the downloaded file.
     * @param uri The distribution URL.
     */
    DownloadedLocalFile(final File dir, final File file, final URI uri) {
        this.distributionDir = dir
        this.downloadedFile = file
        this.markerFile = new File(file.parentFile, file.name + '.ok')
        this.distributionUrl = uri
    }
}
