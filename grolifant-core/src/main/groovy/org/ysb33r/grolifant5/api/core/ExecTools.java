/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Action;
import org.gradle.process.ExecResult;
import org.gradle.process.ExecSpec;
import org.gradle.process.JavaExecSpec;
import org.ysb33r.grolifant5.api.core.downloader.Downloader;
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec;
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;

import java.io.File;
import java.util.function.Function;

/**
 * Various tools to deal with non-JVM, out-of-process executables.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ExecTools {

    /**
     * Manage the output of external processes
     */
    enum OutputType {
        /**
         * Produce no output and capture no output.
         */
        QUIET,

        /**
         * Capture output, but produce no output.
         */
        CAPTURE,

        /**
         * Forward output, but do not capture it.
         */
        FORWARD,

        /**
         * Forward output and capture it.
         */
        FORWARD_AND_CAPTURE,

        /**
         * Use the configured stream to send output to.
         * <p>
         *     No additional capturing will be performed and no output will be forwarded.
         *     The caller is responsible for closing the stream.
         * </p>
         */
        USE_CONFIGURED_STREAM
    }

    /**
     * Executes the specified external process.
     *
     * <p>
     *     Accepts whatever is already configured on the outputs.
     *     Typically this will output both standard error and standard out, and captures nothing.
     * </p>
     *
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    default ExecOutput exec(Action<? super ExecSpec> action) {
        return exec(OutputType.USE_CONFIGURED_STREAM, OutputType.USE_CONFIGURED_STREAM, action);
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecResult} that can be used to check if the execution worked.
     * @since 5.0
     */
    ExecOutput exec(OutputType stdout, OutputType stderr, Action<? super ExecSpec> action);

    /**
     * Returns something that looks like an {@link ExecSpec}.
     *
     * @return Instance of an executable specification.
     */
    ExecSpec execSpec();

    /**
     * Creates a {@link AppRunnerSpec}.
     * <p>
     *   This is primarily used internally by classes that implements execution specifications for non-JVM processes.
     * </p>
     *
     * @return Implementation of {@link AppRunnerSpec}
     */
    AppRunnerSpec appRunnerSpec();

    /**
     * Returns an implementation that is optimised for the running version of Gradle.
     *
     * @return Instance of {@link CommandEntryPoint}. Never {@code null}
     */
    CommandEntryPoint commandEntryPoint();

    /**
     * Creates a new downloader for downloading packages / distributions.
     *
     * @param distributionName Name of distribution to be downloaded.
     * @return Instance of {@link Downloader} that is optimised for the running Gradle version.
     */
    Downloader downloader(final String distributionName);

    /**
     * Executes the specified external java process.
     *
     * <p>Outputs both standard error and standard out, but captures nothing.</p>
     *
     * @param action Configures a {@link JavaExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    default ExecOutput javaexec(Action<? super JavaExecSpec> action) {
        return javaexec(OutputType.FORWARD, OutputType.FORWARD, action);
    }

    /**
     * Executes the specified external process.
     *
     * @param stdout How to capture standard output.
     * @param stderr How to capture error output.
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    ExecOutput javaexec(OutputType stdout, OutputType stderr, Action<? super JavaExecSpec> action);

    /**
     * Executes the specified external process on-demand.
     *
     * @param action Configures an {@link ExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    ExecOutput provideExec(Action<? super ExecSpec> action);

    /**
     * Executes the specified external java process on-demand.
     *
     * @param action Configures a {@link JavaExecSpec}
     * @return {@link ExecOutput} that can be used to check if the execution worked.
     * @since 5.0
     */
    ExecOutput provideJavaExec(Action<? super JavaExecSpec> action);

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser  A parser for the output of running the executable which could extract the version.
     * @return The version string.
     */
    default String parseVersionFromOutput(
            Iterable<String> argsForVersion,
            File executablePath,
            Function<String, String> versionParser
    ) {
        return parseVersionFromOutput(
                argsForVersion,
                executablePath,
                versionParser,
                null
        );
    }

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser  A parser for the output of running the executable which could extract the version.
     * @param configurator   Additional configurator to customise the execution specification.
     * @return The version string.
     */
    String parseVersionFromOutput(
            Iterable<String> argsForVersion,
            File executablePath,
            Function<String, String> versionParser,
            Action<ExecSpec> configurator
    );
}
