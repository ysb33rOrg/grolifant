/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Internal;

import java.io.File;

/**
 * @param <T> A class that extends {@link AbstractToolExtension}
 * @param <E> An execution specification class that extends {@link AbstractExecSpec}
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
abstract public class AbstractExecWrapperWithExtensionTask
        <T extends AbstractToolExtension<T>, E extends AbstractExecSpec<E>>
        extends AbstractExecWrapperTask<E> {

    protected AbstractExecWrapperWithExtensionTask() {
        super();
    }

    /**
     * Access to the task extension of the tool type.
     *
     * @return Extension
     */
    @Internal
    abstract protected T getToolExtension();
}
