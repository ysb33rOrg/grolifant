/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.provider.Provider;

/** Resolves a property within a certain order
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface PropertyResolveOrder {
    /**
     * A provider to a property.
     *
     * @param po ProjectOperations
     * @param name Name of property
     * @param configurationTimeSafe Whether this property should be configuration time-safe.
     * @return Provider to property
     */
   Provider<String> resolve(ProjectOperations po, String name, boolean configurationTimeSafe);
}
