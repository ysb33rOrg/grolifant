/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;

import java.util.List;

/**
 * Returns all command-line arguments within a specific context.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface AllArgsProvider {

    /**
     * A provider to arguments that will be inserted before any supplied arguments.
     *
     * @return Arguments provider
     */
    @Input
    Provider<List<String>> getPreArgs();

    /**
     * All defined arguments, plus all arguments providers via the command-line argument providers.
     *
     * @return Provider to a fully-resolved set of arguments.
     */
    @Internal
    Provider<List<String>> getAllArgs();
}
