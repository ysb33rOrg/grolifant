/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;

/**
 * Describes the data stream redirections for a process including exit checks.
 * <p>
 * Similar to {@link org.gradle.process.BaseExecSpec} except to {@code getCommandLine / setCommandLine}.
 * </p>
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ProcessExecutionSpec {

    /**
     * Tells whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @return whether a non-zero exit value is ignored, or an exception thrown.
     */
    boolean isIgnoreExitValue();

    /**
     * Sets whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @param ignoreExitValue - whether a non-zero exit value is ignored, or an exception thrown
     */
    void setIgnoreExitValue(boolean ignoreExitValue);

    /**
     * After execution has been completed, run this function.
     *
     * <p>
     * This requires at least one of standard output or error output to be captured.
     *
     * This will be still be run if there was an error during execution.
     * </p>
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     *
     * @param resultProcessor Function to execute.
     *                        It is passed the result of the execution.
     * @since 5.0
     */
    void afterExecute(Action<ExecOutput> resultProcessor);

    /**
     * Configure the process output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    void output(Action<OutputStreamSpec> configurator);

    /**
     * Configure the process output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    void output(@DelegatesTo(OutputStreamSpec.class) Closure<?> configurator);

    /**
     * Configure the process error output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    void errorOutput(Action<OutputStreamSpec> configurator);

    /**
     * Configure the process error output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    void errorOutput(@DelegatesTo(OutputStreamSpec.class) Closure<?> configurator);

    /**
     * The output stream configuration.
     *
     * @return Specification.
     */
    OutputStreamSpec getOutput();

    /**
     * The output stream configuration.
     *
     * @return Specification.
     */
    OutputStreamSpec getErrorOutput();
}
