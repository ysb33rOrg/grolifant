/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Nested;
import org.gradle.api.tasks.TaskAction;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;

/**
 * Configures a task that will run a non-JVM execution.
 *
 * @author Schalk W. Cronj̧é
 * @since 2.0
 */
abstract public class AbstractExecTask<T extends AbstractExecSpec<T>> extends GrolifantDefaultTask
        implements Executable<T>, ConfigCacheSafeOperations {
    /**
     * Configures the native entrypoint.
     *
     * @param configurator An action to configure the external executable's entrypoint.
     */
    @Override
    public void entrypoint(Action<ExecutableEntryPoint> configurator) {
        getNativeExecSpec().entrypoint(configurator);
    }

    /**
     * Configures the native entrypoint.
     *
     * @param configurator A closure to configure the external executable's entrypoint.
     */
    @Override
    public void entrypoint(@DelegatesTo(ExecutableEntryPoint.class) Closure<?> configurator) {
        getNativeExecSpec().entrypoint(configurator);
    }

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     */
    @Override
    public void runnerSpec(Action<CmdlineArgumentSpec> configurator) {
        getNativeExecSpec().runnerSpec(configurator);
    }

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     */
    @Override
    public void runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator) {
        getNativeExecSpec().runnerSpec(configurator);
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator An action to configure the execution.
     */
    @Override
    public void process(Action<ProcessExecutionSpec> configurator) {
        getNativeExecSpec().process(configurator);
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     */
    @Override
    public void process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator) {
        getNativeExecSpec().process(configurator);
    }

    /**
     * Access to the entrypoint
     *
     * @return An instance that implements {@link ExecutableEntryPoint}.
     * @since 5.2
     */
    @Override
    @Internal
    public ExecutableEntryPoint getEntrypoint() {
        return getNativeExecSpec().getEntrypoint();
    }

    /**
     * Access to the runner specification.
     *
     * @return An instance that implements {@link CmdlineArgumentSpec}.
     * @since 5.2
     */
    @Override
    @Internal
    public CmdlineArgumentSpec getRunnerSpec() {
        return getNativeExecSpec().getRunnerSpec();
    }

    /**
     * Access to the process specification.
     *
     * @return An instance that implements {@link ProcessExecutionSpec}.
     * @since 5.2
     */
    @Override
    @Internal
    public ProcessExecutionSpec getProcess() {
        return getNativeExecSpec().getProcess();
    }

    protected AbstractExecTask() {
    }

    /**
     * The actual implementation of the execution specification.
     *
     * @return Instance, but never {@code null}.
     */
    @Nested
    abstract protected T getNativeExecSpec();

    @TaskAction
    protected void exec() {
        ExecOutput result = getNativeExecSpec().submitAsExec();
        processOutput(result);
        result.assertNormalExitValue();
    }

    /**
     * Process the output from an {@link ExecOutput}.
     *
     * @param output Output to process.
     *
     * @since 5.0
     */
    protected void processOutput(ExecOutput output) {
        getNativeExecSpec().processOutput(output);
    }

}
