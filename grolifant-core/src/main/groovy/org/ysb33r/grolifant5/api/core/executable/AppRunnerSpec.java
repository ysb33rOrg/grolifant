/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Input;
import org.gradle.process.ExecSpec;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecSpec;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;

import java.util.Collection;

/**
 *
 * A specification to something non-JVM that can be executed out-of-process and
 * can then be used to copy to other methods in the Gradle API that provides a
 * {@link org.gradle.process.ExecSpec} in the parameters.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface AppRunnerSpec {
    /** The default command-line block name.
     *
     */
    public static final String DEFAULT_BLOCK = "default";

    /**
     * Copies these options to the given options.
     *
     * @param target The target options.
     */
    void copyTo(ExecSpec target);

    /**
     * Configures the default command-line block,
     *
     * @param configurator Configurator.
     */
    default void configureCmdline(Action<? extends CmdlineArgumentSpec> configurator) {
        configureCmdline(DEFAULT_BLOCK,configurator);
    }

    /**
     * Configures the default command-line block,
     *
     * @param configurator Configurator.
     */
    default void configureCmdline(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator) {
        configureCmdline(DEFAULT_BLOCK,configurator);
    }

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     * @param cmdBlock THe command block to configure
     * @param configurator Configurator.
     */
    void configureCmdline(final String cmdBlock, Action<? extends CmdlineArgumentSpec> configurator);

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     * @param cmdBlock THe command block to configure
     * @param configurator Configurator.
     */
    void configureCmdline(final String cmdBlock, @DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator);

    /**
     * Configures a {@link ExecutableEntryPoint} instance.
     * @param configurator Configurator.
     */
    void configureEntrypoint(Action<ExecutableEntryPoint> configurator);

    /**
     * Configures a {@link ExecutableEntryPoint} instance.
     * @param configurator Configurator.
     */
    void configureEntrypoint(@DelegatesTo(ExecutableEntryPoint.class) Closure<?> configurator);

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator  Configurator.
     */
    void configureProcess(Action<ProcessExecutionSpec> configurator);

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator  Configurator.
     */
    void configureProcess(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator);

    /**
     * Access to the entrypoint
     *
     * @return An instance that implements {@link ExecutableEntryPoint}.
     *
     * @since 5.2
     */
    ExecutableEntryPoint getEntrypoint();

    /**
     * Access to the runner specification.
     *
     * @return An instance that implements {@link CmdlineArgumentSpec}.
     *
     * @since 5.2
     */
    CmdlineArgumentSpec getRunnerSpec();

    /**
     * Access to the process specification.
     *
     * @return An instance that implements {@link ProcessExecutionSpec}.
     *
     * @since 5.2
     */
    ProcessExecutionSpec getProcess();

    /**
     * Adds a command-line processor that will process command-line arguments in a specific order.
     *
     * For instance in a script, one want to process the exe args before the script args.
     *
     * In a system that has commands and subcommands, one wants to process this in the order of exe args, command args,
     * and then subcommand args.
     *
     * This method allows the implementor to control the order of processing  for all the groupings.
     *
     * @param name Name of command-line processor.
     * @param order Order in queue to process.
     * @param processor The specific grouping.
     */
    void addCommandLineProcessor(String name, Integer order, CmdlineArgumentSpec processor);

    /**
     * Provides direct access to the list of command-line processors.
     *
     * In many cases there will only be one item in the list which is for providing arguments to the executable
     * itself. Some implementations will have more. Implementors can use this interface to manipulate order of
     * evaluation.
     *
     * @return Collection of command-line processors. Can be empty, but never {@code null}.
     */
    Collection<CmdLineArgumentSpecEntry> getCommandLineProcessors();

    /**
     * A unique string which determines whether there were any changes.
     *
     * @return Signature string
     */
    @Input
    String getExecutionSignature();

    /**
     * Submit a specification for execution.
     *
     * @since 5.0
     *
     * @param spec Specification
     * @return Result of native execution.
     */
    ExecOutput submitAsExec(AbstractExecSpec<?> spec);

    /**
     * Submit a specification for execution.
     * Allows an additional action to configure the final {@link ExecSpec} after all of the other configuration
     * has been completed.
     *
     * @since 5.2
     *
     * @param spec Specification
     * @param additionalConfiguration Additional configurator.
     * @return Result of native execution.
     */
    ExecOutput submitAsExec(AbstractExecSpec<?> spec, Action<ExecSpec> additionalConfiguration);
}
