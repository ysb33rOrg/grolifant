/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import javax.crypto.SealedObject;
import java.io.Serializable;

/**
 * A way to try to keep credentials a bit more secure.
 *
 * <p>The default implementation is not fool proof, but does make some effort to keep the string that is in
 * memory encrypted with an arbitrary key.</p>
 *
 * @author Schalk W. Cronjé
 * @since 5.2
 */
public interface SimpleSecureString extends Serializable {

    /**
     * Returns the encrypted object.
     *
     * @return Object
     */
    SealedObject getSealedObject();
}
