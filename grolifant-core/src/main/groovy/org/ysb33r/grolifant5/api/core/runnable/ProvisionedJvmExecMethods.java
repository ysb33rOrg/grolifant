/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.Action;
import org.gradle.process.ExecResult;
import org.ysb33r.grolifant5.api.core.ProjectOperations;

/**
 * Implements the {@code exec} methods from {@link JvmExecMethods}, leaving it to the plugin author to
 * implement a method that can create an appropriate execution specification.
 * <p>Expects that the class implementing the interface can supply an instance of {@link ProjectOperations}</p>.
 *
 * @param <E> Execution specification which must extends {@code AbstractJvmExecSpec}.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ProvisionedJvmExecMethods<E extends AbstractJvmExecSpec<E>> extends JvmExecMethods<E> {

    /**
     * Grolifant's configuration cache-safe operations.
     *
     * @return instance.
     */
    ProjectOperations getProjectOperations();

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     */
    E createExecSpec();

    @Override
    default ExecResult exec(E spec) {
        return getProjectOperations().javaexec(execSpec -> spec.copyTo(execSpec));
    }

    @Override
    default ExecResult exec(Action<E> specConfigurator) {
        E spec = createExecSpec();
        specConfigurator.execute(spec);
        return exec(spec);
    }
}
