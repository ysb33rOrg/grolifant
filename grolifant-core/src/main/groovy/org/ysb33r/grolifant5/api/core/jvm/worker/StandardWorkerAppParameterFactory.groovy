/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm.worker

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.remote.worker.StandardWorkerAppParameters

/**
 * Standard parameters for running a JVM process inside a worker.
 *
 * THis is used when the process has a {@code main} entrypoint.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class StandardWorkerAppParameterFactory implements WorkerAppParameterFactory<StandardWorkerAppParameters> {
    /**
     *
     * @param execSpec Java execution specification that can be used for setting worker execution details.
     * @return A parameter configuration.
     */
    @Override
    StandardWorkerAppParameters createAndConfigure(WorkerExecSpec execSpec) {
        new StandardWorkerAppParameters(
            ignoreExitValue: execSpec.javaExecSpec.ignoreExitValue,
            mainClass: execSpec.jvmEntrypoint.mainClass.get(),
            arguments: execSpec.applicationArguments
        )
    }
}
