/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Action
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileCopyDetails
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Provider
import org.gradle.internal.os.OperatingSystem
import org.ysb33r.grolifant5.api.core.CheckSumVerification
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.LegacyLevel
import org.ysb33r.grolifant5.api.errors.ChecksumFailedException
import org.ysb33r.grolifant5.api.errors.DistributionFailedException
import org.ysb33r.grolifant5.spi.unpackers.GrolifantUnpacker

import javax.annotation.Nullable
import java.security.MessageDigest
import java.util.concurrent.Callable
import java.util.regex.Pattern

/** Common functionality to be able to download a SDK and use it within Gradle
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class AbstractDistributionInstaller implements DistributionInstaller {

    public static final boolean IS_WINDOWS = OperatingSystem.current().windows
    public static final boolean IS_MACOSX = OperatingSystem.current().macOsX

    ArtifactRootVerification artifactRootVerification
    ArtifactUnpacker artifactUnpacker

    /** Set candidate name for SdkMan if the latter should be searched for installed versions
     *
     * @param sdkCandidateName SDK Candidate name. This is the same names that will be shown when
     *   running {@code sdk list candidates} on the script-line.
     */
    void setSdkManCandidateName(final String sdkCandidateName) {
        this.sdkManCandidateName = sdkCandidateName
    }

    /**
     * SDKman candidate name for distribution.
     *
     * @return Name of SDKman candidate. Can be {@code null}.
     */
    @Override
    String getSdkManCandidateName() {
        this.sdkManCandidateName
    }

    /** Add patterns for files to be marked exe,
     *
     * Calling this method multiple times simply appends for patterns
     * @param relPaths One or more ANT-stype include patterns
     */
    void addExecPattern(String... relPaths) {
        this.execPatterns.addAll(relPaths.toList())
    }

    /** Set a checksum that needs to be verified against downloaded archive
     *
     * @param cs SHA-256 Hex-encoded checksum
     */
    void setChecksum(final String cs) {
        if (cs.length() != 64 || !(cs ==~ /[\p{Digit}\p{Alpha}]{64}/)) {
            throw new IllegalArgumentException('Not a valid SHA-256 checksum')
        }
        this.checksum = cs.toLowerCase()
    }

    /** Returns the location which is the top or home folder for a distribution.
     *
     *  This value is affected by {@link #setDownloadRoot(java.io.File)} and
     *  the parameters passed in during construction time.
     *
     * @return Location of the distribution.
     */
    Provider<File> getDistributionRoot(String version) {
        configCacheSafeOperations.providerTools().provider(new Callable<File>() {
            @Override
            File call() throws Exception {
                resolveDistributionLocationForVersion(version)
            }
        })
    }

    /** Sets a download root directory for the distribution.
     *
     * If not supplied the default is to use the Gradle User Home.
     * This method is provided for convenience and is mostly only used for testing
     * purposes.
     *
     * The folder will be created at download time if it does not exist.
     *
     * @param downloadRootDir Any writeable directory on the filesystem.
     */
    void setDownloadRoot(Object downloadRootDir) {
        this.downloadRoot = downloadRootDir
    }

    /**
     * Locates a file within the distribution
     *
     * @param version Version of distribution to search.
     * @param fileRelPath Relative path to the distribution root.
     * @return Location of the file in the distribution.
     */
    @Override
    Provider<File> getDistributionFile(String version, String fileRelPath) {
        getDistributionRoot(version).map { File file ->
            new File(file, fileRelPath)
        }
    }

    /**
     * Resolves distribution location by looking in various locations.
     *
     * @param version Version to resolve
     * @return Location or {@code null} if nothing could be found.
     */
    protected File resolveDistributionLocationForVersion(String version) {
        // tag::download_logic[]
        File location = locateDistributionInCustomLocation(version) // <1>

        if (location == null && sdkManCandidateName) { // <2>
            location = getDistFromSdkMan(version)
        }

        location ?: getDistFromCache(version) // <3>
        // end::download_logic[]
    }

    /** Attempts to locate distribution in the list of SdkMan candidates.
     *
     * @return Location of the distribution if found in the candidate area.
     */
    protected File getDistFromSdkMan(String version) {
        File sdkCandidate = new File(
            "${System.getProperty('user.home')}/.sdkman/${sdkManCandidateName}/${version}"
        )

        sdkCandidate.exists() && sdkCandidate.directory ? sdkCandidate : null
    }

    /** Creates a distribution if it does not exist already.
     *
     * @return Location of distribution
     */
    protected File getDistFromCache(String version) {
        final distUri = uriFromVersion(version)
        final root = downloadRoot ? configCacheSafeOperations.fsOperations().file(downloadRoot) :
            configCacheSafeOperations.fsOperations().gradleUserHomeDir.get()
        final chk = checksum ?
            getCheckSumVerificationInstance(configCacheSafeOperations.stringTools().safeUri(distUri).toString()) :
            null

        final artifactDownloader = new ArtifactDownloader(
            distUri,
            root,
            configCacheSafeOperations,
            basePath,
            artifactRootVerification,
            artifactUnpacker,
            (CheckSumVerification) chk
        )

        artifactDownloader.getFromCache(
            "${distributionName}:${version}",
            configCacheSafeOperations.projectTools().offlineProvider.get(),
            this.downloadWorker
        )
    }

    /**
     * Project operations that can used during downloading.
     *
     */
    protected final ConfigCacheSafeOperations configCacheSafeOperations

    /**
     * Name of the distribution.
     *
     */
    protected final String distributionName

    /**
     * Creates setup for installing to a local cache.
     *
     * @param distributionName Descriptive name of the distribution
     * @param basePath Relative path below Gradle User Home to create cache for all version of this distribution type.
     * @param taskComponents Gradle project operations that this downloader can use.
     */
    protected AbstractDistributionInstaller(
        final String distributionName,
        final String basePath,
        final ConfigCacheSafeOperations taskComponents
    ) {
        this.distributionName = distributionName
        this.configCacheSafeOperations = taskComponents
        this.basePath = basePath
        this.downloadWorker = taskComponents.execTools().downloader(distributionName)
        this.artifactRootVerification = { AbstractDistributionInstaller abi, File distDir ->
            abi.verifyDistributionRoot(distDir)
        }.curry(this) as ArtifactRootVerification
        this.artifactUnpacker = { AbstractDistributionInstaller abi, File src, File dest ->
            abi.unpack(src, dest)
        }.curry(this) as ArtifactUnpacker
        this.tmpDir = taskComponents.fsOperations().buildDirDescendant("tmp/${this.class.name}")

        this.defaultUnpackerParameters = [
            msi: new UnpackParameters(taskComponents),
            dmg: new UnpackParameters(taskComponents)
        ]

        if (IS_WINDOWS) {
            this.defaultUnpackerParameters['msi'].environment(
                TMP: taskComponents.providerTools().environmentVariable('TMP'),
                TEMP: taskComponents.providerTools().environmentVariable('TEMP')
            )
        }
        if (IS_MACOSX) {
            this.defaultUnpackerParameters['dmg'].baseRelativePath = basePath
        }
    }

    /** Validates that the unpacked distribution is good.
     *
     * <p> The default implementation simply checks that only one directory should exist and then uses that.
     * You should override this method if your distribution in question does not follow the common practice of one
     * top-level directory.
     *
     * @param distDir Directory where distribution was unpacked to.
     * @return The directory where the real distribution now exists. In the default implementation it will be
     *   the single directory that exists below {@code distDir}.
     *
     * @throw {@link DistributionFailedException} if distribution failed to
     *   meet criteria.
     */
    protected File verifyDistributionRoot(final File distDir) {
        List<File> dirs = listDirs(distDir)
        if (dirs.empty) {
            throw new DistributionFailedException("${distributionName} " +
                'does not contain any directories. Expected to find exactly 1 directory.'
            )
        }
        if (dirs.size() != 1) {
            throw new DistributionFailedException(
                "${distributionName}  contains too many directories. " +
                    'Expected to find exactly 1 directory.'
            )
        }
        dirs[0]
    }

    /** Verifies the checksum (if provided) of a newly downloaded distribution archive.
     *
     * <p> Only SHA-256 is supported at this point in time.
     *
     * @param sourceUrl The URL/URI where it was downloaded from
     * @param localCompressedFile The location of the downloaded archive
     * @param expectedSum The expected checksum. Can be null in which case no checks will be performed.
     *
     * @throw {@link ChecksumFailedException} if the checksum did not match
     */
    protected void verifyDownloadChecksum(
        final String sourceUrl, final File localCompressedFile, final String expectedSum) {
        if (expectedSum != null) {
            String actualSum = calculateSha256Sum(localCompressedFile)
            if (this.checksum != actualSum) {
                localCompressedFile.delete()
                throw new ChecksumFailedException(
                    distributionName,
                    sourceUrl,
                    localCompressedFile,
                    expectedSum,
                    actualSum
                )
            }
        }
    }

    /** Provides a list of directories below an unpacked distribution
     *
     * @param distDir Unpacked distribution directory
     * @return List of directories. Can be empty is nothing was unpacked or only files exist within the
     *   supplied directory.
     */
    protected List<File> listDirs(File distDir) {
        configCacheSafeOperations.fsOperations().listDirs(distDir)
    }

    /** Unpacks a downloaded archive.
     *
     * <p> The default implementation supports the following formats:
     *
     * <ul>
     *   <li>zip</li>
     *   <li>tar</li>
     *   <li>tar.gz & tgz</li>
     *   <li>tar.bz2 & tbz</li>
     *   <li>tar.xz</li>
     * </ul>
     *
     *
     * @param srcArchive The location of the download archive
     * @param destDir The directory where the archive needs to be unpacked into
     */
    protected void unpack(final File srcArchive, final File destDir) {
        final FileTree archiveTree = compressedTree(srcArchive)
        final List<String> patterns = this.execPatterns

        final Action<FileCopyDetails> setExecMode = { FileCopyDetails fcd ->
            if (!fcd.directory) {
                applyExecutableToFileMode(fcd)
            }
        }

        configCacheSafeOperations.fsOperations().copy { CopySpec cs ->
            cs.with {
                from archiveTree
                into destDir

                if (!IS_WINDOWS && !patterns.empty) {
                    filesMatching(patterns, setExecMode)
                }
            }
        }
    }

    @CompileDynamic
    private static void applyExecutableToFileMode(FileCopyDetails fcd) {
        int exeBits = 0111
        if (LegacyLevel.PRE_8_3) {
            fcd.mode = fcd.mode | exeBits
        } else {
            final fp = fcd.permissions.toUnixNumeric() | exeBits
            fcd.permissions { it.unix(fp) }
        }
    }

    /**
     * Returns custom parameters for initialising an unpack engine.
     *
     * <p>
     *     Implementors can override this to return specific parameters depending on the kind of extension to unpack.
     * </p>
     *
     * <p>
     *     The default implementation sets a relative path for {@code .dmg} files and a basic environment
     *     for {@code .msi} files.
     * </p>
     *
     * @param extension Archive or compressed file extension.
     * @return Custom parameters or {@code null} if  no specific parameters exist.
     *
     * @since 5.0
     */
    protected @Nullable
    GrolifantUnpacker.Parameters unpackParametersForExtension(String extension) {
        this.defaultUnpackerParameters[extension.toLowerCase(Locale.US)]
    }

    /**
     * Extract the last extension in a file name.
     *
     * @param filename Filename
     * @return The right-most extension.
     *
     * @since 5.0
     */
    protected String extractFinalExtension(String filename) {
        extractExtension(filename, SINGLE_EXTENSION)
    }

    private String calculateSha256Sum(final File file) {
        file.withInputStream { InputStream content ->
            MessageDigest digest = MessageDigest.getInstance('SHA-256')
            content.eachByte(4096) { bytes, len -> digest.update(bytes, 0, len) }
            digest.digest().encodeHex().toString()
        }
    }

    private FileTree compressedTree(final File srcArchive) {
        final fs = configCacheSafeOperations.fsOperations()
        final String name = srcArchive.name.toLowerCase()
        if (name.endsWith('.zip')) {
            return fs.zipTree(srcArchive)
        } else if (name.endsWith('.tar')) {
            return fs.tarTree(srcArchive)
        } else if (name.endsWith('.tar.gz') || name.endsWith('.tgz')) {
            return fs.tarTree(fs.gzipResource(srcArchive))
        } else if (name.endsWith('.tar.bz2') || name.endsWith('.tbz')) {
            return fs.tarTree(fs.bzip2Resource(srcArchive))
        } else if (name =~ COMPRESSED_TAR) {
            final ext = extractExtension(name, COMPRESSED_TAR)
            final uncompressed = unpackArchiveByOtherExtension(srcArchive, ext)
            return fs.tarTree(uncompressed)
        }

        final ext = extractFinalExtension(name)
        final unpacked = unpackArchiveByOtherExtension(srcArchive, ext)
        fs.fileTree(unpacked)
    }

    private File unpackArchiveByOtherExtension(File srcArchive, String ext) {
        final loader = ServiceLoader.load(GrolifantUnpacker, this.class.classLoader)
        GrolifantUnpacker unpacker = (GrolifantUnpacker) (loader.find {
            GrolifantUnpacker it -> it.supportExtension(ext)
        })

        if (unpacker != null) {
            final params = unpackParametersForExtension(ext)
            final engine = params == null ? unpacker.create(configCacheSafeOperations) :
                unpacker.create(configCacheSafeOperations, params)
            File workDir = tmpDir.get()
            workDir.mkdirs()
            final File uncompressedFileDir = File.createTempFile(
                srcArchive.name.replaceAll(~/\.${ext}$/, ''),
                '$$$',
                workDir
            )
            uncompressedFileDir.deleteOnExit()
            engine.unpack(srcArchive, uncompressedFileDir)
            uncompressedFileDir
        } else {
            notSupported(ext)
        }
    }

    private void notSupported(String ext) {
        throw new DistributionFailedException(
            "Could not unpack file with extension '${ext}'. This might be due to grolifant5-unpacker-${ext}" +
                'not being on the classpath or that the specific archive/compression format is not supported.'
        )
    }

    private CheckSumVerification getCheckSumVerificationInstance(String textUri) {
        final String chk = checksum

        new CheckSumVerification() {
            @Override
            void verify(File downloadedTarget) {
                verifyDownloadChecksum(textUri, downloadedTarget, chk)
            }

            @Override
            String getChecksum() {
                chk
            }
        }
    }

    @CompileDynamic
    private String extractExtension(String filename, Pattern filePattern) {
        final matcher = filename =~ filePattern
        if (!matcher.matches()) {
            throw new DistributionFailedException("Cannot extract extension from '${filename}' using '${filePattern}'")
        }
        matcher[0][1]
    }

    private String sdkManCandidateName
    private String checksum
    private Object downloadRoot

    private final List<String> execPatterns = []
    private final String basePath
    private final Downloader downloadWorker
    private final Provider<File> tmpDir
    private final Map<String, UnpackParameters> defaultUnpackerParameters
    private static final Pattern COMPRESSED_TAR = ~/.+\.tar\.(\w+)$/
    private static final Pattern SINGLE_EXTENSION = ~/.+\.(\w+)$/

}
