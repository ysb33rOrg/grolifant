/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.GradleException;
import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.errors.UnexpectedNullException;
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException;
import org.ysb33r.grolifant5.internal.core.IterableUtils;
import org.ysb33r.grolifant5.internal.core.property.gradle.GradleProviderSpecialUtilities;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.ysb33r.grolifant5.internal.core.IterableUtils.treatAsIterable;

/**
 * Transformation utilities.
 *
 * <p>
 *     This includes:
 *     <ul>
 *         <li>Collections to other types of collections.</li>
 *         <li>Recursively unwrapping until an object can be converted to a predefined output type.</li>
 *     </ul>
 * </p>
 *
 * @since 5.0 (was added internal in 2.0)
 */
public class Transform {
    /**
     * Transforms a collection to a list.
     *
     * @param collection Input data
     * @param tx         Transformer
     * @return Transformed list
     */
    public static <I, O> List<O> toList(final Collection<I> collection, Function<I, O> tx) {
        return collection.stream().map(tx).collect(Collectors.toList());
    }

    /**
     * Transforms an iterable data set to a list.
     *
     * @param collection Input data
     * @param tx         Transformer
     * @return Transformed list
     */
    public static <I, O> List<O> toList(final Iterable<I> collection, Function<I, O> tx) {
        return IterableUtils.toList(collection).stream().map(tx).collect(Collectors.toList());
    }

    /**
     * Converts a Map to a List using a transformer.
     *
     * @param collection Input map
     * @param tx A function that takes a Map.Entry and return the appropriate transformed type
     * @return A list of transformed type.
     * @param <I> Key type of map.
     * @param <V>  Value type of map.
     * @param <O> Transformed type.
     *
     * @since 5.2
     */
    public static <I,V,O> List<O> toList(final Map<I,V> collection, Function<Map.Entry<I,V>,O> tx ) {
        return collection.entrySet().stream().map(tx).collect(Collectors.toList());
    }

    /**
     * Transforms a collection to a set.
     *
     * @param collection Input data
     * @param tx         Transformer
     * @return Transformed set
     */
    public static <I, O> Set<O> toSet(final Collection<I> collection, Function<I, O> tx) {
        return collection.stream().map(tx).collect(Collectors.toSet());
    }

    /**
     * Transforms an iterable data set to a set.
     *
     * @param collection Input data
     * @param tx         Transformer
     * @return Transformed set
     */
    public static <I, O> Set<O> toSet(final Iterable<I> collection, Function<I, O> tx) {
        return IterableUtils.toList(collection).stream().map(tx).collect(Collectors.toSet());
    }

    /**
     * Converts an item using a specified transformer.
     *
     * <p>
     * The following type are recursively unfolded before conversion:
     *
     * <ul>
     *     <li>{@link Callable}</li>
     *     <li>{@link Optional}</li>
     *     <li>{@link Provider}</li>
     *     <li>{@link Supplier}</li>
     * </ul>
     * </p>
     *
     * @param input       Object to convert
     * @param transformer Transformer. Is p-asses an unfolded value.
     * @param <OUT>       Type to convert to.
     * @return Converted value. Never {@code null}.
     * @throw UnexpectedNullException or UnsupportedConfigurationException
     */
    public static <OUT> OUT convertItem(final Object input, Function<Object, OUT> transformer) {
        return convertItemImpl(input, transformer, false);
    }

    /**
     * Converts an item using a specified transformer.
     * <p>
     * THe following types are recursively unfolded before conversion:
     *
     * <ul>
     *     <li>{@link Callable}</li>
     *     <li>{@link Optional}</li>
     *     <li>{@link Provider}</li>
     *     <li>{@link Supplier}</li>
     * </ul>
     *
     * Empty {@link Provider} and {@link Optional} instances are treated like nulls.
     *
     * </p>
     * @param input       Object to convert
     * @param transformer Transformer. Is passed an unfolded value.
     * @param <OUT>       Type to convert to.
     * @return Converted value or {@code null}.
     * @throw UnsupportedConfigurationException
     */
    public static <OUT> OUT convertItemOrNull(final Object input, Function<Object, OUT> transformer) {
        return convertItemImpl(input, transformer, true);
    }

    /**
     * Converts a collection type using a specified transformer
     * <p></p>
     * A number of non-iterable types are unfolded before conversion.
     * See {@link #convertItems}.
     * <p>
     * Collections are flattened.
     * Null values will raise exceptions.
     *
     * @param inputs      Objects to convert.
     * @param transformer Transformer. Is passed an unfolded value.
     * @param target      Collection to add to.
     * @param <OUT>       Type to convert to.
     * @return List of converted values. Can be empty, but never {@code null}. Will contain no {@code null} values.
     */
    public static <OUT> Collection<OUT> convertItems(
            final Collection<?> inputs,
            Collection<OUT> target,
            Function<Object, OUT> transformer
    ) {
        convertItemsImpl(inputs, transformer, target, false);
        return target;
    }

    /**
     * Converts a collection type using a specified transformer
     * <p></p>
     * A number of non-iterable types are unfolded before conversion.
     * See {@link #convertItems}.
     * <p>
     * Collections are flattened.
     * Null values are dropped.
     * Empty {@link Provider} and {@link Optional} instances are treated like nulls.
     * A null collection will be dropped.
     *
     * @param inputs      Objects to convert.
     * @param transformer Transformer. Is passed an unfolded value.
     * @param target      Collection to add to.
     * @param <OUT>       Type to convert to.
     * @return List of converted values. Can be empty, but never {@code null}. Will contain no {@code null} values.
     */
    public static <OUT> Collection<OUT> convertItemsDropNull(
            final Collection<?> inputs,
            Collection<OUT> target,
            Function<Object, OUT> transformer
    ) {
        convertItemsImpl(inputs, transformer, target, true);
        return target;
    }

    private static <OUT> OUT convertItemImpl(
            final Object input,
            Function<Object, OUT> transformer,
            boolean allowNull
    ) {
        try {
            if (input == null) {
                if (allowNull) {
                    return null;
                } else {
                    throw new UnexpectedNullException("Unexpected null value encountered");
                }
            } else if (input instanceof Provider) {
                return convertItemImpl(unwindProvider((Provider) input, allowNull), transformer, allowNull);
            } else if (input instanceof Optional) {
                return convertItemImpl(unwindOptional((Optional) input, allowNull), transformer, allowNull);
            } else if (input instanceof Supplier) {
                return convertItemImpl(((Supplier) input).get(), transformer, allowNull);
            } else if (input instanceof Callable) {
                return convertItemImpl(((Callable) input).call(), transformer, allowNull);
            } else if (input instanceof Collection) {
                throw new UnsupportedConfigurationException("Collections are not allowed");
            } else {
                return transformer.apply(input);
            }
        } catch (final UnexpectedNullException e) {
            throw e;
        } catch (final UnsupportedConfigurationException e) {
            throw e;
        } catch (final IllegalStateException e) {
            if (allowNull) {
                return null;
            } else {
                throw new UnexpectedNullException("Unexpected empty value encountered", e);
            }
        } catch (final NoSuchElementException e) {
            if (allowNull) {
                return null;
            } else {
                throw new UnexpectedNullException("Unexpected empty value encountered", e);
            }
        } catch (final GradleException e) {
            throw new UnsupportedConfigurationException("Item cannot be resolved", e);
        } catch (final Exception e) {
            throw new UnsupportedConfigurationException(
                    "Unexpected exception when trying to convert item of type " + input.getClass().getCanonicalName(),
                    e
            );
        }
    }

    private static <OUT> void convertItemsImpl(
            final Collection<?> inputs,
            final Function<Object, OUT> transformer,
            final Collection<OUT> collection,
            boolean dropNull
    ) {
        inputs.forEach(item -> {
            final Optional<Collection<?>> potentialList = GradleProviderSpecialUtilities.valuesForMultipleValueProvider(item);
            if (potentialList.isPresent()) {
                convertItemsImpl(potentialList.get(), transformer, collection, dropNull);
            } else if (item instanceof Map) {
                convertItemsImpl(((Map) item).values(), transformer, collection, dropNull);
            } else if (item instanceof Collection) {
                convertItemsImpl((Collection) item, transformer, collection, dropNull);
            } else if (item instanceof Provider) {
                resolveSingleItemOrIterableTo(unwindProvider((Provider) item, dropNull), transformer, collection, dropNull);
            } else if (item instanceof Optional) {
                resolveSingleItemOrIterableTo(unwindOptional((Optional) item, dropNull), transformer, collection, dropNull);
            } else if (item instanceof Supplier) {
                resolveSingleItemOrIterableTo(((Supplier) item).get(), transformer, collection, dropNull);
            } else if (item instanceof Callable) {
                try {
                    resolveSingleItemOrIterableTo(((Callable) item).call(), transformer, collection, dropNull);
                } catch (Exception e) {
                    throw new UnsupportedConfigurationException(
                            "Unexpected exception when trying to convert item of type " + item.getClass().getCanonicalName(),
                            e
                    );
                }
            } else {
                resolveSingleItemOrIterableTo(item, transformer, collection, dropNull);
            }
        });
    }

    private static <OUT> void resolveSingleItemOrIterableTo(
            Object item,
            final Function<Object, OUT> transformer,
            final Collection<OUT> collection,
            boolean dropNull
    ) {
        if (item instanceof Collection) {
            convertItemsImpl((Collection) item, transformer, collection, dropNull);
        } else if (treatAsIterable(item)) {
            convertItemsImpl(IterableUtils.toList((Iterable) item), transformer, collection, dropNull);
        } else {
            OUT value = convertItemImpl(item, transformer, dropNull);

            if (value != null) {
                collection.add(value);
            }
        }
    }

    private static Object unwindProvider(Provider<?> item, boolean allowNull) {
        if (allowNull) {
            return item.getOrNull();
        } else {
            return item.get();
        }
    }

    private static Object unwindOptional(Optional<?> item, boolean allowNull) {
        if (allowNull) {
            return item.orElse(null);
        } else {
            return item.get();
        }
    }
}
