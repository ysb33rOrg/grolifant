/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.ysb33r.grolifant5.api.core.jvm.ExecutionMode;

import java.util.Locale;

/**
 * Models for executing JVMN processes.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface JvmExecutionModel {
    /**
     * Sets the execution mode.
     * <p>
     * This allows for JVM tasks to be executed either on a worker queue inside the JVM using an isolated classpath,
     * ouside the JVM in a separate process, OR using a classic {@code javaexec}.
     *
     * <p>
     * If nothing is set, the default is {@link ExecutionMode#JAVA_EXEC}.
     *
     * @param em Execution mode.
     */
    void setExecutionMode(ExecutionMode em);

    /**
     * Sets the execution mode using a string
     * <p>
     * This allows for JVM tasks to be executed either on a worker queue inside the JVM using an isolated classpath,
     * ouside the JVM in a separate process, OR using a classic {@code javaexec}.
     *
     * <p>
     * If nothing is set, the default is {@ocde JAVA_EXEC}.
     *
     * @param em Execution mode.
     */
    default void setExecutionMode(final String em) {
        setExecutionMode(ExecutionMode.valueOf(em.toUpperCase(Locale.US)));
    }

    /**
     * Set whether other tasks in this project should be blocked until this task's worker queue completed.
     * <p>
     * Ignored if {@code executionMode == JAVA_EXEC}.
     *
     * @param mode {@code true} is blocking is required.
     */
    void setAwaitMode(Boolean mode);
}
