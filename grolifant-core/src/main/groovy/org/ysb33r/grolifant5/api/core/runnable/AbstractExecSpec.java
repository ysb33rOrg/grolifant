/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import groovy.transform.PackageScope;
import org.gradle.api.Action;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Nested;
import org.gradle.process.ExecSpec;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec;
import org.ysb33r.grolifant5.api.core.executable.CmdLineArgumentSpecEntry;
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecOutput;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;

import java.io.Serializable;
import java.util.Collection;

/**
 * Base class for creating execution specifications.
 *
 * @param <T> Class extending {@link AbstractExecSpec}.
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public class AbstractExecSpec<T extends AbstractExecSpec<T>> implements Executable<T>, Serializable {

    /**
     * Configures the native entrypoint.
     *
     * @param configurator An action to configure the external executable's entrypoint.
     */
    @Override
    public void entrypoint(Action<ExecutableEntryPoint> configurator) {
        appRunnerSpec.configureEntrypoint(configurator);
    }

    /**
     * Configures the native entrypoint.
     *
     * @param configurator A closure to configure the external executable's entrypoint.
     */
    @Override
    public void entrypoint(@DelegatesTo(ExecutableEntryPoint.class) Closure<?> configurator) {
        appRunnerSpec.configureEntrypoint(configurator);
    }

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     */
    @Override
    public void runnerSpec(Action<CmdlineArgumentSpec> configurator) {
        appRunnerSpec.configureCmdline(configurator);
    }

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     */
    @Override
    public void runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator) {
        appRunnerSpec.configureCmdline(configurator);
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator An action to configure the execution.
     */
    @Override
    public void process(Action<ProcessExecutionSpec> configurator) {
        appRunnerSpec.configureProcess(configurator);
    }

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     */
    @Override
    public void process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator) {
        appRunnerSpec.configureProcess(configurator);
    }

    /**
     * Access to the entrypoint
     *
     * @return An instance that implements {@link ExecutableEntryPoint}.
     * @since 5.2
     */
    @Override
    public ExecutableEntryPoint getEntrypoint() {
        return appRunnerSpec.getEntrypoint();
    }

    /**
     * Access to the runner specification.
     *
     * @return An instance that implements {@link CmdlineArgumentSpec}.
     * @since 5.2
     */
    @Override
    public CmdlineArgumentSpec getRunnerSpec() {
        return appRunnerSpec.getRunnerSpec();
    }

    /**
     * Access to the process specification.
     *
     * @return An instance that implements {@Link ProcessExecutionSpec}.
     * @since 5.2
     */
    @Override
    public ProcessExecutionSpec getProcess() {
        return appRunnerSpec.getProcess();
    }

    /**
     * Copies this specification to a standard {@link ExecSpec}.
     *
     * @param spec Target execution specification.
     */
    public void copyTo(ExecSpec spec) {
        appRunnerSpec.copyTo(spec);
    }

    /**
     * Runs a job using {@code javaexec}.
     *
     * @return Result of execution.
     *
     * @since 5.0
     */
    public ExecOutput submitAsExec() {
        return appRunnerSpec.submitAsExec(this);
    }

    /**
     * Runs a job using {@code javaexec}.
     *
     * @param additionalConfiguration Allows the caller to perform additional configuration on the final
     *   {@link ExecSpec}. This is called only after all other configuration has been completed.
     *
     * @return Result of execution.
     *
     * @since 5.2
     */
    public ExecOutput submitAsExec(Action<ExecSpec> additionalConfiguration) {
        return appRunnerSpec.submitAsExec(this, additionalConfiguration);
    }


    @Deprecated
    protected AbstractExecSpec(ProjectOperations po) {
        gtc = ConfigCacheSafeOperations.from(po);
        this.appRunnerSpec = getTaskOperations().execTools().appRunnerSpec();
    }

    /**
     * Constructs this from an instance of {@link ConfigCacheSafeOperations}.
     *
     * <p>
     *     A task having {@link GrolifantDefaultTask} in its parent lineage, can just pass a {@code this} reference.
     * </p>
     *
     * @param po Instance of {@link ConfigCacheSafeOperations}.
     *
     * @since 5.0
     */
    protected AbstractExecSpec(ConfigCacheSafeOperations po) {
        gtc = ConfigCacheSafeOperations.from(po);
        this.appRunnerSpec = getTaskOperations().execTools().appRunnerSpec();
    }

    /**
     * Adds a command-line processor that will process command-line arguments in a specific order.
     * <p>
     * For instance in a script, one want to process the exe args before the script args.
     * <p>
     * In a system that has commands and subcommands, one wants to process this in the order of exe args, command args,
     * and then subcommand args.
     * <p>
     * This method allows the implementor to control the order of processing  for all the groupings.
     *
     * @param name      Name of command-line processor.
     * @param order     Order in queue to process.
     * @param processor The specific grouping.
     */
    protected void addCommandLineProcessor(String name, Integer order, CmdlineArgumentSpec processor) {
        appRunnerSpec.addCommandLineProcessor(name, order, processor);
    }

    /**
     * Processes the output.
     *
     * <p>
     * Checks first if any outputs should be forwarded.
     * Thereafter run all registered actions.
     * </p>
     *
     * @param output Output to process
     *
     * @since 5.0
     */
    @PackageScope
    void processOutput(ExecOutput output) {
        if(this.appRunnerSpec instanceof ProcessExecOutput) {
            ((ProcessExecOutput) this.appRunnerSpec).processOutput(output);
        }
    }

    @Internal
    protected Collection<CmdLineArgumentSpecEntry> getCommandLineProcessors() {
        return appRunnerSpec.getCommandLineProcessors();
    }

    @Internal
    protected ConfigCacheSafeOperations getTaskOperations() {
        return this.gtc;
    }

    @Nested
    protected final AppRunnerSpec appRunnerSpec;

    private final ConfigCacheSafeOperations gtc;
}
