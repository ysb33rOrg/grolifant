/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.file.ConfigurableFileTree;
import org.gradle.api.file.CopySpec;
import org.gradle.api.file.DeleteSpec;
import org.gradle.api.file.FileTree;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.logging.configuration.ConsoleOutput;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.resources.ReadableResource;
import org.gradle.api.tasks.WorkResult;
import org.gradle.process.ExecResult;
import org.gradle.process.ExecSpec;
import org.gradle.process.JavaExecSpec;
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin;

import java.io.File;
import java.util.concurrent.Callable;

/**
 * Provides various operations which typically would require the {@link org.gradle.api.Project} or
 * {@link org.gradle.api.file.ProjectLayout} instances to resolve.
 *
 * @since 1.0
 */
public interface ProjectOperations {

    /**
     * Creates a {@code grolifant} extension if it does not exist.
     *
     * @param project Contextual project
     * @return Extension.
     * @deprecated Apply the {@link GrolifantServicePlugin} instead.
     */
    @Deprecated
    static ProjectOperations maybeCreateExtension(Project project) {
        project.getPluginManager().apply(GrolifantServicePlugin.class);
        return find(project);
    }

    /**
     * Creates a reference to {@link ProjectOperations} instance.
     *
     * @param project Contextual project
     * @return Extension.
     * @deprecated Apply the {@link GrolifantServicePlugin} instead.
     */
    @Deprecated
    static ProjectOperations create(Project project) {
        return maybeCreateExtension(project);
    }

    /**
     * Attempts to find the extension.
     * <p>
     * Do not call this method once the configuration phase has finished.
     *
     * @param project Contextual project
     * @return Extension
     */
    static ProjectOperations find(Project project) {
        return project.getExtensions().getByType(ProjectOperations.class);
    }

    /**
     * A provider to this instance.
     *
     * @return Provider.
     */
    Provider<ProjectOperations> asProvider();

    /**
     * Value that can be read safely at configuration time
     * <p>
     * As from Gradle 8.0, methods take this as an input, will ignore this value as all providers that can be resolved
     * at configuration-time are available.
     *
     * @return Always returns true
     * @since 1.1.0
     */
    default boolean atConfigurationTime() {
        return true;
    }

    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param file File evaluated as per {@link #getFsOperations()}.{@link FileSystemOperations#file}.
     * @return Readable resource
     * @deprecated Use {@link #getFsOperations()}.{@link FileSystemOperations#bzip2Resource(Object)}
     */
    @Deprecated
    default ReadableResource bzip2Resource(Object file) {
        return getFsOperations().bzip2Resource(file);
    }

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     *
     * @param file File evaluated as per{@link #getFsOperations()}.{@link FileSystemOperations#file}.
     * @return Readable resource
     * @deprecated Use {@link #getFsOperations()}.{@link FileSystemOperations#gzipResource(Object)}
     */
    @Deprecated
    default ReadableResource gzipResource(Object file) {
        return getFsOperations().gzipResource(file);
    }

    /**
     * Creates an empty CopySpec.
     *
     * @return {@link CopySpec}
     */
    CopySpec copySpec();

    /**
     * Creates a {@link CopySpec} which can later be used to copy files or create an archive. The given action is used
     * to configure the {@link CopySpec} before it is returned by this method.
     *
     * @param action Action to configure the CopySpec
     * @return The CopySpec
     */
    default CopySpec copySpec(Action<? super CopySpec> action) {
        CopySpec cs = copySpec();
        action.execute(cs);
        return cs;
    }

    /**
     * Safely resolve the stringy item as a path relative to the build directory.
     * <p>
     * Shortcut to {@link #getFsOperations()}.{@link FileSystemOperations#buildDirDescendant(Object)}.
     * Use the latter where possible.
     * </p>
     *
     * @param stringy Any item that can be resolved to a string using
     *                {@code org.ysb33r.grolifant5.core.api.grolifant.StringTools#stringize}.
     * @return Provider to a file
     */
    default Provider<File> buildDirDescendant(Object stringy) {
        return getFsOperations().buildDirDescendant(stringy);
    }

    /**
     * Deletes the specified files.  The given action is used to configure a {@link DeleteSpec}, which is then used to
     * delete the files.
     * <p>Example:
     * <pre>
     * project.delete {
     *     delete 'somefile'
     *     followSymlinks = true
     * }
     * </pre>
     *
     * @param action Action to configure the DeleteSpec
     * @return {@link WorkResult} that can be used to check if delete did any work.
     */
    WorkResult delete(Action<? super DeleteSpec> action);

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the an environmental variable.
     * The property cannot safely be read at configuration time.
     * @since 1.1.0
     */
    default Provider<String> environmentVariable(Object name) {
        return environmentVariable(name, false);
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of an environment variable.
     */
    default Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        return getProviderTools().environmentVariable(name, configurationTimeSafety);
    }

    /**
     * Executes the specified external process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     * @deprecated Use {@link #getExecTools()}.{@link ExecTools#exec} instead.
     */
    @Deprecated
    default ExecResult exec(Action<? super ExecSpec> action) {
        return getExecTools().exec(action).getResult().get();
    }

    /**
     * Provider for the build directory.
     *
     * @return Build directory.
     */
    Provider<File> getBuildDir();

    /**
     * Tools to deal with Gradle configurations.
     *
     * @return
     */
    ConfigurationTools getConfigurations();

    /**
     * Tools to deal with out-of-process, non-JVM, executables.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    ExecTools getExecTools();

    /**
     * Get the full project path including the root project name in case of a multi-project.
     *
     * @return The fully qualified project path including root project.
     * @since 1.2
     *
     * @deprecated Use {@link ProjectTools#getFullProjectPath} instead.
     */
    @Deprecated
    String getFullProjectPath();

    /**
     * Gradle distribution home directory.
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#getGradleHomeDir}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @return Directory.
     * @since 2.2
     */
    default Provider<File> getGradleHomeDir() {
        return getFsOperations().getGradleHomeDir();
    }

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#getGradleUserHomeDir}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @return Directory.
     */
    default Provider<File> getGradleUserHomeDir() {
        return getFsOperations().getGradleUserHomeDir();
    }

    /**
     * Lazy-evaluated project group.
     *
     * @return provider to project group
     */
    Provider<String> getGroupProvider();

    /**
     * Returns an object instance for filesystem operations that deals coprrectly with the functionality of the
     * curretn Gradle version.
     *
     * @return Instance of {@link FileSystemOperations}
     */
    FileSystemOperations getFsOperations();

    /**
     * Returns the project cache directory for the given project.
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#getProjectCacheDir}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @return Project cache directory. Never {@code null}.
     */
    default File getProjectCacheDir() {
        return getFsOperations().getProjectCacheDir();
    }

    /**
     * Returns the project directory.
     *
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#getProjectDir()}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @return Project directory.
     * @since 1.0
     */
    default File getProjectDir() {
        return getFsOperations().getProjectDir();
    }

    /**
     * The project name
     *
     * @return Cached value of project name.
     */
    String getProjectName();

    /**
     * Get project path.
     *
     * @return The fully qualified project path.
     * @since 1.2
     */
    String getProjectPath();

    /**
     * Returns the root directory of the project.
     *
     * @return Root directory.
     * @since 2.0
     */
    File getProjectRootDir();

    /**
     * A reference to the provider factory.
     *
     * @return {@link ProviderFactory}
     */
    ProviderFactory getProviders();

    /**
     * Tools to deal with project & configuration specifics down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     * @since 2.0
     */
    ProjectTools getProjectTools();

    /**
     * Tools to deal with provider down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    ProviderTools getProviderTools();

    /**
     * Tools for dealing with repositories.
     *
     * @return Tools instance.
     * @since 2.0
     */
    RepositoryTools getRepositoryTools();

    /**
     * Tools for dealing with conversions of various objects into string or lists of strings.
     *
     * @return String tools instance.
     * @since 2.0
     */
    StringTools getStringTools();

    /**
     * Utilities for working with tasks in a consistent manner across Gradle versions.
     *
     * @return {@link TaskTools} instance.
     * @since 1.3
     */
    TaskTools getTasks();

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    default Provider<String> getVersionProvider() {
        return getProjectTools().getVersionProvider();
    }

    /**
     * Sets a new version for a project.
     * <p>This creates an internal objhect that can safely be evaluated by
     * Gradle's {@link org.gradle.api.Project#getVersion}.
     * </p>
     *
     * @param newVersion Anything that can be converted to a string using
     *                   {@link StringTools}.
     */
    default void setVersionProvider(Object newVersion) {
        getProjectTools().setVersionProvider(newVersion);
    }

    /**
     * Creates a new ConfigurableFileTree. The tree will have no base dir specified.
     *
     * @param base Base directory for file tree,
     * @return File tree.
     */
    ConfigurableFileTree fileTree(Object base);

    /**
     * Console output mode
     *
     * @return How the console output has been requested.
     *
     * @deprecated USe {@link ProjectTools#getConsoleOutput()}
     */
    @Deprecated
    default ConsoleOutput getConsoleOutput() {
        return getProjectTools().getConsoleOutput();
    }

    /**
     * Get the minimum log level for Gradle.
     *
     * @return Log level
     */
    LogLevel getGradleLogLevel();

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the project property.
     * The property cannot safely be read at configuration time
     * @since 1.1.0
     */
    default Provider<String> gradleProperty(Object name) {
        return gradleProperty(name, false);
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the project property.
     */
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety);

    /**
     * Whether configuration cache is enabled for a build.
     *
     * @return {@code true} is configuration cache is available and enabled.
     */
    boolean isConfigurationCacheEnabled();

    /**
     * Whether Gradle is operating in offline mode.
     *
     * @return {@code true} if offline.
     */
    boolean isOffline();

    /**
     * Whether dependencies should be refreshed.
     *
     * @return {@code true} to check dependencies again.
     *
     * @deprecated Use {@link ProjectTools#isRefreshDependencies()}.
     */
    @Deprecated
    default boolean isRefreshDependencies() {
        return getProjectTools().isRefreshDependencies();
    }


    /**
     * Whether tasks should be re-ruin
     *
     * @return {@code true} if tasks were set to be re-run.
     *
     * @deprecated USe {@link ProjectTools#isRerunTasks()}
     */
    default boolean isRerunTasks() {
        return getProjectTools().isRerunTasks();
    }

    /**
     * Whether current project is the root project.
     *
     * @return {@code true} is current project is root project.
     * @since 1.2
     */
    boolean isRoot();

    /**
     * Executes the specified external java process.
     *
     * @param action
     * @return {@link ExecResult} that can be used to check if the execution worked.
     * @deprecated Use {@link #getExecTools()}.{@link ExecTools#javaexec} instead
     */
    @Deprecated
    default ExecResult javaexec(Action<? super JavaExecSpec> action) {
        return getExecTools().javaexec(action).getResult().get();
    }

    /**
     * Tools for dealing for JVM/Java operations.
     *
     * @return Appropriate instance for the running Gradle version.
     */
    JvmTools getJvmTools();

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name Anything convertible to a string
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     */
    default Provider<String> resolveProperty(Object name) {
        return resolveProperty(name, null, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to finding a property by the specified name.
     */
    default Provider<String> resolveProperty(Object name, boolean configurationTimeSafety) {
        return resolveProperty(name, null, configurationTimeSafety);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name         Anything convertible to a string
     * @param defaultValue Default value to return if the property search order does not return any value.
     *                     Can be {@code null}. Anything convertible to a string.
     * @return Provider to finding a property by the specified name.
     * The property cannot safely be read at configuration time.
     */
    default Provider<String> resolveProperty(Object name, Object defaultValue) {
        return resolveProperty(name, defaultValue, false);
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * <p>
     * Shortcut to {@link #getProviderTools()}.{@link ProviderTools#resolveProperty(Object, Object, boolean)}.
     * </p>
     *
     * @param name                    Anything convertible to a string
     * @param defaultValue            Default value to return if the property search order does not return any value.
     *                                Can be {@code null}. Anything convertible to a string.
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to finding a property by the specified name.
     */
    default Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety) {
        return getProviderTools().resolveProperty(name, defaultValue, configurationTimeSafety);
    }

    /**
     * Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    <T> Provider<T> provider(Callable<? extends T> var1);

    /**
     * Creates a provider to a system property.
     * <p>
     * Shortcut to {@link #getProviderTools()}.{@link ProviderTools#systemProperty(Object)}.
     * </p>
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the system property. The property cannot safely be read at configuration time
     * @since 1.1.0
     */
    default Provider<String> systemProperty(Object name) {
        return systemProperty(name, false);
    }

    /**
     * Creates a provider to a system property.
     * <p>
     * Shortcut to {@link #getProviderTools()}.{@link ProviderTools#systemProperty(Object, boolean)}.
     * </p>
     *
     * @param name                    Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the system property.
     */
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety);

    /**
     * Returns a TAR tree presentation
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#tarTree(Object)}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @param tarPath Path to tar file
     * @return File tree
     * @deprecated
     */
    @Deprecated
    default FileTree tarTree(Object tarPath) {
        return getFsOperations().tarTree(tarPath);
    }

    /**
     * Returns a ZIP tree presentation
     * <p>
     * Shortcut for {@link #getFsOperations()}.{@link FileSystemOperations#zipTree(Object)}.
     * It is recommended to use the latter, unless this is access directly from a buildscript.
     * </p>
     *
     * @param zipPath Path to zip file
     * @return File tree
     * @deprecated
     */
    @Deprecated
    default FileTree zipTree(Object zipPath) {
        return getFsOperations().zipTree(zipPath);
    }
}
