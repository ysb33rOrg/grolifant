/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.file.FileCollection;
import org.gradle.process.JavaExecSpec;
import org.gradle.process.JavaForkOptions;
import org.ysb33r.grolifant5.api.core.jvm.JvmAppRunnerSpec;
import org.ysb33r.grolifant5.internal.core.ConfigurationCache;
import org.ysb33r.grolifant5.internal.core.jvm.ClassPathUtils;

import java.util.regex.Pattern;

/**
 * Tools for creating various JVM/Java utility objects.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface JvmTools {

    /**
     * Creates a {@link JavaExecSpec}.
     *
     * @return Returns something compatible with a {@link JavaExecSpec}.
     */
    JavaExecSpec javaExecSpec();

    /**
     * Creates a {@Link JavaForkOptions}.
     *
     * @return Returns something compatible with a {@link JavaForkOptions}.
     */
    default JavaForkOptions javaForkOptions() {
        return javaExecSpec();
    }

    /**
     * Creates a {@link JvmAppRunnerSpec}.
     * <p>
     *   This is primarily used internally by classes that implements execution specifications on the JVM.
     * </p>
     *
     * @return Implementation of {@link JvmAppRunnerSpec}
     */
    JvmAppRunnerSpec jvmAppRunnerSpec();

    /**
     * Returns the classpath location for a specific class
     *
     * @param aClass Class to find.
     * @return Location of class.
     * Can be {@code null} which means class has been found, but cannot be placed on classpath.
     * @throw ClassNotFoundException*
     */
    default ClassLocation resolveClassLocation(Class aClass) {
        return ClassPathUtils.resolveClassLocationSimple(aClass);
    }

    /**
     * Returns the classpath location for a specific class.
     * <p>
     *   It can be used when configuration cache-related instrumentation cause issues in Gradle 6.5+.
     *   See https://github.com/gradle/gradle/issues/14727 for details.
     * </p>
     * <p>
     *   If the JAR name is the same as the {@code instrumentJarName}, then search the substitution collection for the
     *   first hit that matches the provided pattern.
     * </p>
     * <p>
     *   It will ignore any files found in {@code caches/jar-} .
     *   It will also recheck the search if the found item is a JAR.
     * </p>
     *
     * @param aClass             Class to find.
     * @param substitutionSearch Files to search. A typical example would be to look in
     *                           {@code rootProject.buildscript.configurations.getByName( 'classpath' )}.
     * @param substitutionMatch  The pattern to look for. Typically the name of a jar with a version.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     * on classpath
     * @throw ClassNotFoundException
     */
    default ClassLocation resolveClassLocation(
            Class aClass,
            FileCollection substitutionSearch,
            Pattern substitutionMatch
    ) {
        return resolveClassLocation(aClass, substitutionSearch, substitutionMatch, ConfigurationCache.IS_A_JAR, ConfigurationCache.GENERATED_JAR_SUBPATH);
    }

    /**
     * Returns the classpath location for a specific class.
     * <p>
     *   Works around configuration cache-related intrumentation issue in Gradle 6.5+.
     *   See https://github.com/gradle/gradle/issues/14727 for details.
     * </p>
     * <p>
     *   If the JAR name is the same as the {@code instrumentJarName}, then search the substitution collection for the
     *   first hit that matches the provided pattern.
     * </p>
     * <p>
     *   It will ignore any files found in {@code caches/jar-}.
     * </p>
     *
     * @param aClass             Class to find.
     * @param substitutionSearch Files to search. A typical example would be to look in
     *                           {@code rootProject.buildscript.configurations.getByName( 'classpath' )}.
     * @param substitutionMatch  The pattern to look for. Typically the name of a jar with a version.
     * @param redoIfMatch        A pattern that will cause a recheck if the path matches.
     *                           By default, this is if the filename ends in {@code .jar}.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     * on classpath
     * @throw ClassNotFoundException
     */
    default ClassLocation resolveClassLocation(
            Class aClass,
            FileCollection substitutionSearch,
            Pattern substitutionMatch,
            Pattern redoIfMatch
    ) {
        return resolveClassLocation(aClass, substitutionSearch, substitutionMatch, redoIfMatch, ConfigurationCache.GENERATED_JAR_SUBPATH);
    }

    /**
     * Returns the classpath location for a specific class.
     * <p>
     *   Works around configuration cache-related intrumentation issue in Gradle 6.5+.
     *   See https://github.com/gradle/gradle/issues/14727 for details.
     * </p>
     * <p>
     *   If the JAR name is the same as the {@code instrumentJarName}, then search the substitution collection for the
     *   first hit that matches the provided pattern.
     * </p>
     *
     * @param aClass             Class to find.
     * @param substitutionSearch Files to search. A typical example would be to look in
     *                           {@code rootProject.buildscript.configurations.getByName( 'classpath' )}.
     * @param substitutionMatch  The pattern to look for. Typically the name of a jar with a version.
     * @param redoIfMatch        A pattern that will cause a recheck if the path matches.
     *                           By default, this is if the filename ends in {@code .jar}.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     * on classpath
     * @throw ClassNotFoundException
     */
    ClassLocation resolveClassLocation(
            Class aClass,
            FileCollection substitutionSearch,
            Pattern substitutionMatch,
            Pattern redoIfMatch,
            Pattern ignoreFromPaths
    );
}
