/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant5.api.core.AllArgsProvider
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools

import java.util.concurrent.Callable

import static org.ysb33r.grolifant5.api.core.Transform.toList

/**
 * Abstract class to set command-line parameters.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractCmdlineArgumentSpec implements CmdlineArgumentSpec, AllArgsProvider {

    /**
     * Return list of arguments to the entrypoint.
     *
     * @return List of resolved arguments.
     */
    @Override
    List<String> getArgs() {
        stringTools.stringizeDropNull(this.arguments)
    }

    /**
     * Add arguments to the entrypoint.
     *
     * @param args Any arguments resolvable to strings.
     */
    @Override
    void args(Object... args) {
        arguments.addAll(args)
    }

    /**
     * Add arguments to the entrypoint.
     *
     * @param args Any arguments resolvable to strings.
     */
    @Override
    void args(Iterable<?> args) {
        arguments.addAll(args.toList())
    }

    /**
     * Replace current arguments with a new set.
     *
     * @param args Any arguments resolvable to strings.
     */
    @Override
    void setArgs(Iterable<?> args) {
        arguments.clear()
        arguments.addAll(args.toList())
    }

    /**
     * Replace current arguments with a new set.
     *
     * @param args Any arguments resolvable to strings.
     */
    void setArgs(List<String> args) {
        arguments.clear()
        arguments.addAll(args)
    }

    /**
     * A provider to arguments that will be inserted before any supplied arguments.
     *
     * @return Arguments provider
     */
    @Override
    Provider<List<String>> getPreArgs() {
        this.preArgsProvider
    }

    /**
     * All defined arguments, plus all arguments providers via the command-line argument providers.
     *
     * @return Provider to a fully-resolved set of arguments.
     */
    @Override
    Provider<List<String>> getAllArgs() {
        this.allArgsProvider
    }

    /**
     * Add lazy-evaluated providers of arguments.
     *
     * @param providers One or more providers or string lists.
     */
    @Override
    void addCommandLineArgumentProviders(Provider<List<String>>... providers) {
        this.externalArgs.addAll(providers)
    }

    /**
     * Get current list of command-line argument providers.
     *
     * @return Current list of providers.
     */
    @Override
    List<Provider<List<String>>> getCommandLineArgumentProviders() {
        externalArgs
    }

    protected AbstractCmdlineArgumentSpec(
        StringTools stringTools,
        ProviderFactory providers
    ) {
        this.stringTools = stringTools
        this.allArgsProvider = providers.provider(createAllArgsProvider(this))
        this.preArgsProvider = providers.provider { -> Collections.EMPTY_LIST }
    }

    protected AbstractCmdlineArgumentSpec(
        StringTools stringTools,
        ProviderTools providers
    ) {
        this.stringTools = stringTools
        this.allArgsProvider = providers.provider(createAllArgsProvider(this))
        this.preArgsProvider = providers.provider { -> Collections.EMPTY_LIST }
    }

    @Deprecated
    protected AbstractCmdlineArgumentSpec(
        ProjectOperations po
    ) {
        this.stringTools = po.stringTools
        this.allArgsProvider = po.providers.provider(createAllArgsProvider(this))
        this.preArgsProvider = po.providers.provider { -> Collections.EMPTY_LIST }
    }

    @SuppressWarnings('UnnecessaryGetter')
    private static Callable<List<String>> createAllArgsProvider(
        AbstractCmdlineArgumentSpec self
    ) {
        { ->
            def flattenedArguments = []
            flattenedArguments.addAll(self.preArgs.get())
            flattenedArguments.addAll(self.getArgs())
            flattenedArguments.addAll(toList(self.commandLineArgumentProviders) { Provider<List<String>> pv ->
                pv.getOrElse(EMPTY_LIST)
            }.flatten())
            flattenedArguments
        } as Callable<List<String>>
    }

    private final StringTools stringTools
    private final List<Object> arguments = []
    private final List<Provider<List<String>>> externalArgs = []
    private final Provider<List<String>> allArgsProvider
    private final Provider<List<String>> preArgsProvider
    private static final List<String> EMPTY_LIST = Collections.emptyList()
}
