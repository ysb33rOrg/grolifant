/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core

import groovy.transform.CompileStatic

/**
 * Configuration options for declaring files input properties.
 *
 * Some of these options might be ignored, depending on the version of Gradle.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
enum TaskInputFileOptions {
    /**
     * Ignore directories during up-to-date checks and build cache key calculations.
     */
    IGNORE_EMPTY_DIRECTORIES,

    /**
     * Normalize line endings in text files during up-to-date checks and build cache key calculations
     */
    NORMALIZE_LINE_ENDINGS,

    /**
     * Whether the property is optional.
     */
    OPTIONAL,

    /**
     * Skip executing the task if the property contains no files.
     */
    SKIP_WHEN_EMPTY
}