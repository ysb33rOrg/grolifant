/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

/**
 * Sets the definition on how to find a tool.
 * This includes finding it by version.
 *
 * @since 2.0
 */
public interface ToolLocation extends BaseToolLocation {

    /**
     * Locate an executable by version, probably downloading it if not local.
     *
     * @param version Something resolvable to a string.
     */
    void executableByVersion(Object version);
}
