/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.git;

import org.ysb33r.grolifant5.api.core.StringTools;

import java.net.URI;

/**
 * A description of a Git repository from a cloud provider
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface CloudGitConfigurator extends CloudGitDescriptor {
    void setBaseUri(URI uri);

    /**
     * Set the organisation.
     *
     * @param org Anything convertible using {@link StringTools#stringize}
     */
    void setOrganisation(final Object org);

    /**
     * Set the repository
     *
     * @param repo Anything convertible using {@link StringTools#stringize}
     */
    void setRepository(final Object repo);

    /**
     * Set the branch.
     * <p>
     * Overrides {@link #setTag} and {@link #setCommit}.
     *
     * @param branch Anything convertible using {@link StringTools#stringize}
     */
    void setBranch(final Object branch);

    /**
     * Set the tag.
     * <p>
     * Overrides {@link #setBranch} and {@link #setCommit}.
     *
     * @param tag Anything convertible using {@link StringTools#stringize}
     */
    void setTag(final Object tag);

    /**
     * Set the commit to use.
     * <p>
     * Overrides {@link #setTag} and {@link #setBranch}.
     *
     * @param commit Anything convertible using {@link StringTools#stringize}
     */
    void setCommit(final Object commit);
}
