/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.ysb33r.grolifant5.api.core.CheckSumVerification
import org.ysb33r.grolifant5.api.core.ExclusiveFileAccess
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.errors.DistributionFailedException

import java.util.concurrent.Callable

/**
 * Performs low-level downloading work.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
@CompileStatic
@Slf4j
class ArtifactDownloader {

    /**
     * Creates an instance which takes care of the actual downloading and caching.
     *
     * @param downloadURI URI to download package from.
     * @param downloadRoot Base directory where to download to.
     * @param po {@link ProjectOperations} instance to use
     * @param basePath Relative path to the downloadRoot.
     * @param verifyArtifactRoot Callback to verify the unpacked artifact. Never {@code null}.
     * @param verifyDownloadChecksum Callback to verify the checksum of the downloaded target.
     *   Can be {@code null}.
     */
    @SuppressWarnings('ParameterCount')
    ArtifactDownloader(
        final URI downloadURI,
        final File downloadRoot,
        final ConfigCacheSafeOperations po,
        final String basePath,
        final ArtifactRootVerification verifyArtifactRoot,
        final ArtifactUnpacker unpacker,
        final CheckSumVerification verifyDownloadChecksum
    ) {
        this.downloadURI = downloadURI
        this.downloadRoot = downloadRoot
        this.basePath = basePath
        this.verifyDownloadChecksum = verifyDownloadChecksum
        this.verifyArtifactRoot = verifyArtifactRoot
        this.unpacker = unpacker
        this.requiresDownload = DOWNLOAD_IF_NOT_EXISTS
        this.projectDir = po.fsOperations().projectDir
        this.configCacheSafeOperations = po
    }

    /**
     * Downloads an artifact without unpacking it.
     *
     * @param downloadURI URI to download package from.
     * @param downloadRoot Base directory where to download to.
     * @param po {@link ProjectOperations} instance to use
     * @param basePath Relative path to the downloadRoot.
     * @param requiresDownload Indicates whether download is required.
     * @param verifyDownloadChecksum Callback to verify the checksum of the downloaded target.
     *   Can be {@code null}.
     */
    @SuppressWarnings('ParameterCount')
    ArtifactDownloader(
        final URI downloadURI,
        final File downloadRoot,
        final ConfigCacheSafeOperations po,
        final String basePath,
        final ArtifactRequiresDownload requiresDownload,
        final CheckSumVerification verifyDownloadChecksum
    ) {
        this.downloadURI = downloadURI
        this.downloadRoot = downloadRoot
        this.basePath = basePath
        this.verifyDownloadChecksum = verifyDownloadChecksum
        this.requiresDownload = requiresDownload
        this.projectDir = po.fsOperations().projectDir
        this.configCacheSafeOperations = po
    }

    /** Creates a distribution/file it it does not exist already.
     *
     * @param description Name of the downloaded entity.
     * @param offlineMode Whether to operate in download mode.
     * @param downloadInstance Download & logger instances to use
     *
     * @return Location of distribution
     */
    File getFromCache(final String description, boolean offlineMode, final Downloader downloadInstance) {
        final downloadDetails = downloadInstance.downloadLocation(downloadURI, downloadRoot, basePath)

        Callable<File> downloadAction = new DownloadAction(
            offlineMode: offlineMode,
            description: description,
            localFile: downloadDetails,
            downloadURI: this.downloadURI,
            verifyArtifactRoot: this.verifyArtifactRoot,
            requiresDownload: this.requiresDownload,
            downloadInstance: downloadInstance,
            verifyDownloadChecksum: this.verifyDownloadChecksum,
            unpacker: this.unpacker,
            po: configCacheSafeOperations
        )
        exclusiveFileAccessManager.access(downloadDetails.downloadedFile, downloadAction)
    }

    static private final ArtifactRequiresDownload DOWNLOAD_IF_NOT_EXISTS =
        { URI d, File f -> !f.file } as ArtifactRequiresDownload

    private final ExclusiveFileAccess exclusiveFileAccessManager = new ExclusiveFileAccess(120000, 200)
    private final URI downloadURI
    private final File downloadRoot
    private final File projectDir
    private final String basePath
    private final CheckSumVerification verifyDownloadChecksum
    private final ArtifactUnpacker unpacker
    private final ArtifactRootVerification verifyArtifactRoot
    private final ArtifactRequiresDownload requiresDownload
    private final ConfigCacheSafeOperations configCacheSafeOperations

    private static class DownloadAction implements Callable<File> {

        boolean offlineMode
        String description
        DownloadedLocalFile localFile
        URI downloadURI
        ArtifactRootVerification verifyArtifactRoot
        ArtifactRequiresDownload requiresDownload
        Downloader downloadInstance
        CheckSumVerification verifyDownloadChecksum
        ArtifactUnpacker unpacker
        ConfigCacheSafeOperations po

        @Override
        File call() throws Exception {
            final distributionUrl = localFile.distributionUrl
            if (localFile.distributionDir.directory && localFile.markerFile.file) {
                return verifyArtifactRoot.apply(localFile.distributionDir)
            }

            if (requiresDownload.download(downloadURI, localFile.downloadedFile)) {
                if (offlineMode && distributionUrl.scheme != 'file') {
                    throw new DistributionFailedException("Cannot download ${description} as currently offline")
                }

                File tmpDownloadedFile = new File(
                    localFile.downloadedFile.parentFile, "${localFile.downloadedFile.name}.part"
                )
                tmpDownloadedFile.delete()
                downloadInstance.logProgress("Downloading ${po.stringTools().safeUri(distributionUrl)}")
                downloadInstance.download(distributionUrl, tmpDownloadedFile)
                tmpDownloadedFile.renameTo(localFile.downloadedFile)
            }

            List<File> topLevelDirs = po.fsOperations().listDirs(localFile.distributionDir)
            for (File dir : topLevelDirs) {
                downloadInstance.logProgress("Deleting directory ${dir.absolutePath}")
                dir.deleteDir()
            }

            if (verifyDownloadChecksum) {
                verifyDownloadChecksum.verify(localFile.downloadedFile)
            }

            if (unpacker) {
                downloadInstance.logProgress(
                    "Unpacking ${localFile.downloadedFile.absolutePath} to ${localFile.distributionDir.absolutePath}"
                )
                unpacker.accept(localFile.downloadedFile, localFile.distributionDir)
            }

            File root = verifyArtifactRoot ?
                verifyArtifactRoot.apply(localFile.distributionDir) :
                localFile.distributionDir
            localFile.markerFile.createNewFile()

            root
        }
    }
}
