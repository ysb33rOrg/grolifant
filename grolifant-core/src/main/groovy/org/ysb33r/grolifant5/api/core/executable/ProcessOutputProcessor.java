/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Internal;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;

import java.io.File;

/**
 * Processing execution output.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
public interface ProcessOutputProcessor extends ProcessExecOutput {
    /**
     * Whether process output is forwarded to the console.
     *
     * @return {@code true} if output is forwarded.
     */
    @Internal
    boolean isOutputForwarded();

    /**
     * Whether process error output is forwarded to the console.
     *
     * @return {@code true} if output is forwarded.
     */
    @Internal
    boolean isErrorOutputForwarded();

}
