/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.executable.ScriptDefinition;
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec;

/**
 * Basic building block for script specifications.
 *
 * @param <T> implementation that extends {@link AbstractScriptExecSpec}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public class AbstractScriptExecSpec<T extends AbstractScriptExecSpec<T>>
        extends AbstractExecSpec<T>
        implements ExecutableScript<T> {

    /**
     * Configures the script along with its arguments.
     *
     * @param configurator An action to configure the script
     */
    @Override
    public void script(Action<ScriptSpec> configurator) {
        appRunnerSpec.configureCmdline(SCRIPT_BLOCK, configurator);
    }

    /**
     *
     * @param specConfigurator Configurating closure. 
     */
    @Override
    public void script(Closure<?> specConfigurator) {
        appRunnerSpec.configureCmdline(SCRIPT_BLOCK, specConfigurator);
    }

    /**
     * Adds a command-line processor for the script block aboce and beyond anything else the base class does.
     *
     * @param po Configuration-cache safe operations.
     *
     * @since 5.0
     */
    protected AbstractScriptExecSpec(ConfigCacheSafeOperations po) {
        super(po);
        addCommandLineProcessor(SCRIPT_BLOCK, 1, new ScriptDefinition(po));
    }

    @Deprecated
    protected AbstractScriptExecSpec(ProjectOperations po) {
        super(ConfigCacheSafeOperations.from(po));
        addCommandLineProcessor(SCRIPT_BLOCK, 1, new ScriptDefinition(po));
    }
    private static final String SCRIPT_BLOCK = "script";
}
