/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.ExecTools;
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput;

import java.io.File;

/**
 * Describes how output and output need needs to be managed for an external process.
 *
 * @since 5.0
 */
public interface OutputStreamSpec {
    /**
     * Captures the output of the running process.
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    void capture();

    /**
     * Captures the output of the running process, but also forwards it to console.
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    void captureAndForward();

    /**
     * Capture output to this the given file.
     *
     * @param out Provider of a file.
     */
    void captureTo(Provider<File> out);

    /**
     * Forwards the standard output from the running process to console except if running in quiet mode.
     *
     * <p>This is standard behaviour for most cases, except if implementors of task and execution specifications
     *   decide to override it.</p>
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    void forward();

    /**
     * Do not forward or capture output.
     */
    void noOutput();

    /**
     * Get the output type that corresponds to the current configuration.
     * @return Output type.
     */
    ExecTools.OutputType getOutputType();

    /**
     * Obtains the file capture lcoation if it has been set.
     *
     * @return Location. Can be an empty provider,
     */
    Provider<File> getFileCapture();
}
