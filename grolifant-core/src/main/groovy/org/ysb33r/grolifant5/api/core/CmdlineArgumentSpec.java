/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;

import java.util.List;

/**
 * Specifies the entrypoint and arguments for running something on a JVM.
 *
 * @since 2.0
 */
public interface CmdlineArgumentSpec extends AllArgsProvider, CommandLineArgumentProviders {

    /**
     * Return list of arguments to the entrypoint.
     *
     * @return List of resolved arguments.
     */
    @Input
    List<String> getArgs();

    /**
     * Add arguments to the entrypoint.
     *
     * @param args Any arguments resolvable to strings.
     */
    void args(Object... args);

    /**
     * Add arguments to the entrypoint.
     *
     * @param args Any arguments resolvable to strings.
     */
    void args(Iterable<?> args);

    /**
     * Replace current arguments with a new set.
     *
     * @param args Any arguments resolvable to strings.
     */
    void setArgs(Iterable<?> args);

    /**
     * Add lazy-evaluated providers of arguments.
     *
     * @param providers One or more providers or string lists.
     */
    void addCommandLineArgumentProviders(Provider<List<String>>... providers);
}
