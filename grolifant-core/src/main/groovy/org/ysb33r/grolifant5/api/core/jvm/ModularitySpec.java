/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;

/**
 * Handles modularity specification.
 *
 * Prior to Gradle 6.4 operations on these moethods will be NOOPs.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface ModularitySpec {
    /**
     * Provider to the main module name.
     *
     * @return Provider.
     */
    @Input
    @Optional
    Provider<String> getMainModule();

    /**
     * Set the main module name.
     *
     * @param mod Anything convertible to a string.
     */
    void setMainModule(Object mod);

    /**
     * Whether module path should be inferred.
     *
     * Prior to Gradle 6.4, this will always return a false value.
     *
     * @return Provider to the detection state.
     */
    @Internal
    Provider<Boolean> getInferModulePath();

    /**
     * Whether module path should be inferred.
     *
     * Ignored prior to Gradle 6.4.
     *
     * @param toggle Whether to infer the module path.
     */
    void setInferModulePath(boolean toggle);

    /**
     * Whether module path should be inferred.
     *
     * Ignored prior to Gradle 6.4.
     *
     * @param toggle Whether to infer the module path.
     */
    void setInferModulePath(Provider<Boolean> toggle);
}
