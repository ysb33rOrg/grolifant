/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.downloader;

import java.io.File;
import java.net.URI;

/**
 * Interface for downloading a package or distribution and logging progress using internal Gradle mechanisms.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface Downloader {

    /**
     * Logs a progress message.
     *
     * @param msg Message.
     */
    void logProgress(final String msg);

    /**
     * Downloads a package / distribution / file
     *
     * By default file and http(s) schemes are supported. If Gradle adds additional schemes they are automatically
     * supported.
     *
     * @param address URI to download package from.
     * @param destination Destination to download file to.
     */
    void download(URI address, File destination);

    /**
     * Returns information on where files are downloaded to.
     *
     * @param address URI to download package from.
     * @param downloadRoot THe root directory where download are sent to.
     * @param relativeBasePath A relative path to the download root where to place files and temporary artifacts.
     * @return INformation on where the file will be downloaded to.
     */
    DownloadedLocalFile downloadLocation(URI address,File downloadRoot, String relativeBasePath);

    /**
     * Returns information on where files are downloaded to.
     *
     * @param address URI to download package from.
     * @param downloadRoot THe root directory where download are sent to.
     * @param relativeBasePath A relative path to the download root where to place files and temporary artifacts.
     * @param checksum Add checksum verification.
     * @return Information on where the file will be downloaded to.
     */
    DownloadedLocalFile downloadLocation(URI address,File downloadRoot, String relativeBasePath, String checksum);

}
