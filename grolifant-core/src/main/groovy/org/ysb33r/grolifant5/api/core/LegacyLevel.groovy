/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core

import groovy.transform.CompileStatic
import org.gradle.util.GradleVersion

/**
 * Internal used enumeration to help with logic with specific Gradle version differences.
 *
 */
@CompileStatic
class LegacyLevel {

    /**
     *
     * @param gradleVersion A given Gradle version
     * @return if the current version is older than the provided Gradle version.
     *
     * @since 4.0
     */
    static boolean of(String gradleVersion) {
        GradleVersion.current() < GradleVersion.version(gradleVersion)
    }

    @Deprecated
    public static final boolean PRE_4_1 = of('4.1')

    @Deprecated
    public static final boolean PRE_4_2 = of('4.2')

    @Deprecated
    public static final boolean PRE_4_3 = of('4.3')

    @Deprecated
    public static final boolean PRE_4_5 = of('4.5')

    @Deprecated
    public static final boolean PRE_4_6 = of('4.6')

    @Deprecated
    public static final boolean PRE_4_7 = of('4.7')

    @Deprecated
    public static final boolean PRE_4_8 = of('4.8')

    @Deprecated
    public static final boolean PRE_4_9 = of('4.9')

    @Deprecated
    public static final boolean PRE_4_10 = of('4.10')

    @Deprecated
    public static final boolean PRE_5_0 = of('5.0')

    @Deprecated
    public static final boolean PRE_5_1 = of('5.1')

    @Deprecated
    public static final boolean PRE_5_2 = of('5.2')

    @Deprecated
    public static final boolean PRE_5_5 = of('5.5')

    @Deprecated
    public static final boolean PRE_5_6 = of('5.6')

    @Deprecated
    public static final boolean PRE_6_0 = of('6.0')

    @Deprecated
    public static final boolean PRE_6_1 = of('6.1')

    @Deprecated
    public static final boolean PRE_6_4 = of('6.4')

    @Deprecated
    public static final boolean PRE_6_5 = of('6.5')

    @Deprecated
    public static final boolean PRE_6_6 = of('6.6')

    @Deprecated
    public static final boolean PRE_6_8 = of('6.8')

    @Deprecated
    public static final boolean PRE_7_0 = of('7.0')

    @Deprecated
    public static final boolean PRE_7_1 = of('7.1')

    @Deprecated
    public static final boolean PRE_7_2 = of('7.2')

    @Deprecated
    public static final boolean PRE_7_3 = of('7.3')

    /**
     * @since 5.0
     */
    public static final boolean PRE_7_4 = of('7.4')
    /**
     * @since 5.0
     */
    public static final boolean PRE_7_5 = of('7.5')
    public static final boolean PRE_7_6 = of('7.6')
    public static final boolean PRE_8_0 = of('8.0')
    public static final boolean PRE_8_1 = of('8.1')
    /**
     * @since 2.2
     */
    public static final boolean PRE_8_2 = of('8.2')
    /**
     * @since 2.2
     */
    public static final boolean PRE_8_3 = of('8.3')
    /**
     * @since 2.2
     */
    public static final boolean PRE_8_4 = of('8.4')
    /**
     * @since 2.2
     */
    public static final boolean PRE_8_5 = of('8.5')
    /**
     * @since 2.2
     */
    public static final boolean PRE_8_6 = of('8.6')
    /**
     * @since 3.0
     */
    public static final boolean PRE_8_7 = of('8.7')
    /**
     * @since 4.0
     */
    public static final boolean PRE_8_8 = of('8.8')
    /**
     * @since 4.0
     */
    public static final boolean PRE_8_9 = of('8.9')
    /**
     * @since 4.0
     */
    public static final boolean PRE_8_10 = of('8.10')
    /**
     * @since 5.0
     */
    public static final boolean PRE_8_11 = of('8.11')
    /**
     * @since 5.0
     */
    public static final boolean PRE_9_0 = of('9.0')
}
