/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.provider.Provider;

import java.io.File;
import java.util.List;

/**
 * Sets the definition on how to find a tool.
 *
 * @since 2.0
 */
public interface BaseToolLocation {

    /**
     * A provider for a resolved executable.
     *
     * @return File provider.
     */
    Provider<File> getExecutable();


    /**
     * Locate an executable by a local path.
     *
     * @param path Something resolvable to a {@link File}.
     */
    void executableByPath(Object path);

    /**
     * Locate executable by searching the current environmental search path.
     *
     * @param baseName The base name of the executable
     */
    void executableBySearchPath(Object baseName);

    /**
     * When searching for executables, the order in which to use check for file extensions.
     *
     * @param order List of extensions.
     */
    void setWindowsExtensionSearchOrder(Iterable<String> order);

    /**
     * When searching for executables, the order in which to use check for file extensions.
     *
     * @param order List of extensions.
     */
    void setWindowsExtensionSearchOrder(String... order);

    /**
     * The order in which extensions will be checked for locating an executable on Windows.
     *
     * The default is to use whatever is in the {@code PATHEXT} environmental variable.
     *
     * @return Extension search order
     */
    List<String> getWindowsExtensionSearchOrder();

}
