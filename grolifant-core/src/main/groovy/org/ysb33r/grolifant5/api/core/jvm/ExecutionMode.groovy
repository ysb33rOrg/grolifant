/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException

/**
 * Method to execution an application on the JVM
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
enum ExecutionMode {
    CLASSPATH,
    OUT_OF_PROCESS,
    JAVA_EXEC

    /**
     * Construct an execution mode from a worker isolation mode.
     *
     * @param wi Worker isolation.
     *
     * @return Matching exectuion mode.
     */
    static ExecutionMode of(org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation wi) {
        valueOf(wi.name())
    }

    /**
     * If the execution mode is worker related, return the appropriate isolation mode
     *
     * @return Associated worker isolation
     *
     * @throw UnsupportedConfigurationException if {@link #JAVA_EXEC}.
     */
    org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation workerIsolation() {
        if (this == JAVA_EXEC) {
            throw new UnsupportedConfigurationException('JAVA_EXEC is not a worker process')
        } else {
            org.ysb33r.grolifant5.api.core.jvm.worker.WorkerIsolation.valueOf(name())
        }
    }
}