/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.artifacts;

import java.io.File;
import java.net.URI;

/**
 * Performs a cache refresh of a set of dependencies.
 *
 * @author Schalk W> Cronjé
 *
 * @since 2.0
 */
public interface CacheRefresh {
    /**
     * Refreshed the cache from a remtoe site into a local filesystem.
     *
     * @param downloadURI Source.
     * @param localPath Root for local cache. COould also be a single file.
     */
    void refresh(URI downloadURI, File localPath);
}
