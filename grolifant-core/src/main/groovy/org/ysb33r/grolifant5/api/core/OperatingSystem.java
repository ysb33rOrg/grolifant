/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
/*
    This code was lifted from the Gradle org.gradle.internal.os.OperatingSystem class
    which is under the Apache v2.0 license. Original copyright from 2010 remains. Modifications
    from 2017+ are under the copyright and licensed mentioned above
*/
package org.ysb33r.grolifant5.api.core;

import org.ysb33r.grolifant5.internal.core.os.OperatingSystemConstants;
import org.ysb33r.grolifant5.internal.core.os.OperatingSystemUtils;

import java.io.File;
import java.util.List;

/**
 * Operating system information.
 */
public interface OperatingSystem {
    /**
     * Check is this is Microsoft Windows
     *
     * @return {@code true} if Windows
     */
    boolean isWindows();

    /**
     * Check is this is Apple Mac OS X
     *
     * @return {@code true} if Mac OS X
     */
    boolean isMacOsX();

    /**
     * Check is this is a Linux flavour
     *
     * @return {@code true} if any kind of Linux
     */
    boolean isLinux();

    /**
     * Check is this is FreeBSD
     *
     * @return {@code true} if FreeBSD
     */
    boolean isFreeBSD();

    /**
     * Check is this is NetBSD
     *
     * @return {@code true} if NetBSD
     */
    boolean isNetBSD();

    /**
     * Check is this is a Solaris flavour
     *
     * @return {@code true} if Solaris
     */
    boolean isSolaris();

    /**
     * Check is this is any kind of Unix-like O/S
     *
     * @return {@code true} if any kind of Unix-like O/S
     */
    boolean isUnix();

    /**
     * The character used to separate elements in a system search path
     *
     * @return OS-specific separator.
     */
    String getPathSeparator();

    /**
     * Stringize implementation
     *
     * @return Name, Version and Architecture
     */
    String toString();

    /**
     * Locates the given exe in the system path.
     *
     * @param name Name of exe to search for.
     * @return Executable location of {@code null} if not found.
     */
    File findInPath(String name);

    /**
     * List of system search paths
     *
     * @return List of entries (can be empty).
     */
    List<File> getPath();

    /**
     * Find all files in system search path of a certain name.
     *
     * @param name Name to look for
     * @return List of files
     */
    List<File> findAllInPath(String name);

    /**
     * Returns list of possible OS-specific decorated exe names.
     *
     * @param executablePath Name of exe
     * @return Returns a list of possible appropriately decorated exes
     */
    ;

    List<String> getExecutableNames(String executablePath);

    /**
     * Architecture underlying the operating system
     *
     * @return Architecture type. Returns {@code OperatingSystem.Arch.UNKNOWN} is it cannot be identified. In that a
     * caller might need to use {@link #getArchStr()} to help with identification.
     */
    Arch getArch();

    /**
     * Architecture underlying the operating system
     *
     * @return Architecture string
     */
    ;

    String getArchStr();

    /**
     * OS-dependent string that is used to suffix to shared libraries
     *
     * @return Shared library suffix
     */
    String getSharedLibrarySuffix();

    /**
     * OS-dependent string that is used to suffix to static libraries
     *
     * @return Static library suffix
     */
    String getStaticLibrarySuffix();

    /**
     * Returns OS-specific shared library name
     *
     * @param libraryName This can be a base name or a full name.
     * @return Shared library name.
     */
    String getSharedLibraryName(String libraryName);

    /**
     * Returns OS-specific static library name
     *
     * @param libraryName This can be a base name or a full name.
     * @return Static library name.
     */
    String getStaticLibraryName(String libraryName);

    /**
     * Returns a representation of the operating system that the JVM currently runs on.
     *
     * @return An object implementing an extension of {@link OperatingSystem}.
     */
    static OperatingSystem current() {
        return OperatingSystemUtils.CURRENT;
    }

    String getName();

    String getVersion();

    /**
     * The name of the environment variable that keeps the system path.
     *
     * @return Name
     */
    String getPathVar();

    /**
     * The name of the environment variable that keeps the home directory.
     *
     * @return Name
     *
     * @since 5.2
     */
    String getHomeVar();

    /**
     * Enumeration representing common hardware-operating system architectures.
     */
    enum Arch {
        /**
         * Classic x86 64-bit.
         */
        X86_64(OperatingSystemConstants.AMD64),
        /**
         * Classic x86 32-bit.
         */
        X86(OperatingSystemConstants.I386),
        /**
         * PowerPC architecture.
         */
        POWERPC("ppc"),
        /**
         * SPARC (Sun) architecture.
         */
        SPARC("sparc"),
        /**
         * ARM 64-bit.
         */
        ARM64(OperatingSystemConstants.AARCH64),
        /**
         * ARM 32-bit.
         *
         * @since 2.2
         */
        ARM32(OperatingSystemConstants.AARCH32),
        /**
         * S/390 32-bit.
         *
         * @since 2.2
         */
        S390_32("s390_32"),
        /**
         * S/390 64-bit.
         *
         * @since 2.2
         */
        S390_64("s390_64"),
        /**
         * Unknown architecture.
         */
        UNKNOWN("(unknown)");

        private Arch(final String id) {
            this.id = id;
        }

        private final String id;
    }
}
