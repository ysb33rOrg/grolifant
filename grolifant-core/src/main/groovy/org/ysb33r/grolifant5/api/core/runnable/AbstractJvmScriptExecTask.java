/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.executable.ScriptSpec;

/**
 * Configures a task that will run an execution in a forked JVM.
 *
 * @author Schalk W. Cronj̧é
 * @since 2.0
 */
public class AbstractJvmScriptExecTask<T extends AbstractJvmScriptExecSpec<T>>
        extends AbstractJvmExecTask<T>
        implements ForkedJvmScript<T> {

    /**
     * Configures the script specification.
     *
     * @param configurator Action to configure the script.
     * @return This object.
     */
    @Override
    public T script(Action<ScriptSpec> configurator) {
        return ((T) (getJvmExecSpec().script(configurator)));
    }

    /**
     * Configures script elements by Groovy closure.
     *
     * @param specConfigurator Configurating closure.
     * @return {@code this}
     */
    @Override
    public T script(@DelegatesTo(ScriptSpec.class) Closure<?> specConfigurator) {
        return ((T) (getJvmExecSpec().script(specConfigurator)));
    }

    /**
     * JVM exec that can support workers.
     *
     * @param we Worker
     */
    protected AbstractJvmScriptExecTask(WorkerExecutor we) {
        super(we);
    }

    /**
     * JVM exec that does not support workers.
     */
    protected AbstractJvmScriptExecTask() {
        super(null);
    }
}
