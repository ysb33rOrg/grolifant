/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.internal.core.strings.casefmt.CaseFormat
import org.ysb33r.grolifant5.internal.core.strings.casefmt.CaseFormatUtils
import org.ysb33r.grolifant5.internal.core.strings.casefmt.SimpleCaseFormatter

import java.util.function.BiFunction
import java.util.function.Function

import static org.ysb33r.grolifant5.api.core.StringTools.EMPTY

/**
 * Simple case formats.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.2
 */
@CompileStatic
@SuppressWarnings('UnnecessaryCollectCall')
enum CaseFormats {
    /**
     * Hyphenated variable naming convention, e.g., "lower-hyphen".
     */
    KEBAB_CASE(
        { String input, Boolean correctCase ->
            input.replaceAll("[${DOT}${UNDERSCORE}]+", EMPTY)
                .split(/${HYPHEN}+/)
                .collect { correctCase ? it.toLowerCase(Locale.US) : it }
        },
        { Iterable<String> parts ->
            parts.collect { it.toLowerCase(Locale.US) }.join(HYPHEN)
        }
    ),

    /**
     * "upper-hyphen".
     */
    SCREAMING_KEBAB_CASE(
        { String input, Boolean correctCase ->
            input.replaceAll("[${DOT}${UNDERSCORE}]+", EMPTY)
                .split(/${HYPHEN}+/)
                .collect { correctCase ? it.toUpperCase(Locale.US) : it }
        },
        { Iterable<String> parts ->
            parts.collect { it.toUpperCase(Locale.US) }.join(HYPHEN)
        }

    ),

    /**
     * C++ variable naming convention, e.g., "lower_underscore".
     *
     */
    SNAKE_CASE(
        { String input, Boolean correctCase ->
            input.replaceAll("[${DOT}${HYPHEN}]+", EMPTY)
                .split(/${UNDERSCORE}+/)
                .collect { correctCase ? it.toLowerCase(Locale.US) : it }
        },
        { Iterable<String> parts ->
            parts.collect { it.toLowerCase(Locale.US) }.join(UNDERSCORE)
        }
    ),

    /** Java variable naming convention, e.g., "lowerCamel". */
    LOWER_CAMEL(
        { String input, Boolean correctCase ->
            CaseFormatUtils.splitCaseParts(input)
        },
        { Iterable<String> parts ->
            parts.withIndex().collect { it.v2 ? it.v1.capitalize() : it.v1.uncapitalize() }
                .join(EMPTY)
        }
    ),

    /** Java and C++ class naming convention, e.g., "UpperCamel". */
    UPPER_CAMEL(
        { String input, Boolean correctCase ->
            CaseFormatUtils.splitCaseParts(input)
        },
        { Iterable<String> parts -> parts.collect { it.capitalize() }.join(EMPTY) }
    ),

    /**
     * Java and C++ constant naming convention, e.g., "UPPER_UNDERSCORE".
     * Also environment variables.
     */
    SCREAMING_SNAKE_CASE(
        { String input, Boolean correctCase ->
            input.replaceAll("[${DOT}${HYPHEN}]+", EMPTY)
                .split(/${UNDERSCORE}+/)
                .collect { correctCase ? it.toUpperCase(Locale.US) : it }
        },
        { Iterable<String> parts ->
            parts.collect { it.toUpperCase(Locale.US) }.join(UNDERSCORE)
        }
    ),

    /**
     * Dotted as in Gradle and system properties.
     */
    LOWER_DOTTED(
        { String input, Boolean correctCase ->
            input.replaceAll("[${HYPHEN}${UNDERSCORE}]+", EMPTY)
                .split(/${DOT}+/)
                .collect { correctCase ? it.toLowerCase(Locale.US) : it }
        },
        { Iterable<String> parts ->
            parts.collect { it.toLowerCase(Locale.US) }.join(DOT)
        }
    )

    /**
     * Converts an input which is in this format to another format
     * @param format Format to convert to.
     * @param input Input string
     * @return Converted string
     */
    String convertTo(CaseFormats format, String input) {
        final parts = formatter.decompose(input, true)
        format.addParts(parts)
    }

    /**
     * Converts an input which is in this format to another format.
     * Does not perform case correction on the input.
     *
     * @param format Format to convert to.
     * @param input Input string
     * @return Converted string
     */
    String convertWithoutCaseCorrection(CaseFormats format, String input) {
        final parts = formatter.decompose(input, false)
        format.addParts(parts)
    }

    /**
     * Adds a set of input parts together using a specific formatter.
     *
     * @param parts Input parts
     * @return Converted string.
     */
    String addParts(Iterable<String> parts) {
        formatter.compose(parts)
    }

    private CaseFormats(
        BiFunction<String, Boolean, Iterable<String>> decomposer,
        Function<Iterable<String>, String> composer
    ) {
        this.formatter = SimpleCaseFormatter.from(decomposer, composer)
    }

    private final CaseFormat formatter
    private static final String HYPHEN = '-'
    private static final String DOT = '.'
    private static final String UNDERSCORE = '_'
}
