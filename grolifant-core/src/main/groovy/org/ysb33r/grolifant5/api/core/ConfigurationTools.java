/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Action;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.attributes.AttributeContainer;

import java.util.Collection;

/**
 * Additional tools to work with configurations.
 *
 * @author Schalk W. Cronjé
 * @since 2.1
 */
public interface ConfigurationTools {

    /**
     * Resolves an arbitrary item to a {@link Configuration} instance.
     *
     * @param configurationThingy Instance or {@link Configuration} or something that resolves to a string.
     * @return Configuration
     */
    Configuration asConfiguration(Object configurationThingy);

    /**
     * Resolves arbitrary items to a collection of {@link Configuration} instances.
     *
     * @param configurationThingies Collection that might contain {@link Configuration} or string-type instances
     * @return Collection of resolved {@link Configuration} instances
     */
    Collection<Configuration> asConfigurations(Collection<?> configurationThingies);

    /**
     * Creates three configurations that are related to each other.
     * <p>
     * This works on the same model as to how {@code implementation}, {@code runtimeClasspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     * @param consumableConfigurationName       The name of a configuration that can be consumed by other subprojects.
     */
    default void createRoleFocusedConfigurations(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName,
            String consumableConfigurationName
    ) {
        createRoleFocusedConfigurations(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                consumableConfigurationName,
                x -> {
                }
        );
    }

    /**
     * Creates three configurations that are related to each other.
     * <p>
     * This works on the same model as to how {@code implementation}, {@code runtimeClasspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     * @param consumableConfigurationName       The name of a configuration that can be consumed by other subprojects.
     * @param visible                           Whether configurations should be marked visible or invisible.
     *
     * @since 5.0
     */
    default void createRoleFocusedConfigurations(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName,
            String consumableConfigurationName,
            boolean visible
    ) {
        createRoleFocusedConfigurations(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                consumableConfigurationName,
                visible,
                x -> {
                }
        );
    }

    /**
     * Creates three configurations that are related to each other.
     * <p>
     * This works on the same model as to how {@code implementation}, {@code runtimeClasspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     * @param consumableConfigurationName       The name of a configuration that can be consumed by other subprojects.
     * @param attributes                        Action to configure attributes for resolvable and consumable configurations.
     */
    default void createRoleFocusedConfigurations(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName,
            String consumableConfigurationName,
            Action<? super AttributeContainer> attributes
    ) {
        createRoleFocusedConfigurations(
                dependencyScopedConfigurationName,
                resolvableConfigurationName,
                consumableConfigurationName,
                true,
                attributes
        );
    }

    /**
     * Creates three configurations that are related to each other.
     * <p>
     * This works on the same model as to how {@code implementation}, {@code runtimeClasspath} and
     * {@code runtimeElements} are related in a JVM project.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     * @param consumableConfigurationName       The name of a configuration that can be consumed by other subprojects.
     * @param attributes                        Action to configure attributes for resolvable and consumable configurations.
     * @param visible                           Whether configurations should be marked visible or invisible.
     */
    void createRoleFocusedConfigurations(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName,
            String consumableConfigurationName,
            boolean visible,
            Action<? super AttributeContainer> attributes
    );

    /**
     * Creates two configurations that are related to each other and which are only meant to be used within the
     * same (sub)project.
     * <p>
     * This works on the same model as to how {@code implementation} and {@code runtimeClasspath}.
     * Configurations created with this method will be visible.
     * </p>
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     */
    default void createLocalRoleFocusedConfiguration(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName
    ) {
        createLocalRoleFocusedConfiguration(dependencyScopedConfigurationName, resolvableConfigurationName, true);
    }

    /**
     * Creates two configurations that are related to each other and which are only meant to be used within the
     * same (sub)project.
     * <p>
     * This works on the same model as to how {@code implementation} and {@code runtimeClasspath}.
     *
     * @param dependencyScopedConfigurationName The name of a configuration against which dependencies wil be declared.
     * @param resolvableConfigurationName       The name of a configuration which can be resolved.
     * @param visible Whether the configurations should be visible.
     *
     * @since 5.0
     */
    void createLocalRoleFocusedConfiguration(
            String dependencyScopedConfigurationName,
            String resolvableConfigurationName,
            boolean visible
    );

    /**
     * Creates a single configuration to be used for outgoing publications.
     * <p>Very useful for sharing internal outputs between subprojects.</p>
     *
     * @param configurationName Name of outgoing configuration.
     * @param visible Whether the configuration is visible.
     *
     * @since 5.0
     */
    default void createSingleOutgoingConfiguration(
            String configurationName,
            boolean visible
    ) {
        createSingleOutgoingConfiguration(configurationName,visible, x -> {});
    }

    /**
     * Creates a single configuration to be used for outgoing publications.
     * <p>Very useful for sharing internal outputs between subprojects.</p>
     *
     * @param configurationName Name of outgoing configuration.
     * @param visible Whether the configuration is visible.
     * @param attributes Action to configure attributes for resolvable and consumable configurations.
     * @since 5.0
     */
    void createSingleOutgoingConfiguration(
            String configurationName,
            boolean visible,
            Action<? super AttributeContainer> attributes
    );

}
