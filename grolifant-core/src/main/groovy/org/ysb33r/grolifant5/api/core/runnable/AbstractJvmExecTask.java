/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.workers.WorkerExecutor;
import org.ysb33r.grolifant5.api.core.jvm.worker.StandardWorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.core.jvm.worker.WorkerAppParameterFactory;
import org.ysb33r.grolifant5.api.remote.worker.StandardWorkerAppMainExecutorFactory;
import org.ysb33r.grolifant5.api.remote.worker.StandardWorkerAppParameters;
import org.ysb33r.grolifant5.api.remote.worker.WorkerAppExecutorFactory;

/**
 * Configures a task that will run an execution in a forked JVM.
 *
 * @author Schalk W. Cronj̧é
 * @since 2.0
 */
public class AbstractJvmExecTask<T extends AbstractJvmExecSpec>
        extends AbstractJvmModelExecTask<T, StandardWorkerAppParameters>
        implements ForkedJvmExecutable<T> {

    /**
     * A JVM exec task that can support workers.
     *
     * @param we WorkerExecutor to use.
     */
    protected AbstractJvmExecTask(WorkerExecutor we) {
        super(we);
    }

    /**
     * A JVM exec task that cannot utilise workers.
     */
    protected AbstractJvmExecTask() {
        super(null);
    }

    /**
     * Create a worker app executor factory.
     * <p>
     * By default, this factory will look for a {@code public static int/void main} method. If this method uses
     * {@code System.exit} to return exit codes, then it will cause workers to exit prematurely. Only use the default
     * when either only using {@code ExecutionMode.JAVA_EXEC} or if the {@code main} method does not not use
     * {@code System.exit}.
     * </p>
     * <p>
     * For all other cases override this and provide your own implementation.
     * </p>
     *
     * @return Instance of an execution factory.
     */
    @Override
    protected WorkerAppExecutorFactory<StandardWorkerAppParameters> createExecutorFactory() {
        return new StandardWorkerAppMainExecutorFactory();
    }

    /**
     * Create a worker app parameter factory.
     *
     * @return Instance of a parameter factory.
     */
    @Override
    protected WorkerAppParameterFactory<StandardWorkerAppParameters> createParameterFactory() {
        return new StandardWorkerAppParameterFactory();
    }
}
