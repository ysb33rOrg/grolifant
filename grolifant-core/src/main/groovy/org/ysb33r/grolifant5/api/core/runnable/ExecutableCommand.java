/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint;

/**
 * Describes a command-based executable
 */
public interface ExecutableCommand<T extends ExecutableCommand<T>> extends Executable<T> {
    /**
     * Configures the command along with its arguments.
     *
     * @param configurator An action to configure the command
     *
     * @return The configured command
     */
    void cmd(Action<CommandEntryPoint> configurator);

    /**
     * Configures the command along with its arguments.
     *
     * @param configurator A closure to configure the command
     *
     * @return The configured command
     */
    void cmd(@DelegatesTo(CommandEntryPoint.class) Closure<?> configurator);
}
