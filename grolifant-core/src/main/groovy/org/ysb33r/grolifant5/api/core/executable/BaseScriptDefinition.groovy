/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.AbstractCmdlineArgumentSpec

import java.util.concurrent.Callable

/**
 * Defines a base class for implementing a basic script definition.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class BaseScriptDefinition extends AbstractCmdlineArgumentSpec implements CmdlineArgumentSpec, BaseScriptSpec {

    /**
     * A base definition of a script to be executed.
     *
     * @param po Configuration-cache safe operations.
     *
     * @since 5.0
     */
    BaseScriptDefinition(ConfigCacheSafeOperations po) {
        super(po.stringTools(), po.providerTools())
        nameProvider = po.providerTools().provider(new Callable<String>() {
            @Override
            String call() {
                po.stringTools().stringizeOrNull(nameStore.empty ? null : nameStore[0])
            }
        })
    }

    // Deprecated since 5.0
    @Deprecated
    BaseScriptDefinition(ProjectOperations po) {
        super(po.stringTools, po.providers)
        nameProvider = po.provider(new Callable<String>() {
            @Override
            String call() {
                po.stringTools.stringizeOrNull(nameStore.empty ? null : nameStore[0])
            }
        })
    }

    /**
     * Sets the name of the script to use.
     *
     * @param lazyName Lazy evaluated name. Needs to evaluate to a string.
     */
    void setName(Object lazyName) {
        this.nameStore.add(lazyName)
    }

    /**
     * Name of script.
     *
     * @return Provider to a script name.
     */
    @Internal
    Provider<String> getName() {
        nameProvider
    }

    /**
     * A provider to arguments that will be inserted before any supplied arguments.
     *
     * @return Arguments provider
     */
    @Override
    Provider<List<String>> getPreArgs() {
        this.preArgsProvider
    }

    private final List<Object> nameStore = []
    private final Provider<String> nameProvider
    private final Provider<List<String>> preArgsProvider
}
