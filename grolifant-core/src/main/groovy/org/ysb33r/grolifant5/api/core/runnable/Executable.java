/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.tasks.Internal;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint;
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec;

/**
 * A process that can be executed outside the JVM.
 *
 * @param <T> Classes that extends {@code Executable}.
 *
 * @since 2.0
 */
public interface Executable<T extends Executable<T>> {

    /**
     * Configures the native entrypoint.
     *
     * @param configurator An action to configure the external executable's entrypoint.
     *
     */
    void entrypoint(Action<ExecutableEntryPoint> configurator);

    /**
     * Configures the native entrypoint.
     *
     * @param configurator A closure to configure the external executable's entrypoint.
     *
     */
    void entrypoint(@DelegatesTo(ExecutableEntryPoint.class) Closure<?> configurator);

    /**
     * Access to the entrypoint
     *
     * @return An instance that implements {@link ExecutableEntryPoint}.
     *
     * @since 5.2
     */
    @Internal
    ExecutableEntryPoint getEntrypoint();

    /**
     * Configures the arguments.
     *
     * @param configurator An action to configure the arguments.
     *
     */
    void runnerSpec(Action<CmdlineArgumentSpec> configurator);

    /**
     * Configures the arguments.
     *
     * @param configurator A closure to configure the arguments.
     *
     */
    void runnerSpec(@DelegatesTo(CmdlineArgumentSpec.class) Closure<?> configurator);

    /**
     * Access to the runner specification.
     *
     * @return An instance that implements {@link CmdlineArgumentSpec}.
     *
     * @since 5.2
     */
    @Internal
    CmdlineArgumentSpec getRunnerSpec();

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator An action to configure the execution.
     *
     */
    void process(Action<ProcessExecutionSpec> configurator);

    /**
     * Configures the stream redirections and exit checks.
     *
     * @param configurator A closure to configure the execution.
     *
     */
    void process(@DelegatesTo(ProcessExecutionSpec.class) Closure<?> configurator);

    /**
     * Access to the process specification.
     *
     * @return An instance that implements {@Link ProcessExecutionSpec}.
     *
     * @since 5.2
     */
    @Internal
    ProcessExecutionSpec getProcess();
}
