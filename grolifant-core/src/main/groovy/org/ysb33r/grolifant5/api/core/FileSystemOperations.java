/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.file.ConfigurableFileTree;
import org.gradle.api.file.CopySpec;
import org.gradle.api.file.DeleteSpec;
import org.gradle.api.file.FileTree;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.resources.ReadableResource;
import org.gradle.api.tasks.WorkResult;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Abstracts a number of file operations.
 * <p>
 * Also Acts as a proxy for the {@code FileSystemOperations} that was implemented in Gradle 6.0,
 * but allows for a substitute operation on old Gradle versions.
 *
 * @since 1.3
 */
public interface FileSystemOperations {

    /**
     * Safely resolve the stringy item as a path relative to the build directory.
     *
     * @param stringy Any item that can be resolved to a string using
     *                {@code org.ysb33r.grolifant5.core.api.grolifant.StringTools#stringize}.
     * @return Provider to a file
     * @since 5.0
     */
    Provider<File> buildDirDescendant(Object stringy);

    /**
     * Creates resource that points to a bzip2 compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    ReadableResource bzip2Resource(Object file);

    /**
     * Creates resource that points to a gzip compressed file at the given path.
     *
     * @param file File evaluated as per {@link #file}.
     * @return Readable resource
     */
    ReadableResource gzipResource(Object file);

    /**
     * Copies the specified files.
     *
     * @param action Configures a {@link CopySpec}
     * @return Result of copy to check whether it was successful.
     */
    WorkResult copy(Action<? super CopySpec> action);

    /**
     * Creates an empty {@link CopySpec}
     *
     * @return Empty copy specification.
     * @since 5.1
     */
    CopySpec copySpec();

    /**
     * Creates a copy specification and configures it.
     *
     * @param action Configurator for copy specification
     * @return Configured copy specification
     */
    default CopySpec copySpec(Action<? super CopySpec> action) {
        CopySpec cs = copySpec();
        action.execute(cs);
        return cs;
    }

    /**
     * Creates a copy specification and configures it.
     *
     * @param action Configurator for copy specification
     * @return Configured copy specification
     */
    default CopySpec copySpec(@DelegatesTo(CopySpec.class) Closure<?> action) {
        CopySpec cs = copySpec();
        ClosureUtils.configureItem(cs, action);
        return cs;
    }

    /**
     * Copies the file at the resource path to a local file.
     *
     * @param resourcePath Resource path.
     * @param destFile     Destination file.
     * @throws org.gradle.api.resources.ResourceException
     * @since 5.0
     */
    void copyResourceToFile(String resourcePath, File destFile);

    /**
     * Creates a temporary directory with the given prefix.
     * <p>
     * Directory will be set to delete on VM exit, but consumers are allowed to delete earlier.
     * <p>
     * The directory will be created somewhere in the build directory.
     *
     * @param prefix Prefix for directory.
     * @return Directory
     * @since 2.0
     */
    File createTempDirectory(String prefix);

    /**
     * Deletes the specified files.
     *
     * @param action Configures a {@link DeleteSpec}
     * @return Result of deletion to check whether it was successful.
     */
    WorkResult delete(Action<? super DeleteSpec> action);

    /**
     * Creates an empty file collection.
     *
     * @return Empty file collection.
     * @since 2.0
     */
    ConfigurableFileCollection emptyFileCollection();

    /**
     * Converts a file-like object to a {@link java.io.File} instance with project context.
     * <p>
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory} (Gradle 4.1+)
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * @param file Potential {@link File} object
     * @return File instance.
     */
    File file(Object file);

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     */
    File fileOrNull(Object file);

    /**
     * Converts a file-like objects to {@link java.io.File} instances with project context.
     * <p>
     * Converts collections of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory}
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     * <p>
     * Collections are flattened.
     * Null instances are not allowed.
     *
     * @param files Potential {@link File} objects
     * @return File collection.
     */
    ConfigurableFileCollection files(Collection<?> files);

    /**
     * Obtain a file tree given a starting location
     *
     * @param file Starting location.
     *             Anything convertible with {@link #file} will do.
     * @return File tree
     * @since 5.0
     */
    ConfigurableFileTree fileTree(Object file);

    /**
     * Converts a file-like objects to {@link java.io.File} instances with project context.
     * <p>
     * Converts collections of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory}
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     * <p>
     * Collections are flattened.
     * Null instances are removed.
     *
     * @param files Potential {@link File} objects
     * @return File collection.
     */
    ConfigurableFileCollection filesDropNull(Collection<?> files);

    /**
     * Gradle distribution home directory.
     *
     * @return Directory.
     * @since 5.0
     */
    Provider<File> getGradleHomeDir();

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     *
     * @return Directory.
     * @since 5.0
     */
    Provider<File> getGradleUserHomeDir();

    /**
     * Returns the project's local cache directory
     *
     * @return Project cache directory.
     * @since 5.0
     */
    File getProjectCacheDir();

    /**
     * Returns the project directory.
     *
     * @return Project directory.
     * @since 5.0
     */
    File getProjectDir();

    /**
     * The project root dir.
     *
     * @return Root directory.
     * @since 5.0
     */
    File getProjectRootDir();

    /**
     * Provides a list of directories below another directory
     *
     * @param distDir Directory
     * @return List of directories. Can be empty if, but never {@code null}
     * supplied directory.
     */
    List<File> listDirs(File distDir);

    /**
     * Loads properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @return Properties
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    Properties loadPropertiesFromResource(String resourcePath);

    /**
     * Loads properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @param classLoader  Classloader to use for loading the content.
     * @return Properties
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    Properties loadPropertiesFromResource(String resourcePath, ClassLoader classLoader);

    /**
     * Like {@link #file}, but does not immediately evaluate the passed parameter.
     *
     * @param file Potential {@link File} object
     * @return A provider that will evaluate to a file.
     * @since 5.0
     */
    Provider<File> provideFile(Object file);

//    FileCollection provideFileCollection(Collection<Object> files);

    /**
     * Like {@link #getProjectCacheDir()}, but returns a provider instead.
     *
     * @return Provider to the project's cache directory.
     * @since 5.0
     */
    Provider<File> provideProjectCacheDir();

    /**
     * Like {@link #getProjectDir()}, but returns a provider instead.
     *
     * @return Provider to the project directory.
     * @since 5.0
     */
    Provider<File> provideProjectDir();

    /**
     * Lazy-load properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @return Provider to properties.
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    Provider<Properties> providePropertiesFromResource(String resourcePath);

    /**
     * Lazy-load properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @param classLoader Classloader to use for loading the resource.
     * @return Provider to properties.
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    Provider<Properties> providePropertiesFromResource(String resourcePath,ClassLoader classLoader);

    /**
     * The root directory of the project.
     *
     * @return Provider to the root directory.
     * @since 5.0
     */
    Provider<File> provideRootDir();

    /**
     * Provides a subpath below the project's cache directory.
     * Anything that can evaluater to a string using {@link StringTools}.
     *
     * @return Provider to a file or directory below the project's cache directory.
     * @since 5.0
     */
    Provider<File> provideProjectCacheDirDescendant(Object subpath);

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePath(Object f);

    /**
     * Returns a relative path from the project directory to the given path.
     * When the path is empty, return a dot
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}. Will at minimum contain a dot.
     * @since 2.0
     */
    default String relativePathNotEmpty(Object f) {
        final String path = relativePath(f);
        return path.isEmpty() ? "." : path;
    }

    /**
     * Returns the relative path from the root project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativeRootPath(Object f);

    /**
     * Returns the relative path from the given path to the project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePathToProjectDir(Object f);

    /**
     * Returns the relative path from the given path to the root project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePathToRootDir(Object f);

    /**
     * Returns the path of one File relative to another.
     *
     * @param target the target directory
     * @param base   the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     * @since 2.0
     */
    String relativize(File base, File target);

    /**
     * Returns the path of one Path relative to another.
     *
     * @param target the target directory
     * @param base   the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     * @since 2.0
     */
    String relativize(Path base, Path target);

    /**
     * Given a {@Link CopySpec}, resolve all of the input files in to a collection.
     *
     * @param copySpec Input specification
     * @return Collection of files.
     * @since 2.0
     */
    FileTree resolveFilesFromCopySpec(CopySpec copySpec);

    /**
     * Synchronizes the contents of a destination directory with some source directories and files.
     *
     * @param action Action to configure the CopySpec.
     * @return {@link WorkResult} that can be used to check if the sync did any work.
     */
    WorkResult sync(Action<? super CopySpec> action);

    /**
     * Expands a tar file into a {@link FileTree}.
     *
     * @param tarPath Path to tar file.
     *                Anything that can be converted with {@link #file}
     * @return Tree of tar contents.
     * @since 5.0
     */
    FileTree tarTree(Object tarPath);

    /**
     * Converts a string into a string that is safe to use as a file name. T
     * <p>
     * The result will only include ascii characters and numbers, and the "-","_", #, $ and "." characters.
     *
     * @param name A potential file name
     * @return A name that is safe on the local filesystem of the current operating system.
     */
    String toSafeFileName(String name);

    /**
     * Converts a collection of String into a {@link Path} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @since 2.0
     */
    Path toSafePath(String... parts);

    /**
     * Converts a collection of String into a {@@link File} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @since 2.0
     */
    default File toSafeFile(String... parts) {
        return toSafePath(parts).toFile();
    }

    /**
     * Updates a {@code Property<File>}.
     * <p>
     * This method deals with a wide range of possibilities and works around the limitations of
     * {@code Property.set()}
     * </p>
     *
     * @param provider Current provider
     * @param file     Value that should be lazy-resolved.
     * @since 2.0
     */
    void updateFileProperty(Property<File> provider, Object file);

    /**
     * Expands a ZIP file into a {@link FileTree}.
     *
     * @param zipPath Path to tar file.
     *                Anything that can be converted with {@link #file}
     * @return Tree of ZIP contents.
     * @since 5.0
     */
    FileTree zipTree(Object zipPath);
}
