/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.jvm;

import org.gradle.process.JavaForkOptions;

/**
 * Interface for copying JVM debug options.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
public interface CopyDebugOptions {

    /**
     * Proxies between {@link JvmDebugOptions} and Gradle's {@link org.gradle.process.JavaDebugOptions}.
     *
     * @param src Grolifant debug configuration.
     * @param target Java fork options.
     */
    void copyDebugOptions(JvmDebugOptions src, JavaForkOptions target);
}
