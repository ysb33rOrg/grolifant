/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.process.ExecResult;

/**
 * This is modelled on the {@code ExecOutput} that was included as from Gradle 7.6.
 * <p>
 * Grolifant uses a separate class so that it can maintain backwards compatibility.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
public interface ExecOutput {

    /**
     * Throws an exception if the process exited with a non-standard exit value.
     */
    default void assertNormalExitValue() {
        getResult().get().assertNormalExitValue();
    }

    /**
     * Returns a provider of the execution result.
     *
     * <p>
     * The external process is executed only once and only when the value is requested for the first
     * time.
     * </p>
     * <p>
     * If starting the process results in exception then the ensuing exception is permanently
     * propagated to callers of {@link Provider#get}, {@link Provider#getOrElse},
     * {@link Provider#getOrNull} and {@link Provider#isPresent}.
     * </p>
     *
     * @return provider of the execution result.
     */
    Provider<ExecResult> getResult();

    /**
     * Gets a handle to the content of the process' standard output.
     *
     * @return handle of the standard output of the process.
     */
    ExecStreamContent getStandardOutput();

    /**
     * Gets a handle to the content of the process' standard error output.
     *
     * @return handle of the standard error output of the process.
     */
    ExecStreamContent getStandardError();
}
