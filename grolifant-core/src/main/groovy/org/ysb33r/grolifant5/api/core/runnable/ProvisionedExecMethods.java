/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.ClosureUtils;
import org.ysb33r.grolifant5.api.core.ExecTools;
import org.ysb33r.grolifant5.api.core.ProjectOperations;

import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD;

/**
 * Implements the {@code exec} methods from {@link ExecMethods}, leaving it to the plugin author to
 * <p>Expects that the class implementing the interface can supply an instance of {@link ProjectOperations}</p>.
 *
 * @param <E> Execution specification which must extend {@code AbstractExecSpec}.
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ProvisionedExecMethods<E extends AbstractExecSpec<E>>
        extends ExecMethods<E>, ConfigCacheSafeExecMethods<E> {

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     */
    E createExecSpec();

    /**
     * Executes an existing execution specification and forwards output to console.
     *
     * @param spec Specification to execute.
     * @return Execution result.
     */
    default ExecOutput exec(E spec) {
        return exec(FORWARD, FORWARD, spec);
    }

    /**
     * Controls what happens to the output of the execution specification.
     *
     * @param standardOutput What to do with standard output.
     * @param standardError  What to do with standard error.
     * @param spec           Specification to execute
     * @return Execution result.
     * @since 5.0
     */
    @Override
    default ExecOutput exec(ExecTools.OutputType standardOutput, ExecTools.OutputType standardError, E spec) {
        return getConfigCacheSafeOperations().execTools().exec(
                standardOutput,
                standardError,
                execSpec -> spec.copyTo(execSpec)
        );
    }

    /**
     * Controls what happens to the output of the execution specification.
     *
     * @param standardOutput   What to do with standard output.
     * @param standardError    What toi do with standard error.
     * @param specConfigurator Specification configurator.
     * @return Execution result.
     * @since 5.0
     */
    @Override
    default ExecOutput exec(
            ExecTools.OutputType standardOutput,
            ExecTools.OutputType standardError,
            Action<E> specConfigurator
    ) {
        E spec = createExecSpec();
        specConfigurator.execute(spec);
        return exec(standardOutput, standardError, spec);
    }

    /**
     * Creates an execution specification, configures it by Groovy closure and then executes it.
     *
     * @param specConfigurator Configurating closure.
     * @return Result of execution.
     */
    default ExecOutput exec(Closure<?> specConfigurator) {
        E spec = createExecSpec();
        ClosureUtils.configureItem(spec, specConfigurator);
        return exec(FORWARD, FORWARD, spec);
    }

    /**
     * Controls what happens to the output of the execution specification.
     *
     * @param standardOutput   What to do with standard output.
     * @param standardError    What toi do with standard error.
     * @param specConfigurator Specification configurator.
     * @return Execution result.
     * @since 5.0
     */
    default ExecOutput exec(
            ExecTools.OutputType standardOutput,
            ExecTools.OutputType standardError,
            Closure<?> specConfigurator
    ) {
        E spec = createExecSpec();
        ClosureUtils.configureItem(spec, specConfigurator);
        return exec(standardOutput, standardError, spec);
    }
}
