/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.services

import groovy.transform.CompileStatic
import org.gradle.api.services.BuildService
import org.ysb33r.grolifant5.api.core.SimpleSecureString
import org.ysb33r.grolifant5.internal.core.strings.casefmt.DefaultSimpleSecureString

import javax.crypto.Cipher
import javax.crypto.SealedObject
import javax.crypto.spec.DESedeKeySpec
import javax.crypto.spec.SecretKeySpec
import java.security.Key

import static javax.crypto.Cipher.ENCRYPT_MODE

/**
 * A service that can semi-secure credential strings whilst held in memory.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.2
 */
@CompileStatic
@SuppressWarnings(['AbstractClassWithoutAbstractMethod', 'AbstractClassWithPublicConstructor'])
abstract class GrolifantSecurePropertyService implements
    BuildService<GrolifantSecurePropertyServiceParameters>, AutoCloseable {

    GrolifantSecurePropertyService() {
        final bytes = parameters.baseStringForKey.get().sha256().bytes
        this.key = new SecretKeySpec(new DESedeKeySpec(bytes).key, ALGORITHM)
        this.cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(ENCRYPT_MODE, key)
    }

    SimpleSecureString pack(char[] value) {
        new DefaultSimpleSecureString(new SealedObject(value, cipher))
    }

    SimpleSecureString pack(String value) {
        new DefaultSimpleSecureString(new SealedObject(value.toCharArray(), cipher))
    }

    char[] unpack(SimpleSecureString secureString) {
        (char[]) secureString.sealedObject.getObject(key)
    }

    @Override
    void close() throws Exception {
    }

    private final Key key
    private final Cipher cipher
    private static final String ALGORITHM = 'DESede'
}
