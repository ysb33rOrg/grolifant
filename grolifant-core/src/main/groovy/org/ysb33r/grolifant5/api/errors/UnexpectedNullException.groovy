/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.errors

import groovy.transform.CompileStatic

/**
 * Can be thrown when {@code null} is encountered in an unexpected location and the system wants to deal with it
 * more gracefully than a {@link NullPointerException}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class UnexpectedNullException extends RuntimeException implements GrolifantError {
    UnexpectedNullException(final String msg) {
        super(msg)
    }

    UnexpectedNullException(final String message, final Exception e) {
        super(message, e)
    }

    UnexpectedNullException(final String message, final Throwable e) {
        super(message, e)
    }
}
