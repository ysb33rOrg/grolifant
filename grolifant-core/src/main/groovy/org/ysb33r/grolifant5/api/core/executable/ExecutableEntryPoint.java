/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.gradle.process.ProcessForkOptions;

import java.util.Map;

/**
 * Describes the entrypoint for an external, but non-KVM, process.
 */
public interface ExecutableEntryPoint extends ProcessForkOptions {

    /**
     * Adds a provider to environment variables.
     *
     * The values of the provider are processed after any value set via an {@code environment} call.
     *
     * @param envProvider Provider to a resolved map.
     */
    void addEnvironmentProvider(Provider<? extends Map<String, String>> envProvider);
}
