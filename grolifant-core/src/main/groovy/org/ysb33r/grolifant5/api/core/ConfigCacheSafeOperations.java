/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core;

import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService;
import org.ysb33r.grolifant5.internal.core.SimpleGrolifantComponents;

/**
 * Provides methods to access all the main Grolifant components.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ConfigCacheSafeOperations {

    /**
     * Creates an instance from a project reference.
     *
     * @param project project reference.
     * @return Instance that implements {@link ConfigCacheSafeOperations}.
     */
    static ConfigCacheSafeOperations from(Project project) {
        return new SimpleGrolifantComponents(ProjectOperations.find(project));
    }

    /**
     * Creates an instance from an existing {@link ProjectOperations} reference.
     *
     * @param po ProjectOperations reference.
     * @return Instance that implements {@link ConfigCacheSafeOperations}.
     */
    static ConfigCacheSafeOperations from(ProjectOperations po) {
        return new SimpleGrolifantComponents(po);
    }

    /**
     * Creates an instance from a provider to a {@link ProjectOperations} reference.
     *
     * @param po Provider to a ProjectOperations reference.
     * @return Instance that implements {@link ConfigCacheSafeOperations}.
     */
    static ConfigCacheSafeOperations from(Provider<ProjectOperations> po) {
        return new SimpleGrolifantComponents(po.get());
    }

    /**
     * Creates an instance from another implementor of the {@link ConfigCacheSafeOperations} interface.
     *
     * @param po Another instance implementing the {@link ConfigCacheSafeOperations} interface.
     * @return A new instance that implements {@link ConfigCacheSafeOperations}.
     */
    static ConfigCacheSafeOperations from(ConfigCacheSafeOperations po) {
        return new SimpleGrolifantComponents(po);
    }

    /**
     * Access to {@link ExecTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    ExecTools execTools();

    /**
     * Access to {@link FileSystemOperations} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    FileSystemOperations fsOperations();

    /**
     * Access to {@link JvmTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    JvmTools jvmTools();

    /**
     * Access to {@link ProjectTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    ProjectTools projectTools();

    /**
     * Access to {@link ProviderTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    ProviderTools providerTools();

    /**
     * Access to {@link StringTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */

    StringTools stringTools();
}
