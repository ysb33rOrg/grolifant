/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.executable;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec;

/**
 * Specifies a command along with its arguments.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
public interface ExecutableCommandSpec extends CmdlineArgumentSpec {
    /**
     * The command used in this specification as a String.
     *
     * @return Command
     */
    Provider<String> getCommand();

    /**
     * Set the command to use.
     *
     * @param cmd Anything that can be resolved to a string.
     * @return {@code this}
     */
    void setCommand(Object cmd);

}
