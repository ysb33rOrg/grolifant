/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.ExecTools;

import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD;

/**
 * Adds interface to a {@link AbstractToolExtension} implementation, so that the spirit of the original
 * {@code exec} project extension can be maintained for a specific tool.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
public interface ExecMethods<E extends Executable<E>> {

    /**
     * Creates an execution specification, configure it with the supplied configurator, then executes it.
     *
     * <p>
     *     Output is forwarded to console.
     * </p>
     *
     * @param specConfigurator Specification configurator.
     * @return Execution result.
     */
    default ExecOutput exec(Action<E> specConfigurator) {
        return exec(FORWARD,FORWARD, specConfigurator);
    }

    /**
     * Controls what happens to the output of the execution specification.
     *
     * @param standardOutput What to do with standard output.
     * @param standardError What toi do with standard error.
     * @param specConfigurator Specification configurator.
     * @return Execution result.
     *
     * @since 5.0
     */
    ExecOutput exec(ExecTools.OutputType standardOutput, ExecTools.OutputType standardError, Action<E> specConfigurator);

    /**
     * Executes an existing execution specification and forwards output to console.
     *
     * @param spec Specification to execute.
     * @return Execution result.
     */
    ExecOutput exec(E spec);

    /**
     * Controls what happens to the output of the execution specification.
     *
     * @param standardOutput What to do with standard output.
     * @param standardError What to do with standard error.
     * @param spec Specification to execute
     * @return Execution result.
     *
     * @since 5.0
     */
    ExecOutput exec(ExecTools.OutputType standardOutput, ExecTools.OutputType standardError, E spec);
}
