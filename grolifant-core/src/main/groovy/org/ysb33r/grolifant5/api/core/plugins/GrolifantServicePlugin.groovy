/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.ProjectEvaluationListener
import org.gradle.api.ProjectState
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.services.BuildServiceSpec
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProjectTools
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyService
import org.ysb33r.grolifant5.api.core.services.GrolifantSecurePropertyServiceParameters
import org.ysb33r.grolifant5.internal.core.loaders.ProjectOperationsLoader
import org.ysb33r.grolifant5.loadable.core.ProjectToolsProxy

/**
 * Adds an extension called {@code grolifantOps} and a service called {@code grolifant-secure-property-service}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class GrolifantServicePlugin implements Plugin<Project> {

    public static final String SERVICE_NAME = 'grolifant'
    public static final String LEGACY_EXTENSION_NAME = SERVICE_NAME
    public static final String EXTENSION_NAME = 'grolifantOps'
    public static final String SECURE_STRING_SERVICE = 'grolifant-secure-property-service'

    @Override
    void apply(Project project) {
        createGrolifantExtension(project)

        project.gradle.addProjectEvaluationListener(
            new ProjectVersionEvaluator(ProjectOperations.find(project).projectTools)
        )

        addSecureStringService(project)
    }

    private void addSecureStringService(Project project) {
        final path = ProjectOperations.find(project).fsOperations.projectRootDir.absolutePath
        project.gradle.sharedServices.registerIfAbsent(
            SECURE_STRING_SERVICE,
            GrolifantSecurePropertyService
        ) { BuildServiceSpec<GrolifantSecurePropertyServiceParameters> spec ->
            spec.parameters.baseStringForKey.set(path)
        }
    }

    private void createGrolifantExtension(Project project) {
        try {
            ProjectOperations.find(project)
        } catch (UnknownDomainObjectException e) {
            ProjectOperations po = ProjectOperationsLoader.load(project)
            project.extensions.add(ProjectOperations, GrolifantServicePlugin.EXTENSION_NAME, po)
        }

        if (project.extensions.findByName(LEGACY_EXTENSION_NAME)) {
            project.logger.warn "An extension '${LEGACY_EXTENSION_NAME}' was found. " +
                'This means that both Grolifant5 and an older ' +
                "version are running side-by-side. Please use the ${EXTENSION_NAME} DSL block in your buildscripts " +
                'rather than the deprecated version.'
        }
    }

    static class ProjectVersionEvaluator implements ProjectEvaluationListener {
        ProjectVersionEvaluator(ProjectTools pt) {
            this.projectTools = pt
        }

        final ProjectTools projectTools

        @Override
        void beforeEvaluate(Project project) {
        }

        /**
         * <p>This method is called when a project has been evaluated, and before the evaluated project is
         * made available to other projects.</p>
         *
         * @param project The project which was evaluated. Never null.
         * @param state The project evaluation state. If project evaluation failed, the exception is available in this
         * state. Never null.
         */
        @Override
        void afterEvaluate(Project project, ProjectState state) {
            if (state.executed && state.failure == null) {
                if (project.extensions.findByName(EXTENSION_NAME) &&
                    !(project.version instanceof ProjectToolsProxy.GradleProjectVersion)
                ) {
                    final ver = project.version
                    projectTools.versionProvider = ver
                }
            }
        }
    }
}
