/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.api.core.runnable;

import org.gradle.api.provider.Provider;

/**
 * A handle to access content of the process' standard stream (the standard output of the
 * standard error output).
 * <p>
 * This is modelled on  Gradle's {@code ExecOutput.StandardStreamContent} that appeared in Gradle 7.5.
 * </p>
 *
 * @author Schalk W. Cronjé
 * @since 5.0
 */
public interface ExecStreamContent {
    /**
     * Gets a provider for the standard stream's content that returns it as a String. The output
     * is decoded using the default encoding of the JVM running the build.
     *
     * <p>
     * The external process is executed only once and only when the value is requested for the
     * first time.
     * </p>
     * <p>
     * If starting the process results in exception then the ensuing exception is permanently
     * propagated to callers of {@link Provider#get}, {@link Provider#getOrElse},
     * {@link Provider#getOrNull} and {@link Provider#isPresent}.
     * </p>
     */
    Provider<String> getAsText();

    /**
     * Gets a provider for the standard stream's content that returns it as a byte array.
     *
     * <p>
     * The external process is executed only once and only when the value is requested for the
     * first time.
     * </p>
     * <p>
     * If starting the process results in exception then the ensuing exception is permanently
     * propagated to callers of {@link Provider#get}, {@link Provider#getOrElse},
     * {@link Provider#getOrNull} and {@link Provider#isPresent}.
     * </p>
     */
    Provider<byte[]> getAsBytes();
}
