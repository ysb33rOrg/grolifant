/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.spi.unpackers;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;

import java.io.File;
import java.util.Map;

/**
 * The interface an unpacker has to implement.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
public interface GrolifantUnpacker {

    /**
     * Whether the unplacker plugin support a file with a specific extension.
     *
     * @param extension Extension query.
     *                  The extension must be supplied without a leading dot.
     * @return {@code} if the specific extension is supported
     */
    boolean supportExtension(final String extension);

    /**
     * Create a new unpack engine.
     *
     * @param ops Configuration cache-safe operations
     * @return An unpacking engine
     */
    Engine create(ConfigCacheSafeOperations ops);

    /**
     * Creates a new unpack engine along with additional parameters.
     *
     * @param ops Configuration cache-safe operations
     * @param parameters Configuration parameters
     * @return An unpacking engine
     */
    Engine create(ConfigCacheSafeOperations ops, Parameters parameters);

    /**
     * Operations that an unpacking engine can perform.
     */
    interface Engine {
        /**
         * Unpacks a source archive or compressed file to a specific directory
         * @param srcArchive Source archive or compressed file.
         * @param destDir Destination directory.
         *               If the directory does not exist, the unpacking process will create it.
         */
        void unpack(final File srcArchive, final File destDir);
    }

    /**
     * Configuration parameters for an engine.
     */
    interface Parameters {
        /**
         * Customised environment to run under.
         * <p>
         *     Typically used when running an external binary.
         * </p>
         * @return Environment. Content may not be present, but provider will not be {@code null}.
         */
        Provider<Map<String,String>> getEnvironment();

        /**
         * Relative path below Gradle User Home to create cache for all versions of the distribution type.
         *
         * @return Relative path. Content may not be present,, but provider will not be {@code null}.
         */
        Provider<String> getBaseRelativePath();
    }
}
