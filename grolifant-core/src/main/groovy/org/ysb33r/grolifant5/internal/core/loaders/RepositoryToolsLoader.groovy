/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.loaders

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.loadable.core.Loader
import org.ysb33r.grolifant5.api.core.RepositoryTools
import org.ysb33r.grolifant5.loadable.core.LoadableVersion

@CompileStatic
@Slf4j
class RepositoryToolsLoader {
    @Synchronized
    @SuppressWarnings('UnusedMethodParameter')
    static RepositoryTools load(ProjectOperations incompleteReference, Project project, LoadableVersion version) {
        Class loadable = Loader.load('RepositoryTools', 'DefaultRepositoryTools', version)
        (RepositoryTools) loadable.newInstance(incompleteReference, project)
    }
}
