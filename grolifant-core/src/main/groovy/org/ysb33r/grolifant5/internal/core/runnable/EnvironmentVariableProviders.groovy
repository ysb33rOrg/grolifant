/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider

/**
 * Stores environment variable providers
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class EnvironmentVariableProviders {

    void addEnvironmentProvider(Provider<Map<String, String>> envProvider) {
        providers.add(envProvider)
    }

    List<Provider<Map<String, String>>> getEnvironmentProviders() {
        this.providers
    }

    private final List<Provider<Map<String, String>>> providers = []
}
