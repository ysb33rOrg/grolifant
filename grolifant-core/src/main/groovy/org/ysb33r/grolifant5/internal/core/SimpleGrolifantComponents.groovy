/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.JvmTools
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProjectTools
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools

/**
 * Allows for a quick creation of an instance of {@link ConfigCacheSafeOperations}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class SimpleGrolifantComponents implements ConfigCacheSafeOperations {

    SimpleGrolifantComponents(ConfigCacheSafeOperations po) {
        this.execTools = po.execTools()
        this.fsOperations = po.fsOperations()
        this.stringTools = po.stringTools()
        this.providerTools = po.providerTools()
        this.projectTools = po.projectTools()
        this.jvmTools = po.jvmTools()
    }

    SimpleGrolifantComponents(ProjectOperations po) {
        this.execTools = po.execTools
        this.fsOperations = po.fsOperations
        this.stringTools = po.stringTools
        this.providerTools = po.providerTools
        this.projectTools = po.projectTools
        this.jvmTools = po.jvmTools
    }

    /**
     * Access to {@link ExecTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    ExecTools execTools() {
        this.execTools
    }

    /**
     * Access to {@link FileSystemOperations} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    FileSystemOperations fsOperations() {
        this.fsOperations
    }

    /**
     * Access to {@link JvmTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    JvmTools jvmTools() {
        this.jvmTools
    }

    /**
     * Access to {@link ProjectTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    ProjectTools projectTools() {
        this.projectTools
    }

    /**
     * Access to {@link ProviderTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    ProviderTools providerTools() {
        this.providerTools
    }

    /**
     * Access to {@link StringTools} instance that is specific to the current Gradle version.
     *
     * @return Instance.
     */
    @Override
    StringTools stringTools() {
        this.stringTools
    }

    private final StringTools stringTools
    private final ProviderTools providerTools
    private final ProjectTools projectTools
    private final JvmTools jvmTools
    private final FileSystemOperations fsOperations
    private final ExecTools execTools
}
