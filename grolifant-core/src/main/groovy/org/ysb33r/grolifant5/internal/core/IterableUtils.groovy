/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core

import groovy.transform.CompileStatic

import java.nio.file.Path

/**
 * Some simple utilities to deal with Iterable
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class IterableUtils {

    static List<?> toAnyList(Iterable<?> list) {
        list.toList()
    }

    static <T> List<T> toList(Iterable<T> list) {
        list.toList()
    }

    /**
     * Decide whether to treat the object as an {@Link Iterable}
     *
     * @param item Object to check
     * @return {@code true} i fobject should be considered as iterable.
     */
    static boolean treatAsIterable(Object item) {
        if (item instanceof Iterable) {
            !(item instanceof Path || item instanceof CharSequence)
        } else {
            false
        }
    }
}
