/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.errors.ResourceAccessException

/**
 * Internal utilities to deal with resource files.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.0
 */
@CompileStatic
class Resources {
    /**
     * Copy resource to a file.
     *
     * @param resourcePath Resource path
     * @param destFile Destination file
     */
    static void copyResourceToFile(String resourcePath, File destFile) {
        doWithResource(resourcePath) { InputStream strm ->
            destFile.withOutputStream { output ->
                output << strm
            }
        }
    }

    /**
     * Loads text from a resource into a string.
     *
     * @param resourcePath Resource path
     * @return Contents
     */
    static String loadTextFromResource(String resourcePath) {
        doWithResource(resourcePath) { InputStream strm ->
            strm.text
        }
    }

    /**
     * Loads a properties file from a resource path
     * @param resourcePath Resource path
     * @return Properties.
     */
    static Properties loadPropertiesFromResource(String resourcePath) {
        (Properties) doWithResource(resourcePath) { InputStream strm ->
            Properties props = new Properties()
            props.load(strm)
            props
        }
    }

    /**
     * Loads a properties file from a resource path
     *
     * @param resourcePath Resource path
     * @param classLoader Use classloader to load resource.
     * @return Properties.
     *
     * @since 5.0
     */
    static Properties loadPropertiesFromResource(String resourcePath, ClassLoader classLoader) {
        (Properties) doWithResource(resourcePath, classLoader) { InputStream strm ->
            Properties props = new Properties()
            props.load(strm)
            props
        }
    }

    private static Object doWithResource(String resourcePath, Closure runner) {
        doWithResource(resourcePath, Resources.classLoader, runner)
    }

    private static Object doWithResource(String resourcePath, ClassLoader classLoader, Closure runner) {
        InputStream strm = classLoader.getResourceAsStream(resourcePath)
        if (!strm) {
            throw new ResourceAccessException("${resourcePath} does not exist")
        }
        strm.withCloseable(runner)
    }
}
