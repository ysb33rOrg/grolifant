/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.logging.LogLevel
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.executable.OutputStreamSpec
import org.ysb33r.grolifant5.api.errors.UnknownStateObjectException

import javax.inject.Inject

/**
 * Implementation of external process output configuration.
 *
 * @since 5.0
 */
@CompileStatic
class DefaultOutputStreamSpec implements OutputStreamSpec {

    @Inject
    DefaultOutputStreamSpec(
        ObjectFactory objects,
        LogLevel logLevel
    ) {
        this.quietMode = logLevel == LogLevel.QUIET
        this.fileCapture = objects.property(File)
        this.forwardOutput = true
        this.captureOutput = false
    }

    /**
     * Captures the output of the running process.
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    @Override
    void capture() {
        this.captureOutput = true
        this.forwardOutput = false
        this.fileCapture.value((File) null)
    }

    /**
     * Captures the output of the running process, but also forwards it to console.
     *
     * <p>
     *     Forwarding will not happen if loglevel is {@code QUIET}.
     * </p>
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    @Override
    void captureAndForward() {
        this.captureOutput = true
        this.forwardOutput = !quietMode
        this.fileCapture.value((File) null)
    }

    /**
     * Capture output to this the given file.
     *
     * @param out Provider of a file.
     */
    @Override
    void captureTo(Provider<File> out) {
        this.captureOutput = false
        this.forwardOutput = false
        this.fileCapture.set(out)
    }

    /**
     * Forwards the standard output from the running process to console except if running in quiet mode.
     *
     * <p>This is standard behaviour for most cases, except if implementors of task and execution specifications
     *   decide to override it.</p>
     *
     * <p>
     *     Forwarding will not happen if loglevel is {@code QUIET}.
     * </p>
     *
     * <p>NOTE: Processes run in a worker will silently ignore this setting</p>.
     */
    @Override
    void forward() {
        this.captureOutput = false
        this.forwardOutput = !quietMode
        this.fileCapture.value((File) null)
    }

    /**
     * Do not forward or capture output.
     */
    @Override
    void noOutput() {
        this.captureOutput = false
        this.forwardOutput = false
        this.fileCapture.value((File) null)
    }

    /**
     * Get the output type that corresponds to the current configuration.
     * @return Output type.
     */
    @Override
    ExecTools.OutputType getOutputType() {
        if (this.captureOutput && this.forwardOutput) {
            ExecTools.OutputType.FORWARD_AND_CAPTURE
        } else if (this.captureOutput && !this.forwardOutput) {
            ExecTools.OutputType.CAPTURE
        } else if (!this.captureOutput && this.forwardOutput) {
            ExecTools.OutputType.FORWARD
        } else if (!this.captureOutput && !this.forwardOutput && !fileCapture.present) {
            ExecTools.OutputType.QUIET
        } else if (fileCapture.present) {
            ExecTools.OutputType.USE_CONFIGURED_STREAM
        } else {
            throw new UnknownStateObjectException(
                "Unknown condition: capture=${captureOutput}, fwd=${forwardOutput}, file=${fileCapture.present}"
            )
        }
    }

    /**
     * Returns the file to which capture can be done.
     * @return Configured file. Can be an empty provider.
     */
    @Override
    Provider<File> getFileCapture() {
        this.fileCapture
    }

    private boolean captureOutput
    private boolean forwardOutput
    private final boolean quietMode
    private final Property<File> fileCapture
}
