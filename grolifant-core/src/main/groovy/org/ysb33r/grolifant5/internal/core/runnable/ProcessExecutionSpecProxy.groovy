/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.executable.OutputStreamSpec
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec
import org.ysb33r.grolifant5.api.core.executable.ProcessOutputProcessor
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import javax.inject.Inject

import static org.gradle.api.logging.LogLevel.LIFECYCLE
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD_AND_CAPTURE

@CompileStatic
class ProcessExecutionSpecProxy implements ProcessExecutionSpec, ProcessOutputProcessor {

    @Inject
    ProcessExecutionSpecProxy(Project tempProjectReference) {
        this.out = tempProjectReference.objects.newInstance(
            DefaultOutputStreamSpec,
            tempProjectReference.gradle.startParameter.logLevel ?: LIFECYCLE
        )
        this.err = tempProjectReference.objects.newInstance(
            DefaultOutputStreamSpec,
            tempProjectReference.gradle.startParameter.logLevel ?: LIFECYCLE
        )
    }

    @Override
    boolean isIgnoreExitValue() {
        this.ignoreExitValue
    }

    @Override
    void setIgnoreExitValue(boolean iev) {
        this.ignoreExitValue = iev
    }
    /**
     * After execution has been completed, run this function.
     * <p>
     * This will be run if there was an error during execution.
     *
     * @param resultProcessor Function to execute.
     *                        It is passed the result of the execution.
     * @since 5.0
     */
    @Override
    void afterExecute(Action<ExecOutput> resultProcessor) {
        this.actions.add(resultProcessor)
    }

    /**
     * Whether process output is forwarded to the console.
     *
     * @return {@code true} if output is forwarded.
     */
    @Override
    boolean isOutputForwarded() {
        this.out.outputType in [FORWARD_AND_CAPTURE, FORWARD]
    }

    /**
     * Whether process error output is forwarded to the console.
     *
     * @return {@code true} if output is forwarded.
     */
    @Override
    boolean isErrorOutputForwarded() {
        this.err.outputType in [FORWARD_AND_CAPTURE, FORWARD]
    }

    /**
     * Configure the process output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    @Override
    void output(Action<OutputStreamSpec> configurator) {
        configurator.execute(out)
    }

    /**
     * Configure the process output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    @Override
    void output(@DelegatesTo(OutputStreamSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(this.out, configurator)
    }

    /**
     * Configure the process error output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    @Override
    void errorOutput(Action<OutputStreamSpec> configurator) {
        configurator.execute(this.err)
    }

    /**
     * Configure the process error output.
     *
     * @param configurator Output configuration.
     *
     * @since 5.0
     */
    @Override
    void errorOutput(@DelegatesTo(OutputStreamSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(this.err, configurator)
    }

    /**
     * The output stream configuration.
     *
     * @return Specification.
     */
    @Override
    OutputStreamSpec getOutput() {
        this.out
    }

    /**
     * The output stream configuration.
     *
     * @return Specification.
     */
    @Override
    OutputStreamSpec getErrorOutput() {
        this.err
    }

    /**
     * Processes the output.
     *
     *
     * @param output Execution output from {@code exec()} or {@code javaexec()} calls.
     */
    @Override
    void processOutput(ExecOutput output) {
        if (outputForwarded && output.standardOutput.asText.present) {
            System.out.println(output.standardOutput.asText.get())
        }
        if (errorOutputForwarded && output.standardError.asText.present) {
            System.err.println(output.standardError.asText.get())
        }

        actions.each { it.execute(output) }
    }

    private boolean ignoreExitValue = false
    private final List<Action<ExecOutput>> actions = []
    private final DefaultOutputStreamSpec out
    private final DefaultOutputStreamSpec err
}
