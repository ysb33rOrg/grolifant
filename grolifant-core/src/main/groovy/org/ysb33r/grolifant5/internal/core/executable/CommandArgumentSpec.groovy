/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.executable

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.executable.CommandEntryPoint
import org.ysb33r.grolifant5.api.core.runnable.AbstractCmdlineArgumentSpec

import javax.inject.Inject
import java.util.function.Consumer

/**
 * Command-ine argument handler for a command.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class CommandArgumentSpec extends AbstractCmdlineArgumentSpec implements CommandEntryPoint {

    @Inject
    CommandArgumentSpec(
        StringTools stringTools,
        ObjectFactory objectFactory,
        ProviderFactory providerFactory
    ) {
        super(stringTools, providerFactory)
        this.cmd = objectFactory.property(String)
        this.preArgsProvider = this.cmd.map { [it] }
        this.commandUpdater = { Object x -> stringTools.updateStringProperty(owner.cmd, x) }
    }

    @Deprecated
    CommandArgumentSpec(ProjectOperations po, ObjectFactory objectFactory) {
        super(po)
        this.cmd = objectFactory.property(String)
        this.preArgsProvider = this.cmd.map { [it] }
    }

    /**
     * Set the lazy-evaluated command.
     *
     * @param name Name of command. Must evaluate to a string.
     */
    @Override
    void setCommand(Object name) {
        commandUpdater.accept(name)
    }

    /**
     * Get the command.
     *
     * @return A provider to the command.
     */
    @Override
    Provider<String> getCommand() {
        this.cmd
    }

    /**
     * A provider to arguments that will be inserted before any supplied arguments.
     *
     * @return Arguments provider
     */
    @Override
    Provider<List<String>> getPreArgs() {
        this.preArgsProvider
    }

    private final Property<String> cmd
    private final Provider<List<String>> preArgsProvider
    private final Consumer<Object> commandUpdater
}
