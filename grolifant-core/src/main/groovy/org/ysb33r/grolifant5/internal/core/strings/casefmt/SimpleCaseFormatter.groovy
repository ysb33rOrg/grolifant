/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.strings.casefmt

import groovy.transform.CompileStatic

import java.util.function.BiFunction
import java.util.function.Function

/**
 * A simple way to construct a formatter using two function objects.
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.2
 */
@CompileStatic
class SimpleCaseFormatter implements CaseFormat {

    static SimpleCaseFormatter from(
        BiFunction<String, Boolean, Iterable<String>> decomposer,
        Function<Iterable<String>, String> composer
    ) {
        new SimpleCaseFormatter(decomposer, composer)
    }

    @Override
    Iterable<String> decompose(String input, boolean correctBadCase) {
        this.decomposer.apply(input, correctBadCase)
    }

    @Override
    String compose(Iterable<String> inputs) {
        this.composer.apply(inputs)
    }

    private SimpleCaseFormatter(
        BiFunction<String, Boolean, Iterable<String>> decomposer,
        Function<Iterable<String>, String> composer
    ) {
        this.decomposer = decomposer
        this.composer = composer
    }

    private final BiFunction<String, Boolean, Iterable<String>> decomposer
    private final Function<Iterable<String>, String> composer
}
