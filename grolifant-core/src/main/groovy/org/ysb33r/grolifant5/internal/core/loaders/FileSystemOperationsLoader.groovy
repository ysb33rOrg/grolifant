/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.loaders

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.loadable.core.LoadableVersion
import org.ysb33r.grolifant5.loadable.core.Loader

/**
 * Loads the appropriate {@link FileSystemOperations} implementation.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class FileSystemOperationsLoader {
    public static final String INTERFACE_NAME = 'FileSystemOperations'
    public static final String CLASS_NAME = 'DefaultFileSystemOperations'

    @Synchronized
    static FileSystemOperations load(Project project, LoadableVersion version) {
        load(project.objects, version)
    }

    @Synchronized
    static FileSystemOperations load(ObjectFactory objects, LoadableVersion version) {
        Class loadable = Loader.load(INTERFACE_NAME, CLASS_NAME, version)
        (FileSystemOperations) objects.newInstance(loadable)
    }
}
