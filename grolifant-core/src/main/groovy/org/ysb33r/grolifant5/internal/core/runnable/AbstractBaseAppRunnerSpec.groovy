/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.Internal
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec
import org.ysb33r.grolifant5.api.core.executable.CmdLineArgumentSpecEntry
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint
import org.ysb33r.grolifant5.api.core.executable.ProcessExecOutput
import org.ysb33r.grolifant5.api.core.executable.ProcessExecutionSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecSpec
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils

import java.security.MessageDigest
import java.util.function.BiFunction

import static java.util.Collections.EMPTY_MAP

/**
 * Base to use for implementing {@link AppRunnerSpec} for various Gradle implementions.
 *
 * Requires an implementation of {@link #applyCommandlineArgumentsTo} in order to correctly copy arguments
 * and command-line argument providers.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class AbstractBaseAppRunnerSpec implements AppRunnerSpec, ProcessExecOutput {

    /**
     * Submit a specification for execution.
     *
     * @since 5.0
     *
     * @param spec Specification
     * @return Result of native execution.
     */
    @Override
    ExecOutput submitAsExec(AbstractExecSpec<?> spec) {
        ExecUtils.submitAsExec(
            this.execTools,
            spec,
            processProxy.output.outputType,
            processProxy.output.fileCapture,
            processProxy.errorOutput.outputType,
            processProxy.errorOutput.fileCapture
        )
    }

    /**
     * Submit a specification for execution.
     * Allows an additional action to configure the final {@link ExecSpec} after all of the other configuration
     * has been completed.
     *
     * @since 5.2
     *
     * @param spec Specification
     * @param additionalConfiguration Additional configurator.
     * @return Result of native execution.
     */
    @Override
    ExecOutput submitAsExec(AbstractExecSpec<?> spec, Action<ExecSpec> additionalConfiguration) {
        ExecUtils.submitAsExec(
            this.execTools,
            spec,
            processProxy.output.outputType,
            processProxy.output.fileCapture,
            processProxy.errorOutput.outputType,
            processProxy.errorOutput.fileCapture,
            additionalConfiguration
        )
    }

    /**
     * Copies these options to the given options.
     *
     * @param target The target options.
     */
    @Override
    void copyTo(ExecSpec target) {
        execSpec.copyTo(target)
        applyCommandlineArgumentsTo(target)
        target.ignoreExitValue = processProxy.ignoreExitValue

        envProviders.environmentProviders.each {
            target.environment(it.getOrElse(EMPTY_MAP) as Map<String, ?>)
        }
    }

    /**
     * Configures a {@link ExecutableEntryPoint} instance.
     * @param configurator Configurator.
     */
    @Override
    void configureEntrypoint(Action<ExecutableEntryPoint> configurator) {
        configurator.execute(this.entryPointProxy)
    }

    /**
     * Configures a {@link ExecutableEntryPoint} instance.
     * @param configurator Configurator.
     */
    @Override
    void configureEntrypoint(@DelegatesTo(ExecutableEntryPoint) Closure<?> configurator) {
        ClosureUtils.configureItem(this.entryPointProxy, configurator)
    }

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator Configurator.
     */
    @Override
    void configureProcess(Action<ProcessExecutionSpec> configurator) {
        configurator.execute(processProxy)
    }

    /**
     * Configures a {@link ProcessExecutionSpec}.
     *
     * @param configurator Configurator.
     */
    @Override
    void configureProcess(@DelegatesTo(ProcessExecutionSpec) Closure<?> configurator) {
        ClosureUtils.configureItem(processProxy, configurator)
    }

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     * @param cmdBlock The command block to configure
     * @param configurator Configurator.
     */
    @Override
    void configureCmdline(String cmdBlock, Action<? extends CmdlineArgumentSpec> configurator) {
        final processor = cmdlineProcessors.find { it.name == cmdBlock }

        if (!processor) {
            throw new UnsupportedConfigurationException("'${cmdBlock}' is not a registred cmdline processor")
        }

        configurator.execute(processor.argumentSpec)
    }

    /**
     * Configures a {@link CmdlineArgumentSpec} instance.
     * @param cmdBlock The command block to configure
     * @param configurator Configurator.
     */
    @Override
    void configureCmdline(String cmdBlock, @DelegatesTo(CmdlineArgumentSpec) Closure<?> configurator) {
        final processor = cmdlineProcessors.find { it.name == cmdBlock }

        if (!processor) {
            throw new UnsupportedConfigurationException("'${cmdBlock}' is not a registred cmdline processor")
        }

        ClosureUtils.configureItem(processor.argumentSpec, configurator)
    }

    /**
     * Access to the entrypoint
     *
     * @return An instance that implements {@link ExecutableEntryPoint}.
     *
     * @since 5.2
     */
    @Override
    ExecutableEntryPoint getEntrypoint() {
        this.entryPointProxy
    }

    /**
     * Access to the runner specification.
     *
     * @return An instance that implements {@link CmdlineArgumentSpec}.
     *
     * @since 5.2
     */
    @Override
    CmdlineArgumentSpec getRunnerSpec() {
        cmdlineProcessors.find { it.name == DEFAULT_BLOCK }.argumentSpec
    }

    /**
     * Access to the process specification.
     *
     * @return An instance that implements {@Link ProcessExecutionSpec}.
     *
     * @since 5.2
     */
    @Override
    ProcessExecutionSpec getProcess() {
        processProxy
    }

    /**
     * Adds a command-line processor that will process command-line arguments in a specific order.
     *
     * For instance in a script, one want to process the exe args before the script args.
     *
     * In a system that has commands and subcommands, one wants to process this in the order of exe args, command args,
     * and then subcommand args.
     *
     * This method allows the implementor to control the order of processing  for all the groupings.
     *
     * @param name Name of command-line processor.
     * @param order Order in queue to process.
     * @param processor The specific grouping.
     */
    @Override
    void addCommandLineProcessor(String name, Integer order, CmdlineArgumentSpec processor) {
        this.cmdlineProcessors.add(new CmdLineArgumentSpecEntry(name, order, processor))
    }

    /**
     * Provides direct access to the list of command-line processors.
     *
     * In many cases there will only be one item in the list which is for providing arguments to the executable
     * itself. Some implementations will have more. Implementors can use this interface to manipulate order of
     * evaluation.
     *
     * @return List of command-line processors. Can be empty, but never {@code null}.
     */
    @Override
    Collection<CmdLineArgumentSpecEntry> getCommandLineProcessors() {
        this.cmdlineProcessors
    }

    /**
     * A unique string which determines whether there were any changes.
     *
     * @return Signature string
     */
    @Override
    String getExecutionSignature() {
        MessageDigest.getInstance('SHA-256')
            .digest(executionParameters.toString().bytes).sha256()
    }

    /**
     * Processes the output.
     *
     * <p>
     *     Checks first if any outputs should be forwarded.
     *     Thereafter run all registered actions.
     * </p>
     *
     * @param output Output to process
     */
    @Override
    void processOutput(ExecOutput output) {
        processProxy.processOutput(output)
    }

    protected AbstractBaseAppRunnerSpec(
        ConfigCacheSafeOperations gtc,
        ProviderFactory pf,
        ObjectFactory objectFactory,
        BiFunction<ExecSpec, EnvironmentVariableProviders, ExecutableEntryPoint> executableEntryPointFactory
    ) {
        this.stringTools = gtc.stringTools()
        this.execTools = gtc.execTools()
        this.execSpec = gtc.execTools().execSpec()
        this.cmdlineProcessors = [] as SortedSet<CmdLineArgumentSpecEntry>
        this.envProviders = new EnvironmentVariableProviders()
        this.processProxy = objectFactory.newInstance(ProcessExecutionSpecProxy)
        this.entryPointProxy = executableEntryPointFactory.apply(this.execSpec, this.envProviders)
        this.providerTools = gtc.providerTools()
        addCommandLineProcessor(DEFAULT_BLOCK, 0, new DefaultArguments(this.stringTools, pf))
    }

//    // Deprecated in 5.0
//    @Deprecated
//    protected AbstractBaseAppRunnerSpec(
//        ProjectOperations po,
//        BiFunction<ExecSpec, EnvironmentVariableProviders, ExecutableEntryPoint> executableEntryPointFactory
//    ) {
//        this.stringTools = po.stringTools
//        this.execSpec = po.execTools.execSpec()
//        this.cmdlineProcessors = [] as SortedSet<CmdLineArgumentSpecEntry>
//        this.envProviders = new EnvironmentVariableProviders()
//        this.processProxy = new ProcessExecutionSpecProxy(this.execSpec)
//        this.entryPointProxy = executableEntryPointFactory.apply(this.execSpec, this.envProviders)
//        this.providerTools = po.providerTools
//
////        processProxy.identity {
////            standardInput = System.in
////            standardOutput = System.out
////            errorOutput = System.err
////        }
//
//        addCommandLineProcessor(DEFAULT_BLOCK, 0, new DefaultArguments(po.stringTools,po.providers))
//    }

    /**
     * Copies arguments and argument providers.
     *
     * @param spec Target {@link ExecSpec}.
     */
    abstract protected void applyCommandlineArgumentsTo(ExecSpec spec)

    @Internal
    protected SortedSet<CmdLineArgumentSpecEntry> getCmdlineProcessors() {
        this.cmdlineProcessors
    }

    /**
     * Loads executions parameters from the current execution specification.
     * <p>
     *     The primary purpose of this method is to build a map for use by {@link #getExecutionSignature}.
     *     The default implementation will use the executable path and the arguments.
     *     </p>
     * @return Execution parameters.
     */
    @Internal
    protected Map<String, ?> getExecutionParameters() {
        [
            exe  : stringTools.stringize(execSpec.executable),
            args1: stringTools.stringize(execSpec.args),
            args2: stringTools.stringize(execSpec.argumentProviders*.asArguments().flatten())
        ]
    }

    private final StringTools stringTools
    private final ExecTools execTools
    private final SortedSet<CmdLineArgumentSpecEntry> cmdlineProcessors
    private final ExecSpec execSpec
    private final EnvironmentVariableProviders envProviders
    private final ProcessExecutionSpecProxy processProxy
    private final ExecutableEntryPoint entryPointProxy
    private final ProviderTools providerTools
}
