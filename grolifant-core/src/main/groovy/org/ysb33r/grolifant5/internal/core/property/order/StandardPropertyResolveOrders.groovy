/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.property.order

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.PropertyResolveOrder

/** Standard property resolve orders
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class StandardPropertyResolveOrders {
    /** Resolves a property by looking a the project first, then the system, then
     * the environment. Environmental variables will be uppercased and dots replaced with
     * underscores.
     *
     */
    static class ProjectSystemEnvironment implements PropertyResolveOrder {
        @Override
        Provider<String> resolve(ProjectOperations po, String name, boolean configurationTimeSafe) {
            def props = Props.createFrom(po, name, configurationTimeSafe)
            po.providerTools.resolveOrderly(props.gradle, props.system, props.env)
        }
    }

    /** Resolves a property by looking a the project first, then the system, then
     * the environment. Environmental variables will be uppercased and dots replaced with
     * underscores.
     *
     */
    static class SystemEnvironmentProject implements PropertyResolveOrder {
        @Override
        Provider<String> resolve(ProjectOperations po, String name, boolean configurationTimeSafe) {
            def props = Props.createFrom(po, name, configurationTimeSafe)
            po.providerTools.resolveOrderly(props.system, props.env, props.gradle)
        }
    }

    private static class Props {

        static Props createFrom(ProjectOperations po, String name, boolean configurationTimeSafe) {
            new Props(
                po.gradleProperty(name, configurationTimeSafe),
                po.systemProperty(name, configurationTimeSafe),
                po.environmentVariable(PropertyNaming.asEnvVar(name), configurationTimeSafe)
            )
        }

        final Provider<String> gradle
        final Provider<String> system
        final Provider<String> env

        private Props(
            final Provider<String> gradle,
            final Provider<String> system,
            final Provider<String> env
        ) {
            this.gradle = gradle
            this.system = system
            this.env = env
        }
    }
}
