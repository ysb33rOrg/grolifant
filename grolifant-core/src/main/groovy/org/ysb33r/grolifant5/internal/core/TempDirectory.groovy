/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider

import java.nio.file.Files
import java.nio.file.Path

/**
 * Works with temporary directories
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class TempDirectory {
    TempDirectory(Provider<File> tempRoot) {
        this.tempRoot = tempRoot.map { it.toPath() }
    }

    /**
     * A provider to the root where temporary directories will be created.
     */
    final Provider<Path> tempRoot

    /**
     * Creates a temporary directory.
     *
     * @param prefix Prefix for directory
     * @return Created directory
     */
    File createTempDirectory(String prefix) {
        final root = tempRoot.get()
        final rootDir = root.toFile()

        rootDir.mkdirs()
        final dir = Files.createTempDirectory(root, prefix).toFile()
        dir.deleteOnExit()
        dir
    }
}
