/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.strings.casefmt

import groovy.transform.CompileStatic

/**
 * Standalone utilities for working with case formats
 *
 * @author Schalk W. Cronjé
 * @author Samuel Neff
 *
 * @since 5.2
 */
@CompileStatic
class CaseFormatUtils {
    /**
     * Split a camel-case string.
     *
     * @param text Input text
     * @return Name parts
     */
    @SuppressWarnings('ParameterName')
    static Iterable<String> splitCaseParts(String text) {
        // Based upon https://github.com/samuelneff/groovy-case-conversions/blob/main/CaseConversion.groovy
        text.trim()
            .replaceAll(/([^A-Z])([A-Z])/) { _, before, after -> "$before $after" }
            .tokenize(/[-_\s ]+/)
    }
}
