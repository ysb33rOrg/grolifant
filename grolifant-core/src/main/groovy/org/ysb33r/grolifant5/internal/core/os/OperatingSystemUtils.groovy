/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.os

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.OperatingSystem

import static org.ysb33r.grolifant5.internal.core.os.OperatingSystemConstants.OS_NAME

/**
 * Utilities for setting up eoparting system information
 *
 * @author Schalk W. Cronjé
 *
 * @since 5.1
 */
@CompileStatic
class OperatingSystemUtils {
    public static final OperatingSystem CURRENT = detect()

    static OperatingSystem detect() {
        // tag::check_os[]
        if (OS_NAME.contains('windows')) {
            Windows.INSTANCE
        } else if (OS_NAME.contains('mac os x') || OS_NAME.contains('darwin') || OS_NAME.contains('osx')) {
            MacOsX.INSTANCE
        } else if (OS_NAME.contains('linux')) {
            Linux.INSTANCE
        } else if (OS_NAME.contains('freebsd')) {
            FreeBSD.INSTANCE
        } else if (OS_NAME.contains('sunos') || OS_NAME.contains('solaris')) {
            Solaris.INSTANCE
        } else if (OS_NAME.contains('netbsd')) {
            NetBSD.INSTANCE
        } else {
            // Not strictly true, but a good guess
            GenericUnix.INSTANCE
        }
        // end::check_os[]
    }
}
