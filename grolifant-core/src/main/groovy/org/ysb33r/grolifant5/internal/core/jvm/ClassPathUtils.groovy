/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.jvm

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.file.FileCollection
import org.ysb33r.grolifant5.api.core.ClassLocation
import org.ysb33r.grolifant5.api.errors.ClassLocationException

import java.util.regex.Pattern

/**
 * Utilities for resolving classpath
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@Slf4j
@CompileStatic
class ClassPathUtils {
    public static final String SLASH = '/'
    public static final String DOT = '.'
    public static final String JAR = 'jar'
    public static final String JRT = 'jrt'
    public static final String CLASS_EXT = '.class'
    public static final Pattern SCHEME_REGEX = ~/(!.+?)$/

    static ClassLocation resolveClassLocationSimple(Class aClass) {
        String location = aClass?.protectionDomain?.codeSource?.location

        if (location) {
            new ClassLocationImpl(new File(location.toURI()).absoluteFile)
        } else {
            URI uri = aClass.getResource(
                SLASH + aClass.canonicalName.replace(DOT, SLASH) + CLASS_EXT
            )?.toURI()

            if (uri == null) {
                throw new ClassLocationException("Location for ${aClass.name} cannot be located.")
            } else if (uri.scheme == JAR) {
                new ClassLocationImpl(
                    new File(new URI(uri.rawSchemeSpecificPart.replaceAll(SCHEME_REGEX, ''))).absoluteFile
                )
            } else if (uri.scheme == JRT) {
                new ClassLocationImpl(uri.toURL())
            } else {
                new ClassLocationImpl(new File(uri).parentFile.absoluteFile)
            }
        }
    }

    static ClassLocation resolveClassLocationWithSubstitution(
        Class aClass,
        FileCollection substitutionSearch,
        Pattern substitutionMatch,
        Pattern redoIfMatch,
        Pattern ignoreFromPaths
    ) {
        ClassLocation entryPointClasspath = resolveClassLocationSimple(aClass)
        if (entryPointClasspath.file) {
            log.debug "Resolved ${aClass.canonicalName} to ${entryPointClasspath.file}"
            final String canonicalPath = entryPointClasspath.file.canonicalPath
            if (canonicalPath =~ redoIfMatch) {
                Set<File> candidates = substitutionSearch.files.findAll { File it ->
                    it.name =~ substitutionMatch
                }
                log.debug "Found ${candidates.size()} potential candidate(s): ${candidates}"
                Set<File> replacements = candidates.findAll {
                    !(it.canonicalPath =~ ignoreFromPaths)
                }
                if (replacements.empty) {
                    throw new ClassLocationException("Cannot find suitable jar replacement for ${aClass.canonicalName}")
                }
                log.debug "Selected ${replacements[0]} from ${replacements}"
                new ClassLocationImpl(replacements[0])
            } else {
                log.debug("Ignoring: ${canonicalPath} does not match ${redoIfMatch}")
                entryPointClasspath
            }
        } else {
            entryPointClasspath
        }
    }
}
