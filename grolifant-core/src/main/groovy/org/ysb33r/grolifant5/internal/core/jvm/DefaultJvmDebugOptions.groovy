/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.jvm

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.jvm.JvmDebugOptions

/**
 *
 * A holder for JVM debug options.
 *
 * @since 4.0
 */
@CompileStatic
class DefaultJvmDebugOptions implements JvmDebugOptions {

    DefaultJvmDebugOptions(ObjectFactory objects) {
        this.enabledProperty = objects.property(Boolean)
        this.enabledProperty.set(false)

        this.serverProperty = objects.property(Boolean)
        this.serverProperty.set(false)

        this.suspendProperty = objects.property(Boolean)
        this.suspendProperty.set(false)

        this.portProperty = objects.property(Integer)
    }

    /**
     * Whether to attach a debug agent to the forked process.
     *
     * @return Provider to setting.
     */
    @Override
    Provider<Boolean> getEnabled() {
        this.enabledProperty
    }

    /**
     * Whether to attach a debug agent to the forked process.
     * @param flag Value convertible to boolean.
     */
    @Override
    void setEnabled(Object flag) {
        setProvider(this.enabledProperty, flag)
    }

    /**
     * Whether a socket-attach or a socket-listen type of debugger is expected.
     * @return Provider to setting.
     */
    @Override
    Provider<Boolean> getServer() {
        this.serverProperty
    }

    /**
     * Whether a socket-attach or a socket-listen type of debugger is expected.
     *
     * @param flag Value convertible to boolean.
     */
    @Override
    void setServer(Object flag) {
        setProvider(this.serverProperty, flag)
    }

    /**
     * Whether the forked process should be suspended until the connection to the debugger is established.
     * @return Provider to setting.
     */
    @Override
    Provider<Boolean> getSuspend() {
        this.suspendProperty
    }

    /**
     * Whether the forked process should be suspended until the connection to the debugger is established.
     * @param flag Value convertible to boolean.
     */
    @Override
    void setSuspend(Object flag) {
        setProvider(this.suspendProperty, flag)
    }

    /**
     * The debug port.
     *
     * @return Provider to debug port.
     */
    @Override
    Provider<Integer> getPort() {
        this.portProperty
    }

    /**
     * The debug port.
     *
     * @param port Value convertible to integer.
     */
    @Override
    void setPort(Object port) {
        if (port == null) {
            this.portProperty.set((Integer) null)
        } else {
            switch (port) {
                case Provider:
                    this.portProperty.set((Provider) port)
                    break
                case Number:
                    this.portProperty.set(((Number) port).toInteger())
                    break
                default:
                    this.portProperty.set(port.toString().toInteger())
            }
        }
    }

    private setProvider(Property<Boolean> entity, Object value) {
        switch (value) {
            case Provider:
                entity.set((Provider) value)
                break
            default:
                entity.set((Boolean) value)
                break
        }
    }

    private final Property<Boolean> enabledProperty
    private final Property<Boolean> serverProperty
    private final Property<Boolean> suspendProperty
    private final Property<Integer> portProperty
}
