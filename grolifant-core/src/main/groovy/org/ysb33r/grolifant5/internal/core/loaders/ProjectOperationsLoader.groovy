/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.loaders

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.loadable.core.Loader
import org.ysb33r.grolifant5.loadable.core.LoadableVersion

@CompileStatic
class ProjectOperationsLoader {
    public static final String INTERFACE_NAME = 'ProjectOperations'
    public static final String CLASS_NAME = 'DefaultProjectOperations'

    @Synchronized
    static ProjectOperations load(Project project) {
        final loadVersion = System.getProperty('org.ysb33r.grolifant.api.core.load-version')
        if (loadVersion) {
            final version = LoadableVersion.valueOf(loadVersion.toUpperCase(Locale.US))
            Class loadable = Loader.load(INTERFACE_NAME, CLASS_NAME, version)
            (ProjectOperations)project.objects.newInstance(loadable)
        } else {
            Class loadable = Loader.load(INTERFACE_NAME, CLASS_NAME)
            loadable ? (ProjectOperations)project.objects.newInstance(loadable) : null
        }
    }
}
