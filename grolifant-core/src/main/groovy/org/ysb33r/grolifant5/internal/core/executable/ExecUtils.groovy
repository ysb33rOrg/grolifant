/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.executable

import groovy.transform.CompileStatic
import org.apache.commons.io.output.NullOutputStream
import org.apache.commons.io.output.TeeOutputStream
import org.gradle.api.Action
import org.gradle.api.provider.Provider
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.ExecTools.OutputType
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecSpec
import org.ysb33r.grolifant5.api.core.runnable.AbstractJvmExecSpec
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import java.util.function.Function

/**
 * Utilities to help with building runnable classes.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class ExecUtils {

    /**
     * Defines outputs when configuring streams
     *
     * @since 5.0
     */
    static class ConfiguredStreams implements Closeable {

        /**
         * The stream that must be used for an external execution
         */
        final OutputStream out

        /**
         * An optional stream that is used for capturing output.
         */
        final Optional<ByteArrayOutputStream> capture

        /**
         * Whether the main output stream should be closed.
         *
         * <p>If {@link PrintStream}, then the stream will not be closed, regardless of this setting.
         */
        final boolean closeOutStream

        ConfiguredStreams(OutputStream out, boolean closeStream = false) {
            this.out = out
            this.capture = Optional.empty()
            this.closeOutStream = closeStream
        }

        ConfiguredStreams(OutputStream out, ByteArrayOutputStream capture) {
            this.out = out
            this.capture = Optional.of(capture)
            this.closeOutStream = false
        }

        @Override
        void close() throws IOException {
            capture.ifPresent { it.close() }
            if (closeOutStream && !(out instanceof PrintStream)) {
                out.close()
            }
        }
    }

    /**
     * Simplifies running an executable to obtain a version.
     * This is primarily used to implement a {@code runExecutableAndReturnVersion} method.
     *
     * @param execTools Execution tools instance.
     * @param argsForVersion Arguments required to obtain a version
     * @param executablePath Location of the executable
     * @param versionParser A parser for the output of running the executable which could extract the version.
     * @param configurator Optional configurator to customise the execution specification.
     * @return The version string.
     */
    static String parseVersionFromOutput(
        ExecTools execTools,
        Iterable<String> argsForVersion,
        File executablePath,
        Function<String, String> versionParser,
        Action<ExecSpec> configurator = null
    ) {
        final strm = new ByteArrayOutputStream()

        strm.withCloseable {
            Action<ExecSpec> runner = new Action<ExecSpec>() {
                @Override
                void execute(ExecSpec spec) {
                    spec.standardOutput = strm
                    spec.executable(executablePath)
                    spec.args = argsForVersion

                    if (configurator != null) {
                        configurator.execute(spec)
                    }
                }
            }

            execTools.exec(runner).assertNormalExitValue()
            versionParser.apply(strm.toString())
        }
    }

    /**
     * Configures stream outputs for external process execution.
     *
     * @param outType The kind of output that is required for a process stream.
     * @param current The current configured stream.
     * @param systemStream The stdout or stderr stream depending on what type of output stream is being configured.
     * @return The output streams for the execution.
     *
     * @since 5.0
     */
    static ConfiguredStreams configureStreams(
        ExecTools.OutputType outType,
        OutputStream current,
        OutputStream systemStream
    ) {
        switch (outType) {
            case ExecTools.OutputType.QUIET:
                return new ConfiguredStreams(NullOutputStream.INSTANCE)
            case ExecTools.OutputType.CAPTURE:
                final actual = new ByteArrayOutputStream()
                return new ConfiguredStreams(actual, actual)
            case ExecTools.OutputType.FORWARD_AND_CAPTURE:
                final actual = new ByteArrayOutputStream()
                return new ConfiguredStreams(new TeeOutputStream(actual, systemStream), actual)
            case ExecTools.OutputType.USE_CONFIGURED_STREAM:
            case ExecTools.OutputType.FORWARD:
                return new ConfiguredStreams(current)
        }
    }

    /**
     * Decides whether the result of an execution can be failed early.
     * <p>
     *     When capturing is active on any stream, early exit cannot be achieved.
     *     </p>
     *
     * @param result The result of the execution
     * @param ignoreExit The value that was configured by the user for ignoring exits.
     * @param out The kind of output capture configured for standard output.
     * @param err The kind of output capture configured for standard error.
     *
     * @since 5.0
     */
    static void failOnExitCode(ExecResult result, boolean ignoreExit, OutputType out, OutputType err) {
        if (!ignoreExit) {
            if (out in NO_EARLY_FAILURE || err in NO_EARLY_FAILURE) {
                return
            }
            result.assertNormalExitValue()
        }
    }

    /**
     * Submits a native job using potential file captures.
     *
     * @param execTools Execution tools instance
     * @param spec Specification to apply
     * @param outType Type of output handling for standard output.
     * @param outFileCapture Potential file capture for standard output. Can be empty.
     * @param errType Type of output handling for standard error.
     * @param errFileCapture Potential file capture for standard error. Can be empty.
     * @param additionalConfiguration Optional action that can be performed on the final {@link ExecSpec}.
     * @return Result of the execution.
     */
    @SuppressWarnings('ParameterCount')
    static ExecOutput submitAsExec(
        ExecTools execTools,
        AbstractExecSpec<?> spec,
        OutputType outType,
        Provider<File> outFileCapture,
        OutputType errType,
        Provider<File> errFileCapture,
        Action<ExecSpec> additionalConfiguration = { false }
    ) {
        ExecOutput ret
        if (outFileCapture.present && errFileCapture.present) {
            final out = outFileCapture.get()
            final err = errFileCapture.get()
            err.parentFile.mkdirs()
            out.parentFile.mkdirs()

            out.withOutputStream { outStream ->
                err.withOutputStream { errStream ->
                    ret = execTools.exec(
                        outType,
                        errType,
                        y -> {
                            spec.copyTo(y)
                            y.standardOutput = outStream
                            y.errorOutput = errStream
                            additionalConfiguration.execute(y)
                        }
                    )
                }
            }
        } else if (outFileCapture.present) {
            final out = outFileCapture.get()
            out.parentFile.mkdirs()
            out.withOutputStream { w ->
                ret = execTools.exec(
                    outType,
                    errType,
                    y -> {
                        spec.copyTo(y)
                        y.standardOutput = w
                        additionalConfiguration.execute(y)
                    }
                )
            }
        } else if (errFileCapture.present) {
            final err = errFileCapture.get()
            err.parentFile.mkdirs()
            err.withOutputStream { w ->
                ret = execTools.exec(
                    outType,
                    errType,
                    y -> {
                        spec.copyTo(y)
                        y.errorOutput = w
                        additionalConfiguration.execute(y)
                    }
                )
            }
        } else {
            ret = execTools.exec(
                outType,
                errType,
                y -> {
                    spec.copyTo(y)
                    additionalConfiguration.execute(y)
                }
            )
        }
        ret
    }

    /**
     * Submits a JVM job using potential file captures.
     *
     * @param execTools Execution tools instance
     * @param spec Specification to apply
     * @param outType Type of output handling for standard output.
     * @param outFileCapture Potential file capture for standard output. Can be empty.
     * @param errType Type of output handling for standard error.
     * @param errFileCapture Potential file capture for standard error. Can be empty.
     * @return Result of the execution.
     */
    @SuppressWarnings('ParameterCount')
    static ExecOutput submitAsJavaExec(
        ExecTools execTools,
        AbstractJvmExecSpec<?> spec,
        OutputType outType,
        Provider<File> outFileCapture,
        OutputType errType,
        Provider<File> errFileCapture
    ) {
        ExecOutput ret
        if (outFileCapture.present && errFileCapture.present) {
            final out = outFileCapture.get()
            final err = errFileCapture.get()
            err.parentFile.mkdirs()
            out.parentFile.mkdirs()

            out.withOutputStream { outStream ->
                err.withOutputStream { errStream ->
                    ret = execTools.javaexec(
                        outType,
                        errType,
                        y -> {
                            spec.copyTo(y)
                            y.standardOutput = outStream
                            y.errorOutput = errStream
                        }
                    )
                }
            }
        } else if (outFileCapture.present) {
            final out = outFileCapture.get()
            out.parentFile.mkdirs()
            out.withOutputStream { w ->
                ret = execTools.javaexec(
                    outType,
                    errType,
                    y -> {
                        spec.copyTo(y)
                        y.standardOutput = w
                    }
                )
            }
        } else if (errFileCapture.present) {
            final err = errFileCapture.get()
            err.parentFile.mkdirs()
            err.withOutputStream { w ->
                ret = execTools.javaexec(
                    outType,
                    errType,
                    y -> {
                        spec.copyTo(y)
                        y.errorOutput = w
                    }
                )
            }
        } else {
            ret = execTools.javaexec(
                outType,
                errType,
                y -> spec.copyTo(y)
            )
        }
        ret
    }

    private static final List<OutputType> NO_EARLY_FAILURE = [
        OutputType.CAPTURE,
        OutputType.FORWARD_AND_CAPTURE
    ].asImmutable()
}
