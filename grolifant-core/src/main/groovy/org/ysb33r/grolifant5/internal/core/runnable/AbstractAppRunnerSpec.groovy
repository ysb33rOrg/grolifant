/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.runnable

import groovy.transform.CompileStatic
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ProviderFactory
import org.gradle.process.CommandLineArgumentProvider
import org.gradle.process.ExecSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.executable.AppRunnerSpec
import org.ysb33r.grolifant5.api.core.executable.ExecutableEntryPoint

import java.util.function.BiFunction

/**
 * Base to use for implementing {@link AppRunnerSpec} for various Gradle implementions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class AbstractAppRunnerSpec extends AbstractBaseAppRunnerSpec implements AppRunnerSpec {

    protected AbstractAppRunnerSpec(
        ConfigCacheSafeOperations gtc,
        ProviderFactory pf,
        ObjectFactory objectFactory,
        BiFunction<ExecSpec, EnvironmentVariableProviders, ExecutableEntryPoint> executableEntryPointFactory
    ) {
        super(gtc, pf, objectFactory, executableEntryPointFactory)
    }

    @Override
    protected void applyCommandlineArgumentsTo(ExecSpec spec) {
        cmdlineProcessors.each {
            final providedArgs = it.argumentSpec.commandLineArgumentProviders

            if (it.order) {
                final allArgs = it.argumentSpec.allArgs
                spec.argumentProviders.add({ -> allArgs.get() } as CommandLineArgumentProvider)
            } else {
                spec.args(it.argumentSpec.args)

                providedArgs.each { p ->
                    spec.argumentProviders.add({ -> p.get() } as CommandLineArgumentProvider)
                }
            }
        }
    }
}
