/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.internal.core.property.gradle

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider

/**
 * Determines if a class if a special kind of Gradle provider.
 *
 * @since 2.0
 */
@CompileStatic
class GradleProviderSpecialUtilities {
    static boolean isMultiValueProvider(Object p) {
        isListProperty(p) || isSetProperty(p) || isMapProperty(p)
    }

    /**
     * Evaluates potential multi-value providers.
     *
     * @param p Potential provider of multiple values.
     *
     * @return Populated optional if the provider is a multi value provider.
     *  Empty otherwise
     */
    static Optional<Collection<?>> valuesForMultipleValueProvider(Object p) {
        if (isMapProperty(p) && ((Provider) p).present) {
            Optional.of((Collection<?>) ((Map) ((Provider) p).get()).values())
        } else if (isMultiValueProvider(p) && ((Provider) p).present) {
            Optional.of((Collection) ((Provider) p).get())
        } else {
            Optional.empty()
        }
    }

    static private boolean isListProperty(Object o) {
        o instanceof org.gradle.api.provider.ListProperty
    }

    static private boolean isSetProperty(Object o) {
        o instanceof org.gradle.api.provider.SetProperty
    }

    static private boolean isMapProperty(Object o) {
        o instanceof org.gradle.api.provider.MapProperty
    }
}
