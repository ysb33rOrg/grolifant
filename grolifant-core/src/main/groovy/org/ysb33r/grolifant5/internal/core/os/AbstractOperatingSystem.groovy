/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
/*
    This code was lifted from the Gradle org.gradle.internal.os.OperatingSystem class
    which is under the Apache v2.0 license. Original copyright from 2010 remains. Modifications
    from 2017+ are under the copyright and licensed mentioned above
*/
package org.ysb33r.grolifant5.internal.core.os

import groovy.transform.CompileStatic
import org.ysb33r.grolifant5.api.core.OperatingSystem

import java.util.regex.Pattern

/**
 * Base class for {@link OperatingSystem}.
 */
@CompileStatic
abstract class AbstractOperatingSystem implements OperatingSystem {

    /**
     * Enumeration representing common hardware-operating system architectures.
     *
     */
    enum Arch {

        X86_64(OperatingSystemConstants.AMD64),
        X86(OperatingSystemConstants.I386),
        POWERPC('ppc'),
        SPARC('sparc'),
        ARM64(OperatingSystemConstants.AARCH64),
        /**
         * @since 2.2
         */
        ARM32(OperatingSystemConstants.AARCH32),
        /**
         * @since 2.2
         */
        S390_32('s390_32'),
        /**
         * @since 2.2
         */
        S390_64('s390_64'),
        UNKNOWN('(unknown)')

        private Arch(final String id) {
            this.id = id
        }

        private final String id
    }

    /**
     * The short name for the current operating system.
     *
     * Possibly the same as {@code System.getProperty( "os.name" )}.
     */
    final String name = OperatingSystemConstants.OS_NAME

    /**
     * The version for the current operating system.
     */
    final String version = OperatingSystemConstants.OS_VERSION

    /**
     * Name of environmental variable that holds the system search path
     */
    final String pathVar = 'PATH'

    /**
     * Check is this is Microsoft Windows
     *
     * @return {@code true} if Windows
     */
    boolean isWindows() { false }

    /**
     * Check is this is Apple Mac OS X
     *
     * @return {@code true} if Mac OS X
     */
    boolean isMacOsX() { false }

    /**
     * Check is this is a Linux flavour
     *
     * @return {@code true} if any kind of Linux
     */
    boolean isLinux() { false }

    /**
     * Check is this is FreeBSD
     *
     * @return {@code true} if FreeBSD
     */
    boolean isFreeBSD() { false }

    /**
     * Check is this is NetBSD
     *
     * @return {@code true} if NetBSD
     */
    boolean isNetBSD() { false }

    /**
     * Check is this is a Solaris flavour
     *
     * @return {@code true} if Solaris
     */
    boolean isSolaris() { false }

    /**
     * Check is this is any kind of Unix-like O/S
     *
     * @return {@code true} if any kind of Unix-like O/S
     */
    boolean isUnix() { false }

    /**
     * The character used to separate elements in a system search path.
     *
     * @return OS-specific separator.
     */
    String getPathSeparator() {
        File.pathSeparator
    }

    /**
     * Stringize implementation
     *
     * @return Name, Version and Architecture
     */
    String toString() {
        "${name ?: ''} ${version ?: ''} ${arch ?: ''}"
    }

    /**
     * Locates the given exe in the system path.
     *
     * @param name Name of exe to search for.
     * @return Executable location or {@code null} if not found.
     */
    File findInPath(String name) {
        for (String exeName : getExecutableNames(name)) {
            if (exeName.contains(pathSeparator)) {
                File candidate = new File(exeName)
                if (candidate.file) {
                    return candidate
                }
            } else {
                for (File dir : (path)) {
                    File candidate = new File(dir, exeName)
                    if (candidate.file) {
                        return candidate
                    }
                }
            }
        }
        null
    }

    /**
     * List of system search paths.
     *
     * @return List of entries (can be empty).
     */
    List<File> getPath() {
        List<File> entries = []
        String path = System.getenv(pathVar)
        if (path != null) {
            for (String entry : path.split(Pattern.quote(pathSeparator))) {
                entries.add(new File(entry))
            }
        }
        entries
    }

    /**
     * Find all files in system search path of a certain name.
     *
     * @param name Name to look for
     * @return List of files
     */
    List<File> findAllInPath(String name) {
        List<File> all = []

        for (File dir : (path)) {
            File candidate = new File(dir, name)
            if (candidate.file) {
                all.add(candidate)
            }
        }

        all
    }

    /**
     * Returns list of possible OS-specific decorated exe names.
     *
     * @param executablePath Name of exe
     * @return Returns a list of possible appropriately decorated exes
     */
    abstract List<String> getExecutableNames(String executablePath)

    /**
     * Architecture underlying the operating system
     *
     * @return Architecture type. Returns {@code OperatingSystem.Arch.UNKNOWN} is it cannot be identified. In that a
     *   caller might need to use {@link #getArchStr()} to help with identification.
     */
    abstract OperatingSystem.Arch getArch()

    /**
     * Architecture underlying the operating system
     *
     * @return Architecture string
     */
    abstract String getArchStr()

    /**
     * OS-dependent string that is used to suffix to shared libraries
     *
     * @return Shared library suffix
     */
    abstract String getSharedLibrarySuffix()

    /**
     * OS-dependent string that is used to suffix to static libraries
     *
     * @return Static library suffix
     */
    abstract String getStaticLibrarySuffix()

    /**
     * Returns OS-specific shared library name
     *
     * @param libraryName This can be a base name or a full name.
     * @return Shared library name.
     */
    abstract String getSharedLibraryName(String libraryName)

    /**
     * Returns OS-specific static library name
     *
     * @param libraryName This can be a base name or a full name.
     * @return Static library name.
     */
    abstract String getStaticLibraryName(String libraryName)
}
