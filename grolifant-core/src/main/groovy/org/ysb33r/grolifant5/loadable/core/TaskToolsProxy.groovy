/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.TaskInputFilePropertyBuilder
import org.gradle.api.tasks.TaskInputs
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.TaskInputFileOptions
import org.ysb33r.grolifant5.api.core.TaskTools
import org.ysb33r.grolifant5.api.errors.UnexpectedNullException
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException
import org.ysb33r.grolifant5.api.core.Transform

import java.util.function.Function

/**
 * Non-Gradle API-specific common code from {@link TaskTools}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
abstract class TaskToolsProxy implements TaskTools {

    /**
     * Configure a collection of files as a task input.
     *
     * @param inputs The inputs for a specific task.
     * @param files The inputs for a specific task.
     * @param options Various additional options to set.
     *                Some might be ignored, depending on the version of Gradle.
     */
    @Override
    void inputFiles(TaskInputs inputs, Object files, TaskInputFileOptions... options) {
        createTaskInputsFileEntry(
            inputFilePropertyBuilder(inputs, files),
            options.toList()
        )
    }

    /**
     * Configure a collection of files as a task input.
     *
     * @param inputs The inputs for a specific task.
     * @param files The inputs for a specific task.
     * @param ps The {@link PathSensitivity} of the file collection
     * @param options Various additional options to set.
     *                Some might be ignored, depending on the version of Gradle.
     */
    @Override
    void inputFiles(TaskInputs inputs, Object files, PathSensitivity ps, TaskInputFileOptions... options) {
        final builder = inputFilePropertyBuilder(inputs, files)
        builder.withPathSensitivity(ps)
        createTaskInputsFileEntry(
            builder,
            options.toList()
        )
    }

    /**
     * Resolves a list of many objects to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    @Override
    Provider<List<? extends Task>> taskize(Object... taskies) {
        taskize(taskies as List)
    }

    /**
     * Resolves a list of many objects to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link org.gradle.api.Task}
     * or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    @Override
    Provider<List<? extends Task>> taskize(Iterable<Object> taskies) {
        final transformer = singleTransformer(this.tasks)
        providerFactory.provider { ->
            List<? extends Task> resolvedTasks = []
            Transform.convertItems(taskies as List, resolvedTasks, transformer)
            resolvedTasks as List
        }
    }

    /**
     * Creates a basic task {@link Provider} instance from an object.
     *
     * @param tasks Override the task containerto use.
     * @param pf Override the provider factory to use.
     * @param o Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     * @since 2.0
     */
    @Override
    Provider<? extends Task> taskProviderFrom(TaskContainer tasks1, ProviderFactory pf, Object o) {
        if (isTaskProvider(o)) {
            (Provider<Task>) o
        } else {
            final transformer = singleTransformer(tasks1)

            pf.provider { ->
                Transform.convertItem(o, transformer)
            }
        }
    }

    /**
     * Creates a basic task {@link Provider} instance from an object.
     *
     * @param o Object to be evaluated to a task.
     * @return Lazy-evaluatable task.
     * @since 2.0
     */
    @Override
    Provider<? extends Task> taskProviderFrom(Object o) {
        taskProviderFrom(tasks, providerFactory, o)
    }

    protected TaskToolsProxy(ProjectOperations incompleteReference, Project project) {
        this.projectOperations = incompleteReference
        this.providerFactory = project.providers
        this.tasks = project.tasks
    }

    protected final ProjectOperations projectOperations
    protected final ProviderFactory providerFactory
    protected final TaskContainer tasks

    /**
     * Whether this is a {@code TaskProvider<?>}.
     *
     * @param o Object to evaluate
     * @return {@code true} is an instance of {@link org.gradle.api.tasks.TaskProvider}.
     */
    abstract protected boolean isTaskProvider(Object o)

    /**
     * Creates an input to a task that is based upon a cooleciton of files.
     *
     * @param inputsBuilder The property builder
     * @param options Additional options to assign to the task.
     * Some options might be ignored, depending on the version of Gradle.
     */
    abstract protected void createTaskInputsFileEntry(
        TaskInputFilePropertyBuilder inputsBuilder,
        List<TaskInputFileOptions> options
    )

    private TaskInputFilePropertyBuilder inputFilePropertyBuilder(TaskInputs inputs, Object files) {
        switch (files) {
            case FileCollection:
                return inputs.files(files)
            default:
                inputs.files { ->
                    projectOperations.fsOperations.filesDropNull([files])
                }
        }
    }

    private Function<Object, Task> singleTransformer(TaskContainer t1) {
        { TaskContainer tasks1, StringTools st, Object t ->
            try {
                switch (t) {
                    case null:
                        throw new UnexpectedNullException('Null values are not allowed')
                    case Task:
                        (Task) t
                        break
                    default:
                        tasks1.getByName(st.stringize(t))
                }
            } catch (final GradleException e) {
                throw new UnsupportedConfigurationException("Item '${t}' cannot be resolved to a task", e)
            }
        }.curry(t1, projectOperations.stringTools) as Function<Object, Task>
    }
}
