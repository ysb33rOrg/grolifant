/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.StartParameter
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.file.DeleteSpec
import org.gradle.api.invocation.Gradle
import org.gradle.api.logging.LogLevel
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.resources.ResourceHandler
import org.gradle.api.tasks.WorkResult
import org.ysb33r.grolifant5.api.core.ConfigurationTools
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.JvmTools
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProjectTools
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.RepositoryTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.TaskTools
import org.ysb33r.grolifant5.internal.core.ConfigurationCache
import org.ysb33r.grolifant5.internal.core.loaders.ConfigurationToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.ExecToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.FileSystemOperationsLoader
import org.ysb33r.grolifant5.internal.core.loaders.JvmToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.ProjectToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.ProviderToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.RepositoryToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader
import org.ysb33r.grolifant5.internal.core.loaders.TaskToolsLoader

@SuppressWarnings(['MethodCount', 'AbstractClassWithoutAbstractMethod'])
@CompileStatic
abstract class ProjectOperationsProxy implements ProjectOperations {
    /**
     * A provider to this instance.
     *
     * @return Provider.
     */
    @Override
    Provider<ProjectOperations> asProvider() {
        this.thisAsProvider
    }

    @Override
    WorkResult delete(Action<? super DeleteSpec> action) {
        fsOperations.delete(action)
    }

    /**
     * Tools to deal with Gradle configurations.
     * @return
     */
    @Override
    ConfigurationTools getConfigurations() {
        this.configurationTools
    }

    /**
     * Tools to deal with out-of-process, non-JVM, executables.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    @Override
    ExecTools getExecTools() {
        this.execTools
    }

    /**
     * Returns an object instance for filesystem operations that deals correctly with the functionality of the
     * curretn Gradle version.
     *
     * @return Instance of {@link FileSystemOperations}
     */
    @Override
    FileSystemOperations getFsOperations() {
        this.fsOperations
    }

    /**
     * A reference to the provider factory.
     *
     * @return {@link ProviderFactory}
     */
    @Override
    ProviderFactory getProviders() {
        this.providerFactory
    }

    /**
     * Utilities for working with tasks in a consistent manner across Gradle versions.
     *
     * @return {@link TaskTools} instance.
     *
     * @since 1.3
     */
    @Override
    TaskTools getTasks() {
        this.taskTools
    }

    /**
     * Whether configuration cache is enabled for a build.
     *
     * @return {@code true} is configuration cache is available and enabled.
     */
    @Override
    boolean isConfigurationCacheEnabled() {
        this.configurationCacheEnabled
    }

    /**
     * Whether Gradle is operating in offline mode.
     *
     * @return {@code true} if offline.
     */
    @Override
    boolean isOffline() {
        this.offline
    }

//    /**
//     * Executes the specified external java process.
//     * @param action
//     * @return {@link ExecResult} that can be used to check if the execution worked.
//     *
//     */
//    @Override
//    ExecResult javaexec(Action<? super JavaExecSpec> action) {
//        execOperations.javaexec(action)
//    }

    /**
     * Get the minimum log level for Gradle.
     *
     * @return Log level
     */
    @Override
    LogLevel getGradleLogLevel() {
        this.logLevel
    }

    /**
     * Get the full project path including the root project name in case of a multi-project.
     *
     * @return The fully qualified project path including root project.
     *
     * @since 1.2
     */
    @Override
    String getFullProjectPath() {
        projectTools.fullProjectPath
    }

    /**
     * Lazy-evaluated project group.https://gitlab.com/ysb33rOrg/grolifant/-/jobs/3248448094
     *
     * @return provider to project group
     */
    @Override
    Provider<String> getGroupProvider() {
        projectTools.groupProvider
    }

    /**
     * Tools for working with JVMs
     *
     * @return The appropriate provider for the Gradle version.
     *
     * @since 2.0
     */
    JvmTools getJvmTools() {
        this.jvmTools
    }

    /**
     * The project name
     *
     * @return Cached value of project name.
     */
    @Override
    String getProjectName() {
        this.projectName
    }

    /**
     * Get project path.
     *
     * @return The fully qualified project path.
     *
     * @since 1.2
     */
    @Override
    String getProjectPath() {
        this.projectPath
    }

    /**
     * Returns the root directory of the project.
     *
     * @return Root directory.
     *
     * @since 2.0
     */
    @Override
    File getProjectRootDir() {
        this.rootDir
    }

    /**
     * Tools to deal with project & configuration specifics down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    @Override
    ProjectTools getProjectTools() {
        this.projectTools
    }

    /**
     * Tools to deal with provider down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    @Override
    ProviderTools getProviderTools() {
        this.providerTools
    }

    /**
     * Tools for dealing with repositories.
     *
     * @return Tools instance.
     *
     * @since 2.0
     */
    @Override
    RepositoryTools getRepositoryTools() {
        this.repositoryTools
    }

    /**
     * Tools for dealing with conversions of various objects into string or lists of strings.
     *
     * @return String tools instance.
     *
     * @since 2.0
     */
    @Override
    StringTools getStringTools() {
        this.stringTools
    }

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    @Override
    Provider<String> getVersionProvider() {
        projectTools.versionProvider
    }

//    /**
//     * Creates a provider to an environmental variable.
//     *
//     * @param name Anything convertible to a string
//     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
//     *                                to just use {@link #atConfigurationTime} for this parameter
//     * @return Provider to the value of the an environmental variable.
//     */
//    @Override
//    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
//        propertyProvider.environmentVariable(name, configurationTimeSafety)
//    }
//
//    /**
//     * Creates a provider to a project property.
//     *
//     * @param name Anything convertible to a string
//     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
//     *                                to just use {@link #atConfigurationTime} for this parameter
//     * @return Provider to the value of the project property.
//     */
//    @Override
//    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
//        propertyProvider.gradleProperty(name, configurationTimeSafety)
//    }

    /**
     * Whether current project is the root project.
     *
     * @return {@code true} is current project is root project.
     *
     * @since 1.2
     */
    @Override
    boolean isRoot() {
        this.root
    }

//    /**
//     * Returns the relative path from the project directory to the given path.
//     *
//     * @param f Object that is resolvable to a file within project context
//     *
//     * @return Relative path. Never {@code null}.
//     */
//    @Override
//    String relativePath(Object f) {
//        fsOperations.relativePath(f)
//    }

//    /**
//     *
//     * Searches by Gradle property, then system property and finally by environment variable using the
//     * {@code PropertyResolver convention}.
//     *
//     * @param name Anything convertible to a string
//     * @param defaultValue Default value to return if the property search order does not return any value.
//     *                                Can be {@code null}. Anything convertible to a string.
//     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
//     *                                to just use {@link #atConfigurationTime} for this parameter.
//     * @return Provider to finding a property by the specified name.
//     */
//    @Override
//    Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety) {
//        propertyProvider.resolve(name, defaultValue, configurationTimeSafety)
//    }

//    /**
//     * Creates a provider to a system property.
//     *
//     * @param name Anything convertible to a string
//     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
//     *                                to just use {@link #atConfigurationTime} for this parameter
//     * @return Provider to the value of the system property.
//     */
//    @Override
//    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
//        propertyProvider.resolve(name, null, configurationTimeSafety)
//    }

//    /**
//     * Updates a file provider.
//     * <p>
//     * Update property or otherwise the provider will be assigned a new Provider instance.
//     *
//     * @param provider Current property
//     * @param file Value that should be lazy-resolved to a file using {@link #file}.
//     */
//    @Override
//    void updateFileProperty(Provider<File> provider, Object file) {
//        if (provider instanceof Property) {
//            fsOperations.updateFileProperty((Property<File>) provider, file)
//        } else {
//            throw new NotSupportedException('Provider has to be Property<File>')
//        }
//    }

//    /**
//     * Updates a string provider.
//     * <p>
//     * Update property or otherwise the provider will be assigned a new Provider instance.
//     *
//     * @param provider Current property
//     * @param str Value that should be lazy-resolved to a string .
//     */
//    @Override
//    void updateStringProperty(Provider<String> provider, Object str) {
//        if (provider instanceof Property) {
//            stringTools.updateStringProperty((Property<String>) provider, str)
//        } else {
//            throw new NotSupportedException('Provider has to be Property<String>')
//        }
//    }

//    abstract protected ExecOperationsProxy getExecOperations()

//    abstract protected GradleSysEnvProvider getPropertyProvider()

    protected ProjectOperationsProxy(Project tempProjectReference, final LoadableVersion version) {
        final po = this
        this.thisAsProvider = tempProjectReference.provider { -> (ProjectOperations) po }
        this.providerFactory = tempProjectReference.providers
        this.resourceHandler = tempProjectReference.resources

        this.fsOperations = FileSystemOperationsLoader.load(tempProjectReference, version)
        this.taskTools = TaskToolsLoader.load(this, tempProjectReference, version)
        this.repositoryTools = RepositoryToolsLoader.load(this, tempProjectReference, version)
        this.providerTools = ProviderToolsLoader.load(tempProjectReference, version)
        this.projectTools = ProjectToolsLoader.load(tempProjectReference, version)
        this.stringTools = StringToolsLoader.load(tempProjectReference, version)
        this.jvmTools = JvmToolsLoader.load(tempProjectReference, version)
        this.execTools = ExecToolsLoader.load(tempProjectReference, version)
        this.configurationTools = ConfigurationToolsLoader.load(this, tempProjectReference, version)

        Gradle gradle = tempProjectReference.gradle
        StartParameter startParameter = gradle.startParameter

        this.root = tempProjectReference == tempProjectReference.rootProject
        this.offline = startParameter.offline
        this.logLevel = startParameter.logLevel
        this.gradleHomeDir = providerFactory.provider({ File f -> f }.curry(gradle.gradleHomeDir))
        this.gradleUserHomeDir = providerFactory.provider({ File f -> f }.curry(gradle.gradleUserHomeDir))
        this.projectDir = tempProjectReference.projectDir
        this.rootDir = tempProjectReference.rootDir
        this.configurationCacheEnabled = ConfigurationCache.isEnabled(tempProjectReference)
        this.projectName = tempProjectReference.name
        this.projectPath = tempProjectReference.path
    }

    private final ProviderFactory providerFactory
    private final Provider<File> gradleUserHomeDir
    private final Provider<File> gradleHomeDir
    private final File projectDir
    private final File rootDir
    private final String projectName
    private final String projectPath
    private final boolean root
    private final boolean offline
    private final LogLevel logLevel
    private final ResourceHandler resourceHandler
    private final boolean configurationCacheEnabled
    private final FileSystemOperations fsOperations
    private final TaskTools taskTools
    private final RepositoryTools repositoryTools
    private final ProviderTools providerTools
    private final ProjectTools projectTools
    private final StringTools stringTools
    private final JvmTools jvmTools
    private final ExecTools execTools
    private final ConfigurationTools configurationTools
    private final Provider<ProjectOperations> thisAsProvider
}
