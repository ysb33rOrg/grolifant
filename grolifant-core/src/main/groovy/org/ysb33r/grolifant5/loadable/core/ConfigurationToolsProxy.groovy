/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.attributes.AttributeContainer
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.ConfigurationTools
import org.ysb33r.grolifant5.internal.core.IterableUtils
import org.ysb33r.grolifant5.api.core.Transform

import java.util.function.Function

import static org.ysb33r.grolifant5.internal.core.IterableUtils.treatAsIterable

/**
 * Common code across Gradle versions for dealing with Gradle configurations.
 *
 * @since 2.1
 */
@CompileStatic
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class ConfigurationToolsProxy implements ConfigurationTools {

    /**
     * Resolves an arbitrary item to a {@link org.gradle.api.artifacts.Configuration} instance.
     *
     * @param configurationThingy Instance or {@link org.gradle.api.artifacts.Configuration} or something that
     *   resolves to a string.
     * @return Configuration
     */
    @Override
    Configuration asConfiguration(Object configurationThingy) {
        Transform.convertItem(
            configurationThingy,
            transformer
        )
    }

    /**
     * Resolves arbitrary items to a collection of {@link Configuration} instances.
     *
     * @param configurationThingies Collection that might contain {@link Configuration} or string-type instances
     * @return Collection of resolved {@link Configuration} instances
     */
    @Override
    Collection<Configuration> asConfigurations(Collection<?> configurationThingies) {
        Set<Configuration> resolvedConfigurations = []
        configurationThingies.forEach { thingy ->
            switch (thingy) {
                case Configuration:
                    resolvedConfigurations.add((Configuration) thingy)
                    break
                case Collection:
                    resolvedConfigurations.addAll(asConfigurations((Collection) thingy))
                    break
                default:
                    if (treatAsIterable(thingy)) {
                        final asList = IterableUtils.toList((Iterable) thingy)
                        resolvedConfigurations.addAll(asConfigurations((Collection) asList))
                    } else {
                        resolvedConfigurations.add(asConfiguration(thingy))
                    }
            }
        }
        resolvedConfigurations
    }

    protected ConfigurationToolsProxy(
        ProjectOperations incompleteReference, Project project) {
        this.configurations = project.configurations
        this.po = incompleteReference
    }

    protected final ConfigurationContainer configurations

    protected void createTrinityConfigurationsPre84(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        final depScope = configurations.create(dependencyScopedConfigurationName)
        depScope.canBeConsumed = false
        depScope.canBeResolved = false
        depScope.visible = visible

        final resolvable = configurations.create(resolvableConfigurationName)
        resolvable.canBeResolved = true
        resolvable.canBeConsumed = false
        resolvable.visible = visible
        resolvable.extendsFrom(depScope)

        final consumable = configurations.create(consumableConfigurationName)
        consumable.canBeResolved = false
        consumable.canBeConsumed = true
        consumable.visible = visible
        consumable.extendsFrom(depScope)

        [resolvable, consumable].each {
            it.attributes(attributes)
        }
    }

    protected void createDualConfigurationsPre84(
        String dependencyScopedConfigurationName,
        String resolvableConfigurationName,
        boolean visible
    ) {
        final depScope = configurations.create(dependencyScopedConfigurationName)
        depScope.canBeConsumed = false
        depScope.canBeResolved = false
        depScope.visible = visible

        final resolvable = configurations.create(resolvableConfigurationName)
        resolvable.canBeResolved = true
        resolvable.canBeConsumed = false
        resolvable.visible = visible
        resolvable.extendsFrom(depScope)
    }

    protected createOutgoingConfigurationPre84(
        String consumableConfigurationName,
        boolean visible,
        Action<? super AttributeContainer> attributes
    ) {
        final consumable = configurations.create(consumableConfigurationName)
        consumable.canBeResolved = false
        consumable.canBeConsumed = true
        consumable.visible = visible
        consumable.attributes(attributes)
    }

    private Function<Object, Configuration> getTransformer() {
        CONFIGURATION_TRANSFORMER.curry(
            configurations,
            po.stringTools
        ) as Function<Object, Configuration>
    }

    private static final Closure CONFIGURATION_TRANSFORMER = {
        ConfigurationContainer c, StringTools st, Object configurationThingy ->
            switch (configurationThingy) {
                case Configuration:
                    (Configuration) configurationThingy
                    break
                default:
                    c.getByName(st.stringize(configurationThingy))
            }
    }

    private final ProjectOperations po
}
