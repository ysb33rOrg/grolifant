/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.resources.TextResource
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.Transform
import org.ysb33r.grolifant5.api.errors.ChecksumCreationException
import org.ysb33r.grolifant5.api.errors.NotSupportedException
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException
import org.ysb33r.grolifant5.internal.core.Resources

import java.nio.file.FileSystems
import java.nio.file.Path
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.function.BiFunction
import java.util.function.Function
import java.util.regex.Pattern

import static org.ysb33r.grolifant5.internal.core.Issue.ISSUE_URL

/**
 * Common string operations across all Gradle versions.
 *
 * <p>
 * This class and its completing subclasses may not refer to any other Grolifant classes.
 * They may however, refer to this.
 * </p>
 *
 * @author Schalk W. Cronjé
 *
 */
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
@CompileStatic
abstract class StringToolsProxy implements StringTools {
    /**
     * Converts a source to a byte array.
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *            Empty wrappers and {@code null} are not accepted.
     * @return A native {@code byte[]}.
     *
     * @since 5.0
     */
    @Override
    byte[] makeByteArray(Object src) {
        Transform.convertItem(src, x -> BYTE_ARRAY_TRANSFORMER.apply(this, x))
    }

    /**
     * Converts a source to a byte array.
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *
     * @return A native {@code byte[]} or {@code null} if {@code null} or an empty wrapper is supplied.
     *
     * @since 5.0
     */
    @Override
    byte[] makeByteArrayOrNull(Object src) {
        Transform.convertItemOrNull(src, x -> BYTE_ARRAY_TRANSFORMER.apply(this, x))
    }

    /**
     * Creates a SHA-256 hash of a URI.
     *
     * @param uri URI to hash
     * @return SHA-257 hash that is hex-encoded.
     */
    @Override
    String hashUri(URI uri) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance('SHA256')
            messageDigest.update(uri.toString().bytes)
            new BigInteger(1, messageDigest.digest()).toString(36)
        } catch (NoSuchAlgorithmException e) {
            throw new ChecksumCreationException('Could not create SHA-256 checksum of URI', e)
        }
    }

    /**
     * Loads text from a resource file.
     *
     * @param resourcePath Resource path.
     *
     * @return Text
     *
     * @throws org.gradle.api.resources.ResourceException*
     * @since 5.0
     */
    @Override
    String loadTextFromResource(String resourcePath) {
        Resources.loadTextFromResource(resourcePath)
    }

    /**
     * Converts most things to a pattern. Closures are evaluated as well.
     * <p>
     * Converts collections of the following recursively until it gets to a string:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link Pattern}.</li>
     *   <li> Groovy Closures.</li>
     *   <li>{@link java.util.concurrent.Callable}</li>
     *   <li>{@link Optional}</li>
     *   <li>{@link Provider}</li>
     *   <li>{@link java.util.function.Supplier}</li>
     *   <li> {@link org.gradle.api.resources.TextResource}</li>
     * </ul>
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A pattern object
     *
     * @since 5.0
     */
    @Override
    Pattern patternize(Object stringy) {
        Transform.convertItem(stringy, x -> PATTERN_TRANSFORMER.apply(this, x))
    }

    /**
     * Like {@link #patternize}, but returns {@code null} rather than throwing an exception, when item is {@code null},
     * an empty {@Link Provider} or an empty {@link java.util.Optional}.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return Pattern or {@code null}
     *
     * @since 5.0
     */
    @Override
    Pattern patternizeOrNull(Object stringy) {
        Transform.convertItemOrNull(stringy, x -> PATTERN_TRANSFORMER.apply(this, x))
    }

    /**
     * Converts a collection of most things to a list of regex patterns. Closures are evaluated as well.
     *
     * @param stringyThings Iterable list of objects that can be converted to patterns,
     *                      including closure that can be evaluated
     *                      into objects that can be converted to patterns.
     * @return A list of patterns
     *
     * @since 5.0
     */
    @Override
    List<Pattern> patternize(Collection<?> stringyThings) {
        List<Pattern> output = []
        Transform.convertItems(stringyThings, output, x -> PATTERN_TRANSFORMER.apply(this, x))
        output
    }

    /**
     * Like {@link #patternize}, but drops any nulls, or empty instances of {@link Provider} and {@link Optional}.
     *
     * @param stringyThings Iterable list of objects that can be converted to patterns,
     *                      including closure that can be evaluated
     *                      into objects that can be converted to patterns.
     * @return A list of patterns
     *
     * @since 5.0
     */
    @Override
    List<Pattern> patternizeDropNull(Collection<?> stringyThings) {
        List<Pattern> output = []
        Function<Object, Pattern> transformer = (Object x) -> PATTERN_TFMR_NULL.apply(this, x)
        Transform.convertItemsDropNull(stringyThings, output) { Object src ->
            src != null ? transformer.apply(src) : null
        }
        output
    }

    /**
     * Like {@link #provideByteArray(Object)}, but lazy-evaluates the value..
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *            Empty wrappers and {@code null} are not accepted.
     * @return A provider to {@code byte[]}
     *
     * @since 5.0
     */
    @Override
    Provider<byte[]> provideByteArray(Object src) {
        providers.provider { -> makeByteArray(src) }
    }

    /**
     * Like {@link #provideByteArrayOrNull(Object)}, but lazy-evaluates the value..
     *
     * @param src Anything providing a {@code byte[]}, a {@code Byte[]}, or a string in some form other.
     *            Usual recursive unwrapping will occur, before evaluation.
     *
     * @return A provider to {@code byte[]}. Empty wrappers and {@code null} will result in an empty provider.
     *
     * @since 5.0
     */
    @Override
    Provider<byte[]> provideByteArrayOrNull(Object src) {
        providers.provider { -> makeByteArrayOrNull(src) }
    }

    /**
     * Like {@link #patternize}, but does not immediately evaluate the passed parameter.
     * The latter is only done when the provider is resolved.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A provider to a regex pattern.
     *
     * @since 5.0
     */
    @Override
    Provider<Pattern> providePattern(Object stringy) {
        providers.provider { -> patternize(stringy) }
    }

    /**
     * Like {@link #patternizeOrNull(Object)}, but does not evaluate the passed parameter immediately.
     *
     * @param stringy An object that can be converted to a pattern or a closure that
     *                can be evaluated to something that can be converted to a pattern.
     * @return A provider to a regex pattern. Can be empty.
     *
     * @since 5.0
     */
    @Override
    Provider<Pattern> providePatternOrNull(Object stringy) {
        providers.provider { -> patternizeOrNull(stringy) }
    }

    /**
     * Lazy-loads text from a resource file.
     *
     * @param resourcePath Resource path.
     *
     * @return Provider to text
     *
     * @throws org.gradle.api.resources.ResourceException*
     * @since 5.0
     */
    @Override
    Provider<String> provideTextFromResource(String resourcePath) {
        providers.provider { -> loadTextFromResource(resourcePath) }
    }

    /**
     * Get final package or directory name from a URI
     *
     * @param uri URI to process
     * @return Last part of URI path.
     */
    @Override
    String packageNameFromUri(URI uri) {
        final String path = uri.path
        int p = path.lastIndexOf('/')
        (p < 0) ? path : path.substring(p + 1)
    }

    /**
     * Like {@link #stringize}, but does not immediately evaluate the passed parameter.
     * The latter is only done when the provider is resolved.
     *
     * @param stringy An object that can be converted to a string or a closure that
     *                can be evaluated to something that can be converted to a string.
     * @return A provider to a string.
     */
    @Override
    Provider<String> provideString(Object stringy) {
        providers.provider { -> stringize(stringy) }
    }

    /**
     * Like {@link #stringizeOrNull(Object)}, but does not evaluate the passed parameter immediately.
     *
     * @param stringy Anything convertible to a string.
     * @return Provider to a string. Can be empty.
     */
    @Override
    Provider<String> provideStringOrNull(Object stringy) {
        providers.provider { -> stringizeOrNull(stringy) }
    }

    /**
     * Similar to {@link #stringizeValues}, but does not evalute the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A map that is keyed on strings, but for which teh values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    @Override
    Provider<Map<String, String>> provideValues(Map<String, ? extends Object> props) {
        providers.provider { -> stringizeValues(props) }
    }

    /**
     * Similar to {@link #stringizeValues}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A privder to a map that is keyed on strings, but for which teh values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    @Override
    Provider<Map<String, String>> provideValues(Provider<Map<String, ?>> props) {
        props.flatMap { provideValues(it) }
    }

    /**
     * Similar to {@link #stringizeValuesDropNull(Map)}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A provider to a map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    @Override
    Provider<Map<String, String>> provideValuesDropNull(Provider<Map<String, Object>> props) {
        props.flatMap { provideValuesDropNull(it) }
    }

    /**
     * Similar to {@link #stringizeValuesDropNull(Map)}, but does not evaluate the parameter immediately, only when the
     * provider is resolved.
     *
     * @param props A map that is keyed on strings, but for which the values can be converted to strings
     *              using {@link #stringize(Object)}.
     * @return A provider to a lazy-resolved map.
     *
     * @since 5.0
     */
    @Override
    Provider<Map<String, String>> provideValuesDropNull(Map<String, ?> props) {
        providers.provider { -> stringizeValuesDropNull(props) }
    }

    /**
     * Similar to {@link #urize(Object)}, but does not evaluate the parameter immediately, only when the provider is
     * resolved.
     *
     * @param uriThingy Anything that could be converted to a URI
     * @return Provider to a URI
     *
     * @since 5.0
     */
    @Override
    Provider<URI> provideUri(Object uriThingy) {
        providers.provider { ->
            urize(uriThingy)
        }
    }

    /**
     * Converts most things to a string. Closures are evaluated as well.
     *
     * @param stringy An object that can be converted to a string or a closure that
     *                can be evaluated to something that can be converted to a string.
     * @return A string object
     */
    @Override
    String stringize(Object stringy) {
        Transform.convertItem(stringy, STRING_TRANSFORMER)
    }

    /**
     * Like {@link #stringize}, but returns {@code null} rather than throwing an exception, when item is {@code null},
     * an empty {@Link Provider} or an empty {@link java.util.Optional}.
     *
     * @param stringy
     * @return string or {@code null}
     */
    @Override
    String stringizeOrNull(Object stringy) {
        Transform.convertItemOrNull(stringy, STRING_TRANSFORMER_NULL)
    }

    /**
     * Converts a collection of most things to a list of strings. Closures are evaluated as well.
     *
     * @param stringyThings Iterable list of objects that can be converted to strings.
     * @return A list of strings
     */
    @Override
    List<String> stringize(Collection<?> stringyThings) {
        List<String> output = []
        Transform.convertItems(stringyThings, output, STRING_TRANSFORMER)
        output
    }

    /**
     * Like {@link #stringize}, but drops any nulls, or empty instances of {@link Provider} and {@link Optional}.
     *
     * @param stringyThings
     * @return A list of strings
     */
    @Override
    List<String> stringizeDropNull(Collection<?> stringyThings) {
        List<String> output = []
        Transform.convertItemsDropNull(stringyThings, output, STRING_TRANSFORMER_NULL)
        output
    }

    /**
     * Like {@link #stringizeValues(Map)}, but drops the key, if the corresponding value is null or an empty
     * {@link Provider} or {@link Optional}.
     *
     * @param props Map that wil be evaluated
     * @return Converted map. Can be empty, but never {@code null}.
     *
     * @subce 5.0
     */
    @Override
    Map<String, String> stringizeValuesDropNull(Map<String, ?> props) {
        props.collectEntries { k, v ->
            [k, stringizeOrNull(v)]
        }.findAll {
            it.value != null
        }
    }

    /**
     * Create a URI where the user/password is masked out.
     *
     * @param uri Original URI
     * @return URI with no credentials.
     */
    @Override
    URI safeUri(URI uri) {
        new URI(uri.scheme, null, uri.host, uri.port, uri.path, uri.query, uri.fragment)
    }

    /**
     * Updates a {@code Property<String>}.
     *
     * THis provides a more powerful way than {@link Property#set}.
     *
     * @param provider String property
     * @param stringy Objexct that is a string or can be converted to a string.
     */
    @Override
    void updateStringProperty(Property<String> provider, Object stringy) {
        provider.set(providers.provider { -> stringizeOrNull(stringy) })
    }

    /**
     * Attempts to convert object to a URI.
     * <p>
     * Closures can be passed and will be evaluated. Result will then be converted to a URI.
     *
     * @param uriThingy Anything that could be converted to a URI
     * @return URI object
     */
    @Override
    URI urize(Object uriThingy) {
        (URI) Transform.convertItem(uriThingy, URI_TRANSFORMER.curry(this))
    }

    protected StringToolsProxy(ProviderFactory pf) {
        this.providers = pf
    }

    private final ProviderFactory providers

    private static final BiFunction<StringTools, Object, Pattern> PATTERN_TRANSFORMER =
        (StringTools str, Object pat) -> {
            switch (pat) {
                case Pattern:
                    return (Pattern) pat
                case CharSequence:
                    return Pattern.compile(str.stringize(pat))
                default:
                    throw new NotSupportedException("""
                    This type (${pat.class.canonicalName}) cannot be converted to a Pattern. The following is supported:
                    - java.util.Pattern.
                    - Anything implementing CharSequence.
                    - Any Closure, Provider, Callable, Optional, Supplier returning one of the above.
                    """.stripIndent()
                    )
            }
        }

    private static final BiFunction<StringTools, Object, Pattern> PATTERN_TFMR_NULL = (StringTools str, Object pat) -> {
        pat == null ? null : PATTERN_TRANSFORMER.apply(str, pat)
    }

    private static final BiFunction<StringTools, Object, byte[]> BYTE_ARRAY_TRANSFORMER =
        (StringTools str, Object type) -> {
            switch (type) {
                case byte[]:
                    (byte[]) type
                    break
                case Byte[]:
                    final from = ((Byte[]) type)
                    final out = new byte[from.length]
                    for (int i = 0; i < from.length; i++) {
                        out[i] = from[i]
                    }
                    out
                    break
                case File:
                    ((File) type).bytes
                    break
                case Path:
                    if (((Path) type).fileSystem == FileSystems.default) {
                        ((Path) type).toFile().bytes
                    } else {
                        throw new UnsupportedConfigurationException(
                            "Converting ${(Path) type} to byte[] is not supported.\n" +
                                "Raise an issue at ${ISSUE_URL} if you feel this filesystem needs to be supported."
                        )
                    }
                    break
                case CharSequence:
                    str.stringize(type).bytes
                    break
                default:
                    throw new UnsupportedConfigurationException(
                        "Converting ${type.class.canonicalName} to byte[] is not supported.\n" +
                            "Raise an issue at ${ISSUE_URL} if you feel this type of conversion need to be supported."
                    )
            }
        }

    private static final Closure<URI> URI_TRANSFORMER = { StringTools str, Object uri ->
        switch (uri) {
            case URI:
                return (URI) uri
            case URL:
                return ((URL) uri).toURI()
            case File:
                return ((File) uri).toURI()
            case Path:
                return ((Path) uri).toUri()
            default:
                str.stringize(uri).toURI()
        }
    }

    private static final Function<Object, String> STRING_TRANSFORMER = { str ->
        switch (str) {
            case TextResource:
                ((TextResource) str).asString()
                break
            default:
                str.toString()
        }
    } as Function<Object, String>

    private static final Function<Object, String> STRING_TRANSFORMER_NULL = { str ->
        str == null ? null : STRING_TRANSFORMER.apply(str)
    } as Function<Object, String>
}
