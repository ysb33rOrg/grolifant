/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.logging.configuration.ConsoleOutput
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ProjectTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.errors.UnknownStateObjectException
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader

import javax.inject.Inject

/**
 *
 * @author Schalk W. Cronje
 *
 * @since 2.0
 */
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
@CompileStatic
abstract class ProjectToolsProxy implements ProjectTools {

    public static final String UNSPECIFIED_PROJECT_VERSION = 'unspecified'
    public static final String UNSPECIFIED_PROJECT_GROUP = ''
    public static final String UNSPECIFIED_PROJECT_DESCRIPTION = UNSPECIFIED_PROJECT_GROUP

    final String fullProjectPath

    @Override
    ConsoleOutput getConsoleOutput() {
        this.consoleOutput
    }

    /**
     * Description of the project.
     *
     * @return Provider to description. If no description was provided, returns an empty string.
     *
     * @since 5.0
     */
    @Override
    Provider<String> getProjectDescriptionProvider() {
        this.descriptionProvider
    }

    /**
     * A value that indicates whether Gradle is run in offline mode or not.
     *
     * @return Provider that will return {@code true} iof offline.
     *   Never empty.
     *
     * @since 5.0
     */
    @Override
    Provider<Boolean> getOfflineProvider() {
        this.offline
    }

    /**
     * Name of the project.
     *
     * @return Provider to name.
     *
     * @since 5.0
     */
    @Override
    Provider<String> getProjectNameProvider() {
        this.nameProvider
    }

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    @Override
    Provider<String> getVersionProvider() {
        this.versionProvider
    }

    /**
     * Whether the setup is a multi-project or a single project.
     *
     * @return {@code true} for a multi-project
     *
     * @since 5.0
     */
    @Override
    boolean isMultiProject() {
        this.multiProject
    }

    /**
     * Whether the current is the root project.
     *
     * @return {@code true} if the root project.
     *   (Always {@code true} for a single project).
     *
     * @since 5.0
     */
    @Override
    boolean isRootProject() {
        this.root
    }

    @Override
    boolean isRefreshDependencies() {
        this.refreshDeps
    }

    @Override
    boolean isRerunTasks() {
        this.rerunTasks
    }

    /**
     * Sets a new version for a project.
     *
     * <p>This creates an internal objhect that can safely be evaluated by
     * Gradle's {@link org.gradle.api.Project#getVersion}.
     * </p>
     *
     * @param newVersion Anything that can be converted to a string using
     * {@link org.ysb33r.grolifant5.api.core.StringTools}.
     */
    @Override
    void setVersionProvider(Object newVersion) {
        gradleProjectVersion.version = newVersion
        updateLegacyVersion()
    }

    protected final StringTools stringTools

    protected ProjectToolsProxy(
        Project tempProjectReference,
        LoadableVersion loadableVersion
    ) {
        final name = tempProjectReference.name
        final desc = tempProjectReference.description
        this.nameProvider = tempProjectReference.provider { -> name }
        this.descriptionProvider = tempProjectReference.provider { -> desc ?: UNSPECIFIED_PROJECT_DESCRIPTION }
        this.stringTools = StringToolsLoader.load(tempProjectReference, loadableVersion)
        this.gradleProjectVersion = new GradleProjectVersion(this, tempProjectReference.objects)
        this.gradleProjectVersion.version = tempProjectReference.version ?: UNSPECIFIED_PROJECT_VERSION
        this.objectFactory = tempProjectReference.objects

        this.versionProvider = tempProjectReference.provider { ->
            gradleProjectVersion.toString()
        }

        boolean offlineCheck = tempProjectReference.gradle.startParameter.offline
        this.offline = tempProjectReference.provider { -> offlineCheck }
        this.root = tempProjectReference.depth == 0
        this.multiProject = !tempProjectReference.rootProject.subprojects.empty
        this.fullProjectPath = root ? name : "${tempProjectReference.rootProject.name}${tempProjectReference.path}"
        this.refreshDeps = tempProjectReference.gradle.startParameter.refreshDependencies
        this.rerunTasks = tempProjectReference.gradle.startParameter.rerunTasks
        this.consoleOutput = tempProjectReference.gradle.startParameter.consoleOutput ?: ConsoleOutput.Auto
    }

    protected void updateLegacyVersion() {
        objectFactory.newInstance(LegacyUpdater).update(this.gradleProjectVersion)
    }

    static class GradleProjectVersion {
        GradleProjectVersion(ProjectToolsProxy owner, ObjectFactory objectFactory) {
            this.owner = owner
            this.version = objectFactory.property(String)
        }

        void updateVersion(Provider<String> ver) {
            version.set(ver)
        }

        void setVersion(Object ver) {
            owner.stringTools.updateStringProperty(version, ver)
        }

        @Override
        String toString() {
            version.get()
        }

        private final ProjectToolsProxy owner
        private final Property<String> version
    }

    static class LegacyUpdater {
        final Project project

        @Inject
        LegacyUpdater(Project project) {
            if (project == null) {
                throw new UnknownStateObjectException(
                    'You are probably trying to update the project version after configuration phase has completed'
                )
            }

            this.project = project
        }

        void update(Object newVersion) {
            project.version = newVersion
        }
    }

    private final ObjectFactory objectFactory
    private final GradleProjectVersion gradleProjectVersion
    private final Provider<String> versionProvider
    private final Provider<String> nameProvider
    private final Provider<String> descriptionProvider
    private final Provider<Boolean> offline
    private final boolean refreshDeps
    private final boolean rerunTasks
    private final ConsoleOutput consoleOutput

    private final boolean root
    private final boolean multiProject
}
