/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic

/**
 * String prefixes for differnet Gradle major versions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
enum LoadableVersion {

    V9,
    V8,
    V7,
    V6,
    V5,
    V4

    /**
     * Returns the package name for the loadable class.
     *
     * @return Package name.
     */
    String getPackage() {
        "org.ysb33r.grolifant5.loadable.${name().uncapitalize()}"
    }
}