/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.ysb33r.grolifant5.internal.core.loaders.RepositoryToolsLoader

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_5_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_6_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_0
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_8_0

@java.lang.SuppressWarnings('NoWildcardImports')
import static LoadableVersion.*

/**
 * Common code for loading Gradle generation instances.
 *
 * @author Schalk W.Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@Slf4j
class Loader {

    /**
     * Loads a specific version of an API.
     *
     * @param interfaceName API name
     * @param className Class name exlucding package
     * @param version Version of API to load
     * @return An instance that implements the API
     * @throw ClassNotFoundException
     *
     * @since 4.0
     */
    static Object load(String interfaceName, String className, LoadableVersion version) {
        final loadable = loadByName("${version.package}.${className}")
        if (loadable == null) {
            throw new ClassNotFoundException(
                "There is no known ${version.name()} implementation of org.ysb33r.grolifant.api.${interfaceName} " +
                    "on the classpath. Nothing in org.ysb33r.grolifant.loadable.*.${className} matched."
            )
        }
        loadable
    }

    /**
     * Attempts to load a different class depending on the active version of Gradle.
     *
     * @param interfaceName Name of the interface. Must be in package {@code org.ysb33r.grolifant.api}.
     * @param className Name of the class we are tyring to load.
     * Must be in a package {@code org.ysb33r.grolifant.loadable.vX} where {@code X} is the Gradle major version.
     * @return The loaded class or {@code null} if it does not exist.
     */
    static Object load(String interfaceName, String className) {
        Class loadable

        if (!PRE_8_0) {
            loadable = loadByName("${V8.package}.${className}")
        }

        if (!loadable && !PRE_7_0) {
            loadable = loadByName("${V7.package}.${className}")
        }

        if (!loadable && !PRE_6_0) {
            loadable = loadByName("${V6.package}.${className}")
        }

        if (!loadable && !PRE_5_0) {
            loadable = loadByName("${V5.package}.${className}")
        }

        loadable = loadable ?: loadByName("${V4.package}.${className}")
        if (!loadable) {
            throw new ClassNotFoundException(
                "There is no known implementation of org.ysb33r.grolifant.api.${interfaceName} on the classpath. " +
                    "Nothing in org.ysb33r.grolifant.loadable.*.${className} matched."
            )
        }

        loadable
    }

    private static Class loadByName(String className) {
        try {
            log.debug("Trying to load class ${className}")
            RepositoryToolsLoader.classLoader.loadClass(className)
        } catch (ClassNotFoundException e) {
            log.debug("${className} not available")
            null
        }
    }
}
