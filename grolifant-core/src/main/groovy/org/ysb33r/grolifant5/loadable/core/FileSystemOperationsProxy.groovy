/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileTree
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.file.copy.CopySpecInternal
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.api.core.Transform
import org.ysb33r.grolifant5.internal.core.Resources
import org.ysb33r.grolifant5.internal.core.TempDirectory
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader

import java.nio.file.Path
import java.nio.file.Paths
import java.util.regex.Pattern

import static org.ysb33r.grolifant5.api.core.Transform.convertItems
import static org.ysb33r.grolifant5.api.core.Transform.convertItemsDropNull

/**
 * Common filesystem operations
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.3
 */
@SuppressWarnings(['AbstractClassWithoutAbstractMethod', 'MethodCount'])
@CompileStatic
abstract class FileSystemOperationsProxy implements FileSystemOperations {
    /**
     * Safely resolve the stringy item as a path relative to the build directory.
     *
     * @param stringy Any item that can be resolved to a string using
     * {@code org.ysb33r.grolifant5.core.api.grolifant.StringTools#stringize}.
     * @return Provider to a file
     *
     * @since 5.0
     */
    @Override
    Provider<File> buildDirDescendant(Object stringy) {
        projectLayout.buildDirectory.zip(stringTools.provideString(stringy)) { dir, path ->
            dir.dir(path).asFile
        }
    }

    /**
     * Copies the file at the resource path to a local file.
     *
     * @param resourcePath Resource path.
     * @param destFile Destination file.
     *
     * @throws org.gradle.api.resources.ResourceException*
     * @since 5.0
     */
    @Override
    void copyResourceToFile(String resourcePath, File destFile) {
        Resources.copyResourceToFile(resourcePath, destFile)
    }

    /**
     * Creates a temporary directory with the given prefix.
     *
     * Directory will be set to delete on VM exit, but consumers are allowed to delete earlier.
     *
     * The directory will be created somewhere in the build directory.
     *
     * @param prefix Prefix for directory.
     * @return Directory
     *
     * @since 2.0
     */
    @Override
    File createTempDirectory(String prefix) {
        tmpDirFactory.createTempDirectory(prefix)
    }

    /**
     * Converts a file-like objects to {@link java.io.File} instances with project context.
     * <p>
     * Converts collections of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory}
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * Collections are flattened.
     * Null instances are not allowed.
     *
     * @param files Potential {@link File} objects
     * @return File collection.
     */
    @Override
    ConfigurableFileCollection files(Collection<?> files) {
        List<File> outputs = []
        emptyFileCollection().from(convertItems(files.toList(), outputs) { f -> file(f) })
    }

    /**
     * Obtain a file tree given a starting location
     * @param file Starting location.
     *             Anything convertible with {@link #file} will do.
     * @return File tree
     *
     * @since 5.0
     */
    @Override
    ConfigurableFileTree fileTree(Object file) {
        objectFactory.fileTree().from(provideFile(file))
    }

    /**
     * Converts a file-like objects to {@link java.io.File} instances with project context.
     * <p>
     * Converts collections of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory}
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * Collections are flattened.
     * Null instances are removed.
     *
     * @param files Potential {@link File} objects
     * @return File collection.
     */
    @Override
    ConfigurableFileCollection filesDropNull(Collection<?> files) {
        List<File> outputs = []
        emptyFileCollection().from(convertItemsDropNull(files.toList(), outputs) { f -> file(f) })
    }

    /**
     * Gradle distribution home directory.
     *
     * @return Directory.
     * @since 5.0
     */
    @Override
    Provider<File> getGradleHomeDir() {
        this.gradleHomeDir
    }

    /**
     * Gradle user home directory. Usually {@code ~/.gradle} on non -Windows.
     *
     * @return Directory.
     *
     * @since 5.0
     */
    @Override
    Provider<File> getGradleUserHomeDir() {
        this.gradleUserHomeDir
    }

    /**
     * Returns the project directory.
     *
     * @return Project directory.
     *
     * @since 5.0
     */
    @Override
    File getProjectDir() {
        this.projectDir
    }

    /**
     * The project root dir.
     *
     * @return Root directory
     */
    @Override
    File getProjectRootDir() {
        this.rootDir
    }

    /** Provides a list of directories below another directory
     *
     * @param distDir Directory
     * @return List of directories. Can be empty if, but never {@code null}
     *   supplied directory.
     */
    @Override
    List<File> listDirs(File distDir) {
        if (distDir.exists()) {
            distDir.listFiles(new FileFilter() {
                @Override
                boolean accept(File pathname) {
                    pathname.directory
                }
            }) as List<File>
        } else {
            []
        }
    }

    /**
     * Loads properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @return Properties
     *
     * @throws org.gradle.api.resources.ResourceException*
     * @since 5.0
     */
    @Override
    Properties loadPropertiesFromResource(String resourcePath) {
        Resources.loadPropertiesFromResource(resourcePath)
    }

    /**
     * Loads properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @param classLoader Classloader to use for loading the content.
     * @return Properties
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    @Override
    Properties loadPropertiesFromResource(String resourcePath, ClassLoader classLoader) {
        Resources.loadPropertiesFromResource(resourcePath, classLoader)
    }

    /**
     * Like {@link #file}, but does not immediately evaluate the passed parameter.
     *
     * @param file Potential {@link File} object
     * @return A provider that will evaluate to a file.
     */
    @Override
    Provider<File> provideFile(Object file) {
        providerFactory.provider { -> owner.file(file) }
    }

    /**
     * Returns the project's local cache directory
     *
     * @return Project cache directory.
     *
     * @since 5.0
     */
    @Override
    File getProjectCacheDir() {
        this.projectCacheDir.get()
    }

    /**
     * Lazy-load properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @return Provider to properties.
     *
     * @throws org.gradle.api.resources.ResourceException*
     * @since 5.0
     */
    @Override
    Provider<Properties> providePropertiesFromResource(String resourcePath) {
        providerFactory.provider { -> loadPropertiesFromResource(resourcePath) }
    }

    /**
     * Lazy-load properties from a resource file.
     *
     * @param resourcePath Resource path.
     * @param classLoader Classloader to use for loading the resource.
     * @return Provider to properties.
     * @throws org.ysb33r.grolifant5.api.errors.ResourceAccessException when resource if not available.
     * @since 5.0
     */
    @Override
    Provider<Properties> providePropertiesFromResource(String resourcePath, ClassLoader classLoader) {
        providerFactory.provider { -> loadPropertiesFromResource(resourcePath, classLoader) }
    }

    /**
     * The root directory of the project.
     *
     * @return Provider to the root directory.
     */
    @Override
    Provider<File> provideRootDir() {
        this.projectRootDir
    }

    /**
     * Like {@link #getProjectCacheDir()}, but returns a provider instead.
     *
     * @return Provider to the project's cache directory.
     *
     * @since 5.0
     */
    @Override
    Provider<File> provideProjectCacheDir() {
        this.projectCacheDir
    }

    /**
     * Like {@link #getProjectDir()}, but returns a provider instead.
     *
     * @return Provider to the project directory.
     *
     * @since 5.0
     */
    @Override
    Provider<File> provideProjectDir() {
        this.projectDirProvider
    }

    /**
     * Provides a subpath below the project's cache directory.
     *  Anything that can evaluated to a string using {@link StringTools}.
     *
     * @return Provider to a file or directory below the project's cache directory.
     *
     * @since 5.0
     */
    @Override
    Provider<File> provideProjectCacheDirDescendant(Object subpath) {
        provideProjectCacheDir().zip(stringTools.provideString(subpath)) { file, path ->
            new File(file, path)
        }
    }

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context.
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePath(Object f) {
        relativizeImpl(projectDirPath, file(f).toPath())
    }

    /**
     * Returns the relative path from the root project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     *
     */
    @Override
    String relativeRootPath(Object f) {
        relativizeImpl(rootDirPath, file(f).toPath())
    }

    /**
     * Returns the relative path from the given path to the project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePathToProjectDir(Object f) {
        relativizeImpl(file(f).toPath(), projectDirPath)
    }

    /**
     * Returns the relative path from the given path to the root project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    @Override
    String relativePathToRootDir(Object f) {
        relativizeImpl(file(f).toPath(), rootDirPath)
    }

    /**
     * Returns the path of one File relative to another.
     *
     * @param target the target directory
     * @param base the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     *
     * @since 2.0
     */
    @Override
    String relativize(File base, File target) {
        relativizeImpl(base.toPath(), target.toPath())
    }

    /**
     * Returns the path of one Path relative to another.
     *
     * @param target the target directory
     * @param base the base directory
     * @return target's path relative to the base directory
     * @throws IOException if an error occurs while resolving the files' canonical names.
     *
     * @since 2.0
     */
    @Override
    String relativize(Path base, Path target) {
        relativizeImpl(base, target)
    }

    /**
     * Given a {@Link CopySpec}, resolve all of the input files in to a collection.
     *
     * @param copySpec Input specification
     * @return Collection of files.
     */
    @Override
    FileTree resolveFilesFromCopySpec(CopySpec copySpec) {
        ((CopySpecInternal) copySpec).buildRootResolver().allSource
    }

    /** Converts a string into a string that is safe to use as a file name. T
     *
     * The result will only include ascii characters and numbers, and the "-","_", #, $ and "." characters.
     *
     * @param name A potential file name
     * @return A name that is safe on the local filesystem of the current operating system.
     */
    @Override
    @CompileDynamic
    String toSafeFileName(String name) {
        name.replaceAll(SAFE_FILENAME_REGEX) { String match ->
            String bytes = match.bytes.collect { int it -> Integer.toHexString(it) }.join('')
            "#${bytes}!"
        }
    }

    /**
     * Converts a collection of String into a {@link Path} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @since 2.0
     */
    @Override
    Path toSafePath(String... parts) {
        final safeParts = Transform.toList(parts.toList()) { String it -> toSafeFileName(it) }
        if (safeParts.size() > 0) {
            (Path) Paths.get(safeParts[0], safeParts[1..-1].toArray() as String[])
        } else {
            (Path) Paths.get(safeParts[0])
        }
    }

    /**
     * Updates a {@code Property<File>}.
     * <p>
     *     This method deals with a wide range of possibilities and works around the limitations of
     * {@code Property.set()}
     * </p>
     * @param provider Current provider
     * @param fileTarget Value that should be lazy-resolved.
     *
     * @since 2.0
     */
    @Override
    void updateFileProperty(Property<File> provider, Object fileTarget) {
        if (fileTarget == null) {
            provider.set((File) null)
        } else {
            provider.set(providerFactory.provider { ->
                fileOrNull(fileTarget)
            })
        }
    }

    /**
     * The project directory.
     */
    protected final File projectDir

    /**
     * The root project directory.
     */
    protected final File rootDir

    /**
     * The project directory as {@link Path}.
     */
    protected final Path projectDirPath

    /**
     * The project root directory as {@link Path}.
     */
    protected final Path rootDirPath

    /**
     * Provider factory
     */
    protected final ProviderFactory providerFactory

    /**
     * Gradle project layout.
     *
     * @since 5.0
     */
    protected final ProjectLayout projectLayout

    /**
     * Object Factory
     *
     * @since 5.0
     */
    protected final ObjectFactory objectFactory

    /**
     * String tools
     *
     * @since 5.0
     */
    protected final StringTools stringTools

    protected FileSystemOperationsProxy(
        Project tempProjectReference,
        LoadableVersion loadableVersion
    ) {
        this.stringTools = StringToolsLoader.load(tempProjectReference, loadableVersion)
        this.projectDir = tempProjectReference.projectDir
        this.rootDir = tempProjectReference.rootDir
        this.projectDirPath = tempProjectReference.projectDir.toPath()
        this.rootDirPath = tempProjectReference.rootDir.toPath()
        this.providerFactory = tempProjectReference.providers
        this.projectLayout = tempProjectReference.layout
        this.objectFactory = tempProjectReference.objects

        final pcd = tempProjectReference.gradle.startParameter.projectCacheDir ?:
            tempProjectReference.file("${tempProjectReference.rootDir}/.gradle")
        this.projectCacheDir = providerFactory.provider { -> pcd }
        this.projectRootDir = providerFactory.provider { -> rootDir }
        this.projectDirProvider = providerFactory.provider { -> owner.projectDir }
        this.tmpDirFactory = new TempDirectory(buildDirDescendant('tmp/.grolifant'))

        final gh = tempProjectReference.gradle.gradleHomeDir
        this.gradleHomeDir = providerFactory.provider { -> gh }
        final guh = tempProjectReference.gradle.gradleUserHomeDir
        this.gradleUserHomeDir = providerFactory.provider { -> guh }
    }

    private String relativizeImpl(Path base, Path target) {
        base.relativize(target).toFile().toString()
    }

    private final Provider<File> projectCacheDir
    private final Provider<File> projectRootDir
    private final Provider<File> projectDirProvider
    private final Provider<File> gradleHomeDir
    private final Provider<File> gradleUserHomeDir
    private final TempDirectory tmpDirFactory
    private static final Pattern SAFE_FILENAME_REGEX = ~/[^\w_\-.$]/
}