/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant5.loadable.core

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.reflect.ObjectInstantiationException
import org.ysb33r.grolifant5.api.core.ProviderTools
import org.ysb33r.grolifant5.api.core.StringTools
import org.ysb33r.grolifant5.internal.core.loaders.StringToolsLoader
import org.ysb33r.grolifant5.internal.core.property.order.PropertyNaming

import java.util.concurrent.Callable

/**
 * Common code code {@link ProviderTools}.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class ProviderToolsProxy implements ProviderTools {
    protected ProviderToolsProxy(
        Project tempProjectReference,
        LoadableVersion loadableVersion
    ) {
        this.providerFactory = tempProjectReference.providers
        this.objectFactory = tempProjectReference.objects
        this.stringTools = StringToolsLoader.load(tempProjectReference, loadableVersion)
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of an environmental variable.
     * The property cannot safely be read at configuration time.
     * @since 5.0
     */
    @Override
    Provider<String> environmentVariable(Object name) {
        providerFactory.environmentVariable(stringTools.provideString(name))
    }

    /**
     * Creates a provider to an environmental variable.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the an environmental variable.
     * @since 5.0
     */
    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        providerFactory.environmentVariable(stringTools.provideString(name))
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the project property.
     * The property cannot safely be read at configuration time
     * @since 5.0
     */
    @Override
    Provider<String> gradleProperty(Object name) {
        providerFactory.gradleProperty(stringTools.provideString(name))
    }

    /**
     * Creates a provider to a project property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the project property.
     * @since 5.0
     */
    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        providerFactory.gradleProperty(stringTools.provideString(name))
    }

    /**
     * Creates a {@link ListProperty} instance
     *
     * @param valueType Class.
     * @return Property instance
     * @param <T>     Type of property
     *
     * @since 5.0
     */
    @Override
    public <T> ListProperty<T> listProperty(Class<T> valueType) {
        objectFactory.listProperty(valueType)
    }

    /**
     * Creates a {@link MapProperty} instance
     * @param keyType Key type
     * @param valueType Value type
     * @return A map property with uninitialised content.
     * @param <K>    Key type
     * @param <V>    Value type
     *
     * @since 5.0
     */
    @Override
    public <K, V> MapProperty<K, V> mapProperty(Class<K> keyType, Class<V> valueType) {
        objectFactory.mapProperty(keyType, valueType)
    }

    /**
     * Creates a {@link Property} instance
     *
     * @param valueType Class.
     * @return Property instance
     * @param <T>       Type of property
     *
     * @since 2.0
     */
    @Override
    public <T> Property<T> property(Class<T> valueType) {
        objectFactory.property(valueType)
    }

    /**
     * Creates a {@link Property} instance with a default value
     *
     * @param valueType Class.
     * @param defaultValue A provider that can provider this value.
     * @return Property instance
     * @param <T>       Type of property
     *
     * @since 2.0
     */
    @Override
    public <T> Property<T> property(Class<T> valueType, Provider<T> defaultValue) {
        final Property<T> prop = objectFactory.property(valueType)
        prop.set(defaultValue)
        prop
    }

    /**
     * Convenience interface for creating a provider.
     *
     * <p>
     *     Delegates to {@link org.gradle.api.provider.ProviderFactory#provider(Callable)}.
     * </p>
     *
     * @param value A function that will return a value or {@code null}
     * @return Provider (promise) to a future value (or null).
     * @param <T>     Type of value to be returned.
     *
     * @since 5.0
     */
    @Override
    public <T> Provider<T> provider(Callable<? extends T> value) {
        providerFactory.provider(value)
    }

    /**
     * Searches by Gradle property, then system property and finally by environment variable using the
     * {@code PropertyResolver convention}.
     *
     * @param name Anything convertible to a string
     * @param defaultValue Default value to return if the property search order does not return any value.
     *                                Can be {@code null}. Anything convertible to a string.
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to finding a property by the specified name.
     *
     * @since 5.0
     */
    @Override
    Provider<String> resolveProperty(Object name, Object defaultValue, boolean configurationTimeSafety) {
        resolveOrderly(
            gradleProperty(name, configurationTimeSafety),
            systemProperty(name, configurationTimeSafety),
            environmentVariable(
                stringTools.provideString(name).map { PropertyNaming.asEnvVar(it) },
                configurationTimeSafety
            )
        ).orElse(stringTools.provideStringOrNull(defaultValue))
    }

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @return Provider to the value of the system property. The property cannot safely be read at configuration time
     * @since 5.0
     */
    @Override
    Provider<String> systemProperty(Object name) {
        providerFactory.systemProperty(stringTools.provideString(name))
    }

    /**
     * Creates a provider to a system property.
     *
     * @param name Anything convertible to a string
     * @param configurationTimeSafety Whether this property can be read safely at configuration time. It is suggested
     *                                to just use {@link #atConfigurationTime} for this parameter. As from Gradle 8.0
     *                                this value is ignored.
     * @return Provider to the value of the system property.
     * @since 5.0
     */
    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        providerFactory.systemProperty(stringTools.provideString(name))
    }

    @Override
    public <T> T newInstance(Class<? extends T> type, Object... parameters) throws ObjectInstantiationException {
        objectFactory.newInstance(type, parameters)
    }

    protected final ProviderFactory providerFactory
    protected final ObjectFactory objectFactory
    protected final StringTools stringTools
}
